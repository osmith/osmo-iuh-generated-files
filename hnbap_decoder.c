/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/hnbap/HNBAP-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/hnbap/hnbap_common.h>
#include <osmocom/hnbap/hnbap_ies_defs.h>

int hnbap_decode_hnbregisterrequesties(
    HNBAP_HNBRegisterRequestIEs_t *hnbRegisterRequestIEs,
    ANY_t *any_p) {

    HNBAP_HNBRegisterRequest_t *hNBRegisterRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(hnbRegisterRequestIEs != NULL);

    memset(hnbRegisterRequestIEs, 0, sizeof(HNBAP_HNBRegisterRequestIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_HNBRegisterRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_HNBRegisterRequest, (void**)&hNBRegisterRequest_p);

    if (tempDecoded < 0 || hNBRegisterRequest_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_HNBRegisterRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < hNBRegisterRequest_p->hnbRegisterRequest_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = hNBRegisterRequest_p->hnbRegisterRequest_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_HNB_Identity:
            {
                HNBAP_HNB_Identity_t *hnbaP_HNBIdentity_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_HNB_Identity, (void**)&hnbaP_HNBIdentity_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE hnB_Identity failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_HNB_Identity, hnbaP_HNBIdentity_p);
                memcpy(&hnbRegisterRequestIEs->hnB_Identity, hnbaP_HNBIdentity_p, sizeof(HNBAP_HNB_Identity_t));
                FREEMEM(hnbaP_HNBIdentity_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_HNB_Location_Information:
            {
                HNBAP_HNB_Location_Information_t *hnbaP_HNBLocationInformation_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_HNB_Location_Information, (void**)&hnbaP_HNBLocationInformation_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE hnB_Location_Information failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_HNB_Location_Information, hnbaP_HNBLocationInformation_p);
                memcpy(&hnbRegisterRequestIEs->hnB_Location_Information, hnbaP_HNBLocationInformation_p, sizeof(HNBAP_HNB_Location_Information_t));
                FREEMEM(hnbaP_HNBLocationInformation_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_PLMNidentity:
            {
                HNBAP_PLMNidentity_t *hnbaP_PLMNidentity_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_PLMNidentity, (void**)&hnbaP_PLMNidentity_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE plmNidentity failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_PLMNidentity, hnbaP_PLMNidentity_p);
                memcpy(&hnbRegisterRequestIEs->plmNidentity, hnbaP_PLMNidentity_p, sizeof(HNBAP_PLMNidentity_t));
                FREEMEM(hnbaP_PLMNidentity_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_CellIdentity:
            {
                HNBAP_CellIdentity_t *hnbaP_CellIdentity_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_CellIdentity, (void**)&hnbaP_CellIdentity_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE cellIdentity failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_CellIdentity, hnbaP_CellIdentity_p);
                memcpy(&hnbRegisterRequestIEs->cellIdentity, hnbaP_CellIdentity_p, sizeof(HNBAP_CellIdentity_t));
                FREEMEM(hnbaP_CellIdentity_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_LAC:
            {
                HNBAP_LAC_t *hnbap_lac_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_LAC, (void**)&hnbap_lac_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE lac failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_LAC, hnbap_lac_p);
                memcpy(&hnbRegisterRequestIEs->lac, hnbap_lac_p, sizeof(HNBAP_LAC_t));
                FREEMEM(hnbap_lac_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_RAC:
            {
                HNBAP_RAC_t *hnbap_rac_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_RAC, (void**)&hnbap_rac_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE rac failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_RAC, hnbap_rac_p);
                memcpy(&hnbRegisterRequestIEs->rac, hnbap_rac_p, sizeof(HNBAP_RAC_t));
                FREEMEM(hnbap_rac_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_SAC:
            {
                HNBAP_SAC_t *hnbap_sac_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_SAC, (void**)&hnbap_sac_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE sac failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_SAC, hnbap_sac_p);
                memcpy(&hnbRegisterRequestIEs->sac, hnbap_sac_p, sizeof(HNBAP_SAC_t));
                FREEMEM(hnbap_sac_p);
            } break;
            /* Optional field */
            case HNBAP_ProtocolIE_ID_id_CSG_ID:
            {
                HNBAP_CSG_ID_t *hnbap_csgid_p = NULL;
                hnbRegisterRequestIEs->presenceMask |= HNBREGISTERREQUESTIES_HNBAP_CSG_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_CSG_ID, (void**)&hnbap_csgid_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE csg_id failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_CSG_ID, hnbap_csgid_p);
                memcpy(&hnbRegisterRequestIEs->csg_id, hnbap_csgid_p, sizeof(HNBAP_CSG_ID_t));
                FREEMEM(hnbap_csgid_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message hnbregisterrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_HNBRegisterRequest, hNBRegisterRequest_p);
    return decoded;
}

int hnbap_decode_hnbregisteraccepties(
    HNBAP_HNBRegisterAcceptIEs_t *hnbRegisterAcceptIEs,
    ANY_t *any_p) {

    HNBAP_HNBRegisterAccept_t *hNBRegisterAccept_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(hnbRegisterAcceptIEs != NULL);

    memset(hnbRegisterAcceptIEs, 0, sizeof(HNBAP_HNBRegisterAcceptIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_HNBRegisterAcceptIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_HNBRegisterAccept, (void**)&hNBRegisterAccept_p);

    if (tempDecoded < 0 || hNBRegisterAccept_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_HNBRegisterAcceptIEs failed\n");
        return -1;
    }

    for (i = 0; i < hNBRegisterAccept_p->hnbRegisterAccept_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = hNBRegisterAccept_p->hnbRegisterAccept_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_RNC_ID:
            {
                HNBAP_RNC_ID_t *hnbap_rncid_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_RNC_ID, (void**)&hnbap_rncid_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE rnc_id failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_RNC_ID, hnbap_rncid_p);
                memcpy(&hnbRegisterAcceptIEs->rnc_id, hnbap_rncid_p, sizeof(HNBAP_RNC_ID_t));
                FREEMEM(hnbap_rncid_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message hnbregisteraccepties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_HNBRegisterAccept, hNBRegisterAccept_p);
    return decoded;
}

int hnbap_decode_hnbregisterrejecties(
    HNBAP_HNBRegisterRejectIEs_t *hnbRegisterRejectIEs,
    ANY_t *any_p) {

    HNBAP_HNBRegisterReject_t *hNBRegisterReject_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(hnbRegisterRejectIEs != NULL);

    memset(hnbRegisterRejectIEs, 0, sizeof(HNBAP_HNBRegisterRejectIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_HNBRegisterRejectIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_HNBRegisterReject, (void**)&hNBRegisterReject_p);

    if (tempDecoded < 0 || hNBRegisterReject_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_HNBRegisterRejectIEs failed\n");
        return -1;
    }

    for (i = 0; i < hNBRegisterReject_p->hnbRegisterReject_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = hNBRegisterReject_p->hnbRegisterReject_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_Cause:
            {
                HNBAP_Cause_t *hnbaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Cause, (void**)&hnbaP_Cause_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Cause, hnbaP_Cause_p);
                memcpy(&hnbRegisterRejectIEs->cause, hnbaP_Cause_p, sizeof(HNBAP_Cause_t));
                FREEMEM(hnbaP_Cause_p);
            } break;
            /* Optional field */
            case HNBAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                HNBAP_CriticalityDiagnostics_t *hnbaP_CriticalityDiagnostics_p = NULL;
                hnbRegisterRejectIEs->presenceMask |= HNBREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_CriticalityDiagnostics, (void**)&hnbaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_CriticalityDiagnostics, hnbaP_CriticalityDiagnostics_p);
                memcpy(&hnbRegisterRejectIEs->criticalityDiagnostics, hnbaP_CriticalityDiagnostics_p, sizeof(HNBAP_CriticalityDiagnostics_t));
                FREEMEM(hnbaP_CriticalityDiagnostics_p);
            } break;
            /* Conditional field */
            case HNBAP_ProtocolIE_ID_id_BackoffTimer:
            {
                HNBAP_BackoffTimer_t *hnbaP_BackoffTimer_p = NULL;
                hnbRegisterRejectIEs->presenceMask |= HNBREGISTERREJECTIES_HNBAP_BACKOFFTIMER_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_BackoffTimer, (void**)&hnbaP_BackoffTimer_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE backoffTimer failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_BackoffTimer, hnbaP_BackoffTimer_p);
                memcpy(&hnbRegisterRejectIEs->backoffTimer, hnbaP_BackoffTimer_p, sizeof(HNBAP_BackoffTimer_t));
                FREEMEM(hnbaP_BackoffTimer_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message hnbregisterrejecties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_HNBRegisterReject, hNBRegisterReject_p);
    return decoded;
}

int hnbap_decode_hnbde_registeries(
    HNBAP_HNBDe_RegisterIEs_t *hnbDe_RegisterIEs,
    ANY_t *any_p) {

    HNBAP_HNBDe_Register_t *hNBDe_Register_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(hnbDe_RegisterIEs != NULL);

    memset(hnbDe_RegisterIEs, 0, sizeof(HNBAP_HNBDe_RegisterIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_HNBDe_RegisterIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_HNBDe_Register, (void**)&hNBDe_Register_p);

    if (tempDecoded < 0 || hNBDe_Register_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_HNBDe_RegisterIEs failed\n");
        return -1;
    }

    for (i = 0; i < hNBDe_Register_p->hnbDe_Register_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = hNBDe_Register_p->hnbDe_Register_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_Cause:
            {
                HNBAP_Cause_t *hnbaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Cause, (void**)&hnbaP_Cause_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Cause, hnbaP_Cause_p);
                memcpy(&hnbDe_RegisterIEs->cause, hnbaP_Cause_p, sizeof(HNBAP_Cause_t));
                FREEMEM(hnbaP_Cause_p);
            } break;
            /* Conditional field */
            case HNBAP_ProtocolIE_ID_id_BackoffTimer:
            {
                HNBAP_BackoffTimer_t *hnbaP_BackoffTimer_p = NULL;
                hnbDe_RegisterIEs->presenceMask |= HNBDE_REGISTERIES_HNBAP_BACKOFFTIMER_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_BackoffTimer, (void**)&hnbaP_BackoffTimer_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE backoffTimer failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_BackoffTimer, hnbaP_BackoffTimer_p);
                memcpy(&hnbDe_RegisterIEs->backoffTimer, hnbaP_BackoffTimer_p, sizeof(HNBAP_BackoffTimer_t));
                FREEMEM(hnbaP_BackoffTimer_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message hnbde_registeries\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_HNBDe_Register, hNBDe_Register_p);
    return decoded;
}

int hnbap_decode_ueregisterrequesties(
    HNBAP_UERegisterRequestIEs_t *ueRegisterRequestIEs,
    ANY_t *any_p) {

    HNBAP_UERegisterRequest_t *uERegisterRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(ueRegisterRequestIEs != NULL);

    memset(ueRegisterRequestIEs, 0, sizeof(HNBAP_UERegisterRequestIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_UERegisterRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_UERegisterRequest, (void**)&uERegisterRequest_p);

    if (tempDecoded < 0 || uERegisterRequest_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_UERegisterRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < uERegisterRequest_p->ueRegisterRequest_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = uERegisterRequest_p->ueRegisterRequest_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_UE_Identity:
            {
                HNBAP_UE_Identity_t *hnbaP_UEIdentity_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_UE_Identity, (void**)&hnbaP_UEIdentity_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE uE_Identity failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_UE_Identity, hnbaP_UEIdentity_p);
                memcpy(&ueRegisterRequestIEs->uE_Identity, hnbaP_UEIdentity_p, sizeof(HNBAP_UE_Identity_t));
                FREEMEM(hnbaP_UEIdentity_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_Registration_Cause:
            {
                HNBAP_Registration_Cause_t *hnbaP_RegistrationCause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Registration_Cause, (void**)&hnbaP_RegistrationCause_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE registration_Cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Registration_Cause, hnbaP_RegistrationCause_p);
                memcpy(&ueRegisterRequestIEs->registration_Cause, hnbaP_RegistrationCause_p, sizeof(HNBAP_Registration_Cause_t));
                FREEMEM(hnbaP_RegistrationCause_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_UE_Capabilities:
            {
                HNBAP_UE_Capabilities_t *hnbaP_UECapabilities_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_UE_Capabilities, (void**)&hnbaP_UECapabilities_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE uE_Capabilities failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_UE_Capabilities, hnbaP_UECapabilities_p);
                memcpy(&ueRegisterRequestIEs->uE_Capabilities, hnbaP_UECapabilities_p, sizeof(HNBAP_UE_Capabilities_t));
                FREEMEM(hnbaP_UECapabilities_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message ueregisterrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_UERegisterRequest, uERegisterRequest_p);
    return decoded;
}

int hnbap_decode_ueregisteraccepties(
    HNBAP_UERegisterAcceptIEs_t *ueRegisterAcceptIEs,
    ANY_t *any_p) {

    HNBAP_UERegisterAccept_t *uERegisterAccept_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(ueRegisterAcceptIEs != NULL);

    memset(ueRegisterAcceptIEs, 0, sizeof(HNBAP_UERegisterAcceptIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_UERegisterAcceptIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_UERegisterAccept, (void**)&uERegisterAccept_p);

    if (tempDecoded < 0 || uERegisterAccept_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_UERegisterAcceptIEs failed\n");
        return -1;
    }

    for (i = 0; i < uERegisterAccept_p->ueRegisterAccept_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = uERegisterAccept_p->ueRegisterAccept_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_UE_Identity:
            {
                HNBAP_UE_Identity_t *hnbaP_UEIdentity_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_UE_Identity, (void**)&hnbaP_UEIdentity_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE uE_Identity failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_UE_Identity, hnbaP_UEIdentity_p);
                memcpy(&ueRegisterAcceptIEs->uE_Identity, hnbaP_UEIdentity_p, sizeof(HNBAP_UE_Identity_t));
                FREEMEM(hnbaP_UEIdentity_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_Context_ID:
            {
                HNBAP_Context_ID_t *hnbaP_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Context_ID, (void**)&hnbaP_ContextID_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Context_ID, hnbaP_ContextID_p);
                memcpy(&ueRegisterAcceptIEs->context_ID, hnbaP_ContextID_p, sizeof(HNBAP_Context_ID_t));
                FREEMEM(hnbaP_ContextID_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message ueregisteraccepties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_UERegisterAccept, uERegisterAccept_p);
    return decoded;
}

int hnbap_decode_ueregisterrejecties(
    HNBAP_UERegisterRejectIEs_t *ueRegisterRejectIEs,
    ANY_t *any_p) {

    HNBAP_UERegisterReject_t *uERegisterReject_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(ueRegisterRejectIEs != NULL);

    memset(ueRegisterRejectIEs, 0, sizeof(HNBAP_UERegisterRejectIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_UERegisterRejectIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_UERegisterReject, (void**)&uERegisterReject_p);

    if (tempDecoded < 0 || uERegisterReject_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_UERegisterRejectIEs failed\n");
        return -1;
    }

    for (i = 0; i < uERegisterReject_p->ueRegisterReject_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = uERegisterReject_p->ueRegisterReject_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_UE_Identity:
            {
                HNBAP_UE_Identity_t *hnbaP_UEIdentity_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_UE_Identity, (void**)&hnbaP_UEIdentity_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE uE_Identity failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_UE_Identity, hnbaP_UEIdentity_p);
                memcpy(&ueRegisterRejectIEs->uE_Identity, hnbaP_UEIdentity_p, sizeof(HNBAP_UE_Identity_t));
                FREEMEM(hnbaP_UEIdentity_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_Cause:
            {
                HNBAP_Cause_t *hnbaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Cause, (void**)&hnbaP_Cause_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Cause, hnbaP_Cause_p);
                memcpy(&ueRegisterRejectIEs->cause, hnbaP_Cause_p, sizeof(HNBAP_Cause_t));
                FREEMEM(hnbaP_Cause_p);
            } break;
            /* Optional field */
            case HNBAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                HNBAP_CriticalityDiagnostics_t *hnbaP_CriticalityDiagnostics_p = NULL;
                ueRegisterRejectIEs->presenceMask |= UEREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_CriticalityDiagnostics, (void**)&hnbaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_CriticalityDiagnostics, hnbaP_CriticalityDiagnostics_p);
                memcpy(&ueRegisterRejectIEs->criticalityDiagnostics, hnbaP_CriticalityDiagnostics_p, sizeof(HNBAP_CriticalityDiagnostics_t));
                FREEMEM(hnbaP_CriticalityDiagnostics_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message ueregisterrejecties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_UERegisterReject, uERegisterReject_p);
    return decoded;
}

int hnbap_decode_uede_registeries(
    HNBAP_UEDe_RegisterIEs_t *ueDe_RegisterIEs,
    ANY_t *any_p) {

    HNBAP_UEDe_Register_t *uEDe_Register_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(ueDe_RegisterIEs != NULL);

    memset(ueDe_RegisterIEs, 0, sizeof(HNBAP_UEDe_RegisterIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_UEDe_RegisterIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_UEDe_Register, (void**)&uEDe_Register_p);

    if (tempDecoded < 0 || uEDe_Register_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_UEDe_RegisterIEs failed\n");
        return -1;
    }

    for (i = 0; i < uEDe_Register_p->ueDe_Register_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = uEDe_Register_p->ueDe_Register_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_Context_ID:
            {
                HNBAP_Context_ID_t *hnbaP_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Context_ID, (void**)&hnbaP_ContextID_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Context_ID, hnbaP_ContextID_p);
                memcpy(&ueDe_RegisterIEs->context_ID, hnbaP_ContextID_p, sizeof(HNBAP_Context_ID_t));
                FREEMEM(hnbaP_ContextID_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_Cause:
            {
                HNBAP_Cause_t *hnbaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Cause, (void**)&hnbaP_Cause_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Cause, hnbaP_Cause_p);
                memcpy(&ueDe_RegisterIEs->cause, hnbaP_Cause_p, sizeof(HNBAP_Cause_t));
                FREEMEM(hnbaP_Cause_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message uede_registeries\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_UEDe_Register, uEDe_Register_p);
    return decoded;
}

int hnbap_decode_csgmembershipupdateies(
    HNBAP_CSGMembershipUpdateIEs_t *csgMembershipUpdateIEs,
    ANY_t *any_p) {

    HNBAP_CSGMembershipUpdate_t *cSGMembershipUpdate_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(csgMembershipUpdateIEs != NULL);

    memset(csgMembershipUpdateIEs, 0, sizeof(HNBAP_CSGMembershipUpdateIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_CSGMembershipUpdateIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_CSGMembershipUpdate, (void**)&cSGMembershipUpdate_p);

    if (tempDecoded < 0 || cSGMembershipUpdate_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_CSGMembershipUpdateIEs failed\n");
        return -1;
    }

    for (i = 0; i < cSGMembershipUpdate_p->csgMembershipUpdate_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = cSGMembershipUpdate_p->csgMembershipUpdate_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_Context_ID:
            {
                HNBAP_Context_ID_t *hnbaP_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Context_ID, (void**)&hnbaP_ContextID_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Context_ID, hnbaP_ContextID_p);
                memcpy(&csgMembershipUpdateIEs->context_ID, hnbaP_ContextID_p, sizeof(HNBAP_Context_ID_t));
                FREEMEM(hnbaP_ContextID_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_CSGMembershipStatus:
            {
                HNBAP_CSGMembershipStatus_t *hnbaP_CSGMembershipStatus_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_CSGMembershipStatus, (void**)&hnbaP_CSGMembershipStatus_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE csgMembershipStatus failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_CSGMembershipStatus, hnbaP_CSGMembershipStatus_p);
                memcpy(&csgMembershipUpdateIEs->csgMembershipStatus, hnbaP_CSGMembershipStatus_p, sizeof(HNBAP_CSGMembershipStatus_t));
                FREEMEM(hnbaP_CSGMembershipStatus_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message csgmembershipupdateies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_CSGMembershipUpdate, cSGMembershipUpdate_p);
    return decoded;
}

int hnbap_decode_tnlupdaterequesties(
    HNBAP_TNLUpdateRequestIEs_t *tnlUpdateRequestIEs,
    ANY_t *any_p) {

    HNBAP_TNLUpdateRequest_t *tNLUpdateRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(tnlUpdateRequestIEs != NULL);

    memset(tnlUpdateRequestIEs, 0, sizeof(HNBAP_TNLUpdateRequestIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_TNLUpdateRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_TNLUpdateRequest, (void**)&tNLUpdateRequest_p);

    if (tempDecoded < 0 || tNLUpdateRequest_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_TNLUpdateRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < tNLUpdateRequest_p->tnlUpdateRequest_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = tNLUpdateRequest_p->tnlUpdateRequest_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_Context_ID:
            {
                HNBAP_Context_ID_t *hnbaP_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Context_ID, (void**)&hnbaP_ContextID_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Context_ID, hnbaP_ContextID_p);
                memcpy(&tnlUpdateRequestIEs->context_ID, hnbaP_ContextID_p, sizeof(HNBAP_Context_ID_t));
                FREEMEM(hnbaP_ContextID_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_RABList:
            {
                HNBAP_RABList_t *hnbaP_RABList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_RABList, (void**)&hnbaP_RABList_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE rabList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_RABList, hnbaP_RABList_p);
                memcpy(&tnlUpdateRequestIEs->rabList, hnbaP_RABList_p, sizeof(HNBAP_RABList_t));
                FREEMEM(hnbaP_RABList_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_Update_cause:
            {
                HNBAP_Update_cause_t *hnbaP_Updatecause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Update_cause, (void**)&hnbaP_Updatecause_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE update_cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Update_cause, hnbaP_Updatecause_p);
                memcpy(&tnlUpdateRequestIEs->update_cause, hnbaP_Updatecause_p, sizeof(HNBAP_Update_cause_t));
                FREEMEM(hnbaP_Updatecause_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message tnlupdaterequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_TNLUpdateRequest, tNLUpdateRequest_p);
    return decoded;
}

int hnbap_decode_tnlupdateresponseies(
    HNBAP_TNLUpdateResponseIEs_t *tnlUpdateResponseIEs,
    ANY_t *any_p) {

    HNBAP_TNLUpdateResponse_t *tNLUpdateResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(tnlUpdateResponseIEs != NULL);

    memset(tnlUpdateResponseIEs, 0, sizeof(HNBAP_TNLUpdateResponseIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_TNLUpdateResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_TNLUpdateResponse, (void**)&tNLUpdateResponse_p);

    if (tempDecoded < 0 || tNLUpdateResponse_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_TNLUpdateResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < tNLUpdateResponse_p->tnlUpdateResponse_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = tNLUpdateResponse_p->tnlUpdateResponse_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_Context_ID:
            {
                HNBAP_Context_ID_t *hnbaP_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Context_ID, (void**)&hnbaP_ContextID_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Context_ID, hnbaP_ContextID_p);
                memcpy(&tnlUpdateResponseIEs->context_ID, hnbaP_ContextID_p, sizeof(HNBAP_Context_ID_t));
                FREEMEM(hnbaP_ContextID_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message tnlupdateresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_TNLUpdateResponse, tNLUpdateResponse_p);
    return decoded;
}

int hnbap_decode_tnlupdatefailureies(
    HNBAP_TNLUpdateFailureIEs_t *tnlUpdateFailureIEs,
    ANY_t *any_p) {

    HNBAP_TNLUpdateFailure_t *tNLUpdateFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(tnlUpdateFailureIEs != NULL);

    memset(tnlUpdateFailureIEs, 0, sizeof(HNBAP_TNLUpdateFailureIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_TNLUpdateFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_TNLUpdateFailure, (void**)&tNLUpdateFailure_p);

    if (tempDecoded < 0 || tNLUpdateFailure_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_TNLUpdateFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < tNLUpdateFailure_p->tnlUpdateFailure_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = tNLUpdateFailure_p->tnlUpdateFailure_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_Context_ID:
            {
                HNBAP_Context_ID_t *hnbaP_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Context_ID, (void**)&hnbaP_ContextID_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Context_ID, hnbaP_ContextID_p);
                memcpy(&tnlUpdateFailureIEs->context_ID, hnbaP_ContextID_p, sizeof(HNBAP_Context_ID_t));
                FREEMEM(hnbaP_ContextID_p);
            } break;
            case HNBAP_ProtocolIE_ID_id_Cause:
            {
                HNBAP_Cause_t *hnbaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Cause, (void**)&hnbaP_Cause_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Cause, hnbaP_Cause_p);
                memcpy(&tnlUpdateFailureIEs->cause, hnbaP_Cause_p, sizeof(HNBAP_Cause_t));
                FREEMEM(hnbaP_Cause_p);
            } break;
            /* Optional field */
            case HNBAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                HNBAP_CriticalityDiagnostics_t *hnbaP_CriticalityDiagnostics_p = NULL;
                tnlUpdateFailureIEs->presenceMask |= TNLUPDATEFAILUREIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_CriticalityDiagnostics, (void**)&hnbaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_CriticalityDiagnostics, hnbaP_CriticalityDiagnostics_p);
                memcpy(&tnlUpdateFailureIEs->criticalityDiagnostics, hnbaP_CriticalityDiagnostics_p, sizeof(HNBAP_CriticalityDiagnostics_t));
                FREEMEM(hnbaP_CriticalityDiagnostics_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message tnlupdatefailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_TNLUpdateFailure, tNLUpdateFailure_p);
    return decoded;
}

int hnbap_decode_hnbconfigtransferrequesties(
    HNBAP_HNBConfigTransferRequestIEs_t *hnbConfigTransferRequestIEs,
    ANY_t *any_p) {

    HNBAP_HNBConfigTransferRequest_t *hNBConfigTransferRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(hnbConfigTransferRequestIEs != NULL);

    memset(hnbConfigTransferRequestIEs, 0, sizeof(HNBAP_HNBConfigTransferRequestIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_HNBConfigTransferRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_HNBConfigTransferRequest, (void**)&hNBConfigTransferRequest_p);

    if (tempDecoded < 0 || hNBConfigTransferRequest_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_HNBConfigTransferRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < hNBConfigTransferRequest_p->hnbConfigTransferRequest_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = hNBConfigTransferRequest_p->hnbConfigTransferRequest_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_NeighbourInfoRequestList:
            {
                HNBAP_NeighbourInfoRequestList_t *hnbaP_NeighbourInfoRequestList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_NeighbourInfoRequestList, (void**)&hnbaP_NeighbourInfoRequestList_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE neighbourInfoRequestList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_NeighbourInfoRequestList, hnbaP_NeighbourInfoRequestList_p);
                memcpy(&hnbConfigTransferRequestIEs->neighbourInfoRequestList, hnbaP_NeighbourInfoRequestList_p, sizeof(HNBAP_NeighbourInfoRequestList_t));
                FREEMEM(hnbaP_NeighbourInfoRequestList_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message hnbconfigtransferrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_HNBConfigTransferRequest, hNBConfigTransferRequest_p);
    return decoded;
}

int hnbap_decode_hnbconfigtransferresponseies(
    HNBAP_HNBConfigTransferResponseIEs_t *hnbConfigTransferResponseIEs,
    ANY_t *any_p) {

    HNBAP_HNBConfigTransferResponse_t *hNBConfigTransferResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(hnbConfigTransferResponseIEs != NULL);

    memset(hnbConfigTransferResponseIEs, 0, sizeof(HNBAP_HNBConfigTransferResponseIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_HNBConfigTransferResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_HNBConfigTransferResponse, (void**)&hNBConfigTransferResponse_p);

    if (tempDecoded < 0 || hNBConfigTransferResponse_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_HNBConfigTransferResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < hNBConfigTransferResponse_p->hnbConfigTransferResponse_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = hNBConfigTransferResponse_p->hnbConfigTransferResponse_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_NeighbourInfoList:
            {
                HNBAP_NeighbourInfoList_t *hnbaP_NeighbourInfoList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_NeighbourInfoList, (void**)&hnbaP_NeighbourInfoList_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE neighbourInfoList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_NeighbourInfoList, hnbaP_NeighbourInfoList_p);
                memcpy(&hnbConfigTransferResponseIEs->neighbourInfoList, hnbaP_NeighbourInfoList_p, sizeof(HNBAP_NeighbourInfoList_t));
                FREEMEM(hnbaP_NeighbourInfoList_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message hnbconfigtransferresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_HNBConfigTransferResponse, hNBConfigTransferResponse_p);
    return decoded;
}

int hnbap_decode_relocationcompleteies(
    HNBAP_RelocationCompleteIEs_t *relocationCompleteIEs,
    ANY_t *any_p) {

    HNBAP_RelocationComplete_t *relocationComplete_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(relocationCompleteIEs != NULL);

    memset(relocationCompleteIEs, 0, sizeof(HNBAP_RelocationCompleteIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_RelocationCompleteIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_RelocationComplete, (void**)&relocationComplete_p);

    if (tempDecoded < 0 || relocationComplete_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_RelocationCompleteIEs failed\n");
        return -1;
    }

    for (i = 0; i < relocationComplete_p->relocationComplete_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = relocationComplete_p->relocationComplete_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_Context_ID:
            {
                HNBAP_Context_ID_t *hnbaP_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Context_ID, (void**)&hnbaP_ContextID_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Context_ID, hnbaP_ContextID_p);
                memcpy(&relocationCompleteIEs->context_ID, hnbaP_ContextID_p, sizeof(HNBAP_Context_ID_t));
                FREEMEM(hnbaP_ContextID_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message relocationcompleteies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_RelocationComplete, relocationComplete_p);
    return decoded;
}

int hnbap_decode_errorindicationies(
    HNBAP_ErrorIndicationIEs_t *errorIndicationIEs,
    ANY_t *any_p) {

    HNBAP_ErrorIndication_t *errorIndication_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(errorIndicationIEs != NULL);

    memset(errorIndicationIEs, 0, sizeof(HNBAP_ErrorIndicationIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_ErrorIndicationIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_ErrorIndication, (void**)&errorIndication_p);

    if (tempDecoded < 0 || errorIndication_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_ErrorIndicationIEs failed\n");
        return -1;
    }

    for (i = 0; i < errorIndication_p->errorIndication_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = errorIndication_p->errorIndication_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_Cause:
            {
                HNBAP_Cause_t *hnbaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_Cause, (void**)&hnbaP_Cause_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_Cause, hnbaP_Cause_p);
                memcpy(&errorIndicationIEs->cause, hnbaP_Cause_p, sizeof(HNBAP_Cause_t));
                FREEMEM(hnbaP_Cause_p);
            } break;
            /* Optional field */
            case HNBAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                HNBAP_CriticalityDiagnostics_t *hnbaP_CriticalityDiagnostics_p = NULL;
                errorIndicationIEs->presenceMask |= ERRORINDICATIONIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_CriticalityDiagnostics, (void**)&hnbaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_CriticalityDiagnostics, hnbaP_CriticalityDiagnostics_p);
                memcpy(&errorIndicationIEs->criticalityDiagnostics, hnbaP_CriticalityDiagnostics_p, sizeof(HNBAP_CriticalityDiagnostics_t));
                FREEMEM(hnbaP_CriticalityDiagnostics_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message errorindicationies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_ErrorIndication, errorIndication_p);
    return decoded;
}

int hnbap_decode_u_rntiqueryrequesties(
    HNBAP_U_RNTIQueryRequestIEs_t *u_RNTIQueryRequestIEs,
    ANY_t *any_p) {

    HNBAP_U_RNTIQueryRequest_t *u_RNTIQueryRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(u_RNTIQueryRequestIEs != NULL);

    memset(u_RNTIQueryRequestIEs, 0, sizeof(HNBAP_U_RNTIQueryRequestIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_U_RNTIQueryRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_U_RNTIQueryRequest, (void**)&u_RNTIQueryRequest_p);

    if (tempDecoded < 0 || u_RNTIQueryRequest_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_U_RNTIQueryRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < u_RNTIQueryRequest_p->u_RNTIQueryRequest_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = u_RNTIQueryRequest_p->u_RNTIQueryRequest_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_U_RNTI:
            {
                HNBAP_U_RNTI_t *hnbap_urnti_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_U_RNTI, (void**)&hnbap_urnti_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE u_rnti failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_U_RNTI, hnbap_urnti_p);
                memcpy(&u_RNTIQueryRequestIEs->u_rnti, hnbap_urnti_p, sizeof(HNBAP_U_RNTI_t));
                FREEMEM(hnbap_urnti_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message u_rntiqueryrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_U_RNTIQueryRequest, u_RNTIQueryRequest_p);
    return decoded;
}

int hnbap_decode_u_rntiqueryresponseies(
    HNBAP_U_RNTIQueryResponseIEs_t *u_RNTIQueryResponseIEs,
    ANY_t *any_p) {

    HNBAP_U_RNTIQueryResponse_t *u_RNTIQueryResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(u_RNTIQueryResponseIEs != NULL);

    memset(u_RNTIQueryResponseIEs, 0, sizeof(HNBAP_U_RNTIQueryResponseIEs_t));
    HNBAP_DEBUG("Decoding message HNBAP_U_RNTIQueryResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_HNBAP_U_RNTIQueryResponse, (void**)&u_RNTIQueryResponse_p);

    if (tempDecoded < 0 || u_RNTIQueryResponse_p == NULL) {
        HNBAP_DEBUG("Decoding of message HNBAP_U_RNTIQueryResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < u_RNTIQueryResponse_p->u_RNTIQueryResponse_ies.list.count; i++) {
        HNBAP_IE_t *ie_p;
        ie_p = u_RNTIQueryResponse_p->u_RNTIQueryResponse_ies.list.array[i];
        switch(ie_p->id) {
            case HNBAP_ProtocolIE_ID_id_HNB_GWResponse:
            {
                HNBAP_HNB_GWResponse_t *hnbaP_HNBGWResponse_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_HNBAP_HNB_GWResponse, (void**)&hnbaP_HNBGWResponse_p);
                if (tempDecoded < 0) {
                    HNBAP_DEBUG("Decoding of IE hnB_GWResponse failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_HNBAP_HNB_GWResponse, hnbaP_HNBGWResponse_p);
                memcpy(&u_RNTIQueryResponseIEs->hnB_GWResponse, hnbaP_HNBGWResponse_p, sizeof(HNBAP_HNB_GWResponse_t));
                FREEMEM(hnbaP_HNBGWResponse_p);
            } break;
            default:
                HNBAP_DEBUG("Unknown protocol IE id (%d) for message u_rntiqueryresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_HNBAP_U_RNTIQueryResponse, u_RNTIQueryResponse_p);
    return decoded;
}

int hnbap_free_hnbregisterrequesties(
    HNBAP_HNBRegisterRequestIEs_t *hnbRegisterRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_HNB_Identity, &hnbRegisterRequestIEs->hnB_Identity);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_HNB_Location_Information, &hnbRegisterRequestIEs->hnB_Location_Information);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_PLMNidentity, &hnbRegisterRequestIEs->plmNidentity);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_CellIdentity, &hnbRegisterRequestIEs->cellIdentity);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_LAC, &hnbRegisterRequestIEs->lac);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_RAC, &hnbRegisterRequestIEs->rac);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_SAC, &hnbRegisterRequestIEs->sac);
    /* Optional field */
    if ((hnbRegisterRequestIEs->presenceMask & HNBREGISTERREQUESTIES_HNBAP_CSG_ID_PRESENT)
        == HNBREGISTERREQUESTIES_HNBAP_CSG_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_CSG_ID, &hnbRegisterRequestIEs->csg_id);
    return 0;
}

int hnbap_free_hnbregisteraccepties(
    HNBAP_HNBRegisterAcceptIEs_t *hnbRegisterAcceptIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_RNC_ID, &hnbRegisterAcceptIEs->rnc_id);
    return 0;
}

int hnbap_free_hnbregisterrejecties(
    HNBAP_HNBRegisterRejectIEs_t *hnbRegisterRejectIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Cause, &hnbRegisterRejectIEs->cause);
    /* Optional field */
    if ((hnbRegisterRejectIEs->presenceMask & HNBREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == HNBREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_CriticalityDiagnostics, &hnbRegisterRejectIEs->criticalityDiagnostics);
    /* Conditional field */
    if ((hnbRegisterRejectIEs->presenceMask & HNBREGISTERREJECTIES_HNBAP_BACKOFFTIMER_PRESENT)
        == HNBREGISTERREJECTIES_HNBAP_BACKOFFTIMER_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_BackoffTimer, &hnbRegisterRejectIEs->backoffTimer);
    return 0;
}

int hnbap_free_hnbde_registeries(
    HNBAP_HNBDe_RegisterIEs_t *hnbDe_RegisterIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Cause, &hnbDe_RegisterIEs->cause);
    /* Conditional field */
    if ((hnbDe_RegisterIEs->presenceMask & HNBDE_REGISTERIES_HNBAP_BACKOFFTIMER_PRESENT)
        == HNBDE_REGISTERIES_HNBAP_BACKOFFTIMER_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_BackoffTimer, &hnbDe_RegisterIEs->backoffTimer);
    return 0;
}

int hnbap_free_ueregisterrequesties(
    HNBAP_UERegisterRequestIEs_t *ueRegisterRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_UE_Identity, &ueRegisterRequestIEs->uE_Identity);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Registration_Cause, &ueRegisterRequestIEs->registration_Cause);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_UE_Capabilities, &ueRegisterRequestIEs->uE_Capabilities);
    return 0;
}

int hnbap_free_ueregisteraccepties(
    HNBAP_UERegisterAcceptIEs_t *ueRegisterAcceptIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_UE_Identity, &ueRegisterAcceptIEs->uE_Identity);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Context_ID, &ueRegisterAcceptIEs->context_ID);
    return 0;
}

int hnbap_free_ueregisterrejecties(
    HNBAP_UERegisterRejectIEs_t *ueRegisterRejectIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_UE_Identity, &ueRegisterRejectIEs->uE_Identity);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Cause, &ueRegisterRejectIEs->cause);
    /* Optional field */
    if ((ueRegisterRejectIEs->presenceMask & UEREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == UEREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_CriticalityDiagnostics, &ueRegisterRejectIEs->criticalityDiagnostics);
    return 0;
}

int hnbap_free_uede_registeries(
    HNBAP_UEDe_RegisterIEs_t *ueDe_RegisterIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Context_ID, &ueDe_RegisterIEs->context_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Cause, &ueDe_RegisterIEs->cause);
    return 0;
}

int hnbap_free_csgmembershipupdateies(
    HNBAP_CSGMembershipUpdateIEs_t *csgMembershipUpdateIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Context_ID, &csgMembershipUpdateIEs->context_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_CSGMembershipStatus, &csgMembershipUpdateIEs->csgMembershipStatus);
    return 0;
}

int hnbap_free_tnlupdaterequesties(
    HNBAP_TNLUpdateRequestIEs_t *tnlUpdateRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Context_ID, &tnlUpdateRequestIEs->context_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_RABList, &tnlUpdateRequestIEs->rabList);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Update_cause, &tnlUpdateRequestIEs->update_cause);
    return 0;
}

int hnbap_free_tnlupdateresponseies(
    HNBAP_TNLUpdateResponseIEs_t *tnlUpdateResponseIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Context_ID, &tnlUpdateResponseIEs->context_ID);
    return 0;
}

int hnbap_free_tnlupdatefailureies(
    HNBAP_TNLUpdateFailureIEs_t *tnlUpdateFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Context_ID, &tnlUpdateFailureIEs->context_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Cause, &tnlUpdateFailureIEs->cause);
    /* Optional field */
    if ((tnlUpdateFailureIEs->presenceMask & TNLUPDATEFAILUREIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == TNLUPDATEFAILUREIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_CriticalityDiagnostics, &tnlUpdateFailureIEs->criticalityDiagnostics);
    return 0;
}

int hnbap_free_hnbconfigtransferrequesties(
    HNBAP_HNBConfigTransferRequestIEs_t *hnbConfigTransferRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_NeighbourInfoRequestList, &hnbConfigTransferRequestIEs->neighbourInfoRequestList);
    return 0;
}

int hnbap_free_hnbconfigtransferresponseies(
    HNBAP_HNBConfigTransferResponseIEs_t *hnbConfigTransferResponseIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_NeighbourInfoList, &hnbConfigTransferResponseIEs->neighbourInfoList);
    return 0;
}

int hnbap_free_relocationcompleteies(
    HNBAP_RelocationCompleteIEs_t *relocationCompleteIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Context_ID, &relocationCompleteIEs->context_ID);
    return 0;
}

int hnbap_free_errorindicationies(
    HNBAP_ErrorIndicationIEs_t *errorIndicationIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_Cause, &errorIndicationIEs->cause);
    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == ERRORINDICATIONIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_CriticalityDiagnostics, &errorIndicationIEs->criticalityDiagnostics);
    return 0;
}

int hnbap_free_u_rntiqueryrequesties(
    HNBAP_U_RNTIQueryRequestIEs_t *u_RNTIQueryRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_U_RNTI, &u_RNTIQueryRequestIEs->u_rnti);
    return 0;
}

int hnbap_free_u_rntiqueryresponseies(
    HNBAP_U_RNTIQueryResponseIEs_t *u_RNTIQueryResponseIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_HNBAP_HNB_GWResponse, &u_RNTIQueryResponseIEs->hnB_GWResponse);
    return 0;
}

