/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/rua/RUA-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/rua/rua_common.h>
#include <osmocom/rua/rua_ies_defs.h>

int rua_encode_connecties(
    RUA_Connect_t *connect,
    RUA_ConnectIEs_t *connectIEs) {

    RUA_IE_t *ie;

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_CN_DomainIndicator,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_CN_DomainIndicator,
                          &connectIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&connect->connect_ies.list, ie);

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_Context_ID,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_Context_ID,
                          &connectIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&connect->connect_ies.list, ie);

    /* Optional field */
    if ((connectIEs->presenceMask & CONNECTIES_RUA_INTRADOMAINNASNODESELECTOR_PRESENT)
        == CONNECTIES_RUA_INTRADOMAINNASNODESELECTOR_PRESENT) {
        if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_IntraDomainNasNodeSelector,
                              RUA_Criticality_ignore,
                              &asn_DEF_RUA_IntraDomainNasNodeSelector,
                              &connectIEs->intraDomainNasNodeSelector)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&connect->connect_ies.list, ie);
    }

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_Establishment_Cause,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_Establishment_Cause,
                          &connectIEs->establishment_Cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&connect->connect_ies.list, ie);

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_RANAP_Message,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_RANAP_Message,
                          &connectIEs->ranaP_Message)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&connect->connect_ies.list, ie);

    return 0;
}

int rua_encode_directtransferies(
    RUA_DirectTransfer_t *directTransfer,
    RUA_DirectTransferIEs_t *directTransferIEs) {

    RUA_IE_t *ie;

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_CN_DomainIndicator,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_CN_DomainIndicator,
                          &directTransferIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&directTransfer->directTransfer_ies.list, ie);

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_Context_ID,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_Context_ID,
                          &directTransferIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&directTransfer->directTransfer_ies.list, ie);

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_RANAP_Message,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_RANAP_Message,
                          &directTransferIEs->ranaP_Message)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&directTransfer->directTransfer_ies.list, ie);

    return 0;
}

int rua_encode_disconnecties(
    RUA_Disconnect_t *disconnect,
    RUA_DisconnectIEs_t *disconnectIEs) {

    RUA_IE_t *ie;

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_CN_DomainIndicator,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_CN_DomainIndicator,
                          &disconnectIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&disconnect->disconnect_ies.list, ie);

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_Context_ID,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_Context_ID,
                          &disconnectIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&disconnect->disconnect_ies.list, ie);

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_Cause,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_Cause,
                          &disconnectIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&disconnect->disconnect_ies.list, ie);

    /* Conditional field */
    if ((disconnectIEs->presenceMask & DISCONNECTIES_RUA_RANAP_MESSAGE_PRESENT)
        == DISCONNECTIES_RUA_RANAP_MESSAGE_PRESENT) {
        if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_RANAP_Message,
                              RUA_Criticality_reject,
                              &asn_DEF_RUA_RANAP_Message,
                              &disconnectIEs->ranaP_Message)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&disconnect->disconnect_ies.list, ie);
    }

    return 0;
}

int rua_encode_connectionlesstransferies(
    RUA_ConnectionlessTransfer_t *connectionlessTransfer,
    RUA_ConnectionlessTransferIEs_t *connectionlessTransferIEs) {

    RUA_IE_t *ie;

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_RANAP_Message,
                          RUA_Criticality_reject,
                          &asn_DEF_RUA_RANAP_Message,
                          &connectionlessTransferIEs->ranaP_Message)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&connectionlessTransfer->connectionlessTransfer_ies.list, ie);

    return 0;
}

int rua_encode_errorindicationies(
    RUA_ErrorIndication_t *errorIndication,
    RUA_ErrorIndicationIEs_t *errorIndicationIEs) {

    RUA_IE_t *ie;

    if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_Cause,
                          RUA_Criticality_ignore,
                          &asn_DEF_RUA_Cause,
                          &errorIndicationIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&errorIndication->errorIndication_ies.list, ie);

    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RUA_CRITICALITYDIAGNOSTICS_PRESENT)
        == ERRORINDICATIONIES_RUA_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = rua_new_ie(RUA_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RUA_Criticality_ignore,
                              &asn_DEF_RUA_CriticalityDiagnostics,
                              &errorIndicationIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&errorIndication->errorIndication_ies.list, ie);
    }

    return 0;
}

