/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/ranap/RANAP-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/ranap/ranap_common.h>
#include <osmocom/ranap/ranap_ies_defs.h>

int ranap_decode_iu_releasecommandies(
    RANAP_Iu_ReleaseCommandIEs_t *iu_ReleaseCommandIEs,
    ANY_t *any_p) {

    RANAP_Iu_ReleaseCommand_t *iu_ReleaseCommand_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(iu_ReleaseCommandIEs != NULL);

    memset(iu_ReleaseCommandIEs, 0, sizeof(RANAP_Iu_ReleaseCommandIEs_t));
    RANAP_DEBUG("Decoding message RANAP_Iu_ReleaseCommandIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_Iu_ReleaseCommand, (void**)&iu_ReleaseCommand_p);

    if (tempDecoded < 0 || iu_ReleaseCommand_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_Iu_ReleaseCommandIEs failed\n");
        return -1;
    }

    for (i = 0; i < iu_ReleaseCommand_p->iu_ReleaseCommand_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = iu_ReleaseCommand_p->iu_ReleaseCommand_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&iu_ReleaseCommandIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message iu_releasecommandies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_Iu_ReleaseCommand, iu_ReleaseCommand_p);
    return decoded;
}

int ranap_decode_iu_releasecompleteies(
    RANAP_Iu_ReleaseCompleteIEs_t *iu_ReleaseCompleteIEs,
    ANY_t *any_p) {

    RANAP_Iu_ReleaseComplete_t *iu_ReleaseComplete_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(iu_ReleaseCompleteIEs != NULL);

    memset(iu_ReleaseCompleteIEs, 0, sizeof(RANAP_Iu_ReleaseCompleteIEs_t));
    RANAP_DEBUG("Decoding message RANAP_Iu_ReleaseCompleteIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_Iu_ReleaseComplete, (void**)&iu_ReleaseComplete_p);

    if (tempDecoded < 0 || iu_ReleaseComplete_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_Iu_ReleaseCompleteIEs failed\n");
        return -1;
    }

    for (i = 0; i < iu_ReleaseComplete_p->iu_ReleaseComplete_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = iu_ReleaseComplete_p->iu_ReleaseComplete_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportList:
            {
                RANAP_RAB_DataVolumeReportList_t *ranaP_RABDataVolumeReportList_p = NULL;
                iu_ReleaseCompleteIEs->presenceMask |= IU_RELEASECOMPLETEIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataVolumeReportList, (void**)&ranaP_RABDataVolumeReportList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataVolumeReportList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataVolumeReportList, ranaP_RABDataVolumeReportList_p);
                memcpy(&iu_ReleaseCompleteIEs->raB_DataVolumeReportList, ranaP_RABDataVolumeReportList_p, sizeof(RANAP_RAB_DataVolumeReportList_t));
                FREEMEM(ranaP_RABDataVolumeReportList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_ReleasedList_IuRelComp:
            {
                RANAP_RAB_ReleasedList_IuRelComp_t *ranaP_RABReleasedListIuRelComp_p = NULL;
                iu_ReleaseCompleteIEs->presenceMask |= IU_RELEASECOMPLETEIES_RANAP_RAB_RELEASEDLIST_IURELCOMP_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ReleasedList_IuRelComp, (void**)&ranaP_RABReleasedListIuRelComp_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ReleasedList_IuRelComp failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ReleasedList_IuRelComp, ranaP_RABReleasedListIuRelComp_p);
                memcpy(&iu_ReleaseCompleteIEs->raB_ReleasedList_IuRelComp, ranaP_RABReleasedListIuRelComp_p, sizeof(RANAP_RAB_ReleasedList_IuRelComp_t));
                FREEMEM(ranaP_RABReleasedListIuRelComp_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                iu_ReleaseCompleteIEs->presenceMask |= IU_RELEASECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&iu_ReleaseCompleteIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message iu_releasecompleteies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_Iu_ReleaseComplete, iu_ReleaseComplete_p);
    return decoded;
}

int ranap_decode_rab_datavolumereportitemies(
    RANAP_RAB_DataVolumeReportItemIEs_t *raB_DataVolumeReportItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_DataVolumeReportList_t *rAB_DataVolumeReportItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_DataVolumeReportItemIEs != NULL);

    memset(raB_DataVolumeReportItemIEs, 0, sizeof(RANAP_RAB_DataVolumeReportItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_DataVolumeReportItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_DataVolumeReportList, (void**)&rAB_DataVolumeReportItem_p);

    if (tempDecoded < 0 || rAB_DataVolumeReportItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_DataVolumeReportItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_DataVolumeReportItem_p->raB_DataVolumeReportList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_DataVolumeReportItem_p->raB_DataVolumeReportList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportItem:
            {
                RANAP_RAB_DataVolumeReportItem_t *ranaP_RABDataVolumeReportItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataVolumeReportItem, (void**)&ranaP_RABDataVolumeReportItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataVolumeReportItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataVolumeReportItem, ranaP_RABDataVolumeReportItem_p);
                memcpy(&raB_DataVolumeReportItemIEs->raB_DataVolumeReportItem, ranaP_RABDataVolumeReportItem_p, sizeof(RANAP_RAB_DataVolumeReportItem_t));
                FREEMEM(ranaP_RABDataVolumeReportItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_datavolumereportitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_DataVolumeReportList, rAB_DataVolumeReportItem_p);
    return decoded;
}

int ranap_decode_rab_releaseditem_iurelcomp_ies(
    RANAP_RAB_ReleasedItem_IuRelComp_IEs_t *raB_ReleasedItem_IuRelComp_IEs,
    ANY_t *any_p) {

    RANAP_RAB_ReleasedList_IuRelComp_t *rAB_ReleasedItem_IuRelComp_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_ReleasedItem_IuRelComp_IEs != NULL);

    memset(raB_ReleasedItem_IuRelComp_IEs, 0, sizeof(RANAP_RAB_ReleasedItem_IuRelComp_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_ReleasedItem_IuRelComp_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_ReleasedList_IuRelComp, (void**)&rAB_ReleasedItem_IuRelComp_p);

    if (tempDecoded < 0 || rAB_ReleasedItem_IuRelComp_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_ReleasedItem_IuRelComp_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_ReleasedItem_IuRelComp_p->raB_ReleasedList_IuRelComp_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_ReleasedItem_IuRelComp_p->raB_ReleasedList_IuRelComp_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ReleasedItem_IuRelComp:
            {
                RANAP_RAB_ReleasedItem_IuRelComp_t *ranaP_RABReleasedItemIuRelComp_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ReleasedItem_IuRelComp, (void**)&ranaP_RABReleasedItemIuRelComp_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ReleasedItem_IuRelComp failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ReleasedItem_IuRelComp, ranaP_RABReleasedItemIuRelComp_p);
                memcpy(&raB_ReleasedItem_IuRelComp_IEs->raB_ReleasedItem_IuRelComp, ranaP_RABReleasedItemIuRelComp_p, sizeof(RANAP_RAB_ReleasedItem_IuRelComp_t));
                FREEMEM(ranaP_RABReleasedItemIuRelComp_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_releaseditem_iurelcomp_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_ReleasedList_IuRelComp, rAB_ReleasedItem_IuRelComp_p);
    return decoded;
}

int ranap_decode_relocationrequiredies(
    RANAP_RelocationRequiredIEs_t *relocationRequiredIEs,
    ANY_t *any_p) {

    RANAP_RelocationRequired_t *relocationRequired_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(relocationRequiredIEs != NULL);

    memset(relocationRequiredIEs, 0, sizeof(RANAP_RelocationRequiredIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RelocationRequiredIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RelocationRequired, (void**)&relocationRequired_p);

    if (tempDecoded < 0 || relocationRequired_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RelocationRequiredIEs failed\n");
        return -1;
    }

    for (i = 0; i < relocationRequired_p->relocationRequired_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = relocationRequired_p->relocationRequired_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RelocationType:
            {
                RANAP_RelocationType_t *ranaP_RelocationType_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RelocationType, (void**)&ranaP_RelocationType_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE relocationType failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RelocationType, ranaP_RelocationType_p);
                memcpy(&relocationRequiredIEs->relocationType, ranaP_RelocationType_p, sizeof(RANAP_RelocationType_t));
                FREEMEM(ranaP_RelocationType_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&relocationRequiredIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            case RANAP_ProtocolIE_ID_id_SourceID:
            {
                RANAP_SourceID_t *ranaP_SourceID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SourceID, (void**)&ranaP_SourceID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE sourceID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SourceID, ranaP_SourceID_p);
                memcpy(&relocationRequiredIEs->sourceID, ranaP_SourceID_p, sizeof(RANAP_SourceID_t));
                FREEMEM(ranaP_SourceID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_TargetID:
            {
                RANAP_TargetID_t *ranaP_TargetID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TargetID, (void**)&ranaP_TargetID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE targetID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TargetID, ranaP_TargetID_p);
                memcpy(&relocationRequiredIEs->targetID, ranaP_TargetID_p, sizeof(RANAP_TargetID_t));
                FREEMEM(ranaP_TargetID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_OldBSS_ToNewBSS_Information:
            {
                RANAP_OldBSS_ToNewBSS_Information_t *ranaP_OldBSSToNewBSSInformation_p = NULL;
                relocationRequiredIEs->presenceMask |= RELOCATIONREQUIREDIES_RANAP_OLDBSS_TONEWBSS_INFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_OldBSS_ToNewBSS_Information, (void**)&ranaP_OldBSSToNewBSSInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE oldBSS_ToNewBSS_Information failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_OldBSS_ToNewBSS_Information, ranaP_OldBSSToNewBSSInformation_p);
                memcpy(&relocationRequiredIEs->oldBSS_ToNewBSS_Information, ranaP_OldBSSToNewBSSInformation_p, sizeof(RANAP_OldBSS_ToNewBSS_Information_t));
                FREEMEM(ranaP_OldBSSToNewBSSInformation_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message relocationrequiredies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RelocationRequired, relocationRequired_p);
    return decoded;
}

int ranap_decode_relocationcommandies(
    RANAP_RelocationCommandIEs_t *relocationCommandIEs,
    ANY_t *any_p) {

    RANAP_RelocationCommand_t *relocationCommand_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(relocationCommandIEs != NULL);

    memset(relocationCommandIEs, 0, sizeof(RANAP_RelocationCommandIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RelocationCommandIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RelocationCommand, (void**)&relocationCommand_p);

    if (tempDecoded < 0 || relocationCommand_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RelocationCommandIEs failed\n");
        return -1;
    }

    for (i = 0; i < relocationCommand_p->relocationCommand_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = relocationCommand_p->relocationCommand_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_L3_Information:
            {
                RANAP_L3_Information_t *ranaP_L3Information_p = NULL;
                relocationCommandIEs->presenceMask |= RELOCATIONCOMMANDIES_RANAP_L3_INFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_L3_Information, (void**)&ranaP_L3Information_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE l3_Information failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_L3_Information, ranaP_L3Information_p);
                memcpy(&relocationCommandIEs->l3_Information, ranaP_L3Information_p, sizeof(RANAP_L3_Information_t));
                FREEMEM(ranaP_L3Information_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_RelocationReleaseList:
            {
                RANAP_RAB_RelocationReleaseList_t *ranaP_RABRelocationReleaseList_p = NULL;
                relocationCommandIEs->presenceMask |= RELOCATIONCOMMANDIES_RANAP_RAB_RELOCATIONRELEASELIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_RelocationReleaseList, (void**)&ranaP_RABRelocationReleaseList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_RelocationReleaseList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_RelocationReleaseList, ranaP_RABRelocationReleaseList_p);
                memcpy(&relocationCommandIEs->raB_RelocationReleaseList, ranaP_RABRelocationReleaseList_p, sizeof(RANAP_RAB_RelocationReleaseList_t));
                FREEMEM(ranaP_RABRelocationReleaseList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_DataForwardingList:
            {
                RANAP_RAB_DataForwardingList_t *ranaP_RABDataForwardingList_p = NULL;
                relocationCommandIEs->presenceMask |= RELOCATIONCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataForwardingList, (void**)&ranaP_RABDataForwardingList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataForwardingList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataForwardingList, ranaP_RABDataForwardingList_p);
                memcpy(&relocationCommandIEs->raB_DataForwardingList, ranaP_RABDataForwardingList_p, sizeof(RANAP_RAB_DataForwardingList_t));
                FREEMEM(ranaP_RABDataForwardingList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                relocationCommandIEs->presenceMask |= RELOCATIONCOMMANDIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&relocationCommandIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message relocationcommandies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RelocationCommand, relocationCommand_p);
    return decoded;
}

int ranap_decode_rab_relocationreleaseitemies(
    RANAP_RAB_RelocationReleaseItemIEs_t *raB_RelocationReleaseItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_RelocationReleaseList_t *rAB_RelocationReleaseItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_RelocationReleaseItemIEs != NULL);

    memset(raB_RelocationReleaseItemIEs, 0, sizeof(RANAP_RAB_RelocationReleaseItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_RelocationReleaseItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_RelocationReleaseList, (void**)&rAB_RelocationReleaseItem_p);

    if (tempDecoded < 0 || rAB_RelocationReleaseItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_RelocationReleaseItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_RelocationReleaseItem_p->raB_RelocationReleaseList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_RelocationReleaseItem_p->raB_RelocationReleaseList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_RelocationReleaseItem:
            {
                RANAP_RAB_RelocationReleaseItem_t *ranaP_RABRelocationReleaseItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_RelocationReleaseItem, (void**)&ranaP_RABRelocationReleaseItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_RelocationReleaseItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_RelocationReleaseItem, ranaP_RABRelocationReleaseItem_p);
                memcpy(&raB_RelocationReleaseItemIEs->raB_RelocationReleaseItem, ranaP_RABRelocationReleaseItem_p, sizeof(RANAP_RAB_RelocationReleaseItem_t));
                FREEMEM(ranaP_RABRelocationReleaseItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_relocationreleaseitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_RelocationReleaseList, rAB_RelocationReleaseItem_p);
    return decoded;
}

int ranap_decode_rab_dataforwardingitemies(
    RANAP_RAB_DataForwardingItemIEs_t *raB_DataForwardingItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_DataForwardingList_t *rAB_DataForwardingItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_DataForwardingItemIEs != NULL);

    memset(raB_DataForwardingItemIEs, 0, sizeof(RANAP_RAB_DataForwardingItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_DataForwardingItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_DataForwardingList, (void**)&rAB_DataForwardingItem_p);

    if (tempDecoded < 0 || rAB_DataForwardingItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_DataForwardingItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_DataForwardingItem_p->raB_DataForwardingList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_DataForwardingItem_p->raB_DataForwardingList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_DataForwardingItem:
            {
                RANAP_RAB_DataForwardingItem_t *ranaP_RABDataForwardingItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataForwardingItem, (void**)&ranaP_RABDataForwardingItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataForwardingItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataForwardingItem, ranaP_RABDataForwardingItem_p);
                memcpy(&raB_DataForwardingItemIEs->raB_DataForwardingItem, ranaP_RABDataForwardingItem_p, sizeof(RANAP_RAB_DataForwardingItem_t));
                FREEMEM(ranaP_RABDataForwardingItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_dataforwardingitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_DataForwardingList, rAB_DataForwardingItem_p);
    return decoded;
}

int ranap_decode_relocationpreparationfailureies(
    RANAP_RelocationPreparationFailureIEs_t *relocationPreparationFailureIEs,
    ANY_t *any_p) {

    RANAP_RelocationPreparationFailure_t *relocationPreparationFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(relocationPreparationFailureIEs != NULL);

    memset(relocationPreparationFailureIEs, 0, sizeof(RANAP_RelocationPreparationFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RelocationPreparationFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RelocationPreparationFailure, (void**)&relocationPreparationFailure_p);

    if (tempDecoded < 0 || relocationPreparationFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RelocationPreparationFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < relocationPreparationFailure_p->relocationPreparationFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = relocationPreparationFailure_p->relocationPreparationFailure_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&relocationPreparationFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                relocationPreparationFailureIEs->presenceMask |= RELOCATIONPREPARATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&relocationPreparationFailureIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message relocationpreparationfailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RelocationPreparationFailure, relocationPreparationFailure_p);
    return decoded;
}

int ranap_decode_relocationrequesties(
    RANAP_RelocationRequestIEs_t *relocationRequestIEs,
    ANY_t *any_p) {

    RANAP_RelocationRequest_t *relocationRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(relocationRequestIEs != NULL);

    memset(relocationRequestIEs, 0, sizeof(RANAP_RelocationRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RelocationRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RelocationRequest, (void**)&relocationRequest_p);

    if (tempDecoded < 0 || relocationRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RelocationRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < relocationRequest_p->relocationRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = relocationRequest_p->relocationRequest_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_PermanentNAS_UE_ID:
            {
                RANAP_PermanentNAS_UE_ID_t *ranaP_PermanentNASUEID_p = NULL;
                relocationRequestIEs->presenceMask |= RELOCATIONREQUESTIES_RANAP_PERMANENTNAS_UE_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_PermanentNAS_UE_ID, (void**)&ranaP_PermanentNASUEID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE permanentNAS_UE_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_PermanentNAS_UE_ID, ranaP_PermanentNASUEID_p);
                memcpy(&relocationRequestIEs->permanentNAS_UE_ID, ranaP_PermanentNASUEID_p, sizeof(RANAP_PermanentNAS_UE_ID_t));
                FREEMEM(ranaP_PermanentNASUEID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&relocationRequestIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&relocationRequestIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Source_ToTarget_TransparentContainer:
            {
                RANAP_SourceRNC_ToTargetRNC_TransparentContainer_t *ranaP_SourceRNCToTargetRNCTransparentContainer_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SourceRNC_ToTargetRNC_TransparentContainer, (void**)&ranaP_SourceRNCToTargetRNCTransparentContainer_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE source_ToTarget_TransparentContainer failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SourceRNC_ToTargetRNC_TransparentContainer, ranaP_SourceRNCToTargetRNCTransparentContainer_p);
                memcpy(&relocationRequestIEs->source_ToTarget_TransparentContainer, ranaP_SourceRNCToTargetRNCTransparentContainer_p, sizeof(RANAP_SourceRNC_ToTargetRNC_TransparentContainer_t));
                FREEMEM(ranaP_SourceRNCToTargetRNCTransparentContainer_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_SetupList_RelocReq:
            {
                RANAP_RAB_SetupList_RelocReq_t *ranaP_RABSetupListRelocReq_p = NULL;
                relocationRequestIEs->presenceMask |= RELOCATIONREQUESTIES_RANAP_RAB_SETUPLIST_RELOCREQ_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupList_RelocReq, (void**)&ranaP_RABSetupListRelocReq_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupList_RelocReq failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupList_RelocReq, ranaP_RABSetupListRelocReq_p);
                memcpy(&relocationRequestIEs->raB_SetupList_RelocReq, ranaP_RABSetupListRelocReq_p, sizeof(RANAP_RAB_SetupList_RelocReq_t));
                FREEMEM(ranaP_RABSetupListRelocReq_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_IntegrityProtectionInformation:
            {
                RANAP_IntegrityProtectionInformation_t *ranaP_IntegrityProtectionInformation_p = NULL;
                relocationRequestIEs->presenceMask |= RELOCATIONREQUESTIES_RANAP_INTEGRITYPROTECTIONINFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IntegrityProtectionInformation, (void**)&ranaP_IntegrityProtectionInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE integrityProtectionInformation failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IntegrityProtectionInformation, ranaP_IntegrityProtectionInformation_p);
                memcpy(&relocationRequestIEs->integrityProtectionInformation, ranaP_IntegrityProtectionInformation_p, sizeof(RANAP_IntegrityProtectionInformation_t));
                FREEMEM(ranaP_IntegrityProtectionInformation_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_EncryptionInformation:
            {
                RANAP_EncryptionInformation_t *ranaP_EncryptionInformation_p = NULL;
                relocationRequestIEs->presenceMask |= RELOCATIONREQUESTIES_RANAP_ENCRYPTIONINFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_EncryptionInformation, (void**)&ranaP_EncryptionInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE encryptionInformation failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_EncryptionInformation, ranaP_EncryptionInformation_p);
                memcpy(&relocationRequestIEs->encryptionInformation, ranaP_EncryptionInformation_p, sizeof(RANAP_EncryptionInformation_t));
                FREEMEM(ranaP_EncryptionInformation_p);
            } break;
            case RANAP_ProtocolIE_ID_id_IuSigConId:
            {
                RANAP_IuSignallingConnectionIdentifier_t *ranaP_IuSignallingConnectionIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, (void**)&ranaP_IuSignallingConnectionIdentifier_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE iuSigConId failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, ranaP_IuSignallingConnectionIdentifier_p);
                memcpy(&relocationRequestIEs->iuSigConId, ranaP_IuSignallingConnectionIdentifier_p, sizeof(RANAP_IuSignallingConnectionIdentifier_t));
                FREEMEM(ranaP_IuSignallingConnectionIdentifier_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message relocationrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RelocationRequest, relocationRequest_p);
    return decoded;
}

int ranap_decode_rab_setupitem_relocreq_ies(
    RANAP_RAB_SetupItem_RelocReq_IEs_t *raB_SetupItem_RelocReq_IEs,
    ANY_t *any_p) {

    RANAP_RAB_SetupList_RelocReq_t *rAB_SetupItem_RelocReq_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_SetupItem_RelocReq_IEs != NULL);

    memset(raB_SetupItem_RelocReq_IEs, 0, sizeof(RANAP_RAB_SetupItem_RelocReq_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_SetupItem_RelocReq_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_SetupList_RelocReq, (void**)&rAB_SetupItem_RelocReq_p);

    if (tempDecoded < 0 || rAB_SetupItem_RelocReq_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_SetupItem_RelocReq_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_SetupItem_RelocReq_p->raB_SetupList_RelocReq_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_SetupItem_RelocReq_p->raB_SetupList_RelocReq_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_SetupItem_RelocReq:
            {
                RANAP_RAB_SetupItem_RelocReq_t *ranaP_RABSetupItemRelocReq_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupItem_RelocReq, (void**)&ranaP_RABSetupItemRelocReq_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupItem_RelocReq failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupItem_RelocReq, ranaP_RABSetupItemRelocReq_p);
                memcpy(&raB_SetupItem_RelocReq_IEs->raB_SetupItem_RelocReq, ranaP_RABSetupItemRelocReq_p, sizeof(RANAP_RAB_SetupItem_RelocReq_t));
                FREEMEM(ranaP_RABSetupItemRelocReq_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_setupitem_relocreq_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_SetupList_RelocReq, rAB_SetupItem_RelocReq_p);
    return decoded;
}

int ranap_decode_relocationrequestacknowledgeies(
    RANAP_RelocationRequestAcknowledgeIEs_t *relocationRequestAcknowledgeIEs,
    ANY_t *any_p) {

    RANAP_RelocationRequestAcknowledge_t *relocationRequestAcknowledge_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(relocationRequestAcknowledgeIEs != NULL);

    memset(relocationRequestAcknowledgeIEs, 0, sizeof(RANAP_RelocationRequestAcknowledgeIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RelocationRequestAcknowledgeIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RelocationRequestAcknowledge, (void**)&relocationRequestAcknowledge_p);

    if (tempDecoded < 0 || relocationRequestAcknowledge_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RelocationRequestAcknowledgeIEs failed\n");
        return -1;
    }

    for (i = 0; i < relocationRequestAcknowledge_p->relocationRequestAcknowledge_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = relocationRequestAcknowledge_p->relocationRequestAcknowledge_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Target_ToSource_TransparentContainer:
            {
                RANAP_TargetRNC_ToSourceRNC_TransparentContainer_t *ranaP_TargetRNCToSourceRNCTransparentContainer_p = NULL;
                relocationRequestAcknowledgeIEs->presenceMask |= RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TargetRNC_ToSourceRNC_TransparentContainer, (void**)&ranaP_TargetRNCToSourceRNCTransparentContainer_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE target_ToSource_TransparentContainer failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TargetRNC_ToSourceRNC_TransparentContainer, ranaP_TargetRNCToSourceRNCTransparentContainer_p);
                memcpy(&relocationRequestAcknowledgeIEs->target_ToSource_TransparentContainer, ranaP_TargetRNCToSourceRNCTransparentContainer_p, sizeof(RANAP_TargetRNC_ToSourceRNC_TransparentContainer_t));
                FREEMEM(ranaP_TargetRNCToSourceRNCTransparentContainer_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_SetupList_RelocReqAck:
            {
                RANAP_RAB_SetupList_RelocReqAck_t *ranaP_RABSetupListRelocReqAck_p = NULL;
                relocationRequestAcknowledgeIEs->presenceMask |= RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_SETUPLIST_RELOCREQACK_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupList_RelocReqAck, (void**)&ranaP_RABSetupListRelocReqAck_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupList_RelocReqAck failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupList_RelocReqAck, ranaP_RABSetupListRelocReqAck_p);
                memcpy(&relocationRequestAcknowledgeIEs->raB_SetupList_RelocReqAck, ranaP_RABSetupListRelocReqAck_p, sizeof(RANAP_RAB_SetupList_RelocReqAck_t));
                FREEMEM(ranaP_RABSetupListRelocReqAck_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_FailedList:
            {
                RANAP_RAB_FailedList_t *ranaP_RABFailedList_p = NULL;
                relocationRequestAcknowledgeIEs->presenceMask |= RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_FAILEDLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_FailedList, (void**)&ranaP_RABFailedList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_FailedList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_FailedList, ranaP_RABFailedList_p);
                memcpy(&relocationRequestAcknowledgeIEs->raB_FailedList, ranaP_RABFailedList_p, sizeof(RANAP_RAB_FailedList_t));
                FREEMEM(ranaP_RABFailedList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_ChosenIntegrityProtectionAlgorithm:
            {
                RANAP_ChosenIntegrityProtectionAlgorithm_t *ranaP_ChosenIntegrityProtectionAlgorithm_p = NULL;
                relocationRequestAcknowledgeIEs->presenceMask |= RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENINTEGRITYPROTECTIONALGORITHM_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ChosenIntegrityProtectionAlgorithm, (void**)&ranaP_ChosenIntegrityProtectionAlgorithm_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE chosenIntegrityProtectionAlgorithm failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ChosenIntegrityProtectionAlgorithm, ranaP_ChosenIntegrityProtectionAlgorithm_p);
                memcpy(&relocationRequestAcknowledgeIEs->chosenIntegrityProtectionAlgorithm, ranaP_ChosenIntegrityProtectionAlgorithm_p, sizeof(RANAP_ChosenIntegrityProtectionAlgorithm_t));
                FREEMEM(ranaP_ChosenIntegrityProtectionAlgorithm_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_ChosenEncryptionAlgorithm:
            {
                RANAP_ChosenEncryptionAlgorithm_t *ranaP_ChosenEncryptionAlgorithm_p = NULL;
                relocationRequestAcknowledgeIEs->presenceMask |= RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ChosenEncryptionAlgorithm, (void**)&ranaP_ChosenEncryptionAlgorithm_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE chosenEncryptionAlgorithm failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ChosenEncryptionAlgorithm, ranaP_ChosenEncryptionAlgorithm_p);
                memcpy(&relocationRequestAcknowledgeIEs->chosenEncryptionAlgorithm, ranaP_ChosenEncryptionAlgorithm_p, sizeof(RANAP_ChosenEncryptionAlgorithm_t));
                FREEMEM(ranaP_ChosenEncryptionAlgorithm_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                relocationRequestAcknowledgeIEs->presenceMask |= RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&relocationRequestAcknowledgeIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message relocationrequestacknowledgeies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RelocationRequestAcknowledge, relocationRequestAcknowledge_p);
    return decoded;
}

int ranap_decode_rab_setupitem_relocreqack_ies(
    RANAP_RAB_SetupItem_RelocReqAck_IEs_t *raB_SetupItem_RelocReqAck_IEs,
    ANY_t *any_p) {

    RANAP_RAB_SetupList_RelocReqAck_t *rAB_SetupItem_RelocReqAck_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_SetupItem_RelocReqAck_IEs != NULL);

    memset(raB_SetupItem_RelocReqAck_IEs, 0, sizeof(RANAP_RAB_SetupItem_RelocReqAck_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_SetupItem_RelocReqAck_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_SetupList_RelocReqAck, (void**)&rAB_SetupItem_RelocReqAck_p);

    if (tempDecoded < 0 || rAB_SetupItem_RelocReqAck_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_SetupItem_RelocReqAck_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_SetupItem_RelocReqAck_p->raB_SetupList_RelocReqAck_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_SetupItem_RelocReqAck_p->raB_SetupList_RelocReqAck_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_SetupItem_RelocReqAck:
            {
                RANAP_RAB_SetupItem_RelocReqAck_t *ranaP_RABSetupItemRelocReqAck_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupItem_RelocReqAck, (void**)&ranaP_RABSetupItemRelocReqAck_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupItem_RelocReqAck failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupItem_RelocReqAck, ranaP_RABSetupItemRelocReqAck_p);
                memcpy(&raB_SetupItem_RelocReqAck_IEs->raB_SetupItem_RelocReqAck, ranaP_RABSetupItemRelocReqAck_p, sizeof(RANAP_RAB_SetupItem_RelocReqAck_t));
                FREEMEM(ranaP_RABSetupItemRelocReqAck_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_setupitem_relocreqack_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_SetupList_RelocReqAck, rAB_SetupItem_RelocReqAck_p);
    return decoded;
}

int ranap_decode_rab_faileditemies(
    RANAP_RAB_FailedItemIEs_t *raB_FailedItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_FailedList_t *rAB_FailedItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_FailedItemIEs != NULL);

    memset(raB_FailedItemIEs, 0, sizeof(RANAP_RAB_FailedItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_FailedItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_FailedList, (void**)&rAB_FailedItem_p);

    if (tempDecoded < 0 || rAB_FailedItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_FailedItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_FailedItem_p->raB_FailedList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_FailedItem_p->raB_FailedList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_FailedItem:
            {
                RANAP_RAB_FailedItem_t *ranaP_RABFailedItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_FailedItem, (void**)&ranaP_RABFailedItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_FailedItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_FailedItem, ranaP_RABFailedItem_p);
                memcpy(&raB_FailedItemIEs->raB_FailedItem, ranaP_RABFailedItem_p, sizeof(RANAP_RAB_FailedItem_t));
                FREEMEM(ranaP_RABFailedItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_faileditemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_FailedList, rAB_FailedItem_p);
    return decoded;
}

int ranap_decode_relocationfailureies(
    RANAP_RelocationFailureIEs_t *relocationFailureIEs,
    ANY_t *any_p) {

    RANAP_RelocationFailure_t *relocationFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(relocationFailureIEs != NULL);

    memset(relocationFailureIEs, 0, sizeof(RANAP_RelocationFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RelocationFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RelocationFailure, (void**)&relocationFailure_p);

    if (tempDecoded < 0 || relocationFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RelocationFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < relocationFailure_p->relocationFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = relocationFailure_p->relocationFailure_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&relocationFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                relocationFailureIEs->presenceMask |= RELOCATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&relocationFailureIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message relocationfailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RelocationFailure, relocationFailure_p);
    return decoded;
}

int ranap_decode_relocationcancelies(
    RANAP_RelocationCancelIEs_t *relocationCancelIEs,
    ANY_t *any_p) {

    RANAP_RelocationCancel_t *relocationCancel_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(relocationCancelIEs != NULL);

    memset(relocationCancelIEs, 0, sizeof(RANAP_RelocationCancelIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RelocationCancelIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RelocationCancel, (void**)&relocationCancel_p);

    if (tempDecoded < 0 || relocationCancel_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RelocationCancelIEs failed\n");
        return -1;
    }

    for (i = 0; i < relocationCancel_p->relocationCancel_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = relocationCancel_p->relocationCancel_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&relocationCancelIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message relocationcancelies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RelocationCancel, relocationCancel_p);
    return decoded;
}

int ranap_decode_relocationcancelacknowledgeies(
    RANAP_RelocationCancelAcknowledgeIEs_t *relocationCancelAcknowledgeIEs,
    ANY_t *any_p) {

    RANAP_RelocationCancelAcknowledge_t *relocationCancelAcknowledge_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(relocationCancelAcknowledgeIEs != NULL);

    memset(relocationCancelAcknowledgeIEs, 0, sizeof(RANAP_RelocationCancelAcknowledgeIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RelocationCancelAcknowledgeIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RelocationCancelAcknowledge, (void**)&relocationCancelAcknowledge_p);

    if (tempDecoded < 0 || relocationCancelAcknowledge_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RelocationCancelAcknowledgeIEs failed\n");
        return -1;
    }

    for (i = 0; i < relocationCancelAcknowledge_p->relocationCancelAcknowledge_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = relocationCancelAcknowledge_p->relocationCancelAcknowledge_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                relocationCancelAcknowledgeIEs->presenceMask |= RELOCATIONCANCELACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&relocationCancelAcknowledgeIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message relocationcancelacknowledgeies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RelocationCancelAcknowledge, relocationCancelAcknowledge_p);
    return decoded;
}

int ranap_decode_srns_contextrequesties(
    RANAP_SRNS_ContextRequestIEs_t *srnS_ContextRequestIEs,
    ANY_t *any_p) {

    RANAP_SRNS_ContextRequest_t *sRNS_ContextRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(srnS_ContextRequestIEs != NULL);

    memset(srnS_ContextRequestIEs, 0, sizeof(RANAP_SRNS_ContextRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_SRNS_ContextRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_SRNS_ContextRequest, (void**)&sRNS_ContextRequest_p);

    if (tempDecoded < 0 || sRNS_ContextRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_SRNS_ContextRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < sRNS_ContextRequest_p->srnS_ContextRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = sRNS_ContextRequest_p->srnS_ContextRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_DataForwardingList_SRNS_CtxReq:
            {
                RANAP_RAB_DataForwardingList_SRNS_CtxReq_t *ranaP_RABDataForwardingListSRNSCtxReq_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataForwardingList_SRNS_CtxReq, (void**)&ranaP_RABDataForwardingListSRNSCtxReq_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataForwardingList_SRNS_CtxReq failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataForwardingList_SRNS_CtxReq, ranaP_RABDataForwardingListSRNSCtxReq_p);
                memcpy(&srnS_ContextRequestIEs->raB_DataForwardingList_SRNS_CtxReq, ranaP_RABDataForwardingListSRNSCtxReq_p, sizeof(RANAP_RAB_DataForwardingList_SRNS_CtxReq_t));
                FREEMEM(ranaP_RABDataForwardingListSRNSCtxReq_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message srns_contextrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_SRNS_ContextRequest, sRNS_ContextRequest_p);
    return decoded;
}

int ranap_decode_rab_dataforwardingitem_srns_ctxreq_ies(
    RANAP_RAB_DataForwardingItem_SRNS_CtxReq_IEs_t *raB_DataForwardingItem_SRNS_CtxReq_IEs,
    ANY_t *any_p) {

    RANAP_RAB_DataForwardingList_SRNS_CtxReq_t *rAB_DataForwardingItem_SRNS_CtxReq_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_DataForwardingItem_SRNS_CtxReq_IEs != NULL);

    memset(raB_DataForwardingItem_SRNS_CtxReq_IEs, 0, sizeof(RANAP_RAB_DataForwardingItem_SRNS_CtxReq_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_DataForwardingItem_SRNS_CtxReq_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_DataForwardingList_SRNS_CtxReq, (void**)&rAB_DataForwardingItem_SRNS_CtxReq_p);

    if (tempDecoded < 0 || rAB_DataForwardingItem_SRNS_CtxReq_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_DataForwardingItem_SRNS_CtxReq_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_DataForwardingItem_SRNS_CtxReq_p->raB_DataForwardingList_SRNS_CtxReq_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_DataForwardingItem_SRNS_CtxReq_p->raB_DataForwardingList_SRNS_CtxReq_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_DataForwardingItem_SRNS_CtxReq:
            {
                RANAP_RAB_DataForwardingItem_SRNS_CtxReq_t *ranaP_RABDataForwardingItemSRNSCtxReq_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataForwardingItem_SRNS_CtxReq, (void**)&ranaP_RABDataForwardingItemSRNSCtxReq_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataForwardingItem_SRNS_CtxReq failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataForwardingItem_SRNS_CtxReq, ranaP_RABDataForwardingItemSRNSCtxReq_p);
                memcpy(&raB_DataForwardingItem_SRNS_CtxReq_IEs->raB_DataForwardingItem_SRNS_CtxReq, ranaP_RABDataForwardingItemSRNSCtxReq_p, sizeof(RANAP_RAB_DataForwardingItem_SRNS_CtxReq_t));
                FREEMEM(ranaP_RABDataForwardingItemSRNSCtxReq_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_dataforwardingitem_srns_ctxreq_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_DataForwardingList_SRNS_CtxReq, rAB_DataForwardingItem_SRNS_CtxReq_p);
    return decoded;
}

int ranap_decode_srns_contextresponseies(
    RANAP_SRNS_ContextResponseIEs_t *srnS_ContextResponseIEs,
    ANY_t *any_p) {

    RANAP_SRNS_ContextResponse_t *sRNS_ContextResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(srnS_ContextResponseIEs != NULL);

    memset(srnS_ContextResponseIEs, 0, sizeof(RANAP_SRNS_ContextResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_SRNS_ContextResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_SRNS_ContextResponse, (void**)&sRNS_ContextResponse_p);

    if (tempDecoded < 0 || sRNS_ContextResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_SRNS_ContextResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < sRNS_ContextResponse_p->srnS_ContextResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = sRNS_ContextResponse_p->srnS_ContextResponse_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_ContextList:
            {
                RANAP_RAB_ContextList_t *ranaP_RABContextList_p = NULL;
                srnS_ContextResponseIEs->presenceMask |= SRNS_CONTEXTRESPONSEIES_RANAP_RAB_CONTEXTLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ContextList, (void**)&ranaP_RABContextList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ContextList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ContextList, ranaP_RABContextList_p);
                memcpy(&srnS_ContextResponseIEs->raB_ContextList, ranaP_RABContextList_p, sizeof(RANAP_RAB_ContextList_t));
                FREEMEM(ranaP_RABContextList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_ContextFailedtoTransferList:
            {
                RANAP_RABs_ContextFailedtoTransferList_t *ranaP_RABsContextFailedtoTransferList_p = NULL;
                srnS_ContextResponseIEs->presenceMask |= SRNS_CONTEXTRESPONSEIES_RANAP_RABS_CONTEXTFAILEDTOTRANSFERLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RABs_ContextFailedtoTransferList, (void**)&ranaP_RABsContextFailedtoTransferList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ContextFailedtoTransferList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RABs_ContextFailedtoTransferList, ranaP_RABsContextFailedtoTransferList_p);
                memcpy(&srnS_ContextResponseIEs->raB_ContextFailedtoTransferList, ranaP_RABsContextFailedtoTransferList_p, sizeof(RANAP_RABs_ContextFailedtoTransferList_t));
                FREEMEM(ranaP_RABsContextFailedtoTransferList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                srnS_ContextResponseIEs->presenceMask |= SRNS_CONTEXTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&srnS_ContextResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message srns_contextresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_SRNS_ContextResponse, sRNS_ContextResponse_p);
    return decoded;
}

int ranap_decode_rab_contextitemies(
    RANAP_RAB_ContextItemIEs_t *raB_ContextItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_ContextList_t *rAB_ContextItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_ContextItemIEs != NULL);

    memset(raB_ContextItemIEs, 0, sizeof(RANAP_RAB_ContextItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_ContextItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_ContextList, (void**)&rAB_ContextItem_p);

    if (tempDecoded < 0 || rAB_ContextItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_ContextItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_ContextItem_p->raB_ContextList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_ContextItem_p->raB_ContextList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ContextItem:
            {
                RANAP_RAB_ContextItem_t *ranaP_RABContextItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ContextItem, (void**)&ranaP_RABContextItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ContextItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ContextItem, ranaP_RABContextItem_p);
                memcpy(&raB_ContextItemIEs->raB_ContextItem, ranaP_RABContextItem_p, sizeof(RANAP_RAB_ContextItem_t));
                FREEMEM(ranaP_RABContextItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_contextitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_ContextList, rAB_ContextItem_p);
    return decoded;
}

int ranap_decode_rabs_contextfailedtotransferitemies(
    RANAP_RABs_ContextFailedtoTransferItemIEs_t *raBs_ContextFailedtoTransferItemIEs,
    ANY_t *any_p) {

    RANAP_RABs_ContextFailedtoTransferList_t *rABs_ContextFailedtoTransferItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raBs_ContextFailedtoTransferItemIEs != NULL);

    memset(raBs_ContextFailedtoTransferItemIEs, 0, sizeof(RANAP_RABs_ContextFailedtoTransferItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RABs_ContextFailedtoTransferItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RABs_ContextFailedtoTransferList, (void**)&rABs_ContextFailedtoTransferItem_p);

    if (tempDecoded < 0 || rABs_ContextFailedtoTransferItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RABs_ContextFailedtoTransferItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rABs_ContextFailedtoTransferItem_p->raBs_ContextFailedtoTransferList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rABs_ContextFailedtoTransferItem_p->raBs_ContextFailedtoTransferList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ContextFailedtoTransferItem:
            {
                RANAP_RABs_ContextFailedtoTransferItem_t *ranaP_RABsContextFailedtoTransferItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RABs_ContextFailedtoTransferItem, (void**)&ranaP_RABsContextFailedtoTransferItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ContextFailedtoTransferItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RABs_ContextFailedtoTransferItem, ranaP_RABsContextFailedtoTransferItem_p);
                memcpy(&raBs_ContextFailedtoTransferItemIEs->raB_ContextFailedtoTransferItem, ranaP_RABsContextFailedtoTransferItem_p, sizeof(RANAP_RABs_ContextFailedtoTransferItem_t));
                FREEMEM(ranaP_RABsContextFailedtoTransferItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rabs_contextfailedtotransferitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RABs_ContextFailedtoTransferList, rABs_ContextFailedtoTransferItem_p);
    return decoded;
}

int ranap_decode_securitymodecommandies(
    RANAP_SecurityModeCommandIEs_t *securityModeCommandIEs,
    ANY_t *any_p) {

    RANAP_SecurityModeCommand_t *securityModeCommand_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(securityModeCommandIEs != NULL);

    memset(securityModeCommandIEs, 0, sizeof(RANAP_SecurityModeCommandIEs_t));
    RANAP_DEBUG("Decoding message RANAP_SecurityModeCommandIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_SecurityModeCommand, (void**)&securityModeCommand_p);

    if (tempDecoded < 0 || securityModeCommand_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_SecurityModeCommandIEs failed\n");
        return -1;
    }

    for (i = 0; i < securityModeCommand_p->securityModeCommand_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = securityModeCommand_p->securityModeCommand_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_IntegrityProtectionInformation:
            {
                RANAP_IntegrityProtectionInformation_t *ranaP_IntegrityProtectionInformation_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IntegrityProtectionInformation, (void**)&ranaP_IntegrityProtectionInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE integrityProtectionInformation failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IntegrityProtectionInformation, ranaP_IntegrityProtectionInformation_p);
                memcpy(&securityModeCommandIEs->integrityProtectionInformation, ranaP_IntegrityProtectionInformation_p, sizeof(RANAP_IntegrityProtectionInformation_t));
                FREEMEM(ranaP_IntegrityProtectionInformation_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_EncryptionInformation:
            {
                RANAP_EncryptionInformation_t *ranaP_EncryptionInformation_p = NULL;
                securityModeCommandIEs->presenceMask |= SECURITYMODECOMMANDIES_RANAP_ENCRYPTIONINFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_EncryptionInformation, (void**)&ranaP_EncryptionInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE encryptionInformation failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_EncryptionInformation, ranaP_EncryptionInformation_p);
                memcpy(&securityModeCommandIEs->encryptionInformation, ranaP_EncryptionInformation_p, sizeof(RANAP_EncryptionInformation_t));
                FREEMEM(ranaP_EncryptionInformation_p);
            } break;
            case RANAP_ProtocolIE_ID_id_KeyStatus:
            {
                RANAP_KeyStatus_t *ranaP_KeyStatus_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_KeyStatus, (void**)&ranaP_KeyStatus_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE keyStatus failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_KeyStatus, ranaP_KeyStatus_p);
                memcpy(&securityModeCommandIEs->keyStatus, ranaP_KeyStatus_p, sizeof(RANAP_KeyStatus_t));
                FREEMEM(ranaP_KeyStatus_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message securitymodecommandies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_SecurityModeCommand, securityModeCommand_p);
    return decoded;
}

int ranap_decode_securitymodecompleteies(
    RANAP_SecurityModeCompleteIEs_t *securityModeCompleteIEs,
    ANY_t *any_p) {

    RANAP_SecurityModeComplete_t *securityModeComplete_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(securityModeCompleteIEs != NULL);

    memset(securityModeCompleteIEs, 0, sizeof(RANAP_SecurityModeCompleteIEs_t));
    RANAP_DEBUG("Decoding message RANAP_SecurityModeCompleteIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_SecurityModeComplete, (void**)&securityModeComplete_p);

    if (tempDecoded < 0 || securityModeComplete_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_SecurityModeCompleteIEs failed\n");
        return -1;
    }

    for (i = 0; i < securityModeComplete_p->securityModeComplete_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = securityModeComplete_p->securityModeComplete_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_ChosenIntegrityProtectionAlgorithm:
            {
                RANAP_ChosenIntegrityProtectionAlgorithm_t *ranaP_ChosenIntegrityProtectionAlgorithm_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ChosenIntegrityProtectionAlgorithm, (void**)&ranaP_ChosenIntegrityProtectionAlgorithm_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE chosenIntegrityProtectionAlgorithm failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ChosenIntegrityProtectionAlgorithm, ranaP_ChosenIntegrityProtectionAlgorithm_p);
                memcpy(&securityModeCompleteIEs->chosenIntegrityProtectionAlgorithm, ranaP_ChosenIntegrityProtectionAlgorithm_p, sizeof(RANAP_ChosenIntegrityProtectionAlgorithm_t));
                FREEMEM(ranaP_ChosenIntegrityProtectionAlgorithm_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_ChosenEncryptionAlgorithm:
            {
                RANAP_ChosenEncryptionAlgorithm_t *ranaP_ChosenEncryptionAlgorithm_p = NULL;
                securityModeCompleteIEs->presenceMask |= SECURITYMODECOMPLETEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ChosenEncryptionAlgorithm, (void**)&ranaP_ChosenEncryptionAlgorithm_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE chosenEncryptionAlgorithm failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ChosenEncryptionAlgorithm, ranaP_ChosenEncryptionAlgorithm_p);
                memcpy(&securityModeCompleteIEs->chosenEncryptionAlgorithm, ranaP_ChosenEncryptionAlgorithm_p, sizeof(RANAP_ChosenEncryptionAlgorithm_t));
                FREEMEM(ranaP_ChosenEncryptionAlgorithm_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                securityModeCompleteIEs->presenceMask |= SECURITYMODECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&securityModeCompleteIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message securitymodecompleteies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_SecurityModeComplete, securityModeComplete_p);
    return decoded;
}

int ranap_decode_securitymoderejecties(
    RANAP_SecurityModeRejectIEs_t *securityModeRejectIEs,
    ANY_t *any_p) {

    RANAP_SecurityModeReject_t *securityModeReject_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(securityModeRejectIEs != NULL);

    memset(securityModeRejectIEs, 0, sizeof(RANAP_SecurityModeRejectIEs_t));
    RANAP_DEBUG("Decoding message RANAP_SecurityModeRejectIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_SecurityModeReject, (void**)&securityModeReject_p);

    if (tempDecoded < 0 || securityModeReject_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_SecurityModeRejectIEs failed\n");
        return -1;
    }

    for (i = 0; i < securityModeReject_p->securityModeReject_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = securityModeReject_p->securityModeReject_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&securityModeRejectIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                securityModeRejectIEs->presenceMask |= SECURITYMODEREJECTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&securityModeRejectIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message securitymoderejecties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_SecurityModeReject, securityModeReject_p);
    return decoded;
}

int ranap_decode_datavolumereportrequesties(
    RANAP_DataVolumeReportRequestIEs_t *dataVolumeReportRequestIEs,
    ANY_t *any_p) {

    RANAP_DataVolumeReportRequest_t *dataVolumeReportRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(dataVolumeReportRequestIEs != NULL);

    memset(dataVolumeReportRequestIEs, 0, sizeof(RANAP_DataVolumeReportRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_DataVolumeReportRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_DataVolumeReportRequest, (void**)&dataVolumeReportRequest_p);

    if (tempDecoded < 0 || dataVolumeReportRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_DataVolumeReportRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < dataVolumeReportRequest_p->dataVolumeReportRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = dataVolumeReportRequest_p->dataVolumeReportRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportRequestList:
            {
                RANAP_RAB_DataVolumeReportRequestList_t *ranaP_RABDataVolumeReportRequestList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataVolumeReportRequestList, (void**)&ranaP_RABDataVolumeReportRequestList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataVolumeReportRequestList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataVolumeReportRequestList, ranaP_RABDataVolumeReportRequestList_p);
                memcpy(&dataVolumeReportRequestIEs->raB_DataVolumeReportRequestList, ranaP_RABDataVolumeReportRequestList_p, sizeof(RANAP_RAB_DataVolumeReportRequestList_t));
                FREEMEM(ranaP_RABDataVolumeReportRequestList_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message datavolumereportrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_DataVolumeReportRequest, dataVolumeReportRequest_p);
    return decoded;
}

int ranap_decode_rab_datavolumereportrequestitemies(
    RANAP_RAB_DataVolumeReportRequestItemIEs_t *raB_DataVolumeReportRequestItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_DataVolumeReportRequestList_t *rAB_DataVolumeReportRequestItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_DataVolumeReportRequestItemIEs != NULL);

    memset(raB_DataVolumeReportRequestItemIEs, 0, sizeof(RANAP_RAB_DataVolumeReportRequestItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_DataVolumeReportRequestItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_DataVolumeReportRequestList, (void**)&rAB_DataVolumeReportRequestItem_p);

    if (tempDecoded < 0 || rAB_DataVolumeReportRequestItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_DataVolumeReportRequestItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_DataVolumeReportRequestItem_p->raB_DataVolumeReportRequestList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_DataVolumeReportRequestItem_p->raB_DataVolumeReportRequestList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportRequestItem:
            {
                RANAP_RAB_DataVolumeReportRequestItem_t *ranaP_RABDataVolumeReportRequestItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataVolumeReportRequestItem, (void**)&ranaP_RABDataVolumeReportRequestItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataVolumeReportRequestItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataVolumeReportRequestItem, ranaP_RABDataVolumeReportRequestItem_p);
                memcpy(&raB_DataVolumeReportRequestItemIEs->raB_DataVolumeReportRequestItem, ranaP_RABDataVolumeReportRequestItem_p, sizeof(RANAP_RAB_DataVolumeReportRequestItem_t));
                FREEMEM(ranaP_RABDataVolumeReportRequestItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_datavolumereportrequestitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_DataVolumeReportRequestList, rAB_DataVolumeReportRequestItem_p);
    return decoded;
}

int ranap_decode_datavolumereporties(
    RANAP_DataVolumeReportIEs_t *dataVolumeReportIEs,
    ANY_t *any_p) {

    RANAP_DataVolumeReport_t *dataVolumeReport_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(dataVolumeReportIEs != NULL);

    memset(dataVolumeReportIEs, 0, sizeof(RANAP_DataVolumeReportIEs_t));
    RANAP_DEBUG("Decoding message RANAP_DataVolumeReportIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_DataVolumeReport, (void**)&dataVolumeReport_p);

    if (tempDecoded < 0 || dataVolumeReport_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_DataVolumeReportIEs failed\n");
        return -1;
    }

    for (i = 0; i < dataVolumeReport_p->dataVolumeReport_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = dataVolumeReport_p->dataVolumeReport_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportList:
            {
                RANAP_RAB_DataVolumeReportList_t *ranaP_RABDataVolumeReportList_p = NULL;
                dataVolumeReportIEs->presenceMask |= DATAVOLUMEREPORTIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataVolumeReportList, (void**)&ranaP_RABDataVolumeReportList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataVolumeReportList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataVolumeReportList, ranaP_RABDataVolumeReportList_p);
                memcpy(&dataVolumeReportIEs->raB_DataVolumeReportList, ranaP_RABDataVolumeReportList_p, sizeof(RANAP_RAB_DataVolumeReportList_t));
                FREEMEM(ranaP_RABDataVolumeReportList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_FailedtoReportList:
            {
                RANAP_RABs_failed_to_reportList_t *ranaP_RABsfailedtoreportList_p = NULL;
                dataVolumeReportIEs->presenceMask |= DATAVOLUMEREPORTIES_RANAP_RABS_FAILED_TO_REPORTLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RABs_failed_to_reportList, (void**)&ranaP_RABsfailedtoreportList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_FailedtoReportList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RABs_failed_to_reportList, ranaP_RABsfailedtoreportList_p);
                memcpy(&dataVolumeReportIEs->raB_FailedtoReportList, ranaP_RABsfailedtoreportList_p, sizeof(RANAP_RABs_failed_to_reportList_t));
                FREEMEM(ranaP_RABsfailedtoreportList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                dataVolumeReportIEs->presenceMask |= DATAVOLUMEREPORTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&dataVolumeReportIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message datavolumereporties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_DataVolumeReport, dataVolumeReport_p);
    return decoded;
}

int ranap_decode_rabs_failed_to_reportitemies(
    RANAP_RABs_failed_to_reportItemIEs_t *raBs_failed_to_reportItemIEs,
    ANY_t *any_p) {

    RANAP_RABs_failed_to_reportList_t *rABs_failed_to_reportItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raBs_failed_to_reportItemIEs != NULL);

    memset(raBs_failed_to_reportItemIEs, 0, sizeof(RANAP_RABs_failed_to_reportItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RABs_failed_to_reportItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RABs_failed_to_reportList, (void**)&rABs_failed_to_reportItem_p);

    if (tempDecoded < 0 || rABs_failed_to_reportItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RABs_failed_to_reportItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rABs_failed_to_reportItem_p->raBs_failed_to_reportList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rABs_failed_to_reportItem_p->raBs_failed_to_reportList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_FailedtoReportItem:
            {
                RANAP_RABs_failed_to_reportItem_t *ranaP_RABsfailedtoreportItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RABs_failed_to_reportItem, (void**)&ranaP_RABsfailedtoreportItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_FailedtoReportItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RABs_failed_to_reportItem, ranaP_RABsfailedtoreportItem_p);
                memcpy(&raBs_failed_to_reportItemIEs->raB_FailedtoReportItem, ranaP_RABsfailedtoreportItem_p, sizeof(RANAP_RABs_failed_to_reportItem_t));
                FREEMEM(ranaP_RABsfailedtoreportItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rabs_failed_to_reportitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RABs_failed_to_reportList, rABs_failed_to_reportItem_p);
    return decoded;
}

int ranap_decode_reseties(
    RANAP_ResetIEs_t *resetIEs,
    ANY_t *any_p) {

    RANAP_Reset_t *reset_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(resetIEs != NULL);

    memset(resetIEs, 0, sizeof(RANAP_ResetIEs_t));
    RANAP_DEBUG("Decoding message RANAP_ResetIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_Reset, (void**)&reset_p);

    if (tempDecoded < 0 || reset_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_ResetIEs failed\n");
        return -1;
    }

    for (i = 0; i < reset_p->reset_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = reset_p->reset_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&resetIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&resetIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                resetIEs->presenceMask |= RESETIES_RANAP_GLOBALRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&resetIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message reseties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_Reset, reset_p);
    return decoded;
}

int ranap_decode_resetacknowledgeies(
    RANAP_ResetAcknowledgeIEs_t *resetAcknowledgeIEs,
    ANY_t *any_p) {

    RANAP_ResetAcknowledge_t *resetAcknowledge_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(resetAcknowledgeIEs != NULL);

    memset(resetAcknowledgeIEs, 0, sizeof(RANAP_ResetAcknowledgeIEs_t));
    RANAP_DEBUG("Decoding message RANAP_ResetAcknowledgeIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_ResetAcknowledge, (void**)&resetAcknowledge_p);

    if (tempDecoded < 0 || resetAcknowledge_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_ResetAcknowledgeIEs failed\n");
        return -1;
    }

    for (i = 0; i < resetAcknowledge_p->resetAcknowledge_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = resetAcknowledge_p->resetAcknowledge_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&resetAcknowledgeIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                resetAcknowledgeIEs->presenceMask |= RESETACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&resetAcknowledgeIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                resetAcknowledgeIEs->presenceMask |= RESETACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&resetAcknowledgeIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message resetacknowledgeies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_ResetAcknowledge, resetAcknowledge_p);
    return decoded;
}

int ranap_decode_resetresourceies(
    RANAP_ResetResourceIEs_t *resetResourceIEs,
    ANY_t *any_p) {

    RANAP_ResetResource_t *resetResource_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(resetResourceIEs != NULL);

    memset(resetResourceIEs, 0, sizeof(RANAP_ResetResourceIEs_t));
    RANAP_DEBUG("Decoding message RANAP_ResetResourceIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_ResetResource, (void**)&resetResource_p);

    if (tempDecoded < 0 || resetResource_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_ResetResourceIEs failed\n");
        return -1;
    }

    for (i = 0; i < resetResource_p->resetResource_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = resetResource_p->resetResource_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&resetResourceIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&resetResourceIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            case RANAP_ProtocolIE_ID_id_IuSigConIdList:
            {
                RANAP_ResetResourceList_t *ranaP_ResetResourceList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ResetResourceList, (void**)&ranaP_ResetResourceList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE iuSigConIdList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ResetResourceList, ranaP_ResetResourceList_p);
                memcpy(&resetResourceIEs->iuSigConIdList, ranaP_ResetResourceList_p, sizeof(RANAP_ResetResourceList_t));
                FREEMEM(ranaP_ResetResourceList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                resetResourceIEs->presenceMask |= RESETRESOURCEIES_RANAP_GLOBALRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&resetResourceIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message resetresourceies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_ResetResource, resetResource_p);
    return decoded;
}

int ranap_decode_resetresourceitemies(
    RANAP_ResetResourceItemIEs_t *resetResourceItemIEs,
    ANY_t *any_p) {

    RANAP_ResetResourceList_t *resetResourceItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(resetResourceItemIEs != NULL);

    memset(resetResourceItemIEs, 0, sizeof(RANAP_ResetResourceItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_ResetResourceItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_ResetResourceList, (void**)&resetResourceItem_p);

    if (tempDecoded < 0 || resetResourceItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_ResetResourceItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < resetResourceItem_p->resetResourceList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = resetResourceItem_p->resetResourceList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_IuSigConIdItem:
            {
                RANAP_ResetResourceItem_t *ranaP_ResetResourceItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ResetResourceItem, (void**)&ranaP_ResetResourceItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE iuSigConIdItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ResetResourceItem, ranaP_ResetResourceItem_p);
                memcpy(&resetResourceItemIEs->iuSigConIdItem, ranaP_ResetResourceItem_p, sizeof(RANAP_ResetResourceItem_t));
                FREEMEM(ranaP_ResetResourceItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message resetresourceitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_ResetResourceList, resetResourceItem_p);
    return decoded;
}

int ranap_decode_resetresourceacknowledgeies(
    RANAP_ResetResourceAcknowledgeIEs_t *resetResourceAcknowledgeIEs,
    ANY_t *any_p) {

    RANAP_ResetResourceAcknowledge_t *resetResourceAcknowledge_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(resetResourceAcknowledgeIEs != NULL);

    memset(resetResourceAcknowledgeIEs, 0, sizeof(RANAP_ResetResourceAcknowledgeIEs_t));
    RANAP_DEBUG("Decoding message RANAP_ResetResourceAcknowledgeIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_ResetResourceAcknowledge, (void**)&resetResourceAcknowledge_p);

    if (tempDecoded < 0 || resetResourceAcknowledge_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_ResetResourceAcknowledgeIEs failed\n");
        return -1;
    }

    for (i = 0; i < resetResourceAcknowledge_p->resetResourceAcknowledge_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = resetResourceAcknowledge_p->resetResourceAcknowledge_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&resetResourceAcknowledgeIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            case RANAP_ProtocolIE_ID_id_IuSigConIdList:
            {
                RANAP_ResetResourceAckList_t *ranaP_ResetResourceAckList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ResetResourceAckList, (void**)&ranaP_ResetResourceAckList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE iuSigConIdList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ResetResourceAckList, ranaP_ResetResourceAckList_p);
                memcpy(&resetResourceAcknowledgeIEs->iuSigConIdList, ranaP_ResetResourceAckList_p, sizeof(RANAP_ResetResourceAckList_t));
                FREEMEM(ranaP_ResetResourceAckList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                resetResourceAcknowledgeIEs->presenceMask |= RESETRESOURCEACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&resetResourceAcknowledgeIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                resetResourceAcknowledgeIEs->presenceMask |= RESETRESOURCEACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&resetResourceAcknowledgeIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message resetresourceacknowledgeies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_ResetResourceAcknowledge, resetResourceAcknowledge_p);
    return decoded;
}

int ranap_decode_resetresourceackitemies(
    RANAP_ResetResourceAckItemIEs_t *resetResourceAckItemIEs,
    ANY_t *any_p) {

    RANAP_ResetResourceAckList_t *resetResourceAckItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(resetResourceAckItemIEs != NULL);

    memset(resetResourceAckItemIEs, 0, sizeof(RANAP_ResetResourceAckItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_ResetResourceAckItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_ResetResourceAckList, (void**)&resetResourceAckItem_p);

    if (tempDecoded < 0 || resetResourceAckItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_ResetResourceAckItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < resetResourceAckItem_p->resetResourceAckList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = resetResourceAckItem_p->resetResourceAckList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_IuSigConIdItem:
            {
                RANAP_ResetResourceAckItem_t *ranaP_ResetResourceAckItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ResetResourceAckItem, (void**)&ranaP_ResetResourceAckItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE iuSigConIdItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ResetResourceAckItem, ranaP_ResetResourceAckItem_p);
                memcpy(&resetResourceAckItemIEs->iuSigConIdItem, ranaP_ResetResourceAckItem_p, sizeof(RANAP_ResetResourceAckItem_t));
                FREEMEM(ranaP_ResetResourceAckItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message resetresourceackitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_ResetResourceAckList, resetResourceAckItem_p);
    return decoded;
}

int ranap_decode_rab_releaserequesties(
    RANAP_RAB_ReleaseRequestIEs_t *raB_ReleaseRequestIEs,
    ANY_t *any_p) {

    RANAP_RAB_ReleaseRequest_t *rAB_ReleaseRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_ReleaseRequestIEs != NULL);

    memset(raB_ReleaseRequestIEs, 0, sizeof(RANAP_RAB_ReleaseRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_ReleaseRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_ReleaseRequest, (void**)&rAB_ReleaseRequest_p);

    if (tempDecoded < 0 || rAB_ReleaseRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_ReleaseRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_ReleaseRequest_p->raB_ReleaseRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_ReleaseRequest_p->raB_ReleaseRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ReleaseList:
            {
                RANAP_RAB_ReleaseList_t *ranaP_RABReleaseList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ReleaseList, (void**)&ranaP_RABReleaseList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ReleaseList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ReleaseList, ranaP_RABReleaseList_p);
                memcpy(&raB_ReleaseRequestIEs->raB_ReleaseList, ranaP_RABReleaseList_p, sizeof(RANAP_RAB_ReleaseList_t));
                FREEMEM(ranaP_RABReleaseList_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_releaserequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_ReleaseRequest, rAB_ReleaseRequest_p);
    return decoded;
}

int ranap_decode_rab_releaseitemies(
    RANAP_RAB_ReleaseItemIEs_t *raB_ReleaseItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_ReleaseList_t *rAB_ReleaseItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_ReleaseItemIEs != NULL);

    memset(raB_ReleaseItemIEs, 0, sizeof(RANAP_RAB_ReleaseItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_ReleaseItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_ReleaseList, (void**)&rAB_ReleaseItem_p);

    if (tempDecoded < 0 || rAB_ReleaseItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_ReleaseItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_ReleaseItem_p->raB_ReleaseList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_ReleaseItem_p->raB_ReleaseList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ReleaseItem:
            {
                RANAP_RAB_ReleaseItem_t *ranaP_RABReleaseItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ReleaseItem, (void**)&ranaP_RABReleaseItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ReleaseItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ReleaseItem, ranaP_RABReleaseItem_p);
                memcpy(&raB_ReleaseItemIEs->raB_ReleaseItem, ranaP_RABReleaseItem_p, sizeof(RANAP_RAB_ReleaseItem_t));
                FREEMEM(ranaP_RABReleaseItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_releaseitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_ReleaseList, rAB_ReleaseItem_p);
    return decoded;
}

int ranap_decode_iu_releaserequesties(
    RANAP_Iu_ReleaseRequestIEs_t *iu_ReleaseRequestIEs,
    ANY_t *any_p) {

    RANAP_Iu_ReleaseRequest_t *iu_ReleaseRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(iu_ReleaseRequestIEs != NULL);

    memset(iu_ReleaseRequestIEs, 0, sizeof(RANAP_Iu_ReleaseRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_Iu_ReleaseRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_Iu_ReleaseRequest, (void**)&iu_ReleaseRequest_p);

    if (tempDecoded < 0 || iu_ReleaseRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_Iu_ReleaseRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < iu_ReleaseRequest_p->iu_ReleaseRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = iu_ReleaseRequest_p->iu_ReleaseRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&iu_ReleaseRequestIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message iu_releaserequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_Iu_ReleaseRequest, iu_ReleaseRequest_p);
    return decoded;
}

int ranap_decode_enhancedrelocationcompleterequesties(
    RANAP_EnhancedRelocationCompleteRequestIEs_t *enhancedRelocationCompleteRequestIEs,
    ANY_t *any_p) {

    RANAP_EnhancedRelocationCompleteRequest_t *enhancedRelocationCompleteRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(enhancedRelocationCompleteRequestIEs != NULL);

    memset(enhancedRelocationCompleteRequestIEs, 0, sizeof(RANAP_EnhancedRelocationCompleteRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_EnhancedRelocationCompleteRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_EnhancedRelocationCompleteRequest, (void**)&enhancedRelocationCompleteRequest_p);

    if (tempDecoded < 0 || enhancedRelocationCompleteRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_EnhancedRelocationCompleteRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < enhancedRelocationCompleteRequest_p->enhancedRelocationCompleteRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = enhancedRelocationCompleteRequest_p->enhancedRelocationCompleteRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_OldIuSigConId:
            {
                RANAP_IuSignallingConnectionIdentifier_t *ranaP_IuSignallingConnectionIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, (void**)&ranaP_IuSignallingConnectionIdentifier_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE oldIuSigConId failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, ranaP_IuSignallingConnectionIdentifier_p);
                memcpy(&enhancedRelocationCompleteRequestIEs->oldIuSigConId, ranaP_IuSignallingConnectionIdentifier_p, sizeof(RANAP_IuSignallingConnectionIdentifier_t));
                FREEMEM(ranaP_IuSignallingConnectionIdentifier_p);
            } break;
            case RANAP_ProtocolIE_ID_id_IuSigConId:
            {
                RANAP_IuSignallingConnectionIdentifier_t *ranaP_IuSignallingConnectionIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, (void**)&ranaP_IuSignallingConnectionIdentifier_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE iuSigConId failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, ranaP_IuSignallingConnectionIdentifier_p);
                memcpy(&enhancedRelocationCompleteRequestIEs->iuSigConId, ranaP_IuSignallingConnectionIdentifier_p, sizeof(RANAP_IuSignallingConnectionIdentifier_t));
                FREEMEM(ranaP_IuSignallingConnectionIdentifier_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Relocation_SourceRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE relocation_SourceRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&enhancedRelocationCompleteRequestIEs->relocation_SourceRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Relocation_SourceExtendedRNC_ID:
            {
                RANAP_ExtendedRNC_ID_t *ranaP_ExtendedRNCID_p = NULL;
                enhancedRelocationCompleteRequestIEs->presenceMask |= ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_SOURCEEXTENDEDRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ExtendedRNC_ID, (void**)&ranaP_ExtendedRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE relocation_SourceExtendedRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ExtendedRNC_ID, ranaP_ExtendedRNCID_p);
                memcpy(&enhancedRelocationCompleteRequestIEs->relocation_SourceExtendedRNC_ID, ranaP_ExtendedRNCID_p, sizeof(RANAP_ExtendedRNC_ID_t));
                FREEMEM(ranaP_ExtendedRNCID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Relocation_TargetRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE relocation_TargetRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&enhancedRelocationCompleteRequestIEs->relocation_TargetRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Relocation_TargetExtendedRNC_ID:
            {
                RANAP_ExtendedRNC_ID_t *ranaP_ExtendedRNCID_p = NULL;
                enhancedRelocationCompleteRequestIEs->presenceMask |= ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_TARGETEXTENDEDRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ExtendedRNC_ID, (void**)&ranaP_ExtendedRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE relocation_TargetExtendedRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ExtendedRNC_ID, ranaP_ExtendedRNCID_p);
                memcpy(&enhancedRelocationCompleteRequestIEs->relocation_TargetExtendedRNC_ID, ranaP_ExtendedRNCID_p, sizeof(RANAP_ExtendedRNC_ID_t));
                FREEMEM(ranaP_ExtendedRNCID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_SetupList_EnhancedRelocCompleteReq:
            {
                RANAP_RAB_SetupList_EnhancedRelocCompleteReq_t *ranaP_RABSetupListEnhancedRelocCompleteReq_p = NULL;
                enhancedRelocationCompleteRequestIEs->presenceMask |= ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETEREQ_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteReq, (void**)&ranaP_RABSetupListEnhancedRelocCompleteReq_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupList_EnhancedRelocCompleteReq failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteReq, ranaP_RABSetupListEnhancedRelocCompleteReq_p);
                memcpy(&enhancedRelocationCompleteRequestIEs->raB_SetupList_EnhancedRelocCompleteReq, ranaP_RABSetupListEnhancedRelocCompleteReq_p, sizeof(RANAP_RAB_SetupList_EnhancedRelocCompleteReq_t));
                FREEMEM(ranaP_RABSetupListEnhancedRelocCompleteReq_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message enhancedrelocationcompleterequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_EnhancedRelocationCompleteRequest, enhancedRelocationCompleteRequest_p);
    return decoded;
}

int ranap_decode_rab_setupitem_enhancedreloccompletereq_ies(
    RANAP_RAB_SetupItem_EnhancedRelocCompleteReq_IEs_t *raB_SetupItem_EnhancedRelocCompleteReq_IEs,
    ANY_t *any_p) {

    RANAP_RAB_SetupList_EnhancedRelocCompleteReq_t *rAB_SetupItem_EnhancedRelocCompleteReq_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_SetupItem_EnhancedRelocCompleteReq_IEs != NULL);

    memset(raB_SetupItem_EnhancedRelocCompleteReq_IEs, 0, sizeof(RANAP_RAB_SetupItem_EnhancedRelocCompleteReq_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_SetupItem_EnhancedRelocCompleteReq_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteReq, (void**)&rAB_SetupItem_EnhancedRelocCompleteReq_p);

    if (tempDecoded < 0 || rAB_SetupItem_EnhancedRelocCompleteReq_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_SetupItem_EnhancedRelocCompleteReq_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_SetupItem_EnhancedRelocCompleteReq_p->raB_SetupList_EnhancedRelocCompleteReq_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_SetupItem_EnhancedRelocCompleteReq_p->raB_SetupList_EnhancedRelocCompleteReq_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_SetupItem_EnhancedRelocCompleteReq:
            {
                RANAP_RAB_SetupItem_EnhancedRelocCompleteReq_t *ranaP_RABSetupItemEnhancedRelocCompleteReq_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupItem_EnhancedRelocCompleteReq, (void**)&ranaP_RABSetupItemEnhancedRelocCompleteReq_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupItem_EnhancedRelocCompleteReq failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupItem_EnhancedRelocCompleteReq, ranaP_RABSetupItemEnhancedRelocCompleteReq_p);
                memcpy(&raB_SetupItem_EnhancedRelocCompleteReq_IEs->raB_SetupItem_EnhancedRelocCompleteReq, ranaP_RABSetupItemEnhancedRelocCompleteReq_p, sizeof(RANAP_RAB_SetupItem_EnhancedRelocCompleteReq_t));
                FREEMEM(ranaP_RABSetupItemEnhancedRelocCompleteReq_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_setupitem_enhancedreloccompletereq_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteReq, rAB_SetupItem_EnhancedRelocCompleteReq_p);
    return decoded;
}

int ranap_decode_enhancedrelocationcompleteresponseies(
    RANAP_EnhancedRelocationCompleteResponseIEs_t *enhancedRelocationCompleteResponseIEs,
    ANY_t *any_p) {

    RANAP_EnhancedRelocationCompleteResponse_t *enhancedRelocationCompleteResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(enhancedRelocationCompleteResponseIEs != NULL);

    memset(enhancedRelocationCompleteResponseIEs, 0, sizeof(RANAP_EnhancedRelocationCompleteResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_EnhancedRelocationCompleteResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_EnhancedRelocationCompleteResponse, (void**)&enhancedRelocationCompleteResponse_p);

    if (tempDecoded < 0 || enhancedRelocationCompleteResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_EnhancedRelocationCompleteResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < enhancedRelocationCompleteResponse_p->enhancedRelocationCompleteResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = enhancedRelocationCompleteResponse_p->enhancedRelocationCompleteResponse_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_SetupList_EnhancedRelocCompleteRes:
            {
                RANAP_RAB_SetupList_EnhancedRelocCompleteRes_t *ranaP_RABSetupListEnhancedRelocCompleteRes_p = NULL;
                enhancedRelocationCompleteResponseIEs->presenceMask |= ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETERES_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteRes, (void**)&ranaP_RABSetupListEnhancedRelocCompleteRes_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupList_EnhancedRelocCompleteRes failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteRes, ranaP_RABSetupListEnhancedRelocCompleteRes_p);
                memcpy(&enhancedRelocationCompleteResponseIEs->raB_SetupList_EnhancedRelocCompleteRes, ranaP_RABSetupListEnhancedRelocCompleteRes_p, sizeof(RANAP_RAB_SetupList_EnhancedRelocCompleteRes_t));
                FREEMEM(ranaP_RABSetupListEnhancedRelocCompleteRes_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_ToBeReleasedList_EnhancedRelocCompleteRes:
            {
                RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes_t *ranaP_RABToBeReleasedListEnhancedRelocCompleteRes_p = NULL;
                enhancedRelocationCompleteResponseIEs->presenceMask |= ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_TOBERELEASEDLIST_ENHANCEDRELOCCOMPLETERES_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes, (void**)&ranaP_RABToBeReleasedListEnhancedRelocCompleteRes_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ToBeReleasedList_EnhancedRelocCompleteRes failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes, ranaP_RABToBeReleasedListEnhancedRelocCompleteRes_p);
                memcpy(&enhancedRelocationCompleteResponseIEs->raB_ToBeReleasedList_EnhancedRelocCompleteRes, ranaP_RABToBeReleasedListEnhancedRelocCompleteRes_p, sizeof(RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes_t));
                FREEMEM(ranaP_RABToBeReleasedListEnhancedRelocCompleteRes_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                enhancedRelocationCompleteResponseIEs->presenceMask |= ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&enhancedRelocationCompleteResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message enhancedrelocationcompleteresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_EnhancedRelocationCompleteResponse, enhancedRelocationCompleteResponse_p);
    return decoded;
}

int ranap_decode_rab_setupitem_enhancedreloccompleteres_ies(
    RANAP_RAB_SetupItem_EnhancedRelocCompleteRes_IEs_t *raB_SetupItem_EnhancedRelocCompleteRes_IEs,
    ANY_t *any_p) {

    RANAP_RAB_SetupList_EnhancedRelocCompleteRes_t *rAB_SetupItem_EnhancedRelocCompleteRes_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_SetupItem_EnhancedRelocCompleteRes_IEs != NULL);

    memset(raB_SetupItem_EnhancedRelocCompleteRes_IEs, 0, sizeof(RANAP_RAB_SetupItem_EnhancedRelocCompleteRes_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_SetupItem_EnhancedRelocCompleteRes_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteRes, (void**)&rAB_SetupItem_EnhancedRelocCompleteRes_p);

    if (tempDecoded < 0 || rAB_SetupItem_EnhancedRelocCompleteRes_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_SetupItem_EnhancedRelocCompleteRes_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_SetupItem_EnhancedRelocCompleteRes_p->raB_SetupList_EnhancedRelocCompleteRes_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_SetupItem_EnhancedRelocCompleteRes_p->raB_SetupList_EnhancedRelocCompleteRes_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_SetupItem_EnhancedRelocCompleteRes:
            {
                RANAP_RAB_SetupItem_EnhancedRelocCompleteRes_t *ranaP_RABSetupItemEnhancedRelocCompleteRes_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupItem_EnhancedRelocCompleteRes, (void**)&ranaP_RABSetupItemEnhancedRelocCompleteRes_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupItem_EnhancedRelocCompleteRes failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupItem_EnhancedRelocCompleteRes, ranaP_RABSetupItemEnhancedRelocCompleteRes_p);
                memcpy(&raB_SetupItem_EnhancedRelocCompleteRes_IEs->raB_SetupItem_EnhancedRelocCompleteRes, ranaP_RABSetupItemEnhancedRelocCompleteRes_p, sizeof(RANAP_RAB_SetupItem_EnhancedRelocCompleteRes_t));
                FREEMEM(ranaP_RABSetupItemEnhancedRelocCompleteRes_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_setupitem_enhancedreloccompleteres_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteRes, rAB_SetupItem_EnhancedRelocCompleteRes_p);
    return decoded;
}

int ranap_decode_rab_tobereleaseditem_enhancedreloccompleteres_ies(
    RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs_t *raB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs,
    ANY_t *any_p) {

    RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes_t *rAB_ToBeReleasedItem_EnhancedRelocCompleteRes_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs != NULL);

    memset(raB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs, 0, sizeof(RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes, (void**)&rAB_ToBeReleasedItem_EnhancedRelocCompleteRes_p);

    if (tempDecoded < 0 || rAB_ToBeReleasedItem_EnhancedRelocCompleteRes_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_ToBeReleasedItem_EnhancedRelocCompleteRes_p->raB_ToBeReleasedList_EnhancedRelocCompleteRes_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_ToBeReleasedItem_EnhancedRelocCompleteRes_p->raB_ToBeReleasedList_EnhancedRelocCompleteRes_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes:
            {
                RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes_t *ranaP_RABToBeReleasedItemEnhancedRelocCompleteRes_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes, (void**)&ranaP_RABToBeReleasedItemEnhancedRelocCompleteRes_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ToBeReleasedItem_EnhancedRelocCompleteRes failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes, ranaP_RABToBeReleasedItemEnhancedRelocCompleteRes_p);
                memcpy(&raB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs->raB_ToBeReleasedItem_EnhancedRelocCompleteRes, ranaP_RABToBeReleasedItemEnhancedRelocCompleteRes_p, sizeof(RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes_t));
                FREEMEM(ranaP_RABToBeReleasedItemEnhancedRelocCompleteRes_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_tobereleaseditem_enhancedreloccompleteres_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes, rAB_ToBeReleasedItem_EnhancedRelocCompleteRes_p);
    return decoded;
}

int ranap_decode_enhancedrelocationcompletefailureies(
    RANAP_EnhancedRelocationCompleteFailureIEs_t *enhancedRelocationCompleteFailureIEs,
    ANY_t *any_p) {

    RANAP_EnhancedRelocationCompleteFailure_t *enhancedRelocationCompleteFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(enhancedRelocationCompleteFailureIEs != NULL);

    memset(enhancedRelocationCompleteFailureIEs, 0, sizeof(RANAP_EnhancedRelocationCompleteFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_EnhancedRelocationCompleteFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_EnhancedRelocationCompleteFailure, (void**)&enhancedRelocationCompleteFailure_p);

    if (tempDecoded < 0 || enhancedRelocationCompleteFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_EnhancedRelocationCompleteFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < enhancedRelocationCompleteFailure_p->enhancedRelocationCompleteFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = enhancedRelocationCompleteFailure_p->enhancedRelocationCompleteFailure_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&enhancedRelocationCompleteFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                enhancedRelocationCompleteFailureIEs->presenceMask |= ENHANCEDRELOCATIONCOMPLETEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&enhancedRelocationCompleteFailureIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message enhancedrelocationcompletefailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_EnhancedRelocationCompleteFailure, enhancedRelocationCompleteFailure_p);
    return decoded;
}

int ranap_decode_enhancedrelocationcompleteconfirmies(
    RANAP_EnhancedRelocationCompleteConfirmIEs_t *enhancedRelocationCompleteConfirmIEs,
    ANY_t *any_p) {

    RANAP_EnhancedRelocationCompleteConfirm_t *enhancedRelocationCompleteConfirm_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(enhancedRelocationCompleteConfirmIEs != NULL);

    memset(enhancedRelocationCompleteConfirmIEs, 0, sizeof(RANAP_EnhancedRelocationCompleteConfirmIEs_t));
    RANAP_DEBUG("Decoding message RANAP_EnhancedRelocationCompleteConfirmIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_EnhancedRelocationCompleteConfirm, (void**)&enhancedRelocationCompleteConfirm_p);

    if (tempDecoded < 0 || enhancedRelocationCompleteConfirm_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_EnhancedRelocationCompleteConfirmIEs failed\n");
        return -1;
    }

    for (i = 0; i < enhancedRelocationCompleteConfirm_p->enhancedRelocationCompleteConfirm_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = enhancedRelocationCompleteConfirm_p->enhancedRelocationCompleteConfirm_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_FailedList:
            {
                RANAP_RAB_FailedList_t *ranaP_RABFailedList_p = NULL;
                enhancedRelocationCompleteConfirmIEs->presenceMask |= ENHANCEDRELOCATIONCOMPLETECONFIRMIES_RANAP_RAB_FAILEDLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_FailedList, (void**)&ranaP_RABFailedList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_FailedList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_FailedList, ranaP_RABFailedList_p);
                memcpy(&enhancedRelocationCompleteConfirmIEs->raB_FailedList, ranaP_RABFailedList_p, sizeof(RANAP_RAB_FailedList_t));
                FREEMEM(ranaP_RABFailedList_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message enhancedrelocationcompleteconfirmies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_EnhancedRelocationCompleteConfirm, enhancedRelocationCompleteConfirm_p);
    return decoded;
}

int ranap_decode_pagingies(
    RANAP_PagingIEs_t *pagingIEs,
    ANY_t *any_p) {

    RANAP_Paging_t *paging_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(pagingIEs != NULL);

    memset(pagingIEs, 0, sizeof(RANAP_PagingIEs_t));
    RANAP_DEBUG("Decoding message RANAP_PagingIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_Paging, (void**)&paging_p);

    if (tempDecoded < 0 || paging_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_PagingIEs failed\n");
        return -1;
    }

    for (i = 0; i < paging_p->paging_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = paging_p->paging_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&pagingIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            case RANAP_ProtocolIE_ID_id_PermanentNAS_UE_ID:
            {
                RANAP_PermanentNAS_UE_ID_t *ranaP_PermanentNASUEID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_PermanentNAS_UE_ID, (void**)&ranaP_PermanentNASUEID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE permanentNAS_UE_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_PermanentNAS_UE_ID, ranaP_PermanentNASUEID_p);
                memcpy(&pagingIEs->permanentNAS_UE_ID, ranaP_PermanentNASUEID_p, sizeof(RANAP_PermanentNAS_UE_ID_t));
                FREEMEM(ranaP_PermanentNASUEID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_TemporaryUE_ID:
            {
                RANAP_TemporaryUE_ID_t *ranaP_TemporaryUEID_p = NULL;
                pagingIEs->presenceMask |= PAGINGIES_RANAP_TEMPORARYUE_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TemporaryUE_ID, (void**)&ranaP_TemporaryUEID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE temporaryUE_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TemporaryUE_ID, ranaP_TemporaryUEID_p);
                memcpy(&pagingIEs->temporaryUE_ID, ranaP_TemporaryUEID_p, sizeof(RANAP_TemporaryUE_ID_t));
                FREEMEM(ranaP_TemporaryUEID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_PagingAreaID:
            {
                RANAP_PagingAreaID_t *ranaP_PagingAreaID_p = NULL;
                pagingIEs->presenceMask |= PAGINGIES_RANAP_PAGINGAREAID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_PagingAreaID, (void**)&ranaP_PagingAreaID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE pagingAreaID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_PagingAreaID, ranaP_PagingAreaID_p);
                memcpy(&pagingIEs->pagingAreaID, ranaP_PagingAreaID_p, sizeof(RANAP_PagingAreaID_t));
                FREEMEM(ranaP_PagingAreaID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_PagingCause:
            {
                RANAP_PagingCause_t *ranaP_PagingCause_p = NULL;
                pagingIEs->presenceMask |= PAGINGIES_RANAP_PAGINGCAUSE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_PagingCause, (void**)&ranaP_PagingCause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE pagingCause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_PagingCause, ranaP_PagingCause_p);
                memcpy(&pagingIEs->pagingCause, ranaP_PagingCause_p, sizeof(RANAP_PagingCause_t));
                FREEMEM(ranaP_PagingCause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_NonSearchingIndication:
            {
                RANAP_NonSearchingIndication_t *ranaP_NonSearchingIndication_p = NULL;
                pagingIEs->presenceMask |= PAGINGIES_RANAP_NONSEARCHINGINDICATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_NonSearchingIndication, (void**)&ranaP_NonSearchingIndication_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE nonSearchingIndication failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_NonSearchingIndication, ranaP_NonSearchingIndication_p);
                memcpy(&pagingIEs->nonSearchingIndication, ranaP_NonSearchingIndication_p, sizeof(RANAP_NonSearchingIndication_t));
                FREEMEM(ranaP_NonSearchingIndication_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_DRX_CycleLengthCoefficient:
            {
                RANAP_DRX_CycleLengthCoefficient_t *ranaP_DRXCycleLengthCoefficient_p = NULL;
                pagingIEs->presenceMask |= PAGINGIES_RANAP_DRX_CYCLELENGTHCOEFFICIENT_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_DRX_CycleLengthCoefficient, (void**)&ranaP_DRXCycleLengthCoefficient_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE drX_CycleLengthCoefficient failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_DRX_CycleLengthCoefficient, ranaP_DRXCycleLengthCoefficient_p);
                memcpy(&pagingIEs->drX_CycleLengthCoefficient, ranaP_DRXCycleLengthCoefficient_p, sizeof(RANAP_DRX_CycleLengthCoefficient_t));
                FREEMEM(ranaP_DRXCycleLengthCoefficient_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message pagingies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_Paging, paging_p);
    return decoded;
}

int ranap_decode_commonid_ies(
    RANAP_CommonID_IEs_t *commonID_IEs,
    ANY_t *any_p) {

    RANAP_CommonID_t *commonID_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(commonID_IEs != NULL);

    memset(commonID_IEs, 0, sizeof(RANAP_CommonID_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_CommonID_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_CommonID, (void**)&commonID_p);

    if (tempDecoded < 0 || commonID_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_CommonID_IEs failed\n");
        return -1;
    }

    for (i = 0; i < commonID_p->commonID_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = commonID_p->commonID_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_PermanentNAS_UE_ID:
            {
                RANAP_PermanentNAS_UE_ID_t *ranaP_PermanentNASUEID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_PermanentNAS_UE_ID, (void**)&ranaP_PermanentNASUEID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE permanentNAS_UE_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_PermanentNAS_UE_ID, ranaP_PermanentNASUEID_p);
                memcpy(&commonID_IEs->permanentNAS_UE_ID, ranaP_PermanentNASUEID_p, sizeof(RANAP_PermanentNAS_UE_ID_t));
                FREEMEM(ranaP_PermanentNASUEID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message commonid_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_CommonID, commonID_p);
    return decoded;
}

int ranap_decode_cn_invoketraceies(
    RANAP_CN_InvokeTraceIEs_t *cN_InvokeTraceIEs,
    ANY_t *any_p) {

    RANAP_CN_InvokeTrace_t *cN_InvokeTrace_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(cN_InvokeTraceIEs != NULL);

    memset(cN_InvokeTraceIEs, 0, sizeof(RANAP_CN_InvokeTraceIEs_t));
    RANAP_DEBUG("Decoding message RANAP_CN_InvokeTraceIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_CN_InvokeTrace, (void**)&cN_InvokeTrace_p);

    if (tempDecoded < 0 || cN_InvokeTrace_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_CN_InvokeTraceIEs failed\n");
        return -1;
    }

    for (i = 0; i < cN_InvokeTrace_p->cN_InvokeTrace_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = cN_InvokeTrace_p->cN_InvokeTrace_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_TraceType:
            {
                RANAP_TraceType_t *ranaP_TraceType_p = NULL;
                cN_InvokeTraceIEs->presenceMask |= CN_INVOKETRACEIES_RANAP_TRACETYPE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TraceType, (void**)&ranaP_TraceType_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE traceType failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TraceType, ranaP_TraceType_p);
                memcpy(&cN_InvokeTraceIEs->traceType, ranaP_TraceType_p, sizeof(RANAP_TraceType_t));
                FREEMEM(ranaP_TraceType_p);
            } break;
            case RANAP_ProtocolIE_ID_id_TraceReference:
            {
                RANAP_TraceReference_t *ranaP_TraceReference_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TraceReference, (void**)&ranaP_TraceReference_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE traceReference failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TraceReference, ranaP_TraceReference_p);
                memcpy(&cN_InvokeTraceIEs->traceReference, ranaP_TraceReference_p, sizeof(RANAP_TraceReference_t));
                FREEMEM(ranaP_TraceReference_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_TriggerID:
            {
                RANAP_TriggerID_t *ranaP_TriggerID_p = NULL;
                cN_InvokeTraceIEs->presenceMask |= CN_INVOKETRACEIES_RANAP_TRIGGERID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TriggerID, (void**)&ranaP_TriggerID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE triggerID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TriggerID, ranaP_TriggerID_p);
                memcpy(&cN_InvokeTraceIEs->triggerID, ranaP_TriggerID_p, sizeof(RANAP_TriggerID_t));
                FREEMEM(ranaP_TriggerID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_UE_ID:
            {
                RANAP_UE_ID_t *ranap_ueid_p = NULL;
                cN_InvokeTraceIEs->presenceMask |= CN_INVOKETRACEIES_RANAP_UE_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_UE_ID, (void**)&ranap_ueid_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE ue_id failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_UE_ID, ranap_ueid_p);
                memcpy(&cN_InvokeTraceIEs->ue_id, ranap_ueid_p, sizeof(RANAP_UE_ID_t));
                FREEMEM(ranap_ueid_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_OMC_ID:
            {
                RANAP_OMC_ID_t *ranap_omcid_p = NULL;
                cN_InvokeTraceIEs->presenceMask |= CN_INVOKETRACEIES_RANAP_OMC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_OMC_ID, (void**)&ranap_omcid_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE omc_id failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_OMC_ID, ranap_omcid_p);
                memcpy(&cN_InvokeTraceIEs->omc_id, ranap_omcid_p, sizeof(RANAP_OMC_ID_t));
                FREEMEM(ranap_omcid_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message cn_invoketraceies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_CN_InvokeTrace, cN_InvokeTrace_p);
    return decoded;
}

int ranap_decode_cn_deactivatetraceies(
    RANAP_CN_DeactivateTraceIEs_t *cN_DeactivateTraceIEs,
    ANY_t *any_p) {

    RANAP_CN_DeactivateTrace_t *cN_DeactivateTrace_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(cN_DeactivateTraceIEs != NULL);

    memset(cN_DeactivateTraceIEs, 0, sizeof(RANAP_CN_DeactivateTraceIEs_t));
    RANAP_DEBUG("Decoding message RANAP_CN_DeactivateTraceIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_CN_DeactivateTrace, (void**)&cN_DeactivateTrace_p);

    if (tempDecoded < 0 || cN_DeactivateTrace_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_CN_DeactivateTraceIEs failed\n");
        return -1;
    }

    for (i = 0; i < cN_DeactivateTrace_p->cN_DeactivateTrace_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = cN_DeactivateTrace_p->cN_DeactivateTrace_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_TraceReference:
            {
                RANAP_TraceReference_t *ranaP_TraceReference_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TraceReference, (void**)&ranaP_TraceReference_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE traceReference failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TraceReference, ranaP_TraceReference_p);
                memcpy(&cN_DeactivateTraceIEs->traceReference, ranaP_TraceReference_p, sizeof(RANAP_TraceReference_t));
                FREEMEM(ranaP_TraceReference_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_TriggerID:
            {
                RANAP_TriggerID_t *ranaP_TriggerID_p = NULL;
                cN_DeactivateTraceIEs->presenceMask |= CN_DEACTIVATETRACEIES_RANAP_TRIGGERID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TriggerID, (void**)&ranaP_TriggerID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE triggerID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TriggerID, ranaP_TriggerID_p);
                memcpy(&cN_DeactivateTraceIEs->triggerID, ranaP_TriggerID_p, sizeof(RANAP_TriggerID_t));
                FREEMEM(ranaP_TriggerID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message cn_deactivatetraceies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_CN_DeactivateTrace, cN_DeactivateTrace_p);
    return decoded;
}

int ranap_decode_locationreportingcontrolies(
    RANAP_LocationReportingControlIEs_t *locationReportingControlIEs,
    ANY_t *any_p) {

    RANAP_LocationReportingControl_t *locationReportingControl_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(locationReportingControlIEs != NULL);

    memset(locationReportingControlIEs, 0, sizeof(RANAP_LocationReportingControlIEs_t));
    RANAP_DEBUG("Decoding message RANAP_LocationReportingControlIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_LocationReportingControl, (void**)&locationReportingControl_p);

    if (tempDecoded < 0 || locationReportingControl_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_LocationReportingControlIEs failed\n");
        return -1;
    }

    for (i = 0; i < locationReportingControl_p->locationReportingControl_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = locationReportingControl_p->locationReportingControl_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RequestType:
            {
                RANAP_RequestType_t *ranaP_RequestType_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RequestType, (void**)&ranaP_RequestType_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE requestType failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RequestType, ranaP_RequestType_p);
                memcpy(&locationReportingControlIEs->requestType, ranaP_RequestType_p, sizeof(RANAP_RequestType_t));
                FREEMEM(ranaP_RequestType_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message locationreportingcontrolies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_LocationReportingControl, locationReportingControl_p);
    return decoded;
}

int ranap_decode_locationreporties(
    RANAP_LocationReportIEs_t *locationReportIEs,
    ANY_t *any_p) {

    RANAP_LocationReport_t *locationReport_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(locationReportIEs != NULL);

    memset(locationReportIEs, 0, sizeof(RANAP_LocationReportIEs_t));
    RANAP_DEBUG("Decoding message RANAP_LocationReportIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_LocationReport, (void**)&locationReport_p);

    if (tempDecoded < 0 || locationReport_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_LocationReportIEs failed\n");
        return -1;
    }

    for (i = 0; i < locationReport_p->locationReport_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = locationReport_p->locationReport_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_AreaIdentity:
            {
                RANAP_AreaIdentity_t *ranaP_AreaIdentity_p = NULL;
                locationReportIEs->presenceMask |= LOCATIONREPORTIES_RANAP_AREAIDENTITY_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_AreaIdentity, (void**)&ranaP_AreaIdentity_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE areaIdentity failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_AreaIdentity, ranaP_AreaIdentity_p);
                memcpy(&locationReportIEs->areaIdentity, ranaP_AreaIdentity_p, sizeof(RANAP_AreaIdentity_t));
                FREEMEM(ranaP_AreaIdentity_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                locationReportIEs->presenceMask |= LOCATIONREPORTIES_RANAP_CAUSE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&locationReportIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RequestType:
            {
                RANAP_RequestType_t *ranaP_RequestType_p = NULL;
                locationReportIEs->presenceMask |= LOCATIONREPORTIES_RANAP_REQUESTTYPE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RequestType, (void**)&ranaP_RequestType_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE requestType failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RequestType, ranaP_RequestType_p);
                memcpy(&locationReportIEs->requestType, ranaP_RequestType_p, sizeof(RANAP_RequestType_t));
                FREEMEM(ranaP_RequestType_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message locationreporties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_LocationReport, locationReport_p);
    return decoded;
}

int ranap_decode_initialue_messageies(
    RANAP_InitialUE_MessageIEs_t *initialUE_MessageIEs,
    ANY_t *any_p) {

    RANAP_InitialUE_Message_t *initialUE_Message_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(initialUE_MessageIEs != NULL);

    memset(initialUE_MessageIEs, 0, sizeof(RANAP_InitialUE_MessageIEs_t));
    RANAP_DEBUG("Decoding message RANAP_InitialUE_MessageIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_InitialUE_Message, (void**)&initialUE_Message_p);

    if (tempDecoded < 0 || initialUE_Message_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_InitialUE_MessageIEs failed\n");
        return -1;
    }

    for (i = 0; i < initialUE_Message_p->initialUE_Message_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = initialUE_Message_p->initialUE_Message_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&initialUE_MessageIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            case RANAP_ProtocolIE_ID_id_LAI:
            {
                RANAP_LAI_t *ranap_lai_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_LAI, (void**)&ranap_lai_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE lai failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_LAI, ranap_lai_p);
                memcpy(&initialUE_MessageIEs->lai, ranap_lai_p, sizeof(RANAP_LAI_t));
                FREEMEM(ranap_lai_p);
            } break;
            /* Conditional field */
            case RANAP_ProtocolIE_ID_id_RAC:
            {
                RANAP_RAC_t *ranap_rac_p = NULL;
                initialUE_MessageIEs->presenceMask |= INITIALUE_MESSAGEIES_RANAP_RAC_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAC, (void**)&ranap_rac_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE rac failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAC, ranap_rac_p);
                memcpy(&initialUE_MessageIEs->rac, ranap_rac_p, sizeof(RANAP_RAC_t));
                FREEMEM(ranap_rac_p);
            } break;
            case RANAP_ProtocolIE_ID_id_SAI:
            {
                RANAP_SAI_t *ranap_sai_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SAI, (void**)&ranap_sai_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE sai failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SAI, ranap_sai_p);
                memcpy(&initialUE_MessageIEs->sai, ranap_sai_p, sizeof(RANAP_SAI_t));
                FREEMEM(ranap_sai_p);
            } break;
            case RANAP_ProtocolIE_ID_id_NAS_PDU:
            {
                RANAP_NAS_PDU_t *ranap_naspdu_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_NAS_PDU, (void**)&ranap_naspdu_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE nas_pdu failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_NAS_PDU, ranap_naspdu_p);
                memcpy(&initialUE_MessageIEs->nas_pdu, ranap_naspdu_p, sizeof(RANAP_NAS_PDU_t));
                FREEMEM(ranap_naspdu_p);
            } break;
            case RANAP_ProtocolIE_ID_id_IuSigConId:
            {
                RANAP_IuSignallingConnectionIdentifier_t *ranaP_IuSignallingConnectionIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, (void**)&ranaP_IuSignallingConnectionIdentifier_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE iuSigConId failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, ranaP_IuSignallingConnectionIdentifier_p);
                memcpy(&initialUE_MessageIEs->iuSigConId, ranaP_IuSignallingConnectionIdentifier_p, sizeof(RANAP_IuSignallingConnectionIdentifier_t));
                FREEMEM(ranaP_IuSignallingConnectionIdentifier_p);
            } break;
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&initialUE_MessageIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message initialue_messageies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_InitialUE_Message, initialUE_Message_p);
    return decoded;
}

int ranap_decode_directtransferies(
    RANAP_DirectTransferIEs_t *directTransferIEs,
    ANY_t *any_p) {

    RANAP_DirectTransfer_t *directTransfer_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(directTransferIEs != NULL);

    memset(directTransferIEs, 0, sizeof(RANAP_DirectTransferIEs_t));
    RANAP_DEBUG("Decoding message RANAP_DirectTransferIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_DirectTransfer, (void**)&directTransfer_p);

    if (tempDecoded < 0 || directTransfer_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_DirectTransferIEs failed\n");
        return -1;
    }

    for (i = 0; i < directTransfer_p->directTransfer_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = directTransfer_p->directTransfer_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_NAS_PDU:
            {
                RANAP_NAS_PDU_t *ranap_naspdu_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_NAS_PDU, (void**)&ranap_naspdu_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE nas_pdu failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_NAS_PDU, ranap_naspdu_p);
                memcpy(&directTransferIEs->nas_pdu, ranap_naspdu_p, sizeof(RANAP_NAS_PDU_t));
                FREEMEM(ranap_naspdu_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_LAI:
            {
                RANAP_LAI_t *ranap_lai_p = NULL;
                directTransferIEs->presenceMask |= DIRECTTRANSFERIES_RANAP_LAI_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_LAI, (void**)&ranap_lai_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE lai failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_LAI, ranap_lai_p);
                memcpy(&directTransferIEs->lai, ranap_lai_p, sizeof(RANAP_LAI_t));
                FREEMEM(ranap_lai_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAC:
            {
                RANAP_RAC_t *ranap_rac_p = NULL;
                directTransferIEs->presenceMask |= DIRECTTRANSFERIES_RANAP_RAC_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAC, (void**)&ranap_rac_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE rac failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAC, ranap_rac_p);
                memcpy(&directTransferIEs->rac, ranap_rac_p, sizeof(RANAP_RAC_t));
                FREEMEM(ranap_rac_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_SAI:
            {
                RANAP_SAI_t *ranap_sai_p = NULL;
                directTransferIEs->presenceMask |= DIRECTTRANSFERIES_RANAP_SAI_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SAI, (void**)&ranap_sai_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE sai failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SAI, ranap_sai_p);
                memcpy(&directTransferIEs->sai, ranap_sai_p, sizeof(RANAP_SAI_t));
                FREEMEM(ranap_sai_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_SAPI:
            {
                RANAP_SAPI_t *ranap_sapi_p = NULL;
                directTransferIEs->presenceMask |= DIRECTTRANSFERIES_RANAP_SAPI_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SAPI, (void**)&ranap_sapi_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE sapi failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SAPI, ranap_sapi_p);
                memcpy(&directTransferIEs->sapi, ranap_sapi_p, sizeof(RANAP_SAPI_t));
                FREEMEM(ranap_sapi_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message directtransferies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_DirectTransfer, directTransfer_p);
    return decoded;
}

int ranap_decode_redirectionindication_ies(
    RANAP_RedirectionIndication_IEs_t *redirectionIndication_IEs,
    ANY_t *any_p) {

    RANAP_RedirectionIndication_t *redirectionIndication_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(redirectionIndication_IEs != NULL);

    memset(redirectionIndication_IEs, 0, sizeof(RANAP_RedirectionIndication_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RedirectionIndication_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RedirectionIndication, (void**)&redirectionIndication_p);

    if (tempDecoded < 0 || redirectionIndication_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RedirectionIndication_IEs failed\n");
        return -1;
    }

    for (i = 0; i < redirectionIndication_p->redirectionIndication_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = redirectionIndication_p->redirectionIndication_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_NAS_PDU:
            {
                RANAP_NAS_PDU_t *ranap_naspdu_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_NAS_PDU, (void**)&ranap_naspdu_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE nas_pdu failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_NAS_PDU, ranap_naspdu_p);
                memcpy(&redirectionIndication_IEs->nas_pdu, ranap_naspdu_p, sizeof(RANAP_NAS_PDU_t));
                FREEMEM(ranap_naspdu_p);
            } break;
            case RANAP_ProtocolIE_ID_id_RejectCauseValue:
            {
                RANAP_RejectCauseValue_t *ranaP_RejectCauseValue_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RejectCauseValue, (void**)&ranaP_RejectCauseValue_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE rejectCauseValue failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RejectCauseValue, ranaP_RejectCauseValue_p);
                memcpy(&redirectionIndication_IEs->rejectCauseValue, ranaP_RejectCauseValue_p, sizeof(RANAP_RejectCauseValue_t));
                FREEMEM(ranaP_RejectCauseValue_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_NAS_SequenceNumber:
            {
                RANAP_NAS_SequenceNumber_t *ranaP_NASSequenceNumber_p = NULL;
                redirectionIndication_IEs->presenceMask |= REDIRECTIONINDICATION_IES_RANAP_NAS_SEQUENCENUMBER_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_NAS_SequenceNumber, (void**)&ranaP_NASSequenceNumber_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE naS_SequenceNumber failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_NAS_SequenceNumber, ranaP_NASSequenceNumber_p);
                memcpy(&redirectionIndication_IEs->naS_SequenceNumber, ranaP_NASSequenceNumber_p, sizeof(RANAP_NAS_SequenceNumber_t));
                FREEMEM(ranaP_NASSequenceNumber_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_PermanentNAS_UE_ID:
            {
                RANAP_PermanentNAS_UE_ID_t *ranaP_PermanentNASUEID_p = NULL;
                redirectionIndication_IEs->presenceMask |= REDIRECTIONINDICATION_IES_RANAP_PERMANENTNAS_UE_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_PermanentNAS_UE_ID, (void**)&ranaP_PermanentNASUEID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE permanentNAS_UE_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_PermanentNAS_UE_ID, ranaP_PermanentNASUEID_p);
                memcpy(&redirectionIndication_IEs->permanentNAS_UE_ID, ranaP_PermanentNASUEID_p, sizeof(RANAP_PermanentNAS_UE_ID_t));
                FREEMEM(ranaP_PermanentNASUEID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message redirectionindication_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RedirectionIndication, redirectionIndication_p);
    return decoded;
}

int ranap_decode_overloadies(
    RANAP_OverloadIEs_t *overloadIEs,
    ANY_t *any_p) {

    RANAP_Overload_t *overload_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(overloadIEs != NULL);

    memset(overloadIEs, 0, sizeof(RANAP_OverloadIEs_t));
    RANAP_DEBUG("Decoding message RANAP_OverloadIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_Overload, (void**)&overload_p);

    if (tempDecoded < 0 || overload_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_OverloadIEs failed\n");
        return -1;
    }

    for (i = 0; i < overload_p->overload_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = overload_p->overload_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_NumberOfSteps:
            {
                RANAP_NumberOfSteps_t *ranaP_NumberOfSteps_p = NULL;
                overloadIEs->presenceMask |= OVERLOADIES_RANAP_NUMBEROFSTEPS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_NumberOfSteps, (void**)&ranaP_NumberOfSteps_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE numberOfSteps failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_NumberOfSteps, ranaP_NumberOfSteps_p);
                memcpy(&overloadIEs->numberOfSteps, ranaP_NumberOfSteps_p, sizeof(RANAP_NumberOfSteps_t));
                FREEMEM(ranaP_NumberOfSteps_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                overloadIEs->presenceMask |= OVERLOADIES_RANAP_GLOBALRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&overloadIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message overloadies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_Overload, overload_p);
    return decoded;
}

int ranap_decode_errorindicationies(
    RANAP_ErrorIndicationIEs_t *errorIndicationIEs,
    ANY_t *any_p) {

    RANAP_ErrorIndication_t *errorIndication_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(errorIndicationIEs != NULL);

    memset(errorIndicationIEs, 0, sizeof(RANAP_ErrorIndicationIEs_t));
    RANAP_DEBUG("Decoding message RANAP_ErrorIndicationIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_ErrorIndication, (void**)&errorIndication_p);

    if (tempDecoded < 0 || errorIndication_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_ErrorIndicationIEs failed\n");
        return -1;
    }

    for (i = 0; i < errorIndication_p->errorIndication_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = errorIndication_p->errorIndication_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                errorIndicationIEs->presenceMask |= ERRORINDICATIONIES_RANAP_CAUSE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&errorIndicationIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                errorIndicationIEs->presenceMask |= ERRORINDICATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&errorIndicationIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                errorIndicationIEs->presenceMask |= ERRORINDICATIONIES_RANAP_CN_DOMAININDICATOR_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&errorIndicationIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                errorIndicationIEs->presenceMask |= ERRORINDICATIONIES_RANAP_GLOBALRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&errorIndicationIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message errorindicationies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_ErrorIndication, errorIndication_p);
    return decoded;
}

int ranap_decode_srns_dataforwardcommandies(
    RANAP_SRNS_DataForwardCommandIEs_t *srnS_DataForwardCommandIEs,
    ANY_t *any_p) {

    RANAP_SRNS_DataForwardCommand_t *sRNS_DataForwardCommand_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(srnS_DataForwardCommandIEs != NULL);

    memset(srnS_DataForwardCommandIEs, 0, sizeof(RANAP_SRNS_DataForwardCommandIEs_t));
    RANAP_DEBUG("Decoding message RANAP_SRNS_DataForwardCommandIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_SRNS_DataForwardCommand, (void**)&sRNS_DataForwardCommand_p);

    if (tempDecoded < 0 || sRNS_DataForwardCommand_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_SRNS_DataForwardCommandIEs failed\n");
        return -1;
    }

    for (i = 0; i < sRNS_DataForwardCommand_p->srnS_DataForwardCommand_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = sRNS_DataForwardCommand_p->srnS_DataForwardCommand_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_DataForwardingList:
            {
                RANAP_RAB_DataForwardingList_t *ranaP_RABDataForwardingList_p = NULL;
                srnS_DataForwardCommandIEs->presenceMask |= SRNS_DATAFORWARDCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_DataForwardingList, (void**)&ranaP_RABDataForwardingList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_DataForwardingList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_DataForwardingList, ranaP_RABDataForwardingList_p);
                memcpy(&srnS_DataForwardCommandIEs->raB_DataForwardingList, ranaP_RABDataForwardingList_p, sizeof(RANAP_RAB_DataForwardingList_t));
                FREEMEM(ranaP_RABDataForwardingList_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message srns_dataforwardcommandies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_SRNS_DataForwardCommand, sRNS_DataForwardCommand_p);
    return decoded;
}

int ranap_decode_forwardsrns_contexties(
    RANAP_ForwardSRNS_ContextIEs_t *forwardSRNS_ContextIEs,
    ANY_t *any_p) {

    RANAP_ForwardSRNS_Context_t *forwardSRNS_Context_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(forwardSRNS_ContextIEs != NULL);

    memset(forwardSRNS_ContextIEs, 0, sizeof(RANAP_ForwardSRNS_ContextIEs_t));
    RANAP_DEBUG("Decoding message RANAP_ForwardSRNS_ContextIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_ForwardSRNS_Context, (void**)&forwardSRNS_Context_p);

    if (tempDecoded < 0 || forwardSRNS_Context_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_ForwardSRNS_ContextIEs failed\n");
        return -1;
    }

    for (i = 0; i < forwardSRNS_Context_p->forwardSRNS_Context_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = forwardSRNS_Context_p->forwardSRNS_Context_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ContextList:
            {
                RANAP_RAB_ContextList_t *ranaP_RABContextList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ContextList, (void**)&ranaP_RABContextList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ContextList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ContextList, ranaP_RABContextList_p);
                memcpy(&forwardSRNS_ContextIEs->raB_ContextList, ranaP_RABContextList_p, sizeof(RANAP_RAB_ContextList_t));
                FREEMEM(ranaP_RABContextList_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message forwardsrns_contexties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_ForwardSRNS_Context, forwardSRNS_Context_p);
    return decoded;
}

int ranap_decode_rab_assignmentrequesties(
    RANAP_RAB_AssignmentRequestIEs_t *raB_AssignmentRequestIEs,
    ANY_t *any_p) {

    RANAP_RAB_AssignmentRequest_t *rAB_AssignmentRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_AssignmentRequestIEs != NULL);

    memset(raB_AssignmentRequestIEs, 0, sizeof(RANAP_RAB_AssignmentRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_AssignmentRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_AssignmentRequest, (void**)&rAB_AssignmentRequest_p);

    if (tempDecoded < 0 || rAB_AssignmentRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_AssignmentRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_AssignmentRequest_p->raB_AssignmentRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_AssignmentRequest_p->raB_AssignmentRequest_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_SetupOrModifyList:
            {
                RANAP_RAB_SetupOrModifyList_t *ranaP_RABSetupOrModifyList_p = NULL;
                raB_AssignmentRequestIEs->presenceMask |= RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_SETUPORMODIFYLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupOrModifyList, (void**)&ranaP_RABSetupOrModifyList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupOrModifyList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupOrModifyList, ranaP_RABSetupOrModifyList_p);
                memcpy(&raB_AssignmentRequestIEs->raB_SetupOrModifyList, ranaP_RABSetupOrModifyList_p, sizeof(RANAP_RAB_SetupOrModifyList_t));
                FREEMEM(ranaP_RABSetupOrModifyList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_ReleaseList:
            {
                RANAP_RAB_ReleaseList_t *ranaP_RABReleaseList_p = NULL;
                raB_AssignmentRequestIEs->presenceMask |= RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_RELEASELIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ReleaseList, (void**)&ranaP_RABReleaseList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ReleaseList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ReleaseList, ranaP_RABReleaseList_p);
                memcpy(&raB_AssignmentRequestIEs->raB_ReleaseList, ranaP_RABReleaseList_p, sizeof(RANAP_RAB_ReleaseList_t));
                FREEMEM(ranaP_RABReleaseList_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_assignmentrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_AssignmentRequest, rAB_AssignmentRequest_p);
    return decoded;
}

int ranap_decode_rab_assignmentresponseies(
    RANAP_RAB_AssignmentResponseIEs_t *raB_AssignmentResponseIEs,
    ANY_t *any_p) {

    RANAP_RAB_AssignmentResponse_t *rAB_AssignmentResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_AssignmentResponseIEs != NULL);

    memset(raB_AssignmentResponseIEs, 0, sizeof(RANAP_RAB_AssignmentResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_AssignmentResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_AssignmentResponse, (void**)&rAB_AssignmentResponse_p);

    if (tempDecoded < 0 || rAB_AssignmentResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_AssignmentResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_AssignmentResponse_p->raB_AssignmentResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_AssignmentResponse_p->raB_AssignmentResponse_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_SetupOrModifiedList:
            {
                RANAP_RAB_SetupOrModifiedList_t *ranaP_RABSetupOrModifiedList_p = NULL;
                raB_AssignmentResponseIEs->presenceMask |= RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_SETUPORMODIFIEDLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupOrModifiedList, (void**)&ranaP_RABSetupOrModifiedList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupOrModifiedList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupOrModifiedList, ranaP_RABSetupOrModifiedList_p);
                memcpy(&raB_AssignmentResponseIEs->raB_SetupOrModifiedList, ranaP_RABSetupOrModifiedList_p, sizeof(RANAP_RAB_SetupOrModifiedList_t));
                FREEMEM(ranaP_RABSetupOrModifiedList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_ReleasedList:
            {
                RANAP_RAB_ReleasedList_t *ranaP_RABReleasedList_p = NULL;
                raB_AssignmentResponseIEs->presenceMask |= RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEDLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ReleasedList, (void**)&ranaP_RABReleasedList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ReleasedList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ReleasedList, ranaP_RABReleasedList_p);
                memcpy(&raB_AssignmentResponseIEs->raB_ReleasedList, ranaP_RABReleasedList_p, sizeof(RANAP_RAB_ReleasedList_t));
                FREEMEM(ranaP_RABReleasedList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_QueuedList:
            {
                RANAP_RAB_QueuedList_t *ranaP_RABQueuedList_p = NULL;
                raB_AssignmentResponseIEs->presenceMask |= RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_QUEUEDLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_QueuedList, (void**)&ranaP_RABQueuedList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_QueuedList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_QueuedList, ranaP_RABQueuedList_p);
                memcpy(&raB_AssignmentResponseIEs->raB_QueuedList, ranaP_RABQueuedList_p, sizeof(RANAP_RAB_QueuedList_t));
                FREEMEM(ranaP_RABQueuedList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_FailedList:
            {
                RANAP_RAB_FailedList_t *ranaP_RABFailedList_p = NULL;
                raB_AssignmentResponseIEs->presenceMask |= RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_FAILEDLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_FailedList, (void**)&ranaP_RABFailedList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_FailedList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_FailedList, ranaP_RABFailedList_p);
                memcpy(&raB_AssignmentResponseIEs->raB_FailedList, ranaP_RABFailedList_p, sizeof(RANAP_RAB_FailedList_t));
                FREEMEM(ranaP_RABFailedList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_ReleaseFailedList:
            {
                RANAP_RAB_ReleaseFailedList_t *ranaP_RABReleaseFailedList_p = NULL;
                raB_AssignmentResponseIEs->presenceMask |= RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEFAILEDLIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ReleaseFailedList, (void**)&ranaP_RABReleaseFailedList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ReleaseFailedList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ReleaseFailedList, ranaP_RABReleaseFailedList_p);
                memcpy(&raB_AssignmentResponseIEs->raB_ReleaseFailedList, ranaP_RABReleaseFailedList_p, sizeof(RANAP_RAB_ReleaseFailedList_t));
                FREEMEM(ranaP_RABReleaseFailedList_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                raB_AssignmentResponseIEs->presenceMask |= RAB_ASSIGNMENTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&raB_AssignmentResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_assignmentresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_AssignmentResponse, rAB_AssignmentResponse_p);
    return decoded;
}

int ranap_decode_rab_setupormodifieditemies(
    RANAP_RAB_SetupOrModifiedItemIEs_t *raB_SetupOrModifiedItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_SetupOrModifiedList_t *rAB_SetupOrModifiedItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_SetupOrModifiedItemIEs != NULL);

    memset(raB_SetupOrModifiedItemIEs, 0, sizeof(RANAP_RAB_SetupOrModifiedItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_SetupOrModifiedItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_SetupOrModifiedList, (void**)&rAB_SetupOrModifiedItem_p);

    if (tempDecoded < 0 || rAB_SetupOrModifiedItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_SetupOrModifiedItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_SetupOrModifiedItem_p->raB_SetupOrModifiedList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_SetupOrModifiedItem_p->raB_SetupOrModifiedList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_SetupOrModifiedItem:
            {
                RANAP_RAB_SetupOrModifiedItem_t *ranaP_RABSetupOrModifiedItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupOrModifiedItem, (void**)&ranaP_RABSetupOrModifiedItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupOrModifiedItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupOrModifiedItem, ranaP_RABSetupOrModifiedItem_p);
                memcpy(&raB_SetupOrModifiedItemIEs->raB_SetupOrModifiedItem, ranaP_RABSetupOrModifiedItem_p, sizeof(RANAP_RAB_SetupOrModifiedItem_t));
                FREEMEM(ranaP_RABSetupOrModifiedItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_setupormodifieditemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_SetupOrModifiedList, rAB_SetupOrModifiedItem_p);
    return decoded;
}

int ranap_decode_rab_releaseditemies(
    RANAP_RAB_ReleasedItemIEs_t *raB_ReleasedItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_ReleasedList_t *rAB_ReleasedItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_ReleasedItemIEs != NULL);

    memset(raB_ReleasedItemIEs, 0, sizeof(RANAP_RAB_ReleasedItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_ReleasedItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_ReleasedList, (void**)&rAB_ReleasedItem_p);

    if (tempDecoded < 0 || rAB_ReleasedItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_ReleasedItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_ReleasedItem_p->raB_ReleasedList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_ReleasedItem_p->raB_ReleasedList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ReleasedItem:
            {
                RANAP_RAB_ReleasedItem_t *ranaP_RABReleasedItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ReleasedItem, (void**)&ranaP_RABReleasedItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ReleasedItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ReleasedItem, ranaP_RABReleasedItem_p);
                memcpy(&raB_ReleasedItemIEs->raB_ReleasedItem, ranaP_RABReleasedItem_p, sizeof(RANAP_RAB_ReleasedItem_t));
                FREEMEM(ranaP_RABReleasedItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_releaseditemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_ReleasedList, rAB_ReleasedItem_p);
    return decoded;
}

int ranap_decode_rab_queueditemies(
    RANAP_RAB_QueuedItemIEs_t *raB_QueuedItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_QueuedList_t *rAB_QueuedItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_QueuedItemIEs != NULL);

    memset(raB_QueuedItemIEs, 0, sizeof(RANAP_RAB_QueuedItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_QueuedItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_QueuedList, (void**)&rAB_QueuedItem_p);

    if (tempDecoded < 0 || rAB_QueuedItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_QueuedItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_QueuedItem_p->raB_QueuedList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_QueuedItem_p->raB_QueuedList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_QueuedItem:
            {
                RANAP_RAB_QueuedItem_t *ranaP_RABQueuedItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_QueuedItem, (void**)&ranaP_RABQueuedItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_QueuedItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_QueuedItem, ranaP_RABQueuedItem_p);
                memcpy(&raB_QueuedItemIEs->raB_QueuedItem, ranaP_RABQueuedItem_p, sizeof(RANAP_RAB_QueuedItem_t));
                FREEMEM(ranaP_RABQueuedItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_queueditemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_QueuedList, rAB_QueuedItem_p);
    return decoded;
}

int ranap_decode_geran_iumode_rab_failed_rabassgntresponse_itemies(
    RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs_t *geraN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs,
    ANY_t *any_p) {

    RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_List_t *gERAN_Iumode_RAB_Failed_RABAssgntResponse_Item_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(geraN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs != NULL);

    memset(geraN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs, 0, sizeof(RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_List, (void**)&gERAN_Iumode_RAB_Failed_RABAssgntResponse_Item_p);

    if (tempDecoded < 0 || gERAN_Iumode_RAB_Failed_RABAssgntResponse_Item_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < gERAN_Iumode_RAB_Failed_RABAssgntResponse_Item_p->geraN_Iumode_RAB_Failed_RABAssgntResponse_List_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = gERAN_Iumode_RAB_Failed_RABAssgntResponse_Item_p->geraN_Iumode_RAB_Failed_RABAssgntResponse_List_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_GERAN_Iumode_RAB_Failed_RABAssgntResponse_Item:
            {
                RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_Item_t *ranaP_GERANIumodeRABFailedRABAssgntResponseItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_Item, (void**)&ranaP_GERANIumodeRABFailedRABAssgntResponseItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE geraN_Iumode_RAB_Failed_RABAssgntResponse_Item failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_Item, ranaP_GERANIumodeRABFailedRABAssgntResponseItem_p);
                memcpy(&geraN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs->geraN_Iumode_RAB_Failed_RABAssgntResponse_Item, ranaP_GERANIumodeRABFailedRABAssgntResponseItem_p, sizeof(RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_Item_t));
                FREEMEM(ranaP_GERANIumodeRABFailedRABAssgntResponseItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message geran_iumode_rab_failed_rabassgntresponse_itemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_List, gERAN_Iumode_RAB_Failed_RABAssgntResponse_Item_p);
    return decoded;
}

int ranap_decode_ranap_relocationinformationies(
    RANAP_RANAP_RelocationInformationIEs_t *ranaP_RelocationInformationIEs,
    ANY_t *any_p) {

    RANAP_RANAP_RelocationInformation_t *rANAP_RelocationInformation_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(ranaP_RelocationInformationIEs != NULL);

    memset(ranaP_RelocationInformationIEs, 0, sizeof(RANAP_RANAP_RelocationInformationIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RANAP_RelocationInformationIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RANAP_RelocationInformation, (void**)&rANAP_RelocationInformation_p);

    if (tempDecoded < 0 || rANAP_RelocationInformation_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RANAP_RelocationInformationIEs failed\n");
        return -1;
    }

    for (i = 0; i < rANAP_RelocationInformation_p->ranaP_RelocationInformation_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rANAP_RelocationInformation_p->ranaP_RelocationInformation_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_ContextList_RANAP_RelocInf:
            {
                RANAP_RAB_ContextList_RANAP_RelocInf_t *ranaP_RABContextListRANAPRelocInf_p = NULL;
                ranaP_RelocationInformationIEs->presenceMask |= RANAP_RELOCATIONINFORMATIONIES_RANAP_RAB_CONTEXTLIST_RANAP_RELOCINF_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ContextList_RANAP_RelocInf, (void**)&ranaP_RABContextListRANAPRelocInf_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ContextList_RANAP_RelocInf failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ContextList_RANAP_RelocInf, ranaP_RABContextListRANAPRelocInf_p);
                memcpy(&ranaP_RelocationInformationIEs->raB_ContextList_RANAP_RelocInf, ranaP_RABContextListRANAPRelocInf_p, sizeof(RANAP_RAB_ContextList_RANAP_RelocInf_t));
                FREEMEM(ranaP_RABContextListRANAPRelocInf_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message ranap_relocationinformationies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RANAP_RelocationInformation, rANAP_RelocationInformation_p);
    return decoded;
}

int ranap_decode_rab_contextitemies_ranap_relocinf(
    RANAP_RAB_ContextItemIEs_RANAP_RelocInf_t *raB_ContextItemIEs_RANAP_RelocInf,
    ANY_t *any_p) {

    RANAP_RAB_ContextList_RANAP_RelocInf_t *rAB_ContextItem_RANAP_RelocInf_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_ContextItemIEs_RANAP_RelocInf != NULL);

    memset(raB_ContextItemIEs_RANAP_RelocInf, 0, sizeof(RANAP_RAB_ContextItemIEs_RANAP_RelocInf_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_ContextItemIEs_RANAP_RelocInf (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_ContextList_RANAP_RelocInf, (void**)&rAB_ContextItem_RANAP_RelocInf_p);

    if (tempDecoded < 0 || rAB_ContextItem_RANAP_RelocInf_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_ContextItemIEs_RANAP_RelocInf failed\n");
        return -1;
    }

    for (i = 0; i < rAB_ContextItem_RANAP_RelocInf_p->raB_ContextList_RANAP_RelocInf_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_ContextItem_RANAP_RelocInf_p->raB_ContextList_RANAP_RelocInf_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ContextItem_RANAP_RelocInf:
            {
                RANAP_RAB_ContextItem_RANAP_RelocInf_t *ranaP_RABContextItemRANAPRelocInf_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ContextItem_RANAP_RelocInf, (void**)&ranaP_RABContextItemRANAPRelocInf_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ContextItem_RANAP_RelocInf failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ContextItem_RANAP_RelocInf, ranaP_RABContextItemRANAPRelocInf_p);
                memcpy(&raB_ContextItemIEs_RANAP_RelocInf->raB_ContextItem_RANAP_RelocInf, ranaP_RABContextItemRANAPRelocInf_p, sizeof(RANAP_RAB_ContextItem_RANAP_RelocInf_t));
                FREEMEM(ranaP_RABContextItemRANAPRelocInf_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_contextitemies_ranap_relocinf\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_ContextList_RANAP_RelocInf, rAB_ContextItem_RANAP_RelocInf_p);
    return decoded;
}

int ranap_decode_ranap_enhancedrelocationinformationrequesties(
    RANAP_RANAP_EnhancedRelocationInformationRequestIEs_t *ranaP_EnhancedRelocationInformationRequestIEs,
    ANY_t *any_p) {

    RANAP_RANAP_EnhancedRelocationInformationRequest_t *rANAP_EnhancedRelocationInformationRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(ranaP_EnhancedRelocationInformationRequestIEs != NULL);

    memset(ranaP_EnhancedRelocationInformationRequestIEs, 0, sizeof(RANAP_RANAP_EnhancedRelocationInformationRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RANAP_EnhancedRelocationInformationRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RANAP_EnhancedRelocationInformationRequest, (void**)&rANAP_EnhancedRelocationInformationRequest_p);

    if (tempDecoded < 0 || rANAP_EnhancedRelocationInformationRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RANAP_EnhancedRelocationInformationRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < rANAP_EnhancedRelocationInformationRequest_p->ranaP_EnhancedRelocationInformationRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rANAP_EnhancedRelocationInformationRequest_p->ranaP_EnhancedRelocationInformationRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Source_ToTarget_TransparentContainer:
            {
                RANAP_SourceRNC_ToTargetRNC_TransparentContainer_t *ranaP_SourceRNCToTargetRNCTransparentContainer_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SourceRNC_ToTargetRNC_TransparentContainer, (void**)&ranaP_SourceRNCToTargetRNCTransparentContainer_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE source_ToTarget_TransparentContainer failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SourceRNC_ToTargetRNC_TransparentContainer, ranaP_SourceRNCToTargetRNCTransparentContainer_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->source_ToTarget_TransparentContainer, ranaP_SourceRNCToTargetRNCTransparentContainer_p, sizeof(RANAP_SourceRNC_ToTargetRNC_TransparentContainer_t));
                FREEMEM(ranaP_SourceRNCToTargetRNCTransparentContainer_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_OldIuSigConIdCS:
            {
                RANAP_IuSignallingConnectionIdentifier_t *ranaP_IuSignallingConnectionIdentifier_p = NULL;
                ranaP_EnhancedRelocationInformationRequestIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDCS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, (void**)&ranaP_IuSignallingConnectionIdentifier_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE oldIuSigConIdCS failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, ranaP_IuSignallingConnectionIdentifier_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->oldIuSigConIdCS, ranaP_IuSignallingConnectionIdentifier_p, sizeof(RANAP_IuSignallingConnectionIdentifier_t));
                FREEMEM(ranaP_IuSignallingConnectionIdentifier_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_IDCS:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                ranaP_EnhancedRelocationInformationRequestIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDCS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_IDCS failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->globalCN_IDCS, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_OldIuSigConIdPS:
            {
                RANAP_IuSignallingConnectionIdentifier_t *ranaP_IuSignallingConnectionIdentifier_p = NULL;
                ranaP_EnhancedRelocationInformationRequestIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDPS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, (void**)&ranaP_IuSignallingConnectionIdentifier_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE oldIuSigConIdPS failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, ranaP_IuSignallingConnectionIdentifier_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->oldIuSigConIdPS, ranaP_IuSignallingConnectionIdentifier_p, sizeof(RANAP_IuSignallingConnectionIdentifier_t));
                FREEMEM(ranaP_IuSignallingConnectionIdentifier_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_IDPS:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                ranaP_EnhancedRelocationInformationRequestIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDPS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_IDPS failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->globalCN_IDPS, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_SetupList_EnhRelocInfoReq:
            {
                RANAP_RAB_SetupList_EnhRelocInfoReq_t *ranaP_RABSetupListEnhRelocInfoReq_p = NULL;
                ranaP_EnhancedRelocationInformationRequestIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_RAB_SETUPLIST_ENHRELOCINFOREQ_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoReq, (void**)&ranaP_RABSetupListEnhRelocInfoReq_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupList_EnhRelocInfoReq failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoReq, ranaP_RABSetupListEnhRelocInfoReq_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->raB_SetupList_EnhRelocInfoReq, ranaP_RABSetupListEnhRelocInfoReq_p, sizeof(RANAP_RAB_SetupList_EnhRelocInfoReq_t));
                FREEMEM(ranaP_RABSetupListEnhRelocInfoReq_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_SNA_Access_Information:
            {
                RANAP_SNA_Access_Information_t *ranaP_SNAAccessInformation_p = NULL;
                ranaP_EnhancedRelocationInformationRequestIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_SNA_ACCESS_INFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SNA_Access_Information, (void**)&ranaP_SNAAccessInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE snA_Access_Information failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SNA_Access_Information, ranaP_SNAAccessInformation_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->snA_Access_Information, ranaP_SNAAccessInformation_p, sizeof(RANAP_SNA_Access_Information_t));
                FREEMEM(ranaP_SNAAccessInformation_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_UESBI_Iu:
            {
                RANAP_UESBI_Iu_t *ranaP_UESBIIu_p = NULL;
                ranaP_EnhancedRelocationInformationRequestIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_UESBI_IU_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_UESBI_Iu, (void**)&ranaP_UESBIIu_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE uesbI_Iu failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_UESBI_Iu, ranaP_UESBIIu_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->uesbI_Iu, ranaP_UESBIIu_p, sizeof(RANAP_UESBI_Iu_t));
                FREEMEM(ranaP_UESBIIu_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_SelectedPLMN_ID:
            {
                RANAP_PLMNidentity_t *ranaP_PLMNidentity_p = NULL;
                ranaP_EnhancedRelocationInformationRequestIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_PLMNIDENTITY_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_PLMNidentity, (void**)&ranaP_PLMNidentity_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE selectedPLMN_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_PLMNidentity, ranaP_PLMNidentity_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->selectedPLMN_ID, ranaP_PLMNidentity_p, sizeof(RANAP_PLMNidentity_t));
                FREEMEM(ranaP_PLMNidentity_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CNMBMSLinkingInformation:
            {
                RANAP_CNMBMSLinkingInformation_t *ranaP_CNMBMSLinkingInformation_p = NULL;
                ranaP_EnhancedRelocationInformationRequestIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_CNMBMSLINKINGINFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CNMBMSLinkingInformation, (void**)&ranaP_CNMBMSLinkingInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cnmbmsLinkingInformation failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CNMBMSLinkingInformation, ranaP_CNMBMSLinkingInformation_p);
                memcpy(&ranaP_EnhancedRelocationInformationRequestIEs->cnmbmsLinkingInformation, ranaP_CNMBMSLinkingInformation_p, sizeof(RANAP_CNMBMSLinkingInformation_t));
                FREEMEM(ranaP_CNMBMSLinkingInformation_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message ranap_enhancedrelocationinformationrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RANAP_EnhancedRelocationInformationRequest, rANAP_EnhancedRelocationInformationRequest_p);
    return decoded;
}

int ranap_decode_rab_setupitem_enhrelocinforeq_ies(
    RANAP_RAB_SetupItem_EnhRelocInfoReq_IEs_t *raB_SetupItem_EnhRelocInfoReq_IEs,
    ANY_t *any_p) {

    RANAP_RAB_SetupList_EnhRelocInfoReq_t *rAB_SetupItem_EnhRelocInfoReq_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_SetupItem_EnhRelocInfoReq_IEs != NULL);

    memset(raB_SetupItem_EnhRelocInfoReq_IEs, 0, sizeof(RANAP_RAB_SetupItem_EnhRelocInfoReq_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_SetupItem_EnhRelocInfoReq_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoReq, (void**)&rAB_SetupItem_EnhRelocInfoReq_p);

    if (tempDecoded < 0 || rAB_SetupItem_EnhRelocInfoReq_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_SetupItem_EnhRelocInfoReq_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_SetupItem_EnhRelocInfoReq_p->raB_SetupList_EnhRelocInfoReq_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_SetupItem_EnhRelocInfoReq_p->raB_SetupList_EnhRelocInfoReq_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_SetupItem_EnhRelocInfoReq:
            {
                RANAP_RAB_SetupItem_EnhRelocInfoReq_t *ranaP_RABSetupItemEnhRelocInfoReq_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupItem_EnhRelocInfoReq, (void**)&ranaP_RABSetupItemEnhRelocInfoReq_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupItem_EnhRelocInfoReq failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupItem_EnhRelocInfoReq, ranaP_RABSetupItemEnhRelocInfoReq_p);
                memcpy(&raB_SetupItem_EnhRelocInfoReq_IEs->raB_SetupItem_EnhRelocInfoReq, ranaP_RABSetupItemEnhRelocInfoReq_p, sizeof(RANAP_RAB_SetupItem_EnhRelocInfoReq_t));
                FREEMEM(ranaP_RABSetupItemEnhRelocInfoReq_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_setupitem_enhrelocinforeq_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoReq, rAB_SetupItem_EnhRelocInfoReq_p);
    return decoded;
}

int ranap_decode_ranap_enhancedrelocationinformationresponseies(
    RANAP_RANAP_EnhancedRelocationInformationResponseIEs_t *ranaP_EnhancedRelocationInformationResponseIEs,
    ANY_t *any_p) {

    RANAP_RANAP_EnhancedRelocationInformationResponse_t *rANAP_EnhancedRelocationInformationResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(ranaP_EnhancedRelocationInformationResponseIEs != NULL);

    memset(ranaP_EnhancedRelocationInformationResponseIEs, 0, sizeof(RANAP_RANAP_EnhancedRelocationInformationResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RANAP_EnhancedRelocationInformationResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RANAP_EnhancedRelocationInformationResponse, (void**)&rANAP_EnhancedRelocationInformationResponse_p);

    if (tempDecoded < 0 || rANAP_EnhancedRelocationInformationResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RANAP_EnhancedRelocationInformationResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < rANAP_EnhancedRelocationInformationResponse_p->ranaP_EnhancedRelocationInformationResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rANAP_EnhancedRelocationInformationResponse_p->ranaP_EnhancedRelocationInformationResponse_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Target_ToSource_TransparentContainer:
            {
                RANAP_TargetRNC_ToSourceRNC_TransparentContainer_t *ranaP_TargetRNCToSourceRNCTransparentContainer_p = NULL;
                ranaP_EnhancedRelocationInformationResponseIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TargetRNC_ToSourceRNC_TransparentContainer, (void**)&ranaP_TargetRNCToSourceRNCTransparentContainer_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE target_ToSource_TransparentContainer failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TargetRNC_ToSourceRNC_TransparentContainer, ranaP_TargetRNCToSourceRNCTransparentContainer_p);
                memcpy(&ranaP_EnhancedRelocationInformationResponseIEs->target_ToSource_TransparentContainer, ranaP_TargetRNCToSourceRNCTransparentContainer_p, sizeof(RANAP_TargetRNC_ToSourceRNC_TransparentContainer_t));
                FREEMEM(ranaP_TargetRNCToSourceRNCTransparentContainer_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_SetupList_EnhRelocInfoRes:
            {
                RANAP_RAB_SetupList_EnhRelocInfoRes_t *ranaP_RABSetupListEnhRelocInfoRes_p = NULL;
                ranaP_EnhancedRelocationInformationResponseIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_SETUPLIST_ENHRELOCINFORES_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoRes, (void**)&ranaP_RABSetupListEnhRelocInfoRes_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupList_EnhRelocInfoRes failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoRes, ranaP_RABSetupListEnhRelocInfoRes_p);
                memcpy(&ranaP_EnhancedRelocationInformationResponseIEs->raB_SetupList_EnhRelocInfoRes, ranaP_RABSetupListEnhRelocInfoRes_p, sizeof(RANAP_RAB_SetupList_EnhRelocInfoRes_t));
                FREEMEM(ranaP_RABSetupListEnhRelocInfoRes_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAB_FailedList_EnhRelocInfoRes:
            {
                RANAP_RAB_FailedList_EnhRelocInfoRes_t *ranaP_RABFailedListEnhRelocInfoRes_p = NULL;
                ranaP_EnhancedRelocationInformationResponseIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_FAILEDLIST_ENHRELOCINFORES_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_FailedList_EnhRelocInfoRes, (void**)&ranaP_RABFailedListEnhRelocInfoRes_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_FailedList_EnhRelocInfoRes failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_FailedList_EnhRelocInfoRes, ranaP_RABFailedListEnhRelocInfoRes_p);
                memcpy(&ranaP_EnhancedRelocationInformationResponseIEs->raB_FailedList_EnhRelocInfoRes, ranaP_RABFailedListEnhRelocInfoRes_p, sizeof(RANAP_RAB_FailedList_EnhRelocInfoRes_t));
                FREEMEM(ranaP_RABFailedListEnhRelocInfoRes_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                ranaP_EnhancedRelocationInformationResponseIEs->presenceMask |= RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&ranaP_EnhancedRelocationInformationResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message ranap_enhancedrelocationinformationresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RANAP_EnhancedRelocationInformationResponse, rANAP_EnhancedRelocationInformationResponse_p);
    return decoded;
}

int ranap_decode_rab_setupitem_enhrelocinfores_ies(
    RANAP_RAB_SetupItem_EnhRelocInfoRes_IEs_t *raB_SetupItem_EnhRelocInfoRes_IEs,
    ANY_t *any_p) {

    RANAP_RAB_SetupList_EnhRelocInfoRes_t *rAB_SetupItem_EnhRelocInfoRes_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_SetupItem_EnhRelocInfoRes_IEs != NULL);

    memset(raB_SetupItem_EnhRelocInfoRes_IEs, 0, sizeof(RANAP_RAB_SetupItem_EnhRelocInfoRes_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_SetupItem_EnhRelocInfoRes_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoRes, (void**)&rAB_SetupItem_EnhRelocInfoRes_p);

    if (tempDecoded < 0 || rAB_SetupItem_EnhRelocInfoRes_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_SetupItem_EnhRelocInfoRes_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_SetupItem_EnhRelocInfoRes_p->raB_SetupList_EnhRelocInfoRes_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_SetupItem_EnhRelocInfoRes_p->raB_SetupList_EnhRelocInfoRes_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_SetupItem_EnhRelocInfoRes:
            {
                RANAP_RAB_SetupItem_EnhRelocInfoRes_t *ranaP_RABSetupItemEnhRelocInfoRes_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_SetupItem_EnhRelocInfoRes, (void**)&ranaP_RABSetupItemEnhRelocInfoRes_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_SetupItem_EnhRelocInfoRes failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_SetupItem_EnhRelocInfoRes, ranaP_RABSetupItemEnhRelocInfoRes_p);
                memcpy(&raB_SetupItem_EnhRelocInfoRes_IEs->raB_SetupItem_EnhRelocInfoRes, ranaP_RABSetupItemEnhRelocInfoRes_p, sizeof(RANAP_RAB_SetupItem_EnhRelocInfoRes_t));
                FREEMEM(ranaP_RABSetupItemEnhRelocInfoRes_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_setupitem_enhrelocinfores_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoRes, rAB_SetupItem_EnhRelocInfoRes_p);
    return decoded;
}

int ranap_decode_rab_faileditem_enhrelocinfores_ies(
    RANAP_RAB_FailedItem_EnhRelocInfoRes_IEs_t *raB_FailedItem_EnhRelocInfoRes_IEs,
    ANY_t *any_p) {

    RANAP_RAB_FailedList_EnhRelocInfoRes_t *rAB_FailedItem_EnhRelocInfoRes_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_FailedItem_EnhRelocInfoRes_IEs != NULL);

    memset(raB_FailedItem_EnhRelocInfoRes_IEs, 0, sizeof(RANAP_RAB_FailedItem_EnhRelocInfoRes_IEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_FailedItem_EnhRelocInfoRes_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_FailedList_EnhRelocInfoRes, (void**)&rAB_FailedItem_EnhRelocInfoRes_p);

    if (tempDecoded < 0 || rAB_FailedItem_EnhRelocInfoRes_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_FailedItem_EnhRelocInfoRes_IEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_FailedItem_EnhRelocInfoRes_p->raB_FailedList_EnhRelocInfoRes_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_FailedItem_EnhRelocInfoRes_p->raB_FailedList_EnhRelocInfoRes_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_FailedItem_EnhRelocInfoRes:
            {
                RANAP_RAB_FailedItem_EnhRelocInfoRes_t *ranaP_RABFailedItemEnhRelocInfoRes_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_FailedItem_EnhRelocInfoRes, (void**)&ranaP_RABFailedItemEnhRelocInfoRes_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_FailedItem_EnhRelocInfoRes failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_FailedItem_EnhRelocInfoRes, ranaP_RABFailedItemEnhRelocInfoRes_p);
                memcpy(&raB_FailedItem_EnhRelocInfoRes_IEs->raB_FailedItem_EnhRelocInfoRes, ranaP_RABFailedItemEnhRelocInfoRes_p, sizeof(RANAP_RAB_FailedItem_EnhRelocInfoRes_t));
                FREEMEM(ranaP_RABFailedItemEnhRelocInfoRes_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_faileditem_enhrelocinfores_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_FailedList_EnhRelocInfoRes, rAB_FailedItem_EnhRelocInfoRes_p);
    return decoded;
}

int ranap_decode_rab_modifyrequesties(
    RANAP_RAB_ModifyRequestIEs_t *raB_ModifyRequestIEs,
    ANY_t *any_p) {

    RANAP_RAB_ModifyRequest_t *rAB_ModifyRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_ModifyRequestIEs != NULL);

    memset(raB_ModifyRequestIEs, 0, sizeof(RANAP_RAB_ModifyRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_ModifyRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_ModifyRequest, (void**)&rAB_ModifyRequest_p);

    if (tempDecoded < 0 || rAB_ModifyRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_ModifyRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_ModifyRequest_p->raB_ModifyRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_ModifyRequest_p->raB_ModifyRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ModifyList:
            {
                RANAP_RAB_ModifyList_t *ranaP_RABModifyList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ModifyList, (void**)&ranaP_RABModifyList_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ModifyList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ModifyList, ranaP_RABModifyList_p);
                memcpy(&raB_ModifyRequestIEs->raB_ModifyList, ranaP_RABModifyList_p, sizeof(RANAP_RAB_ModifyList_t));
                FREEMEM(ranaP_RABModifyList_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_modifyrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_ModifyRequest, rAB_ModifyRequest_p);
    return decoded;
}

int ranap_decode_rab_modifyitemies(
    RANAP_RAB_ModifyItemIEs_t *raB_ModifyItemIEs,
    ANY_t *any_p) {

    RANAP_RAB_ModifyList_t *rAB_ModifyItem_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(raB_ModifyItemIEs != NULL);

    memset(raB_ModifyItemIEs, 0, sizeof(RANAP_RAB_ModifyItemIEs_t));
    RANAP_DEBUG("Decoding message RANAP_RAB_ModifyItemIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_RAB_ModifyList, (void**)&rAB_ModifyItem_p);

    if (tempDecoded < 0 || rAB_ModifyItem_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_RAB_ModifyItemIEs failed\n");
        return -1;
    }

    for (i = 0; i < rAB_ModifyItem_p->raB_ModifyList_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = rAB_ModifyItem_p->raB_ModifyList_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_RAB_ModifyItem:
            {
                RANAP_RAB_ModifyItem_t *ranaP_RABModifyItem_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_ModifyItem, (void**)&ranaP_RABModifyItem_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_ModifyItem failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_ModifyItem, ranaP_RABModifyItem_p);
                memcpy(&raB_ModifyItemIEs->raB_ModifyItem, ranaP_RABModifyItem_p, sizeof(RANAP_RAB_ModifyItem_t));
                FREEMEM(ranaP_RABModifyItem_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message rab_modifyitemies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_RAB_ModifyList, rAB_ModifyItem_p);
    return decoded;
}

int ranap_decode_locationrelateddatarequesties(
    RANAP_LocationRelatedDataRequestIEs_t *locationRelatedDataRequestIEs,
    ANY_t *any_p) {

    RANAP_LocationRelatedDataRequest_t *locationRelatedDataRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(locationRelatedDataRequestIEs != NULL);

    memset(locationRelatedDataRequestIEs, 0, sizeof(RANAP_LocationRelatedDataRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_LocationRelatedDataRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_LocationRelatedDataRequest, (void**)&locationRelatedDataRequest_p);

    if (tempDecoded < 0 || locationRelatedDataRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_LocationRelatedDataRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < locationRelatedDataRequest_p->locationRelatedDataRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = locationRelatedDataRequest_p->locationRelatedDataRequest_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_LocationRelatedDataRequestType:
            {
                RANAP_LocationRelatedDataRequestType_t *ranaP_LocationRelatedDataRequestType_p = NULL;
                locationRelatedDataRequestIEs->presenceMask |= LOCATIONRELATEDDATAREQUESTIES_RANAP_LOCATIONRELATEDDATAREQUESTTYPE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_LocationRelatedDataRequestType, (void**)&ranaP_LocationRelatedDataRequestType_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE locationRelatedDataRequestType failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_LocationRelatedDataRequestType, ranaP_LocationRelatedDataRequestType_p);
                memcpy(&locationRelatedDataRequestIEs->locationRelatedDataRequestType, ranaP_LocationRelatedDataRequestType_p, sizeof(RANAP_LocationRelatedDataRequestType_t));
                FREEMEM(ranaP_LocationRelatedDataRequestType_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message locationrelateddatarequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_LocationRelatedDataRequest, locationRelatedDataRequest_p);
    return decoded;
}

int ranap_decode_locationrelateddataresponseies(
    RANAP_LocationRelatedDataResponseIEs_t *locationRelatedDataResponseIEs,
    ANY_t *any_p) {

    RANAP_LocationRelatedDataResponse_t *locationRelatedDataResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(locationRelatedDataResponseIEs != NULL);

    memset(locationRelatedDataResponseIEs, 0, sizeof(RANAP_LocationRelatedDataResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_LocationRelatedDataResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_LocationRelatedDataResponse, (void**)&locationRelatedDataResponse_p);

    if (tempDecoded < 0 || locationRelatedDataResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_LocationRelatedDataResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < locationRelatedDataResponse_p->locationRelatedDataResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = locationRelatedDataResponse_p->locationRelatedDataResponse_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_BroadcastAssistanceDataDecipheringKeys:
            {
                RANAP_BroadcastAssistanceDataDecipheringKeys_t *ranaP_BroadcastAssistanceDataDecipheringKeys_p = NULL;
                locationRelatedDataResponseIEs->presenceMask |= LOCATIONRELATEDDATARESPONSEIES_RANAP_BROADCASTASSISTANCEDATADECIPHERINGKEYS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_BroadcastAssistanceDataDecipheringKeys, (void**)&ranaP_BroadcastAssistanceDataDecipheringKeys_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE broadcastAssistanceDataDecipheringKeys failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_BroadcastAssistanceDataDecipheringKeys, ranaP_BroadcastAssistanceDataDecipheringKeys_p);
                memcpy(&locationRelatedDataResponseIEs->broadcastAssistanceDataDecipheringKeys, ranaP_BroadcastAssistanceDataDecipheringKeys_p, sizeof(RANAP_BroadcastAssistanceDataDecipheringKeys_t));
                FREEMEM(ranaP_BroadcastAssistanceDataDecipheringKeys_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message locationrelateddataresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_LocationRelatedDataResponse, locationRelatedDataResponse_p);
    return decoded;
}

int ranap_decode_locationrelateddatafailureies(
    RANAP_LocationRelatedDataFailureIEs_t *locationRelatedDataFailureIEs,
    ANY_t *any_p) {

    RANAP_LocationRelatedDataFailure_t *locationRelatedDataFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(locationRelatedDataFailureIEs != NULL);

    memset(locationRelatedDataFailureIEs, 0, sizeof(RANAP_LocationRelatedDataFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_LocationRelatedDataFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_LocationRelatedDataFailure, (void**)&locationRelatedDataFailure_p);

    if (tempDecoded < 0 || locationRelatedDataFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_LocationRelatedDataFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < locationRelatedDataFailure_p->locationRelatedDataFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = locationRelatedDataFailure_p->locationRelatedDataFailure_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&locationRelatedDataFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message locationrelateddatafailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_LocationRelatedDataFailure, locationRelatedDataFailure_p);
    return decoded;
}

int ranap_decode_informationtransferindicationies(
    RANAP_InformationTransferIndicationIEs_t *informationTransferIndicationIEs,
    ANY_t *any_p) {

    RANAP_InformationTransferIndication_t *informationTransferIndication_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(informationTransferIndicationIEs != NULL);

    memset(informationTransferIndicationIEs, 0, sizeof(RANAP_InformationTransferIndicationIEs_t));
    RANAP_DEBUG("Decoding message RANAP_InformationTransferIndicationIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_InformationTransferIndication, (void**)&informationTransferIndication_p);

    if (tempDecoded < 0 || informationTransferIndication_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_InformationTransferIndicationIEs failed\n");
        return -1;
    }

    for (i = 0; i < informationTransferIndication_p->informationTransferIndication_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = informationTransferIndication_p->informationTransferIndication_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_InformationTransferID:
            {
                RANAP_InformationTransferID_t *ranaP_InformationTransferID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_InformationTransferID, (void**)&ranaP_InformationTransferID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE informationTransferID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_InformationTransferID, ranaP_InformationTransferID_p);
                memcpy(&informationTransferIndicationIEs->informationTransferID, ranaP_InformationTransferID_p, sizeof(RANAP_InformationTransferID_t));
                FREEMEM(ranaP_InformationTransferID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_ProvidedData:
            {
                RANAP_ProvidedData_t *ranaP_ProvidedData_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_ProvidedData, (void**)&ranaP_ProvidedData_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE providedData failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_ProvidedData, ranaP_ProvidedData_p);
                memcpy(&informationTransferIndicationIEs->providedData, ranaP_ProvidedData_p, sizeof(RANAP_ProvidedData_t));
                FREEMEM(ranaP_ProvidedData_p);
            } break;
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&informationTransferIndicationIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_ID:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                informationTransferIndicationIEs->presenceMask |= INFORMATIONTRANSFERINDICATIONIES_RANAP_GLOBALCN_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&informationTransferIndicationIEs->globalCN_ID, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message informationtransferindicationies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_InformationTransferIndication, informationTransferIndication_p);
    return decoded;
}

int ranap_decode_informationtransferconfirmationies(
    RANAP_InformationTransferConfirmationIEs_t *informationTransferConfirmationIEs,
    ANY_t *any_p) {

    RANAP_InformationTransferConfirmation_t *informationTransferConfirmation_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(informationTransferConfirmationIEs != NULL);

    memset(informationTransferConfirmationIEs, 0, sizeof(RANAP_InformationTransferConfirmationIEs_t));
    RANAP_DEBUG("Decoding message RANAP_InformationTransferConfirmationIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_InformationTransferConfirmation, (void**)&informationTransferConfirmation_p);

    if (tempDecoded < 0 || informationTransferConfirmation_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_InformationTransferConfirmationIEs failed\n");
        return -1;
    }

    for (i = 0; i < informationTransferConfirmation_p->informationTransferConfirmation_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = informationTransferConfirmation_p->informationTransferConfirmation_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_InformationTransferID:
            {
                RANAP_InformationTransferID_t *ranaP_InformationTransferID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_InformationTransferID, (void**)&ranaP_InformationTransferID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE informationTransferID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_InformationTransferID, ranaP_InformationTransferID_p);
                memcpy(&informationTransferConfirmationIEs->informationTransferID, ranaP_InformationTransferID_p, sizeof(RANAP_InformationTransferID_t));
                FREEMEM(ranaP_InformationTransferID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&informationTransferConfirmationIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                informationTransferConfirmationIEs->presenceMask |= INFORMATIONTRANSFERCONFIRMATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&informationTransferConfirmationIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&informationTransferConfirmationIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message informationtransferconfirmationies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_InformationTransferConfirmation, informationTransferConfirmation_p);
    return decoded;
}

int ranap_decode_informationtransferfailureies(
    RANAP_InformationTransferFailureIEs_t *informationTransferFailureIEs,
    ANY_t *any_p) {

    RANAP_InformationTransferFailure_t *informationTransferFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(informationTransferFailureIEs != NULL);

    memset(informationTransferFailureIEs, 0, sizeof(RANAP_InformationTransferFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_InformationTransferFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_InformationTransferFailure, (void**)&informationTransferFailure_p);

    if (tempDecoded < 0 || informationTransferFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_InformationTransferFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < informationTransferFailure_p->informationTransferFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = informationTransferFailure_p->informationTransferFailure_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_InformationTransferID:
            {
                RANAP_InformationTransferID_t *ranaP_InformationTransferID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_InformationTransferID, (void**)&ranaP_InformationTransferID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE informationTransferID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_InformationTransferID, ranaP_InformationTransferID_p);
                memcpy(&informationTransferFailureIEs->informationTransferID, ranaP_InformationTransferID_p, sizeof(RANAP_InformationTransferID_t));
                FREEMEM(ranaP_InformationTransferID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&informationTransferFailureIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&informationTransferFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                informationTransferFailureIEs->presenceMask |= INFORMATIONTRANSFERFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&informationTransferFailureIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&informationTransferFailureIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message informationtransferfailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_InformationTransferFailure, informationTransferFailure_p);
    return decoded;
}

int ranap_decode_uespecificinformationindicationies(
    RANAP_UESpecificInformationIndicationIEs_t *ueSpecificInformationIndicationIEs,
    ANY_t *any_p) {

    RANAP_UESpecificInformationIndication_t *uESpecificInformationIndication_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(ueSpecificInformationIndicationIEs != NULL);

    memset(ueSpecificInformationIndicationIEs, 0, sizeof(RANAP_UESpecificInformationIndicationIEs_t));
    RANAP_DEBUG("Decoding message RANAP_UESpecificInformationIndicationIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_UESpecificInformationIndication, (void**)&uESpecificInformationIndication_p);

    if (tempDecoded < 0 || uESpecificInformationIndication_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_UESpecificInformationIndicationIEs failed\n");
        return -1;
    }

    for (i = 0; i < uESpecificInformationIndication_p->ueSpecificInformationIndication_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = uESpecificInformationIndication_p->ueSpecificInformationIndication_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_UESBI_Iu:
            {
                RANAP_UESBI_Iu_t *ranaP_UESBIIu_p = NULL;
                ueSpecificInformationIndicationIEs->presenceMask |= UESPECIFICINFORMATIONINDICATIONIES_RANAP_UESBI_IU_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_UESBI_Iu, (void**)&ranaP_UESBIIu_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE uesbI_Iu failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_UESBI_Iu, ranaP_UESBIIu_p);
                memcpy(&ueSpecificInformationIndicationIEs->uesbI_Iu, ranaP_UESBIIu_p, sizeof(RANAP_UESBI_Iu_t));
                FREEMEM(ranaP_UESBIIu_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message uespecificinformationindicationies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_UESpecificInformationIndication, uESpecificInformationIndication_p);
    return decoded;
}

int ranap_decode_directinformationtransferies(
    RANAP_DirectInformationTransferIEs_t *directInformationTransferIEs,
    ANY_t *any_p) {

    RANAP_DirectInformationTransfer_t *directInformationTransfer_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(directInformationTransferIEs != NULL);

    memset(directInformationTransferIEs, 0, sizeof(RANAP_DirectInformationTransferIEs_t));
    RANAP_DEBUG("Decoding message RANAP_DirectInformationTransferIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_DirectInformationTransfer, (void**)&directInformationTransfer_p);

    if (tempDecoded < 0 || directInformationTransfer_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_DirectInformationTransferIEs failed\n");
        return -1;
    }

    for (i = 0; i < directInformationTransfer_p->directInformationTransfer_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = directInformationTransfer_p->directInformationTransfer_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_InterSystemInformationTransferType:
            {
                RANAP_InterSystemInformationTransferType_t *ranaP_InterSystemInformationTransferType_p = NULL;
                directInformationTransferIEs->presenceMask |= DIRECTINFORMATIONTRANSFERIES_RANAP_INTERSYSTEMINFORMATIONTRANSFERTYPE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_InterSystemInformationTransferType, (void**)&ranaP_InterSystemInformationTransferType_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE interSystemInformationTransferType failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_InterSystemInformationTransferType, ranaP_InterSystemInformationTransferType_p);
                memcpy(&directInformationTransferIEs->interSystemInformationTransferType, ranaP_InterSystemInformationTransferType_p, sizeof(RANAP_InterSystemInformationTransferType_t));
                FREEMEM(ranaP_InterSystemInformationTransferType_p);
            } break;
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&directInformationTransferIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                directInformationTransferIEs->presenceMask |= DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&directInformationTransferIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_ID:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                directInformationTransferIEs->presenceMask |= DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALCN_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&directInformationTransferIEs->globalCN_ID, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message directinformationtransferies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_DirectInformationTransfer, directInformationTransfer_p);
    return decoded;
}

int ranap_decode_uplinkinformationexchangerequesties(
    RANAP_UplinkInformationExchangeRequestIEs_t *uplinkInformationExchangeRequestIEs,
    ANY_t *any_p) {

    RANAP_UplinkInformationExchangeRequest_t *uplinkInformationExchangeRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(uplinkInformationExchangeRequestIEs != NULL);

    memset(uplinkInformationExchangeRequestIEs, 0, sizeof(RANAP_UplinkInformationExchangeRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_UplinkInformationExchangeRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_UplinkInformationExchangeRequest, (void**)&uplinkInformationExchangeRequest_p);

    if (tempDecoded < 0 || uplinkInformationExchangeRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_UplinkInformationExchangeRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < uplinkInformationExchangeRequest_p->uplinkInformationExchangeRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = uplinkInformationExchangeRequest_p->uplinkInformationExchangeRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_InformationExchangeID:
            {
                RANAP_InformationExchangeID_t *ranaP_InformationExchangeID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_InformationExchangeID, (void**)&ranaP_InformationExchangeID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE informationExchangeID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_InformationExchangeID, ranaP_InformationExchangeID_p);
                memcpy(&uplinkInformationExchangeRequestIEs->informationExchangeID, ranaP_InformationExchangeID_p, sizeof(RANAP_InformationExchangeID_t));
                FREEMEM(ranaP_InformationExchangeID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_InformationExchangeType:
            {
                RANAP_InformationExchangeType_t *ranaP_InformationExchangeType_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_InformationExchangeType, (void**)&ranaP_InformationExchangeType_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE informationExchangeType failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_InformationExchangeType, ranaP_InformationExchangeType_p);
                memcpy(&uplinkInformationExchangeRequestIEs->informationExchangeType, ranaP_InformationExchangeType_p, sizeof(RANAP_InformationExchangeType_t));
                FREEMEM(ranaP_InformationExchangeType_p);
            } break;
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&uplinkInformationExchangeRequestIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&uplinkInformationExchangeRequestIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message uplinkinformationexchangerequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_UplinkInformationExchangeRequest, uplinkInformationExchangeRequest_p);
    return decoded;
}

int ranap_decode_uplinkinformationexchangeresponseies(
    RANAP_UplinkInformationExchangeResponseIEs_t *uplinkInformationExchangeResponseIEs,
    ANY_t *any_p) {

    RANAP_UplinkInformationExchangeResponse_t *uplinkInformationExchangeResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(uplinkInformationExchangeResponseIEs != NULL);

    memset(uplinkInformationExchangeResponseIEs, 0, sizeof(RANAP_UplinkInformationExchangeResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_UplinkInformationExchangeResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_UplinkInformationExchangeResponse, (void**)&uplinkInformationExchangeResponse_p);

    if (tempDecoded < 0 || uplinkInformationExchangeResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_UplinkInformationExchangeResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < uplinkInformationExchangeResponse_p->uplinkInformationExchangeResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = uplinkInformationExchangeResponse_p->uplinkInformationExchangeResponse_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_InformationExchangeID:
            {
                RANAP_InformationExchangeID_t *ranaP_InformationExchangeID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_InformationExchangeID, (void**)&ranaP_InformationExchangeID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE informationExchangeID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_InformationExchangeID, ranaP_InformationExchangeID_p);
                memcpy(&uplinkInformationExchangeResponseIEs->informationExchangeID, ranaP_InformationExchangeID_p, sizeof(RANAP_InformationExchangeID_t));
                FREEMEM(ranaP_InformationExchangeID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_InformationRequested:
            {
                RANAP_InformationRequested_t *ranaP_InformationRequested_p = NULL;
                uplinkInformationExchangeResponseIEs->presenceMask |= UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_INFORMATIONREQUESTED_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_InformationRequested, (void**)&ranaP_InformationRequested_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE informationRequested failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_InformationRequested, ranaP_InformationRequested_p);
                memcpy(&uplinkInformationExchangeResponseIEs->informationRequested, ranaP_InformationRequested_p, sizeof(RANAP_InformationRequested_t));
                FREEMEM(ranaP_InformationRequested_p);
            } break;
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&uplinkInformationExchangeResponseIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_ID:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                uplinkInformationExchangeResponseIEs->presenceMask |= UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_GLOBALCN_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&uplinkInformationExchangeResponseIEs->globalCN_ID, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                uplinkInformationExchangeResponseIEs->presenceMask |= UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&uplinkInformationExchangeResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message uplinkinformationexchangeresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_UplinkInformationExchangeResponse, uplinkInformationExchangeResponse_p);
    return decoded;
}

int ranap_decode_uplinkinformationexchangefailureies(
    RANAP_UplinkInformationExchangeFailureIEs_t *uplinkInformationExchangeFailureIEs,
    ANY_t *any_p) {

    RANAP_UplinkInformationExchangeFailure_t *uplinkInformationExchangeFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(uplinkInformationExchangeFailureIEs != NULL);

    memset(uplinkInformationExchangeFailureIEs, 0, sizeof(RANAP_UplinkInformationExchangeFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_UplinkInformationExchangeFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_UplinkInformationExchangeFailure, (void**)&uplinkInformationExchangeFailure_p);

    if (tempDecoded < 0 || uplinkInformationExchangeFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_UplinkInformationExchangeFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < uplinkInformationExchangeFailure_p->uplinkInformationExchangeFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = uplinkInformationExchangeFailure_p->uplinkInformationExchangeFailure_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_InformationExchangeID:
            {
                RANAP_InformationExchangeID_t *ranaP_InformationExchangeID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_InformationExchangeID, (void**)&ranaP_InformationExchangeID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE informationExchangeID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_InformationExchangeID, ranaP_InformationExchangeID_p);
                memcpy(&uplinkInformationExchangeFailureIEs->informationExchangeID, ranaP_InformationExchangeID_p, sizeof(RANAP_InformationExchangeID_t));
                FREEMEM(ranaP_InformationExchangeID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RANAP_CN_DomainIndicator_t *ranaP_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CN_DomainIndicator, (void**)&ranaP_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CN_DomainIndicator, ranaP_CNDomainIndicator_p);
                memcpy(&uplinkInformationExchangeFailureIEs->cN_DomainIndicator, ranaP_CNDomainIndicator_p, sizeof(RANAP_CN_DomainIndicator_t));
                FREEMEM(ranaP_CNDomainIndicator_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_ID:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                uplinkInformationExchangeFailureIEs->presenceMask |= UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_GLOBALCN_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&uplinkInformationExchangeFailureIEs->globalCN_ID, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&uplinkInformationExchangeFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                uplinkInformationExchangeFailureIEs->presenceMask |= UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&uplinkInformationExchangeFailureIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message uplinkinformationexchangefailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_UplinkInformationExchangeFailure, uplinkInformationExchangeFailure_p);
    return decoded;
}

int ranap_decode_mbmssessionstarties(
    RANAP_MBMSSessionStartIEs_t *mbmsSessionStartIEs,
    ANY_t *any_p) {

    RANAP_MBMSSessionStart_t *mBMSSessionStart_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsSessionStartIEs != NULL);

    memset(mbmsSessionStartIEs, 0, sizeof(RANAP_MBMSSessionStartIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSSessionStartIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSSessionStart, (void**)&mBMSSessionStart_p);

    if (tempDecoded < 0 || mBMSSessionStart_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSSessionStartIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSSessionStart_p->mbmsSessionStart_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSSessionStart_p->mbmsSessionStart_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_TMGI:
            {
                RANAP_TMGI_t *ranap_tmgi_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TMGI, (void**)&ranap_tmgi_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE tmgi failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TMGI, ranap_tmgi_p);
                memcpy(&mbmsSessionStartIEs->tmgi, ranap_tmgi_p, sizeof(RANAP_TMGI_t));
                FREEMEM(ranap_tmgi_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_MBMSSessionIdentity:
            {
                RANAP_MBMSSessionIdentity_t *ranaP_MBMSSessionIdentity_p = NULL;
                mbmsSessionStartIEs->presenceMask |= MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONIDENTITY_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_MBMSSessionIdentity, (void**)&ranaP_MBMSSessionIdentity_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE mbmsSessionIdentity failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_MBMSSessionIdentity, ranaP_MBMSSessionIdentity_p);
                memcpy(&mbmsSessionStartIEs->mbmsSessionIdentity, ranaP_MBMSSessionIdentity_p, sizeof(RANAP_MBMSSessionIdentity_t));
                FREEMEM(ranaP_MBMSSessionIdentity_p);
            } break;
            case RANAP_ProtocolIE_ID_id_MBMSBearerServiceType:
            {
                RANAP_MBMSBearerServiceType_t *ranaP_MBMSBearerServiceType_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_MBMSBearerServiceType, (void**)&ranaP_MBMSBearerServiceType_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE mbmsBearerServiceType failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_MBMSBearerServiceType, ranaP_MBMSBearerServiceType_p);
                memcpy(&mbmsSessionStartIEs->mbmsBearerServiceType, ranaP_MBMSBearerServiceType_p, sizeof(RANAP_MBMSBearerServiceType_t));
                FREEMEM(ranaP_MBMSBearerServiceType_p);
            } break;
            case RANAP_ProtocolIE_ID_id_IuSigConId:
            {
                RANAP_IuSignallingConnectionIdentifier_t *ranaP_IuSignallingConnectionIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, (void**)&ranaP_IuSignallingConnectionIdentifier_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE iuSigConId failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IuSignallingConnectionIdentifier, ranaP_IuSignallingConnectionIdentifier_p);
                memcpy(&mbmsSessionStartIEs->iuSigConId, ranaP_IuSignallingConnectionIdentifier_p, sizeof(RANAP_IuSignallingConnectionIdentifier_t));
                FREEMEM(ranaP_IuSignallingConnectionIdentifier_p);
            } break;
            case RANAP_ProtocolIE_ID_id_RAB_Parameters:
            {
                RANAP_RAB_Parameters_t *ranaP_RABParameters_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAB_Parameters, (void**)&ranaP_RABParameters_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raB_Parameters failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAB_Parameters, ranaP_RABParameters_p);
                memcpy(&mbmsSessionStartIEs->raB_Parameters, ranaP_RABParameters_p, sizeof(RANAP_RAB_Parameters_t));
                FREEMEM(ranaP_RABParameters_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_PDP_TypeInformation:
            {
                RANAP_PDP_TypeInformation_t *ranaP_PDPTypeInformation_p = NULL;
                mbmsSessionStartIEs->presenceMask |= MBMSSESSIONSTARTIES_RANAP_PDP_TYPEINFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_PDP_TypeInformation, (void**)&ranaP_PDPTypeInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE pdP_TypeInformation failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_PDP_TypeInformation, ranaP_PDPTypeInformation_p);
                memcpy(&mbmsSessionStartIEs->pdP_TypeInformation, ranaP_PDPTypeInformation_p, sizeof(RANAP_PDP_TypeInformation_t));
                FREEMEM(ranaP_PDPTypeInformation_p);
            } break;
            case RANAP_ProtocolIE_ID_id_MBMSSessionDuration:
            {
                RANAP_MBMSSessionDuration_t *ranaP_MBMSSessionDuration_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_MBMSSessionDuration, (void**)&ranaP_MBMSSessionDuration_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE mbmsSessionDuration failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_MBMSSessionDuration, ranaP_MBMSSessionDuration_p);
                memcpy(&mbmsSessionStartIEs->mbmsSessionDuration, ranaP_MBMSSessionDuration_p, sizeof(RANAP_MBMSSessionDuration_t));
                FREEMEM(ranaP_MBMSSessionDuration_p);
            } break;
            case RANAP_ProtocolIE_ID_id_MBMSServiceArea:
            {
                RANAP_MBMSServiceArea_t *ranaP_MBMSServiceArea_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_MBMSServiceArea, (void**)&ranaP_MBMSServiceArea_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE mbmsServiceArea failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_MBMSServiceArea, ranaP_MBMSServiceArea_p);
                memcpy(&mbmsSessionStartIEs->mbmsServiceArea, ranaP_MBMSServiceArea_p, sizeof(RANAP_MBMSServiceArea_t));
                FREEMEM(ranaP_MBMSServiceArea_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_FrequenceLayerConvergenceFlag:
            {
                RANAP_FrequenceLayerConvergenceFlag_t *ranaP_FrequenceLayerConvergenceFlag_p = NULL;
                mbmsSessionStartIEs->presenceMask |= MBMSSESSIONSTARTIES_RANAP_FREQUENCELAYERCONVERGENCEFLAG_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_FrequenceLayerConvergenceFlag, (void**)&ranaP_FrequenceLayerConvergenceFlag_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE frequenceLayerConvergenceFlag failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_FrequenceLayerConvergenceFlag, ranaP_FrequenceLayerConvergenceFlag_p);
                memcpy(&mbmsSessionStartIEs->frequenceLayerConvergenceFlag, ranaP_FrequenceLayerConvergenceFlag_p, sizeof(RANAP_FrequenceLayerConvergenceFlag_t));
                FREEMEM(ranaP_FrequenceLayerConvergenceFlag_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_RAListofIdleModeUEs:
            {
                RANAP_RAListofIdleModeUEs_t *ranaP_RAListofIdleModeUEs_p = NULL;
                mbmsSessionStartIEs->presenceMask |= MBMSSESSIONSTARTIES_RANAP_RALISTOFIDLEMODEUES_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_RAListofIdleModeUEs, (void**)&ranaP_RAListofIdleModeUEs_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE raListofIdleModeUEs failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_RAListofIdleModeUEs, ranaP_RAListofIdleModeUEs_p);
                memcpy(&mbmsSessionStartIEs->raListofIdleModeUEs, ranaP_RAListofIdleModeUEs_p, sizeof(RANAP_RAListofIdleModeUEs_t));
                FREEMEM(ranaP_RAListofIdleModeUEs_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_ID:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                mbmsSessionStartIEs->presenceMask |= MBMSSESSIONSTARTIES_RANAP_GLOBALCN_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&mbmsSessionStartIEs->globalCN_ID, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_MBMSSessionRepetitionNumber:
            {
                RANAP_MBMSSessionRepetitionNumber_t *ranaP_MBMSSessionRepetitionNumber_p = NULL;
                mbmsSessionStartIEs->presenceMask |= MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONREPETITIONNUMBER_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_MBMSSessionRepetitionNumber, (void**)&ranaP_MBMSSessionRepetitionNumber_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE mbmsSessionRepetitionNumber failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_MBMSSessionRepetitionNumber, ranaP_MBMSSessionRepetitionNumber_p);
                memcpy(&mbmsSessionStartIEs->mbmsSessionRepetitionNumber, ranaP_MBMSSessionRepetitionNumber_p, sizeof(RANAP_MBMSSessionRepetitionNumber_t));
                FREEMEM(ranaP_MBMSSessionRepetitionNumber_p);
            } break;
            case RANAP_ProtocolIE_ID_id_TimeToMBMSDataTransfer:
            {
                RANAP_TimeToMBMSDataTransfer_t *ranaP_TimeToMBMSDataTransfer_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TimeToMBMSDataTransfer, (void**)&ranaP_TimeToMBMSDataTransfer_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE timeToMBMSDataTransfer failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TimeToMBMSDataTransfer, ranaP_TimeToMBMSDataTransfer_p);
                memcpy(&mbmsSessionStartIEs->timeToMBMSDataTransfer, ranaP_TimeToMBMSDataTransfer_p, sizeof(RANAP_TimeToMBMSDataTransfer_t));
                FREEMEM(ranaP_TimeToMBMSDataTransfer_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmssessionstarties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSSessionStart, mBMSSessionStart_p);
    return decoded;
}

int ranap_decode_mbmssessionstartresponseies(
    RANAP_MBMSSessionStartResponseIEs_t *mbmsSessionStartResponseIEs,
    ANY_t *any_p) {

    RANAP_MBMSSessionStartResponse_t *mBMSSessionStartResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsSessionStartResponseIEs != NULL);

    memset(mbmsSessionStartResponseIEs, 0, sizeof(RANAP_MBMSSessionStartResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSSessionStartResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSSessionStartResponse, (void**)&mBMSSessionStartResponse_p);

    if (tempDecoded < 0 || mBMSSessionStartResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSSessionStartResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSSessionStartResponse_p->mbmsSessionStartResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSSessionStartResponse_p->mbmsSessionStartResponse_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_TransportLayerInformation:
            {
                RANAP_TransportLayerInformation_t *ranaP_TransportLayerInformation_p = NULL;
                mbmsSessionStartResponseIEs->presenceMask |= MBMSSESSIONSTARTRESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TransportLayerInformation, (void**)&ranaP_TransportLayerInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE transportLayerInformation failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TransportLayerInformation, ranaP_TransportLayerInformation_p);
                memcpy(&mbmsSessionStartResponseIEs->transportLayerInformation, ranaP_TransportLayerInformation_p, sizeof(RANAP_TransportLayerInformation_t));
                FREEMEM(ranaP_TransportLayerInformation_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                mbmsSessionStartResponseIEs->presenceMask |= MBMSSESSIONSTARTRESPONSEIES_RANAP_CAUSE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmsSessionStartResponseIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsSessionStartResponseIEs->presenceMask |= MBMSSESSIONSTARTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsSessionStartResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmssessionstartresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSSessionStartResponse, mBMSSessionStartResponse_p);
    return decoded;
}

int ranap_decode_mbmssessionstartfailureies(
    RANAP_MBMSSessionStartFailureIEs_t *mbmsSessionStartFailureIEs,
    ANY_t *any_p) {

    RANAP_MBMSSessionStartFailure_t *mBMSSessionStartFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsSessionStartFailureIEs != NULL);

    memset(mbmsSessionStartFailureIEs, 0, sizeof(RANAP_MBMSSessionStartFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSSessionStartFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSSessionStartFailure, (void**)&mBMSSessionStartFailure_p);

    if (tempDecoded < 0 || mBMSSessionStartFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSSessionStartFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSSessionStartFailure_p->mbmsSessionStartFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSSessionStartFailure_p->mbmsSessionStartFailure_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmsSessionStartFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsSessionStartFailureIEs->presenceMask |= MBMSSESSIONSTARTFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsSessionStartFailureIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmssessionstartfailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSSessionStartFailure, mBMSSessionStartFailure_p);
    return decoded;
}

int ranap_decode_mbmssessionupdateies(
    RANAP_MBMSSessionUpdateIEs_t *mbmsSessionUpdateIEs,
    ANY_t *any_p) {

    RANAP_MBMSSessionUpdate_t *mBMSSessionUpdate_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsSessionUpdateIEs != NULL);

    memset(mbmsSessionUpdateIEs, 0, sizeof(RANAP_MBMSSessionUpdateIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSSessionUpdateIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSSessionUpdate, (void**)&mBMSSessionUpdate_p);

    if (tempDecoded < 0 || mBMSSessionUpdate_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSSessionUpdateIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSSessionUpdate_p->mbmsSessionUpdate_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSSessionUpdate_p->mbmsSessionUpdate_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_SessionUpdateID:
            {
                RANAP_SessionUpdateID_t *ranaP_SessionUpdateID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SessionUpdateID, (void**)&ranaP_SessionUpdateID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE sessionUpdateID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SessionUpdateID, ranaP_SessionUpdateID_p);
                memcpy(&mbmsSessionUpdateIEs->sessionUpdateID, ranaP_SessionUpdateID_p, sizeof(RANAP_SessionUpdateID_t));
                FREEMEM(ranaP_SessionUpdateID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_DeltaRAListofIdleModeUEs:
            {
                RANAP_DeltaRAListofIdleModeUEs_t *ranaP_DeltaRAListofIdleModeUEs_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_DeltaRAListofIdleModeUEs, (void**)&ranaP_DeltaRAListofIdleModeUEs_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE deltaRAListofIdleModeUEs failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_DeltaRAListofIdleModeUEs, ranaP_DeltaRAListofIdleModeUEs_p);
                memcpy(&mbmsSessionUpdateIEs->deltaRAListofIdleModeUEs, ranaP_DeltaRAListofIdleModeUEs_p, sizeof(RANAP_DeltaRAListofIdleModeUEs_t));
                FREEMEM(ranaP_DeltaRAListofIdleModeUEs_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmssessionupdateies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSSessionUpdate, mBMSSessionUpdate_p);
    return decoded;
}

int ranap_decode_mbmssessionupdateresponseies(
    RANAP_MBMSSessionUpdateResponseIEs_t *mbmsSessionUpdateResponseIEs,
    ANY_t *any_p) {

    RANAP_MBMSSessionUpdateResponse_t *mBMSSessionUpdateResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsSessionUpdateResponseIEs != NULL);

    memset(mbmsSessionUpdateResponseIEs, 0, sizeof(RANAP_MBMSSessionUpdateResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSSessionUpdateResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSSessionUpdateResponse, (void**)&mBMSSessionUpdateResponse_p);

    if (tempDecoded < 0 || mBMSSessionUpdateResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSSessionUpdateResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSSessionUpdateResponse_p->mbmsSessionUpdateResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSSessionUpdateResponse_p->mbmsSessionUpdateResponse_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_SessionUpdateID:
            {
                RANAP_SessionUpdateID_t *ranaP_SessionUpdateID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SessionUpdateID, (void**)&ranaP_SessionUpdateID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE sessionUpdateID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SessionUpdateID, ranaP_SessionUpdateID_p);
                memcpy(&mbmsSessionUpdateResponseIEs->sessionUpdateID, ranaP_SessionUpdateID_p, sizeof(RANAP_SessionUpdateID_t));
                FREEMEM(ranaP_SessionUpdateID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_TransportLayerInformation:
            {
                RANAP_TransportLayerInformation_t *ranaP_TransportLayerInformation_p = NULL;
                mbmsSessionUpdateResponseIEs->presenceMask |= MBMSSESSIONUPDATERESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TransportLayerInformation, (void**)&ranaP_TransportLayerInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE transportLayerInformation failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TransportLayerInformation, ranaP_TransportLayerInformation_p);
                memcpy(&mbmsSessionUpdateResponseIEs->transportLayerInformation, ranaP_TransportLayerInformation_p, sizeof(RANAP_TransportLayerInformation_t));
                FREEMEM(ranaP_TransportLayerInformation_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                mbmsSessionUpdateResponseIEs->presenceMask |= MBMSSESSIONUPDATERESPONSEIES_RANAP_CAUSE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmsSessionUpdateResponseIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsSessionUpdateResponseIEs->presenceMask |= MBMSSESSIONUPDATERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsSessionUpdateResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmssessionupdateresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSSessionUpdateResponse, mBMSSessionUpdateResponse_p);
    return decoded;
}

int ranap_decode_mbmssessionupdatefailureies(
    RANAP_MBMSSessionUpdateFailureIEs_t *mbmsSessionUpdateFailureIEs,
    ANY_t *any_p) {

    RANAP_MBMSSessionUpdateFailure_t *mBMSSessionUpdateFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsSessionUpdateFailureIEs != NULL);

    memset(mbmsSessionUpdateFailureIEs, 0, sizeof(RANAP_MBMSSessionUpdateFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSSessionUpdateFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSSessionUpdateFailure, (void**)&mBMSSessionUpdateFailure_p);

    if (tempDecoded < 0 || mBMSSessionUpdateFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSSessionUpdateFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSSessionUpdateFailure_p->mbmsSessionUpdateFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSSessionUpdateFailure_p->mbmsSessionUpdateFailure_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_SessionUpdateID:
            {
                RANAP_SessionUpdateID_t *ranaP_SessionUpdateID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SessionUpdateID, (void**)&ranaP_SessionUpdateID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE sessionUpdateID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SessionUpdateID, ranaP_SessionUpdateID_p);
                memcpy(&mbmsSessionUpdateFailureIEs->sessionUpdateID, ranaP_SessionUpdateID_p, sizeof(RANAP_SessionUpdateID_t));
                FREEMEM(ranaP_SessionUpdateID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmsSessionUpdateFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsSessionUpdateFailureIEs->presenceMask |= MBMSSESSIONUPDATEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsSessionUpdateFailureIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmssessionupdatefailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSSessionUpdateFailure, mBMSSessionUpdateFailure_p);
    return decoded;
}

int ranap_decode_mbmssessionstopies(
    RANAP_MBMSSessionStopIEs_t *mbmsSessionStopIEs,
    ANY_t *any_p) {

    RANAP_MBMSSessionStop_t *mBMSSessionStop_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsSessionStopIEs != NULL);

    memset(mbmsSessionStopIEs, 0, sizeof(RANAP_MBMSSessionStopIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSSessionStopIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSSessionStop, (void**)&mBMSSessionStop_p);

    if (tempDecoded < 0 || mBMSSessionStop_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSSessionStopIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSSessionStop_p->mbmsSessionStop_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSSessionStop_p->mbmsSessionStop_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_MBMSCNDe_Registration:
            {
                RANAP_MBMSCNDe_Registration_t *ranaP_MBMSCNDeRegistration_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_MBMSCNDe_Registration, (void**)&ranaP_MBMSCNDeRegistration_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE mbmscnDe_Registration failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_MBMSCNDe_Registration, ranaP_MBMSCNDeRegistration_p);
                memcpy(&mbmsSessionStopIEs->mbmscnDe_Registration, ranaP_MBMSCNDeRegistration_p, sizeof(RANAP_MBMSCNDe_Registration_t));
                FREEMEM(ranaP_MBMSCNDeRegistration_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmssessionstopies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSSessionStop, mBMSSessionStop_p);
    return decoded;
}

int ranap_decode_mbmssessionstopresponseies(
    RANAP_MBMSSessionStopResponseIEs_t *mbmsSessionStopResponseIEs,
    ANY_t *any_p) {

    RANAP_MBMSSessionStopResponse_t *mBMSSessionStopResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsSessionStopResponseIEs != NULL);

    memset(mbmsSessionStopResponseIEs, 0, sizeof(RANAP_MBMSSessionStopResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSSessionStopResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSSessionStopResponse, (void**)&mBMSSessionStopResponse_p);

    if (tempDecoded < 0 || mBMSSessionStopResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSSessionStopResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSSessionStopResponse_p->mbmsSessionStopResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSSessionStopResponse_p->mbmsSessionStopResponse_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                mbmsSessionStopResponseIEs->presenceMask |= MBMSSESSIONSTOPRESPONSEIES_RANAP_CAUSE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmsSessionStopResponseIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsSessionStopResponseIEs->presenceMask |= MBMSSESSIONSTOPRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsSessionStopResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmssessionstopresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSSessionStopResponse, mBMSSessionStopResponse_p);
    return decoded;
}

int ranap_decode_mbmsuelinkingrequesties(
    RANAP_MBMSUELinkingRequestIEs_t *mbmsueLinkingRequestIEs,
    ANY_t *any_p) {

    RANAP_MBMSUELinkingRequest_t *mBMSUELinkingRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsueLinkingRequestIEs != NULL);

    memset(mbmsueLinkingRequestIEs, 0, sizeof(RANAP_MBMSUELinkingRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSUELinkingRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSUELinkingRequest, (void**)&mBMSUELinkingRequest_p);

    if (tempDecoded < 0 || mBMSUELinkingRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSUELinkingRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSUELinkingRequest_p->mbmsueLinkingRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSUELinkingRequest_p->mbmsueLinkingRequest_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_JoinedMBMSBearerServicesList:
            {
                RANAP_JoinedMBMSBearerService_IEs_t *ranaP_JoinedMBMSBearerServiceIEs_p = NULL;
                mbmsueLinkingRequestIEs->presenceMask |= MBMSUELINKINGREQUESTIES_RANAP_JOINEDMBMSBEARERSERVICE_IES_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_JoinedMBMSBearerService_IEs, (void**)&ranaP_JoinedMBMSBearerServiceIEs_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE joinedMBMSBearerServicesList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_JoinedMBMSBearerService_IEs, ranaP_JoinedMBMSBearerServiceIEs_p);
                memcpy(&mbmsueLinkingRequestIEs->joinedMBMSBearerServicesList, ranaP_JoinedMBMSBearerServiceIEs_p, sizeof(RANAP_JoinedMBMSBearerService_IEs_t));
                FREEMEM(ranaP_JoinedMBMSBearerServiceIEs_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_LeftMBMSBearerServicesList:
            {
                RANAP_LeftMBMSBearerService_IEs_t *ranaP_LeftMBMSBearerServiceIEs_p = NULL;
                mbmsueLinkingRequestIEs->presenceMask |= MBMSUELINKINGREQUESTIES_RANAP_LEFTMBMSBEARERSERVICE_IES_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_LeftMBMSBearerService_IEs, (void**)&ranaP_LeftMBMSBearerServiceIEs_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE leftMBMSBearerServicesList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_LeftMBMSBearerService_IEs, ranaP_LeftMBMSBearerServiceIEs_p);
                memcpy(&mbmsueLinkingRequestIEs->leftMBMSBearerServicesList, ranaP_LeftMBMSBearerServiceIEs_p, sizeof(RANAP_LeftMBMSBearerService_IEs_t));
                FREEMEM(ranaP_LeftMBMSBearerServiceIEs_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmsuelinkingrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSUELinkingRequest, mBMSUELinkingRequest_p);
    return decoded;
}

int ranap_decode_mbmsuelinkingresponseies(
    RANAP_MBMSUELinkingResponseIEs_t *mbmsueLinkingResponseIEs,
    ANY_t *any_p) {

    RANAP_MBMSUELinkingResponse_t *mBMSUELinkingResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsueLinkingResponseIEs != NULL);

    memset(mbmsueLinkingResponseIEs, 0, sizeof(RANAP_MBMSUELinkingResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSUELinkingResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSUELinkingResponse, (void**)&mBMSUELinkingResponse_p);

    if (tempDecoded < 0 || mBMSUELinkingResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSUELinkingResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSUELinkingResponse_p->mbmsueLinkingResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSUELinkingResponse_p->mbmsueLinkingResponse_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_UnsuccessfulLinkingList:
            {
                RANAP_UnsuccessfulLinking_IEs_t *ranaP_UnsuccessfulLinkingIEs_p = NULL;
                mbmsueLinkingResponseIEs->presenceMask |= MBMSUELINKINGRESPONSEIES_RANAP_UNSUCCESSFULLINKING_IES_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_UnsuccessfulLinking_IEs, (void**)&ranaP_UnsuccessfulLinkingIEs_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE unsuccessfulLinkingList failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_UnsuccessfulLinking_IEs, ranaP_UnsuccessfulLinkingIEs_p);
                memcpy(&mbmsueLinkingResponseIEs->unsuccessfulLinkingList, ranaP_UnsuccessfulLinkingIEs_p, sizeof(RANAP_UnsuccessfulLinking_IEs_t));
                FREEMEM(ranaP_UnsuccessfulLinkingIEs_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsueLinkingResponseIEs->presenceMask |= MBMSUELINKINGRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsueLinkingResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmsuelinkingresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSUELinkingResponse, mBMSUELinkingResponse_p);
    return decoded;
}

int ranap_decode_mbmsregistrationrequesties(
    RANAP_MBMSRegistrationRequestIEs_t *mbmsRegistrationRequestIEs,
    ANY_t *any_p) {

    RANAP_MBMSRegistrationRequest_t *mBMSRegistrationRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsRegistrationRequestIEs != NULL);

    memset(mbmsRegistrationRequestIEs, 0, sizeof(RANAP_MBMSRegistrationRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSRegistrationRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSRegistrationRequest, (void**)&mBMSRegistrationRequest_p);

    if (tempDecoded < 0 || mBMSRegistrationRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSRegistrationRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSRegistrationRequest_p->mbmsRegistrationRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSRegistrationRequest_p->mbmsRegistrationRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_MBMSRegistrationRequestType:
            {
                RANAP_MBMSRegistrationRequestType_t *ranaP_MBMSRegistrationRequestType_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_MBMSRegistrationRequestType, (void**)&ranaP_MBMSRegistrationRequestType_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE mbmsRegistrationRequestType failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_MBMSRegistrationRequestType, ranaP_MBMSRegistrationRequestType_p);
                memcpy(&mbmsRegistrationRequestIEs->mbmsRegistrationRequestType, ranaP_MBMSRegistrationRequestType_p, sizeof(RANAP_MBMSRegistrationRequestType_t));
                FREEMEM(ranaP_MBMSRegistrationRequestType_p);
            } break;
            case RANAP_ProtocolIE_ID_id_TMGI:
            {
                RANAP_TMGI_t *ranap_tmgi_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TMGI, (void**)&ranap_tmgi_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE tmgi failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TMGI, ranap_tmgi_p);
                memcpy(&mbmsRegistrationRequestIEs->tmgi, ranap_tmgi_p, sizeof(RANAP_TMGI_t));
                FREEMEM(ranap_tmgi_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                mbmsRegistrationRequestIEs->presenceMask |= MBMSREGISTRATIONREQUESTIES_RANAP_GLOBALRNC_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&mbmsRegistrationRequestIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmsregistrationrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSRegistrationRequest, mBMSRegistrationRequest_p);
    return decoded;
}

int ranap_decode_mbmsregistrationresponseies(
    RANAP_MBMSRegistrationResponseIEs_t *mbmsRegistrationResponseIEs,
    ANY_t *any_p) {

    RANAP_MBMSRegistrationResponse_t *mBMSRegistrationResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsRegistrationResponseIEs != NULL);

    memset(mbmsRegistrationResponseIEs, 0, sizeof(RANAP_MBMSRegistrationResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSRegistrationResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSRegistrationResponse, (void**)&mBMSRegistrationResponse_p);

    if (tempDecoded < 0 || mBMSRegistrationResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSRegistrationResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSRegistrationResponse_p->mbmsRegistrationResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSRegistrationResponse_p->mbmsRegistrationResponse_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_TMGI:
            {
                RANAP_TMGI_t *ranap_tmgi_p = NULL;
                mbmsRegistrationResponseIEs->presenceMask |= MBMSREGISTRATIONRESPONSEIES_RANAP_TMGI_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TMGI, (void**)&ranap_tmgi_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE tmgi failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TMGI, ranap_tmgi_p);
                memcpy(&mbmsRegistrationResponseIEs->tmgi, ranap_tmgi_p, sizeof(RANAP_TMGI_t));
                FREEMEM(ranap_tmgi_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_ID:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                mbmsRegistrationResponseIEs->presenceMask |= MBMSREGISTRATIONRESPONSEIES_RANAP_GLOBALCN_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&mbmsRegistrationResponseIEs->globalCN_ID, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsRegistrationResponseIEs->presenceMask |= MBMSREGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsRegistrationResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmsregistrationresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSRegistrationResponse, mBMSRegistrationResponse_p);
    return decoded;
}

int ranap_decode_mbmsregistrationfailureies(
    RANAP_MBMSRegistrationFailureIEs_t *mbmsRegistrationFailureIEs,
    ANY_t *any_p) {

    RANAP_MBMSRegistrationFailure_t *mBMSRegistrationFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsRegistrationFailureIEs != NULL);

    memset(mbmsRegistrationFailureIEs, 0, sizeof(RANAP_MBMSRegistrationFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSRegistrationFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSRegistrationFailure, (void**)&mBMSRegistrationFailure_p);

    if (tempDecoded < 0 || mBMSRegistrationFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSRegistrationFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSRegistrationFailure_p->mbmsRegistrationFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSRegistrationFailure_p->mbmsRegistrationFailure_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_TMGI:
            {
                RANAP_TMGI_t *ranap_tmgi_p = NULL;
                mbmsRegistrationFailureIEs->presenceMask |= MBMSREGISTRATIONFAILUREIES_RANAP_TMGI_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TMGI, (void**)&ranap_tmgi_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE tmgi failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TMGI, ranap_tmgi_p);
                memcpy(&mbmsRegistrationFailureIEs->tmgi, ranap_tmgi_p, sizeof(RANAP_TMGI_t));
                FREEMEM(ranap_tmgi_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_ID:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                mbmsRegistrationFailureIEs->presenceMask |= MBMSREGISTRATIONFAILUREIES_RANAP_GLOBALCN_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&mbmsRegistrationFailureIEs->globalCN_ID, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmsRegistrationFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsRegistrationFailureIEs->presenceMask |= MBMSREGISTRATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsRegistrationFailureIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmsregistrationfailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSRegistrationFailure, mBMSRegistrationFailure_p);
    return decoded;
}

int ranap_decode_mbmscnde_registrationrequesties(
    RANAP_MBMSCNDe_RegistrationRequestIEs_t *mbmscnDe_RegistrationRequestIEs,
    ANY_t *any_p) {

    RANAP_MBMSCNDe_RegistrationRequest_t *mBMSCNDe_RegistrationRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmscnDe_RegistrationRequestIEs != NULL);

    memset(mbmscnDe_RegistrationRequestIEs, 0, sizeof(RANAP_MBMSCNDe_RegistrationRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSCNDe_RegistrationRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSCNDe_RegistrationRequest, (void**)&mBMSCNDe_RegistrationRequest_p);

    if (tempDecoded < 0 || mBMSCNDe_RegistrationRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSCNDe_RegistrationRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSCNDe_RegistrationRequest_p->mbmscnDe_RegistrationRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSCNDe_RegistrationRequest_p->mbmscnDe_RegistrationRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_TMGI:
            {
                RANAP_TMGI_t *ranap_tmgi_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TMGI, (void**)&ranap_tmgi_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE tmgi failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TMGI, ranap_tmgi_p);
                memcpy(&mbmscnDe_RegistrationRequestIEs->tmgi, ranap_tmgi_p, sizeof(RANAP_TMGI_t));
                FREEMEM(ranap_tmgi_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_GlobalCN_ID:
            {
                RANAP_GlobalCN_ID_t *ranaP_GlobalCNID_p = NULL;
                mbmscnDe_RegistrationRequestIEs->presenceMask |= MBMSCNDE_REGISTRATIONREQUESTIES_RANAP_GLOBALCN_ID_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalCN_ID, (void**)&ranaP_GlobalCNID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalCN_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalCN_ID, ranaP_GlobalCNID_p);
                memcpy(&mbmscnDe_RegistrationRequestIEs->globalCN_ID, ranaP_GlobalCNID_p, sizeof(RANAP_GlobalCN_ID_t));
                FREEMEM(ranaP_GlobalCNID_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmscnde_registrationrequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSCNDe_RegistrationRequest, mBMSCNDe_RegistrationRequest_p);
    return decoded;
}

int ranap_decode_mbmscnde_registrationresponseies(
    RANAP_MBMSCNDe_RegistrationResponseIEs_t *mbmscnDe_RegistrationResponseIEs,
    ANY_t *any_p) {

    RANAP_MBMSCNDe_RegistrationResponse_t *mBMSCNDe_RegistrationResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmscnDe_RegistrationResponseIEs != NULL);

    memset(mbmscnDe_RegistrationResponseIEs, 0, sizeof(RANAP_MBMSCNDe_RegistrationResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSCNDe_RegistrationResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSCNDe_RegistrationResponse, (void**)&mBMSCNDe_RegistrationResponse_p);

    if (tempDecoded < 0 || mBMSCNDe_RegistrationResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSCNDe_RegistrationResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSCNDe_RegistrationResponse_p->mbmscnDe_RegistrationResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSCNDe_RegistrationResponse_p->mbmscnDe_RegistrationResponse_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_TMGI:
            {
                RANAP_TMGI_t *ranap_tmgi_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TMGI, (void**)&ranap_tmgi_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE tmgi failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TMGI, ranap_tmgi_p);
                memcpy(&mbmscnDe_RegistrationResponseIEs->tmgi, ranap_tmgi_p, sizeof(RANAP_TMGI_t));
                FREEMEM(ranap_tmgi_p);
            } break;
            case RANAP_ProtocolIE_ID_id_GlobalRNC_ID:
            {
                RANAP_GlobalRNC_ID_t *ranaP_GlobalRNCID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_GlobalRNC_ID, (void**)&ranaP_GlobalRNCID_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE globalRNC_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_GlobalRNC_ID, ranaP_GlobalRNCID_p);
                memcpy(&mbmscnDe_RegistrationResponseIEs->globalRNC_ID, ranaP_GlobalRNCID_p, sizeof(RANAP_GlobalRNC_ID_t));
                FREEMEM(ranaP_GlobalRNCID_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                mbmscnDe_RegistrationResponseIEs->presenceMask |= MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CAUSE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmscnDe_RegistrationResponseIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmscnDe_RegistrationResponseIEs->presenceMask |= MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmscnDe_RegistrationResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmscnde_registrationresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSCNDe_RegistrationResponse, mBMSCNDe_RegistrationResponse_p);
    return decoded;
}

int ranap_decode_mbmsrabestablishmentindicationies(
    RANAP_MBMSRABEstablishmentIndicationIEs_t *mbmsrabEstablishmentIndicationIEs,
    ANY_t *any_p) {

    RANAP_MBMSRABEstablishmentIndication_t *mBMSRABEstablishmentIndication_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsrabEstablishmentIndicationIEs != NULL);

    memset(mbmsrabEstablishmentIndicationIEs, 0, sizeof(RANAP_MBMSRABEstablishmentIndicationIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSRABEstablishmentIndicationIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSRABEstablishmentIndication, (void**)&mBMSRABEstablishmentIndication_p);

    if (tempDecoded < 0 || mBMSRABEstablishmentIndication_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSRABEstablishmentIndicationIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSRABEstablishmentIndication_p->mbmsrabEstablishmentIndication_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSRABEstablishmentIndication_p->mbmsrabEstablishmentIndication_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_TransportLayerInformation:
            {
                RANAP_TransportLayerInformation_t *ranaP_TransportLayerInformation_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_TransportLayerInformation, (void**)&ranaP_TransportLayerInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE transportLayerInformation failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_TransportLayerInformation, ranaP_TransportLayerInformation_p);
                memcpy(&mbmsrabEstablishmentIndicationIEs->transportLayerInformation, ranaP_TransportLayerInformation_p, sizeof(RANAP_TransportLayerInformation_t));
                FREEMEM(ranaP_TransportLayerInformation_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmsrabestablishmentindicationies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSRABEstablishmentIndication, mBMSRABEstablishmentIndication_p);
    return decoded;
}

int ranap_decode_mbmsrabreleaserequesties(
    RANAP_MBMSRABReleaseRequestIEs_t *mbmsrabReleaseRequestIEs,
    ANY_t *any_p) {

    RANAP_MBMSRABReleaseRequest_t *mBMSRABReleaseRequest_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsrabReleaseRequestIEs != NULL);

    memset(mbmsrabReleaseRequestIEs, 0, sizeof(RANAP_MBMSRABReleaseRequestIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSRABReleaseRequestIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSRABReleaseRequest, (void**)&mBMSRABReleaseRequest_p);

    if (tempDecoded < 0 || mBMSRABReleaseRequest_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSRABReleaseRequestIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSRABReleaseRequest_p->mbmsrabReleaseRequest_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSRABReleaseRequest_p->mbmsrabReleaseRequest_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmsrabReleaseRequestIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmsrabreleaserequesties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSRABReleaseRequest, mBMSRABReleaseRequest_p);
    return decoded;
}

int ranap_decode_mbmsrabreleaseies(
    RANAP_MBMSRABReleaseIEs_t *mbmsrabReleaseIEs,
    ANY_t *any_p) {

    RANAP_MBMSRABRelease_t *mBMSRABRelease_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsrabReleaseIEs != NULL);

    memset(mbmsrabReleaseIEs, 0, sizeof(RANAP_MBMSRABReleaseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSRABReleaseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSRABRelease, (void**)&mBMSRABRelease_p);

    if (tempDecoded < 0 || mBMSRABRelease_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSRABReleaseIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSRABRelease_p->mbmsrabRelease_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSRABRelease_p->mbmsrabRelease_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmsrabReleaseIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsrabReleaseIEs->presenceMask |= MBMSRABRELEASEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsrabReleaseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmsrabreleaseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSRABRelease, mBMSRABRelease_p);
    return decoded;
}

int ranap_decode_mbmsrabreleasefailureies(
    RANAP_MBMSRABReleaseFailureIEs_t *mbmsrabReleaseFailureIEs,
    ANY_t *any_p) {

    RANAP_MBMSRABReleaseFailure_t *mBMSRABReleaseFailure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(mbmsrabReleaseFailureIEs != NULL);

    memset(mbmsrabReleaseFailureIEs, 0, sizeof(RANAP_MBMSRABReleaseFailureIEs_t));
    RANAP_DEBUG("Decoding message RANAP_MBMSRABReleaseFailureIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_MBMSRABReleaseFailure, (void**)&mBMSRABReleaseFailure_p);

    if (tempDecoded < 0 || mBMSRABReleaseFailure_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_MBMSRABReleaseFailureIEs failed\n");
        return -1;
    }

    for (i = 0; i < mBMSRABReleaseFailure_p->mbmsrabReleaseFailure_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = mBMSRABReleaseFailure_p->mbmsrabReleaseFailure_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_Cause:
            {
                RANAP_Cause_t *ranaP_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_Cause, (void**)&ranaP_Cause_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_Cause, ranaP_Cause_p);
                memcpy(&mbmsrabReleaseFailureIEs->cause, ranaP_Cause_p, sizeof(RANAP_Cause_t));
                FREEMEM(ranaP_Cause_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                mbmsrabReleaseFailureIEs->presenceMask |= MBMSRABRELEASEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&mbmsrabReleaseFailureIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message mbmsrabreleasefailureies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_MBMSRABReleaseFailure, mBMSRABReleaseFailure_p);
    return decoded;
}

int ranap_decode_srvcc_cskeysresponseies(
    RANAP_SRVCC_CSKeysResponseIEs_t *srvcC_CSKeysResponseIEs,
    ANY_t *any_p) {

    RANAP_SRVCC_CSKeysResponse_t *sRVCC_CSKeysResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(srvcC_CSKeysResponseIEs != NULL);

    memset(srvcC_CSKeysResponseIEs, 0, sizeof(RANAP_SRVCC_CSKeysResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_SRVCC_CSKeysResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_SRVCC_CSKeysResponse, (void**)&sRVCC_CSKeysResponse_p);

    if (tempDecoded < 0 || sRVCC_CSKeysResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_SRVCC_CSKeysResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < sRVCC_CSKeysResponse_p->srvcC_CSKeysResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = sRVCC_CSKeysResponse_p->srvcC_CSKeysResponse_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_IntegrityProtectionKey:
            {
                RANAP_IntegrityProtectionKey_t *ranaP_IntegrityProtectionKey_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_IntegrityProtectionKey, (void**)&ranaP_IntegrityProtectionKey_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE integrityProtectionKey failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_IntegrityProtectionKey, ranaP_IntegrityProtectionKey_p);
                memcpy(&srvcC_CSKeysResponseIEs->integrityProtectionKey, ranaP_IntegrityProtectionKey_p, sizeof(RANAP_IntegrityProtectionKey_t));
                FREEMEM(ranaP_IntegrityProtectionKey_p);
            } break;
            case RANAP_ProtocolIE_ID_id_EncryptionKey:
            {
                RANAP_EncryptionKey_t *ranaP_EncryptionKey_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_EncryptionKey, (void**)&ranaP_EncryptionKey_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE encryptionKey failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_EncryptionKey, ranaP_EncryptionKey_p);
                memcpy(&srvcC_CSKeysResponseIEs->encryptionKey, ranaP_EncryptionKey_p, sizeof(RANAP_EncryptionKey_t));
                FREEMEM(ranaP_EncryptionKey_p);
            } break;
            case RANAP_ProtocolIE_ID_id_SRVCC_Information:
            {
                RANAP_SRVCC_Information_t *ranaP_SRVCCInformation_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_SRVCC_Information, (void**)&ranaP_SRVCCInformation_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE srvcC_Information failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_SRVCC_Information, ranaP_SRVCCInformation_p);
                memcpy(&srvcC_CSKeysResponseIEs->srvcC_Information, ranaP_SRVCCInformation_p, sizeof(RANAP_SRVCC_Information_t));
                FREEMEM(ranaP_SRVCCInformation_p);
            } break;
            /* Optional field */
            case RANAP_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RANAP_CriticalityDiagnostics_t *ranaP_CriticalityDiagnostics_p = NULL;
                srvcC_CSKeysResponseIEs->presenceMask |= SRVCC_CSKEYSRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_CriticalityDiagnostics, (void**)&ranaP_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_CriticalityDiagnostics, ranaP_CriticalityDiagnostics_p);
                memcpy(&srvcC_CSKeysResponseIEs->criticalityDiagnostics, ranaP_CriticalityDiagnostics_p, sizeof(RANAP_CriticalityDiagnostics_t));
                FREEMEM(ranaP_CriticalityDiagnostics_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message srvcc_cskeysresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_SRVCC_CSKeysResponse, sRVCC_CSKeysResponse_p);
    return decoded;
}

int ranap_decode_ueradiocapabilitymatchresponseies(
    RANAP_UeRadioCapabilityMatchResponseIEs_t *ueRadioCapabilityMatchResponseIEs,
    ANY_t *any_p) {

    RANAP_UeRadioCapabilityMatchResponse_t *ueRadioCapabilityMatchResponse_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(ueRadioCapabilityMatchResponseIEs != NULL);

    memset(ueRadioCapabilityMatchResponseIEs, 0, sizeof(RANAP_UeRadioCapabilityMatchResponseIEs_t));
    RANAP_DEBUG("Decoding message RANAP_UeRadioCapabilityMatchResponseIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RANAP_UeRadioCapabilityMatchResponse, (void**)&ueRadioCapabilityMatchResponse_p);

    if (tempDecoded < 0 || ueRadioCapabilityMatchResponse_p == NULL) {
        RANAP_DEBUG("Decoding of message RANAP_UeRadioCapabilityMatchResponseIEs failed\n");
        return -1;
    }

    for (i = 0; i < ueRadioCapabilityMatchResponse_p->ueRadioCapabilityMatchResponse_ies.list.count; i++) {
        RANAP_IE_t *ie_p;
        ie_p = ueRadioCapabilityMatchResponse_p->ueRadioCapabilityMatchResponse_ies.list.array[i];
        switch(ie_p->id) {
            case RANAP_ProtocolIE_ID_id_VoiceSupportMatchIndicator:
            {
                RANAP_VoiceSupportMatchIndicator_t *ranaP_VoiceSupportMatchIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RANAP_VoiceSupportMatchIndicator, (void**)&ranaP_VoiceSupportMatchIndicator_p);
                if (tempDecoded < 0) {
                    RANAP_DEBUG("Decoding of IE voiceSupportMatchIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RANAP_VoiceSupportMatchIndicator, ranaP_VoiceSupportMatchIndicator_p);
                memcpy(&ueRadioCapabilityMatchResponseIEs->voiceSupportMatchIndicator, ranaP_VoiceSupportMatchIndicator_p, sizeof(RANAP_VoiceSupportMatchIndicator_t));
                FREEMEM(ranaP_VoiceSupportMatchIndicator_p);
            } break;
            default:
                RANAP_DEBUG("Unknown protocol IE id (%d) for message ueradiocapabilitymatchresponseies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RANAP_UeRadioCapabilityMatchResponse, ueRadioCapabilityMatchResponse_p);
    return decoded;
}

int ranap_free_iu_releasecommandies(
    RANAP_Iu_ReleaseCommandIEs_t *iu_ReleaseCommandIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &iu_ReleaseCommandIEs->cause);
    return 0;
}

int ranap_free_iu_releasecompleteies(
    RANAP_Iu_ReleaseCompleteIEs_t *iu_ReleaseCompleteIEs) {

    /* Optional field */
    if ((iu_ReleaseCompleteIEs->presenceMask & IU_RELEASECOMPLETEIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT)
        == IU_RELEASECOMPLETEIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataVolumeReportList, &iu_ReleaseCompleteIEs->raB_DataVolumeReportList);
    /* Optional field */
    if ((iu_ReleaseCompleteIEs->presenceMask & IU_RELEASECOMPLETEIES_RANAP_RAB_RELEASEDLIST_IURELCOMP_PRESENT)
        == IU_RELEASECOMPLETEIES_RANAP_RAB_RELEASEDLIST_IURELCOMP_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ReleasedList_IuRelComp, &iu_ReleaseCompleteIEs->raB_ReleasedList_IuRelComp);
    /* Optional field */
    if ((iu_ReleaseCompleteIEs->presenceMask & IU_RELEASECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == IU_RELEASECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &iu_ReleaseCompleteIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_rab_datavolumereportitemies(
    RANAP_RAB_DataVolumeReportItemIEs_t *raB_DataVolumeReportItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataVolumeReportItem, &raB_DataVolumeReportItemIEs->raB_DataVolumeReportItem);
    return 0;
}

int ranap_free_rab_releaseditem_iurelcomp_ies(
    RANAP_RAB_ReleasedItem_IuRelComp_IEs_t *raB_ReleasedItem_IuRelComp_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ReleasedItem_IuRelComp, &raB_ReleasedItem_IuRelComp_IEs->raB_ReleasedItem_IuRelComp);
    return 0;
}

int ranap_free_relocationrequiredies(
    RANAP_RelocationRequiredIEs_t *relocationRequiredIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RelocationType, &relocationRequiredIEs->relocationType);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &relocationRequiredIEs->cause);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SourceID, &relocationRequiredIEs->sourceID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TargetID, &relocationRequiredIEs->targetID);
    /* Optional field */
    if ((relocationRequiredIEs->presenceMask & RELOCATIONREQUIREDIES_RANAP_OLDBSS_TONEWBSS_INFORMATION_PRESENT)
        == RELOCATIONREQUIREDIES_RANAP_OLDBSS_TONEWBSS_INFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_OldBSS_ToNewBSS_Information, &relocationRequiredIEs->oldBSS_ToNewBSS_Information);
    return 0;
}

int ranap_free_relocationcommandies(
    RANAP_RelocationCommandIEs_t *relocationCommandIEs) {

    /* Optional field */
    if ((relocationCommandIEs->presenceMask & RELOCATIONCOMMANDIES_RANAP_L3_INFORMATION_PRESENT)
        == RELOCATIONCOMMANDIES_RANAP_L3_INFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_L3_Information, &relocationCommandIEs->l3_Information);
    /* Optional field */
    if ((relocationCommandIEs->presenceMask & RELOCATIONCOMMANDIES_RANAP_RAB_RELOCATIONRELEASELIST_PRESENT)
        == RELOCATIONCOMMANDIES_RANAP_RAB_RELOCATIONRELEASELIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_RelocationReleaseList, &relocationCommandIEs->raB_RelocationReleaseList);
    /* Optional field */
    if ((relocationCommandIEs->presenceMask & RELOCATIONCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT)
        == RELOCATIONCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataForwardingList, &relocationCommandIEs->raB_DataForwardingList);
    /* Optional field */
    if ((relocationCommandIEs->presenceMask & RELOCATIONCOMMANDIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONCOMMANDIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &relocationCommandIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_rab_relocationreleaseitemies(
    RANAP_RAB_RelocationReleaseItemIEs_t *raB_RelocationReleaseItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_RelocationReleaseItem, &raB_RelocationReleaseItemIEs->raB_RelocationReleaseItem);
    return 0;
}

int ranap_free_rab_dataforwardingitemies(
    RANAP_RAB_DataForwardingItemIEs_t *raB_DataForwardingItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataForwardingItem, &raB_DataForwardingItemIEs->raB_DataForwardingItem);
    return 0;
}

int ranap_free_relocationpreparationfailureies(
    RANAP_RelocationPreparationFailureIEs_t *relocationPreparationFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &relocationPreparationFailureIEs->cause);
    /* Optional field */
    if ((relocationPreparationFailureIEs->presenceMask & RELOCATIONPREPARATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONPREPARATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &relocationPreparationFailureIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_relocationrequesties(
    RANAP_RelocationRequestIEs_t *relocationRequestIEs) {

    /* Optional field */
    if ((relocationRequestIEs->presenceMask & RELOCATIONREQUESTIES_RANAP_PERMANENTNAS_UE_ID_PRESENT)
        == RELOCATIONREQUESTIES_RANAP_PERMANENTNAS_UE_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_PermanentNAS_UE_ID, &relocationRequestIEs->permanentNAS_UE_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &relocationRequestIEs->cause);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &relocationRequestIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SourceRNC_ToTargetRNC_TransparentContainer, &relocationRequestIEs->source_ToTarget_TransparentContainer);
    /* Optional field */
    if ((relocationRequestIEs->presenceMask & RELOCATIONREQUESTIES_RANAP_RAB_SETUPLIST_RELOCREQ_PRESENT)
        == RELOCATIONREQUESTIES_RANAP_RAB_SETUPLIST_RELOCREQ_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupList_RelocReq, &relocationRequestIEs->raB_SetupList_RelocReq);
    /* Optional field */
    if ((relocationRequestIEs->presenceMask & RELOCATIONREQUESTIES_RANAP_INTEGRITYPROTECTIONINFORMATION_PRESENT)
        == RELOCATIONREQUESTIES_RANAP_INTEGRITYPROTECTIONINFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IntegrityProtectionInformation, &relocationRequestIEs->integrityProtectionInformation);
    /* Optional field */
    if ((relocationRequestIEs->presenceMask & RELOCATIONREQUESTIES_RANAP_ENCRYPTIONINFORMATION_PRESENT)
        == RELOCATIONREQUESTIES_RANAP_ENCRYPTIONINFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_EncryptionInformation, &relocationRequestIEs->encryptionInformation);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IuSignallingConnectionIdentifier, &relocationRequestIEs->iuSigConId);
    return 0;
}

int ranap_free_rab_setupitem_relocreq_ies(
    RANAP_RAB_SetupItem_RelocReq_IEs_t *raB_SetupItem_RelocReq_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupItem_RelocReq, &raB_SetupItem_RelocReq_IEs->raB_SetupItem_RelocReq);
    return 0;
}

int ranap_free_relocationrequestacknowledgeies(
    RANAP_RelocationRequestAcknowledgeIEs_t *relocationRequestAcknowledgeIEs) {

    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TargetRNC_ToSourceRNC_TransparentContainer, &relocationRequestAcknowledgeIEs->target_ToSource_TransparentContainer);
    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_SETUPLIST_RELOCREQACK_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_SETUPLIST_RELOCREQACK_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupList_RelocReqAck, &relocationRequestAcknowledgeIEs->raB_SetupList_RelocReqAck);
    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_FAILEDLIST_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_FAILEDLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_FailedList, &relocationRequestAcknowledgeIEs->raB_FailedList);
    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENINTEGRITYPROTECTIONALGORITHM_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENINTEGRITYPROTECTIONALGORITHM_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ChosenIntegrityProtectionAlgorithm, &relocationRequestAcknowledgeIEs->chosenIntegrityProtectionAlgorithm);
    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ChosenEncryptionAlgorithm, &relocationRequestAcknowledgeIEs->chosenEncryptionAlgorithm);
    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &relocationRequestAcknowledgeIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_rab_setupitem_relocreqack_ies(
    RANAP_RAB_SetupItem_RelocReqAck_IEs_t *raB_SetupItem_RelocReqAck_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupItem_RelocReqAck, &raB_SetupItem_RelocReqAck_IEs->raB_SetupItem_RelocReqAck);
    return 0;
}

int ranap_free_rab_faileditemies(
    RANAP_RAB_FailedItemIEs_t *raB_FailedItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_FailedItem, &raB_FailedItemIEs->raB_FailedItem);
    return 0;
}

int ranap_free_relocationfailureies(
    RANAP_RelocationFailureIEs_t *relocationFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &relocationFailureIEs->cause);
    /* Optional field */
    if ((relocationFailureIEs->presenceMask & RELOCATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &relocationFailureIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_relocationcancelies(
    RANAP_RelocationCancelIEs_t *relocationCancelIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &relocationCancelIEs->cause);
    return 0;
}

int ranap_free_relocationcancelacknowledgeies(
    RANAP_RelocationCancelAcknowledgeIEs_t *relocationCancelAcknowledgeIEs) {

    /* Optional field */
    if ((relocationCancelAcknowledgeIEs->presenceMask & RELOCATIONCANCELACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONCANCELACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &relocationCancelAcknowledgeIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_srns_contextrequesties(
    RANAP_SRNS_ContextRequestIEs_t *srnS_ContextRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataForwardingList_SRNS_CtxReq, &srnS_ContextRequestIEs->raB_DataForwardingList_SRNS_CtxReq);
    return 0;
}

int ranap_free_rab_dataforwardingitem_srns_ctxreq_ies(
    RANAP_RAB_DataForwardingItem_SRNS_CtxReq_IEs_t *raB_DataForwardingItem_SRNS_CtxReq_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataForwardingItem_SRNS_CtxReq, &raB_DataForwardingItem_SRNS_CtxReq_IEs->raB_DataForwardingItem_SRNS_CtxReq);
    return 0;
}

int ranap_free_srns_contextresponseies(
    RANAP_SRNS_ContextResponseIEs_t *srnS_ContextResponseIEs) {

    /* Optional field */
    if ((srnS_ContextResponseIEs->presenceMask & SRNS_CONTEXTRESPONSEIES_RANAP_RAB_CONTEXTLIST_PRESENT)
        == SRNS_CONTEXTRESPONSEIES_RANAP_RAB_CONTEXTLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ContextList, &srnS_ContextResponseIEs->raB_ContextList);
    /* Optional field */
    if ((srnS_ContextResponseIEs->presenceMask & SRNS_CONTEXTRESPONSEIES_RANAP_RABS_CONTEXTFAILEDTOTRANSFERLIST_PRESENT)
        == SRNS_CONTEXTRESPONSEIES_RANAP_RABS_CONTEXTFAILEDTOTRANSFERLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RABs_ContextFailedtoTransferList, &srnS_ContextResponseIEs->raB_ContextFailedtoTransferList);
    /* Optional field */
    if ((srnS_ContextResponseIEs->presenceMask & SRNS_CONTEXTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == SRNS_CONTEXTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &srnS_ContextResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_rab_contextitemies(
    RANAP_RAB_ContextItemIEs_t *raB_ContextItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ContextItem, &raB_ContextItemIEs->raB_ContextItem);
    return 0;
}

int ranap_free_rabs_contextfailedtotransferitemies(
    RANAP_RABs_ContextFailedtoTransferItemIEs_t *raBs_ContextFailedtoTransferItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RABs_ContextFailedtoTransferItem, &raBs_ContextFailedtoTransferItemIEs->raB_ContextFailedtoTransferItem);
    return 0;
}

int ranap_free_securitymodecommandies(
    RANAP_SecurityModeCommandIEs_t *securityModeCommandIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IntegrityProtectionInformation, &securityModeCommandIEs->integrityProtectionInformation);
    /* Optional field */
    if ((securityModeCommandIEs->presenceMask & SECURITYMODECOMMANDIES_RANAP_ENCRYPTIONINFORMATION_PRESENT)
        == SECURITYMODECOMMANDIES_RANAP_ENCRYPTIONINFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_EncryptionInformation, &securityModeCommandIEs->encryptionInformation);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_KeyStatus, &securityModeCommandIEs->keyStatus);
    return 0;
}

int ranap_free_securitymodecompleteies(
    RANAP_SecurityModeCompleteIEs_t *securityModeCompleteIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ChosenIntegrityProtectionAlgorithm, &securityModeCompleteIEs->chosenIntegrityProtectionAlgorithm);
    /* Optional field */
    if ((securityModeCompleteIEs->presenceMask & SECURITYMODECOMPLETEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT)
        == SECURITYMODECOMPLETEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ChosenEncryptionAlgorithm, &securityModeCompleteIEs->chosenEncryptionAlgorithm);
    /* Optional field */
    if ((securityModeCompleteIEs->presenceMask & SECURITYMODECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == SECURITYMODECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &securityModeCompleteIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_securitymoderejecties(
    RANAP_SecurityModeRejectIEs_t *securityModeRejectIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &securityModeRejectIEs->cause);
    /* Optional field */
    if ((securityModeRejectIEs->presenceMask & SECURITYMODEREJECTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == SECURITYMODEREJECTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &securityModeRejectIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_datavolumereportrequesties(
    RANAP_DataVolumeReportRequestIEs_t *dataVolumeReportRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataVolumeReportRequestList, &dataVolumeReportRequestIEs->raB_DataVolumeReportRequestList);
    return 0;
}

int ranap_free_rab_datavolumereportrequestitemies(
    RANAP_RAB_DataVolumeReportRequestItemIEs_t *raB_DataVolumeReportRequestItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataVolumeReportRequestItem, &raB_DataVolumeReportRequestItemIEs->raB_DataVolumeReportRequestItem);
    return 0;
}

int ranap_free_datavolumereporties(
    RANAP_DataVolumeReportIEs_t *dataVolumeReportIEs) {

    /* Optional field */
    if ((dataVolumeReportIEs->presenceMask & DATAVOLUMEREPORTIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT)
        == DATAVOLUMEREPORTIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataVolumeReportList, &dataVolumeReportIEs->raB_DataVolumeReportList);
    /* Optional field */
    if ((dataVolumeReportIEs->presenceMask & DATAVOLUMEREPORTIES_RANAP_RABS_FAILED_TO_REPORTLIST_PRESENT)
        == DATAVOLUMEREPORTIES_RANAP_RABS_FAILED_TO_REPORTLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RABs_failed_to_reportList, &dataVolumeReportIEs->raB_FailedtoReportList);
    /* Optional field */
    if ((dataVolumeReportIEs->presenceMask & DATAVOLUMEREPORTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == DATAVOLUMEREPORTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &dataVolumeReportIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_rabs_failed_to_reportitemies(
    RANAP_RABs_failed_to_reportItemIEs_t *raBs_failed_to_reportItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RABs_failed_to_reportItem, &raBs_failed_to_reportItemIEs->raB_FailedtoReportItem);
    return 0;
}

int ranap_free_reseties(
    RANAP_ResetIEs_t *resetIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &resetIEs->cause);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &resetIEs->cN_DomainIndicator);
    /* Optional field */
    if ((resetIEs->presenceMask & RESETIES_RANAP_GLOBALRNC_ID_PRESENT)
        == RESETIES_RANAP_GLOBALRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &resetIEs->globalRNC_ID);
    return 0;
}

int ranap_free_resetacknowledgeies(
    RANAP_ResetAcknowledgeIEs_t *resetAcknowledgeIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &resetAcknowledgeIEs->cN_DomainIndicator);
    /* Optional field */
    if ((resetAcknowledgeIEs->presenceMask & RESETACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RESETACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &resetAcknowledgeIEs->criticalityDiagnostics);
    /* Optional field */
    if ((resetAcknowledgeIEs->presenceMask & RESETACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT)
        == RESETACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &resetAcknowledgeIEs->globalRNC_ID);
    return 0;
}

int ranap_free_resetresourceies(
    RANAP_ResetResourceIEs_t *resetResourceIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &resetResourceIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &resetResourceIEs->cause);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ResetResourceList, &resetResourceIEs->iuSigConIdList);
    /* Optional field */
    if ((resetResourceIEs->presenceMask & RESETRESOURCEIES_RANAP_GLOBALRNC_ID_PRESENT)
        == RESETRESOURCEIES_RANAP_GLOBALRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &resetResourceIEs->globalRNC_ID);
    return 0;
}

int ranap_free_resetresourceitemies(
    RANAP_ResetResourceItemIEs_t *resetResourceItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ResetResourceItem, &resetResourceItemIEs->iuSigConIdItem);
    return 0;
}

int ranap_free_resetresourceacknowledgeies(
    RANAP_ResetResourceAcknowledgeIEs_t *resetResourceAcknowledgeIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &resetResourceAcknowledgeIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ResetResourceAckList, &resetResourceAcknowledgeIEs->iuSigConIdList);
    /* Optional field */
    if ((resetResourceAcknowledgeIEs->presenceMask & RESETRESOURCEACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT)
        == RESETRESOURCEACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &resetResourceAcknowledgeIEs->globalRNC_ID);
    /* Optional field */
    if ((resetResourceAcknowledgeIEs->presenceMask & RESETRESOURCEACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RESETRESOURCEACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &resetResourceAcknowledgeIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_resetresourceackitemies(
    RANAP_ResetResourceAckItemIEs_t *resetResourceAckItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ResetResourceAckItem, &resetResourceAckItemIEs->iuSigConIdItem);
    return 0;
}

int ranap_free_rab_releaserequesties(
    RANAP_RAB_ReleaseRequestIEs_t *raB_ReleaseRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ReleaseList, &raB_ReleaseRequestIEs->raB_ReleaseList);
    return 0;
}

int ranap_free_rab_releaseitemies(
    RANAP_RAB_ReleaseItemIEs_t *raB_ReleaseItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ReleaseItem, &raB_ReleaseItemIEs->raB_ReleaseItem);
    return 0;
}

int ranap_free_iu_releaserequesties(
    RANAP_Iu_ReleaseRequestIEs_t *iu_ReleaseRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &iu_ReleaseRequestIEs->cause);
    return 0;
}

int ranap_free_enhancedrelocationcompleterequesties(
    RANAP_EnhancedRelocationCompleteRequestIEs_t *enhancedRelocationCompleteRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IuSignallingConnectionIdentifier, &enhancedRelocationCompleteRequestIEs->oldIuSigConId);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IuSignallingConnectionIdentifier, &enhancedRelocationCompleteRequestIEs->iuSigConId);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &enhancedRelocationCompleteRequestIEs->relocation_SourceRNC_ID);
    /* Optional field */
    if ((enhancedRelocationCompleteRequestIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_SOURCEEXTENDEDRNC_ID_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_SOURCEEXTENDEDRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ExtendedRNC_ID, &enhancedRelocationCompleteRequestIEs->relocation_SourceExtendedRNC_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &enhancedRelocationCompleteRequestIEs->relocation_TargetRNC_ID);
    /* Optional field */
    if ((enhancedRelocationCompleteRequestIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_TARGETEXTENDEDRNC_ID_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_TARGETEXTENDEDRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ExtendedRNC_ID, &enhancedRelocationCompleteRequestIEs->relocation_TargetExtendedRNC_ID);
    /* Optional field */
    if ((enhancedRelocationCompleteRequestIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETEREQ_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETEREQ_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteReq, &enhancedRelocationCompleteRequestIEs->raB_SetupList_EnhancedRelocCompleteReq);
    return 0;
}

int ranap_free_rab_setupitem_enhancedreloccompletereq_ies(
    RANAP_RAB_SetupItem_EnhancedRelocCompleteReq_IEs_t *raB_SetupItem_EnhancedRelocCompleteReq_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupItem_EnhancedRelocCompleteReq, &raB_SetupItem_EnhancedRelocCompleteReq_IEs->raB_SetupItem_EnhancedRelocCompleteReq);
    return 0;
}

int ranap_free_enhancedrelocationcompleteresponseies(
    RANAP_EnhancedRelocationCompleteResponseIEs_t *enhancedRelocationCompleteResponseIEs) {

    /* Optional field */
    if ((enhancedRelocationCompleteResponseIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETERES_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETERES_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteRes, &enhancedRelocationCompleteResponseIEs->raB_SetupList_EnhancedRelocCompleteRes);
    /* Optional field */
    if ((enhancedRelocationCompleteResponseIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_TOBERELEASEDLIST_ENHANCEDRELOCCOMPLETERES_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_TOBERELEASEDLIST_ENHANCEDRELOCCOMPLETERES_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes, &enhancedRelocationCompleteResponseIEs->raB_ToBeReleasedList_EnhancedRelocCompleteRes);
    /* Optional field */
    if ((enhancedRelocationCompleteResponseIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &enhancedRelocationCompleteResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_rab_setupitem_enhancedreloccompleteres_ies(
    RANAP_RAB_SetupItem_EnhancedRelocCompleteRes_IEs_t *raB_SetupItem_EnhancedRelocCompleteRes_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupItem_EnhancedRelocCompleteRes, &raB_SetupItem_EnhancedRelocCompleteRes_IEs->raB_SetupItem_EnhancedRelocCompleteRes);
    return 0;
}

int ranap_free_rab_tobereleaseditem_enhancedreloccompleteres_ies(
    RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs_t *raB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes, &raB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs->raB_ToBeReleasedItem_EnhancedRelocCompleteRes);
    return 0;
}

int ranap_free_enhancedrelocationcompletefailureies(
    RANAP_EnhancedRelocationCompleteFailureIEs_t *enhancedRelocationCompleteFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &enhancedRelocationCompleteFailureIEs->cause);
    /* Optional field */
    if ((enhancedRelocationCompleteFailureIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &enhancedRelocationCompleteFailureIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_enhancedrelocationcompleteconfirmies(
    RANAP_EnhancedRelocationCompleteConfirmIEs_t *enhancedRelocationCompleteConfirmIEs) {

    /* Optional field */
    if ((enhancedRelocationCompleteConfirmIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETECONFIRMIES_RANAP_RAB_FAILEDLIST_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETECONFIRMIES_RANAP_RAB_FAILEDLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_FailedList, &enhancedRelocationCompleteConfirmIEs->raB_FailedList);
    return 0;
}

int ranap_free_pagingies(
    RANAP_PagingIEs_t *pagingIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &pagingIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_PermanentNAS_UE_ID, &pagingIEs->permanentNAS_UE_ID);
    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_TEMPORARYUE_ID_PRESENT)
        == PAGINGIES_RANAP_TEMPORARYUE_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TemporaryUE_ID, &pagingIEs->temporaryUE_ID);
    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_PAGINGAREAID_PRESENT)
        == PAGINGIES_RANAP_PAGINGAREAID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_PagingAreaID, &pagingIEs->pagingAreaID);
    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_PAGINGCAUSE_PRESENT)
        == PAGINGIES_RANAP_PAGINGCAUSE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_PagingCause, &pagingIEs->pagingCause);
    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_NONSEARCHINGINDICATION_PRESENT)
        == PAGINGIES_RANAP_NONSEARCHINGINDICATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_NonSearchingIndication, &pagingIEs->nonSearchingIndication);
    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_DRX_CYCLELENGTHCOEFFICIENT_PRESENT)
        == PAGINGIES_RANAP_DRX_CYCLELENGTHCOEFFICIENT_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_DRX_CycleLengthCoefficient, &pagingIEs->drX_CycleLengthCoefficient);
    return 0;
}

int ranap_free_commonid_ies(
    RANAP_CommonID_IEs_t *commonID_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_PermanentNAS_UE_ID, &commonID_IEs->permanentNAS_UE_ID);
    return 0;
}

int ranap_free_cn_invoketraceies(
    RANAP_CN_InvokeTraceIEs_t *cN_InvokeTraceIEs) {

    /* Optional field */
    if ((cN_InvokeTraceIEs->presenceMask & CN_INVOKETRACEIES_RANAP_TRACETYPE_PRESENT)
        == CN_INVOKETRACEIES_RANAP_TRACETYPE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TraceType, &cN_InvokeTraceIEs->traceType);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TraceReference, &cN_InvokeTraceIEs->traceReference);
    /* Optional field */
    if ((cN_InvokeTraceIEs->presenceMask & CN_INVOKETRACEIES_RANAP_TRIGGERID_PRESENT)
        == CN_INVOKETRACEIES_RANAP_TRIGGERID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TriggerID, &cN_InvokeTraceIEs->triggerID);
    /* Optional field */
    if ((cN_InvokeTraceIEs->presenceMask & CN_INVOKETRACEIES_RANAP_UE_ID_PRESENT)
        == CN_INVOKETRACEIES_RANAP_UE_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_UE_ID, &cN_InvokeTraceIEs->ue_id);
    /* Optional field */
    if ((cN_InvokeTraceIEs->presenceMask & CN_INVOKETRACEIES_RANAP_OMC_ID_PRESENT)
        == CN_INVOKETRACEIES_RANAP_OMC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_OMC_ID, &cN_InvokeTraceIEs->omc_id);
    return 0;
}

int ranap_free_cn_deactivatetraceies(
    RANAP_CN_DeactivateTraceIEs_t *cN_DeactivateTraceIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TraceReference, &cN_DeactivateTraceIEs->traceReference);
    /* Optional field */
    if ((cN_DeactivateTraceIEs->presenceMask & CN_DEACTIVATETRACEIES_RANAP_TRIGGERID_PRESENT)
        == CN_DEACTIVATETRACEIES_RANAP_TRIGGERID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TriggerID, &cN_DeactivateTraceIEs->triggerID);
    return 0;
}

int ranap_free_locationreportingcontrolies(
    RANAP_LocationReportingControlIEs_t *locationReportingControlIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RequestType, &locationReportingControlIEs->requestType);
    return 0;
}

int ranap_free_locationreporties(
    RANAP_LocationReportIEs_t *locationReportIEs) {

    /* Optional field */
    if ((locationReportIEs->presenceMask & LOCATIONREPORTIES_RANAP_AREAIDENTITY_PRESENT)
        == LOCATIONREPORTIES_RANAP_AREAIDENTITY_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_AreaIdentity, &locationReportIEs->areaIdentity);
    /* Optional field */
    if ((locationReportIEs->presenceMask & LOCATIONREPORTIES_RANAP_CAUSE_PRESENT)
        == LOCATIONREPORTIES_RANAP_CAUSE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &locationReportIEs->cause);
    /* Optional field */
    if ((locationReportIEs->presenceMask & LOCATIONREPORTIES_RANAP_REQUESTTYPE_PRESENT)
        == LOCATIONREPORTIES_RANAP_REQUESTTYPE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RequestType, &locationReportIEs->requestType);
    return 0;
}

int ranap_free_initialue_messageies(
    RANAP_InitialUE_MessageIEs_t *initialUE_MessageIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &initialUE_MessageIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_LAI, &initialUE_MessageIEs->lai);
    /* Conditional field */
    if ((initialUE_MessageIEs->presenceMask & INITIALUE_MESSAGEIES_RANAP_RAC_PRESENT)
        == INITIALUE_MESSAGEIES_RANAP_RAC_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAC, &initialUE_MessageIEs->rac);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SAI, &initialUE_MessageIEs->sai);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_NAS_PDU, &initialUE_MessageIEs->nas_pdu);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IuSignallingConnectionIdentifier, &initialUE_MessageIEs->iuSigConId);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &initialUE_MessageIEs->globalRNC_ID);
    return 0;
}

int ranap_free_directtransferies(
    RANAP_DirectTransferIEs_t *directTransferIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_NAS_PDU, &directTransferIEs->nas_pdu);
    /* Optional field */
    if ((directTransferIEs->presenceMask & DIRECTTRANSFERIES_RANAP_LAI_PRESENT)
        == DIRECTTRANSFERIES_RANAP_LAI_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_LAI, &directTransferIEs->lai);
    /* Optional field */
    if ((directTransferIEs->presenceMask & DIRECTTRANSFERIES_RANAP_RAC_PRESENT)
        == DIRECTTRANSFERIES_RANAP_RAC_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAC, &directTransferIEs->rac);
    /* Optional field */
    if ((directTransferIEs->presenceMask & DIRECTTRANSFERIES_RANAP_SAI_PRESENT)
        == DIRECTTRANSFERIES_RANAP_SAI_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SAI, &directTransferIEs->sai);
    /* Optional field */
    if ((directTransferIEs->presenceMask & DIRECTTRANSFERIES_RANAP_SAPI_PRESENT)
        == DIRECTTRANSFERIES_RANAP_SAPI_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SAPI, &directTransferIEs->sapi);
    return 0;
}

int ranap_free_redirectionindication_ies(
    RANAP_RedirectionIndication_IEs_t *redirectionIndication_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_NAS_PDU, &redirectionIndication_IEs->nas_pdu);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RejectCauseValue, &redirectionIndication_IEs->rejectCauseValue);
    /* Optional field */
    if ((redirectionIndication_IEs->presenceMask & REDIRECTIONINDICATION_IES_RANAP_NAS_SEQUENCENUMBER_PRESENT)
        == REDIRECTIONINDICATION_IES_RANAP_NAS_SEQUENCENUMBER_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_NAS_SequenceNumber, &redirectionIndication_IEs->naS_SequenceNumber);
    /* Optional field */
    if ((redirectionIndication_IEs->presenceMask & REDIRECTIONINDICATION_IES_RANAP_PERMANENTNAS_UE_ID_PRESENT)
        == REDIRECTIONINDICATION_IES_RANAP_PERMANENTNAS_UE_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_PermanentNAS_UE_ID, &redirectionIndication_IEs->permanentNAS_UE_ID);
    return 0;
}

int ranap_free_overloadies(
    RANAP_OverloadIEs_t *overloadIEs) {

    /* Optional field */
    if ((overloadIEs->presenceMask & OVERLOADIES_RANAP_NUMBEROFSTEPS_PRESENT)
        == OVERLOADIES_RANAP_NUMBEROFSTEPS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_NumberOfSteps, &overloadIEs->numberOfSteps);
    /* Optional field */
    if ((overloadIEs->presenceMask & OVERLOADIES_RANAP_GLOBALRNC_ID_PRESENT)
        == OVERLOADIES_RANAP_GLOBALRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &overloadIEs->globalRNC_ID);
    return 0;
}

int ranap_free_errorindicationies(
    RANAP_ErrorIndicationIEs_t *errorIndicationIEs) {

    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RANAP_CAUSE_PRESENT)
        == ERRORINDICATIONIES_RANAP_CAUSE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &errorIndicationIEs->cause);
    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == ERRORINDICATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &errorIndicationIEs->criticalityDiagnostics);
    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RANAP_CN_DOMAININDICATOR_PRESENT)
        == ERRORINDICATIONIES_RANAP_CN_DOMAININDICATOR_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &errorIndicationIEs->cN_DomainIndicator);
    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RANAP_GLOBALRNC_ID_PRESENT)
        == ERRORINDICATIONIES_RANAP_GLOBALRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &errorIndicationIEs->globalRNC_ID);
    return 0;
}

int ranap_free_srns_dataforwardcommandies(
    RANAP_SRNS_DataForwardCommandIEs_t *srnS_DataForwardCommandIEs) {

    /* Optional field */
    if ((srnS_DataForwardCommandIEs->presenceMask & SRNS_DATAFORWARDCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT)
        == SRNS_DATAFORWARDCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_DataForwardingList, &srnS_DataForwardCommandIEs->raB_DataForwardingList);
    return 0;
}

int ranap_free_forwardsrns_contexties(
    RANAP_ForwardSRNS_ContextIEs_t *forwardSRNS_ContextIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ContextList, &forwardSRNS_ContextIEs->raB_ContextList);
    return 0;
}

int ranap_free_rab_assignmentrequesties(
    RANAP_RAB_AssignmentRequestIEs_t *raB_AssignmentRequestIEs) {

    /* Optional field */
    if ((raB_AssignmentRequestIEs->presenceMask & RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_SETUPORMODIFYLIST_PRESENT)
        == RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_SETUPORMODIFYLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupOrModifyList, &raB_AssignmentRequestIEs->raB_SetupOrModifyList);
    /* Optional field */
    if ((raB_AssignmentRequestIEs->presenceMask & RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_RELEASELIST_PRESENT)
        == RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_RELEASELIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ReleaseList, &raB_AssignmentRequestIEs->raB_ReleaseList);
    return 0;
}

int ranap_free_rab_assignmentresponseies(
    RANAP_RAB_AssignmentResponseIEs_t *raB_AssignmentResponseIEs) {

    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_SETUPORMODIFIEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_SETUPORMODIFIEDLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupOrModifiedList, &raB_AssignmentResponseIEs->raB_SetupOrModifiedList);
    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEDLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ReleasedList, &raB_AssignmentResponseIEs->raB_ReleasedList);
    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_QUEUEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_QUEUEDLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_QueuedList, &raB_AssignmentResponseIEs->raB_QueuedList);
    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_FAILEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_FAILEDLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_FailedList, &raB_AssignmentResponseIEs->raB_FailedList);
    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEFAILEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEFAILEDLIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ReleaseFailedList, &raB_AssignmentResponseIEs->raB_ReleaseFailedList);
    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &raB_AssignmentResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_rab_setupormodifieditemies(
    RANAP_RAB_SetupOrModifiedItemIEs_t *raB_SetupOrModifiedItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupOrModifiedItem, &raB_SetupOrModifiedItemIEs->raB_SetupOrModifiedItem);
    return 0;
}

int ranap_free_rab_releaseditemies(
    RANAP_RAB_ReleasedItemIEs_t *raB_ReleasedItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ReleasedItem, &raB_ReleasedItemIEs->raB_ReleasedItem);
    return 0;
}

int ranap_free_rab_queueditemies(
    RANAP_RAB_QueuedItemIEs_t *raB_QueuedItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_QueuedItem, &raB_QueuedItemIEs->raB_QueuedItem);
    return 0;
}

int ranap_free_geran_iumode_rab_failed_rabassgntresponse_itemies(
    RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs_t *geraN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_Item, &geraN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs->geraN_Iumode_RAB_Failed_RABAssgntResponse_Item);
    return 0;
}

int ranap_free_ranap_relocationinformationies(
    RANAP_RANAP_RelocationInformationIEs_t *ranaP_RelocationInformationIEs) {

    /* Optional field */
    if ((ranaP_RelocationInformationIEs->presenceMask & RANAP_RELOCATIONINFORMATIONIES_RANAP_RAB_CONTEXTLIST_RANAP_RELOCINF_PRESENT)
        == RANAP_RELOCATIONINFORMATIONIES_RANAP_RAB_CONTEXTLIST_RANAP_RELOCINF_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ContextList_RANAP_RelocInf, &ranaP_RelocationInformationIEs->raB_ContextList_RANAP_RelocInf);
    return 0;
}

int ranap_free_rab_contextitemies_ranap_relocinf(
    RANAP_RAB_ContextItemIEs_RANAP_RelocInf_t *raB_ContextItemIEs_RANAP_RelocInf) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ContextItem_RANAP_RelocInf, &raB_ContextItemIEs_RANAP_RelocInf->raB_ContextItem_RANAP_RelocInf);
    return 0;
}

int ranap_free_ranap_enhancedrelocationinformationrequesties(
    RANAP_RANAP_EnhancedRelocationInformationRequestIEs_t *ranaP_EnhancedRelocationInformationRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SourceRNC_ToTargetRNC_TransparentContainer, &ranaP_EnhancedRelocationInformationRequestIEs->source_ToTarget_TransparentContainer);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDCS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDCS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IuSignallingConnectionIdentifier, &ranaP_EnhancedRelocationInformationRequestIEs->oldIuSigConIdCS);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDCS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDCS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &ranaP_EnhancedRelocationInformationRequestIEs->globalCN_IDCS);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDPS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDPS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IuSignallingConnectionIdentifier, &ranaP_EnhancedRelocationInformationRequestIEs->oldIuSigConIdPS);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDPS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDPS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &ranaP_EnhancedRelocationInformationRequestIEs->globalCN_IDPS);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_RAB_SETUPLIST_ENHRELOCINFOREQ_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_RAB_SETUPLIST_ENHRELOCINFOREQ_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoReq, &ranaP_EnhancedRelocationInformationRequestIEs->raB_SetupList_EnhRelocInfoReq);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_SNA_ACCESS_INFORMATION_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_SNA_ACCESS_INFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SNA_Access_Information, &ranaP_EnhancedRelocationInformationRequestIEs->snA_Access_Information);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_UESBI_IU_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_UESBI_IU_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_UESBI_Iu, &ranaP_EnhancedRelocationInformationRequestIEs->uesbI_Iu);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_PLMNIDENTITY_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_PLMNIDENTITY_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_PLMNidentity, &ranaP_EnhancedRelocationInformationRequestIEs->selectedPLMN_ID);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_CNMBMSLINKINGINFORMATION_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_CNMBMSLINKINGINFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CNMBMSLinkingInformation, &ranaP_EnhancedRelocationInformationRequestIEs->cnmbmsLinkingInformation);
    return 0;
}

int ranap_free_rab_setupitem_enhrelocinforeq_ies(
    RANAP_RAB_SetupItem_EnhRelocInfoReq_IEs_t *raB_SetupItem_EnhRelocInfoReq_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupItem_EnhRelocInfoReq, &raB_SetupItem_EnhRelocInfoReq_IEs->raB_SetupItem_EnhRelocInfoReq);
    return 0;
}

int ranap_free_ranap_enhancedrelocationinformationresponseies(
    RANAP_RANAP_EnhancedRelocationInformationResponseIEs_t *ranaP_EnhancedRelocationInformationResponseIEs) {

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationResponseIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TargetRNC_ToSourceRNC_TransparentContainer, &ranaP_EnhancedRelocationInformationResponseIEs->target_ToSource_TransparentContainer);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationResponseIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_SETUPLIST_ENHRELOCINFORES_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_SETUPLIST_ENHRELOCINFORES_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoRes, &ranaP_EnhancedRelocationInformationResponseIEs->raB_SetupList_EnhRelocInfoRes);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationResponseIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_FAILEDLIST_ENHRELOCINFORES_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_FAILEDLIST_ENHRELOCINFORES_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_FailedList_EnhRelocInfoRes, &ranaP_EnhancedRelocationInformationResponseIEs->raB_FailedList_EnhRelocInfoRes);
    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationResponseIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &ranaP_EnhancedRelocationInformationResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_rab_setupitem_enhrelocinfores_ies(
    RANAP_RAB_SetupItem_EnhRelocInfoRes_IEs_t *raB_SetupItem_EnhRelocInfoRes_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_SetupItem_EnhRelocInfoRes, &raB_SetupItem_EnhRelocInfoRes_IEs->raB_SetupItem_EnhRelocInfoRes);
    return 0;
}

int ranap_free_rab_faileditem_enhrelocinfores_ies(
    RANAP_RAB_FailedItem_EnhRelocInfoRes_IEs_t *raB_FailedItem_EnhRelocInfoRes_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_FailedItem_EnhRelocInfoRes, &raB_FailedItem_EnhRelocInfoRes_IEs->raB_FailedItem_EnhRelocInfoRes);
    return 0;
}

int ranap_free_rab_modifyrequesties(
    RANAP_RAB_ModifyRequestIEs_t *raB_ModifyRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ModifyList, &raB_ModifyRequestIEs->raB_ModifyList);
    return 0;
}

int ranap_free_rab_modifyitemies(
    RANAP_RAB_ModifyItemIEs_t *raB_ModifyItemIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_ModifyItem, &raB_ModifyItemIEs->raB_ModifyItem);
    return 0;
}

int ranap_free_locationrelateddatarequesties(
    RANAP_LocationRelatedDataRequestIEs_t *locationRelatedDataRequestIEs) {

    /* Optional field */
    if ((locationRelatedDataRequestIEs->presenceMask & LOCATIONRELATEDDATAREQUESTIES_RANAP_LOCATIONRELATEDDATAREQUESTTYPE_PRESENT)
        == LOCATIONRELATEDDATAREQUESTIES_RANAP_LOCATIONRELATEDDATAREQUESTTYPE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_LocationRelatedDataRequestType, &locationRelatedDataRequestIEs->locationRelatedDataRequestType);
    return 0;
}

int ranap_free_locationrelateddataresponseies(
    RANAP_LocationRelatedDataResponseIEs_t *locationRelatedDataResponseIEs) {

    /* Optional field */
    if ((locationRelatedDataResponseIEs->presenceMask & LOCATIONRELATEDDATARESPONSEIES_RANAP_BROADCASTASSISTANCEDATADECIPHERINGKEYS_PRESENT)
        == LOCATIONRELATEDDATARESPONSEIES_RANAP_BROADCASTASSISTANCEDATADECIPHERINGKEYS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_BroadcastAssistanceDataDecipheringKeys, &locationRelatedDataResponseIEs->broadcastAssistanceDataDecipheringKeys);
    return 0;
}

int ranap_free_locationrelateddatafailureies(
    RANAP_LocationRelatedDataFailureIEs_t *locationRelatedDataFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &locationRelatedDataFailureIEs->cause);
    return 0;
}

int ranap_free_informationtransferindicationies(
    RANAP_InformationTransferIndicationIEs_t *informationTransferIndicationIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_InformationTransferID, &informationTransferIndicationIEs->informationTransferID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_ProvidedData, &informationTransferIndicationIEs->providedData);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &informationTransferIndicationIEs->cN_DomainIndicator);
    /* Optional field */
    if ((informationTransferIndicationIEs->presenceMask & INFORMATIONTRANSFERINDICATIONIES_RANAP_GLOBALCN_ID_PRESENT)
        == INFORMATIONTRANSFERINDICATIONIES_RANAP_GLOBALCN_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &informationTransferIndicationIEs->globalCN_ID);
    return 0;
}

int ranap_free_informationtransferconfirmationies(
    RANAP_InformationTransferConfirmationIEs_t *informationTransferConfirmationIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_InformationTransferID, &informationTransferConfirmationIEs->informationTransferID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &informationTransferConfirmationIEs->cN_DomainIndicator);
    /* Optional field */
    if ((informationTransferConfirmationIEs->presenceMask & INFORMATIONTRANSFERCONFIRMATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == INFORMATIONTRANSFERCONFIRMATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &informationTransferConfirmationIEs->criticalityDiagnostics);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &informationTransferConfirmationIEs->globalRNC_ID);
    return 0;
}

int ranap_free_informationtransferfailureies(
    RANAP_InformationTransferFailureIEs_t *informationTransferFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_InformationTransferID, &informationTransferFailureIEs->informationTransferID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &informationTransferFailureIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &informationTransferFailureIEs->cause);
    /* Optional field */
    if ((informationTransferFailureIEs->presenceMask & INFORMATIONTRANSFERFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == INFORMATIONTRANSFERFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &informationTransferFailureIEs->criticalityDiagnostics);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &informationTransferFailureIEs->globalRNC_ID);
    return 0;
}

int ranap_free_uespecificinformationindicationies(
    RANAP_UESpecificInformationIndicationIEs_t *ueSpecificInformationIndicationIEs) {

    /* Optional field */
    if ((ueSpecificInformationIndicationIEs->presenceMask & UESPECIFICINFORMATIONINDICATIONIES_RANAP_UESBI_IU_PRESENT)
        == UESPECIFICINFORMATIONINDICATIONIES_RANAP_UESBI_IU_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_UESBI_Iu, &ueSpecificInformationIndicationIEs->uesbI_Iu);
    return 0;
}

int ranap_free_directinformationtransferies(
    RANAP_DirectInformationTransferIEs_t *directInformationTransferIEs) {

    /* Optional field */
    if ((directInformationTransferIEs->presenceMask & DIRECTINFORMATIONTRANSFERIES_RANAP_INTERSYSTEMINFORMATIONTRANSFERTYPE_PRESENT)
        == DIRECTINFORMATIONTRANSFERIES_RANAP_INTERSYSTEMINFORMATIONTRANSFERTYPE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_InterSystemInformationTransferType, &directInformationTransferIEs->interSystemInformationTransferType);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &directInformationTransferIEs->cN_DomainIndicator);
    /* Optional field */
    if ((directInformationTransferIEs->presenceMask & DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALRNC_ID_PRESENT)
        == DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &directInformationTransferIEs->globalRNC_ID);
    /* Optional field */
    if ((directInformationTransferIEs->presenceMask & DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALCN_ID_PRESENT)
        == DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALCN_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &directInformationTransferIEs->globalCN_ID);
    return 0;
}

int ranap_free_uplinkinformationexchangerequesties(
    RANAP_UplinkInformationExchangeRequestIEs_t *uplinkInformationExchangeRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_InformationExchangeID, &uplinkInformationExchangeRequestIEs->informationExchangeID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_InformationExchangeType, &uplinkInformationExchangeRequestIEs->informationExchangeType);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &uplinkInformationExchangeRequestIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &uplinkInformationExchangeRequestIEs->globalRNC_ID);
    return 0;
}

int ranap_free_uplinkinformationexchangeresponseies(
    RANAP_UplinkInformationExchangeResponseIEs_t *uplinkInformationExchangeResponseIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_InformationExchangeID, &uplinkInformationExchangeResponseIEs->informationExchangeID);
    /* Optional field */
    if ((uplinkInformationExchangeResponseIEs->presenceMask & UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_INFORMATIONREQUESTED_PRESENT)
        == UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_INFORMATIONREQUESTED_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_InformationRequested, &uplinkInformationExchangeResponseIEs->informationRequested);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &uplinkInformationExchangeResponseIEs->cN_DomainIndicator);
    /* Optional field */
    if ((uplinkInformationExchangeResponseIEs->presenceMask & UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_GLOBALCN_ID_PRESENT)
        == UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_GLOBALCN_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &uplinkInformationExchangeResponseIEs->globalCN_ID);
    /* Optional field */
    if ((uplinkInformationExchangeResponseIEs->presenceMask & UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &uplinkInformationExchangeResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_uplinkinformationexchangefailureies(
    RANAP_UplinkInformationExchangeFailureIEs_t *uplinkInformationExchangeFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_InformationExchangeID, &uplinkInformationExchangeFailureIEs->informationExchangeID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CN_DomainIndicator, &uplinkInformationExchangeFailureIEs->cN_DomainIndicator);
    /* Optional field */
    if ((uplinkInformationExchangeFailureIEs->presenceMask & UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_GLOBALCN_ID_PRESENT)
        == UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_GLOBALCN_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &uplinkInformationExchangeFailureIEs->globalCN_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &uplinkInformationExchangeFailureIEs->cause);
    /* Optional field */
    if ((uplinkInformationExchangeFailureIEs->presenceMask & UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &uplinkInformationExchangeFailureIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmssessionstarties(
    RANAP_MBMSSessionStartIEs_t *mbmsSessionStartIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TMGI, &mbmsSessionStartIEs->tmgi);
    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONIDENTITY_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONIDENTITY_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_MBMSSessionIdentity, &mbmsSessionStartIEs->mbmsSessionIdentity);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_MBMSBearerServiceType, &mbmsSessionStartIEs->mbmsBearerServiceType);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IuSignallingConnectionIdentifier, &mbmsSessionStartIEs->iuSigConId);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAB_Parameters, &mbmsSessionStartIEs->raB_Parameters);
    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_PDP_TYPEINFORMATION_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_PDP_TYPEINFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_PDP_TypeInformation, &mbmsSessionStartIEs->pdP_TypeInformation);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_MBMSSessionDuration, &mbmsSessionStartIEs->mbmsSessionDuration);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_MBMSServiceArea, &mbmsSessionStartIEs->mbmsServiceArea);
    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_FREQUENCELAYERCONVERGENCEFLAG_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_FREQUENCELAYERCONVERGENCEFLAG_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_FrequenceLayerConvergenceFlag, &mbmsSessionStartIEs->frequenceLayerConvergenceFlag);
    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_RALISTOFIDLEMODEUES_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_RALISTOFIDLEMODEUES_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_RAListofIdleModeUEs, &mbmsSessionStartIEs->raListofIdleModeUEs);
    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_GLOBALCN_ID_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_GLOBALCN_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &mbmsSessionStartIEs->globalCN_ID);
    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONREPETITIONNUMBER_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONREPETITIONNUMBER_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_MBMSSessionRepetitionNumber, &mbmsSessionStartIEs->mbmsSessionRepetitionNumber);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TimeToMBMSDataTransfer, &mbmsSessionStartIEs->timeToMBMSDataTransfer);
    return 0;
}

int ranap_free_mbmssessionstartresponseies(
    RANAP_MBMSSessionStartResponseIEs_t *mbmsSessionStartResponseIEs) {

    /* Optional field */
    if ((mbmsSessionStartResponseIEs->presenceMask & MBMSSESSIONSTARTRESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT)
        == MBMSSESSIONSTARTRESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TransportLayerInformation, &mbmsSessionStartResponseIEs->transportLayerInformation);
    /* Optional field */
    if ((mbmsSessionStartResponseIEs->presenceMask & MBMSSESSIONSTARTRESPONSEIES_RANAP_CAUSE_PRESENT)
        == MBMSSESSIONSTARTRESPONSEIES_RANAP_CAUSE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmsSessionStartResponseIEs->cause);
    /* Optional field */
    if ((mbmsSessionStartResponseIEs->presenceMask & MBMSSESSIONSTARTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONSTARTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsSessionStartResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmssessionstartfailureies(
    RANAP_MBMSSessionStartFailureIEs_t *mbmsSessionStartFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmsSessionStartFailureIEs->cause);
    /* Optional field */
    if ((mbmsSessionStartFailureIEs->presenceMask & MBMSSESSIONSTARTFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONSTARTFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsSessionStartFailureIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmssessionupdateies(
    RANAP_MBMSSessionUpdateIEs_t *mbmsSessionUpdateIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SessionUpdateID, &mbmsSessionUpdateIEs->sessionUpdateID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_DeltaRAListofIdleModeUEs, &mbmsSessionUpdateIEs->deltaRAListofIdleModeUEs);
    return 0;
}

int ranap_free_mbmssessionupdateresponseies(
    RANAP_MBMSSessionUpdateResponseIEs_t *mbmsSessionUpdateResponseIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SessionUpdateID, &mbmsSessionUpdateResponseIEs->sessionUpdateID);
    /* Optional field */
    if ((mbmsSessionUpdateResponseIEs->presenceMask & MBMSSESSIONUPDATERESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT)
        == MBMSSESSIONUPDATERESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TransportLayerInformation, &mbmsSessionUpdateResponseIEs->transportLayerInformation);
    /* Optional field */
    if ((mbmsSessionUpdateResponseIEs->presenceMask & MBMSSESSIONUPDATERESPONSEIES_RANAP_CAUSE_PRESENT)
        == MBMSSESSIONUPDATERESPONSEIES_RANAP_CAUSE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmsSessionUpdateResponseIEs->cause);
    /* Optional field */
    if ((mbmsSessionUpdateResponseIEs->presenceMask & MBMSSESSIONUPDATERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONUPDATERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsSessionUpdateResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmssessionupdatefailureies(
    RANAP_MBMSSessionUpdateFailureIEs_t *mbmsSessionUpdateFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SessionUpdateID, &mbmsSessionUpdateFailureIEs->sessionUpdateID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmsSessionUpdateFailureIEs->cause);
    /* Optional field */
    if ((mbmsSessionUpdateFailureIEs->presenceMask & MBMSSESSIONUPDATEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONUPDATEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsSessionUpdateFailureIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmssessionstopies(
    RANAP_MBMSSessionStopIEs_t *mbmsSessionStopIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_MBMSCNDe_Registration, &mbmsSessionStopIEs->mbmscnDe_Registration);
    return 0;
}

int ranap_free_mbmssessionstopresponseies(
    RANAP_MBMSSessionStopResponseIEs_t *mbmsSessionStopResponseIEs) {

    /* Optional field */
    if ((mbmsSessionStopResponseIEs->presenceMask & MBMSSESSIONSTOPRESPONSEIES_RANAP_CAUSE_PRESENT)
        == MBMSSESSIONSTOPRESPONSEIES_RANAP_CAUSE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmsSessionStopResponseIEs->cause);
    /* Optional field */
    if ((mbmsSessionStopResponseIEs->presenceMask & MBMSSESSIONSTOPRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONSTOPRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsSessionStopResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmsuelinkingrequesties(
    RANAP_MBMSUELinkingRequestIEs_t *mbmsueLinkingRequestIEs) {

    /* Optional field */
    if ((mbmsueLinkingRequestIEs->presenceMask & MBMSUELINKINGREQUESTIES_RANAP_JOINEDMBMSBEARERSERVICE_IES_PRESENT)
        == MBMSUELINKINGREQUESTIES_RANAP_JOINEDMBMSBEARERSERVICE_IES_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_JoinedMBMSBearerService_IEs, &mbmsueLinkingRequestIEs->joinedMBMSBearerServicesList);
    /* Optional field */
    if ((mbmsueLinkingRequestIEs->presenceMask & MBMSUELINKINGREQUESTIES_RANAP_LEFTMBMSBEARERSERVICE_IES_PRESENT)
        == MBMSUELINKINGREQUESTIES_RANAP_LEFTMBMSBEARERSERVICE_IES_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_LeftMBMSBearerService_IEs, &mbmsueLinkingRequestIEs->leftMBMSBearerServicesList);
    return 0;
}

int ranap_free_mbmsuelinkingresponseies(
    RANAP_MBMSUELinkingResponseIEs_t *mbmsueLinkingResponseIEs) {

    /* Optional field */
    if ((mbmsueLinkingResponseIEs->presenceMask & MBMSUELINKINGRESPONSEIES_RANAP_UNSUCCESSFULLINKING_IES_PRESENT)
        == MBMSUELINKINGRESPONSEIES_RANAP_UNSUCCESSFULLINKING_IES_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_UnsuccessfulLinking_IEs, &mbmsueLinkingResponseIEs->unsuccessfulLinkingList);
    /* Optional field */
    if ((mbmsueLinkingResponseIEs->presenceMask & MBMSUELINKINGRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSUELINKINGRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsueLinkingResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmsregistrationrequesties(
    RANAP_MBMSRegistrationRequestIEs_t *mbmsRegistrationRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_MBMSRegistrationRequestType, &mbmsRegistrationRequestIEs->mbmsRegistrationRequestType);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TMGI, &mbmsRegistrationRequestIEs->tmgi);
    /* Optional field */
    if ((mbmsRegistrationRequestIEs->presenceMask & MBMSREGISTRATIONREQUESTIES_RANAP_GLOBALRNC_ID_PRESENT)
        == MBMSREGISTRATIONREQUESTIES_RANAP_GLOBALRNC_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &mbmsRegistrationRequestIEs->globalRNC_ID);
    return 0;
}

int ranap_free_mbmsregistrationresponseies(
    RANAP_MBMSRegistrationResponseIEs_t *mbmsRegistrationResponseIEs) {

    /* Optional field */
    if ((mbmsRegistrationResponseIEs->presenceMask & MBMSREGISTRATIONRESPONSEIES_RANAP_TMGI_PRESENT)
        == MBMSREGISTRATIONRESPONSEIES_RANAP_TMGI_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TMGI, &mbmsRegistrationResponseIEs->tmgi);
    /* Optional field */
    if ((mbmsRegistrationResponseIEs->presenceMask & MBMSREGISTRATIONRESPONSEIES_RANAP_GLOBALCN_ID_PRESENT)
        == MBMSREGISTRATIONRESPONSEIES_RANAP_GLOBALCN_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &mbmsRegistrationResponseIEs->globalCN_ID);
    /* Optional field */
    if ((mbmsRegistrationResponseIEs->presenceMask & MBMSREGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSREGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsRegistrationResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmsregistrationfailureies(
    RANAP_MBMSRegistrationFailureIEs_t *mbmsRegistrationFailureIEs) {

    /* Optional field */
    if ((mbmsRegistrationFailureIEs->presenceMask & MBMSREGISTRATIONFAILUREIES_RANAP_TMGI_PRESENT)
        == MBMSREGISTRATIONFAILUREIES_RANAP_TMGI_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TMGI, &mbmsRegistrationFailureIEs->tmgi);
    /* Optional field */
    if ((mbmsRegistrationFailureIEs->presenceMask & MBMSREGISTRATIONFAILUREIES_RANAP_GLOBALCN_ID_PRESENT)
        == MBMSREGISTRATIONFAILUREIES_RANAP_GLOBALCN_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &mbmsRegistrationFailureIEs->globalCN_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmsRegistrationFailureIEs->cause);
    /* Optional field */
    if ((mbmsRegistrationFailureIEs->presenceMask & MBMSREGISTRATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSREGISTRATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsRegistrationFailureIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmscnde_registrationrequesties(
    RANAP_MBMSCNDe_RegistrationRequestIEs_t *mbmscnDe_RegistrationRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TMGI, &mbmscnDe_RegistrationRequestIEs->tmgi);
    /* Optional field */
    if ((mbmscnDe_RegistrationRequestIEs->presenceMask & MBMSCNDE_REGISTRATIONREQUESTIES_RANAP_GLOBALCN_ID_PRESENT)
        == MBMSCNDE_REGISTRATIONREQUESTIES_RANAP_GLOBALCN_ID_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalCN_ID, &mbmscnDe_RegistrationRequestIEs->globalCN_ID);
    return 0;
}

int ranap_free_mbmscnde_registrationresponseies(
    RANAP_MBMSCNDe_RegistrationResponseIEs_t *mbmscnDe_RegistrationResponseIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TMGI, &mbmscnDe_RegistrationResponseIEs->tmgi);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_GlobalRNC_ID, &mbmscnDe_RegistrationResponseIEs->globalRNC_ID);
    /* Optional field */
    if ((mbmscnDe_RegistrationResponseIEs->presenceMask & MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CAUSE_PRESENT)
        == MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CAUSE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmscnDe_RegistrationResponseIEs->cause);
    /* Optional field */
    if ((mbmscnDe_RegistrationResponseIEs->presenceMask & MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmscnDe_RegistrationResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmsrabestablishmentindicationies(
    RANAP_MBMSRABEstablishmentIndicationIEs_t *mbmsrabEstablishmentIndicationIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_TransportLayerInformation, &mbmsrabEstablishmentIndicationIEs->transportLayerInformation);
    return 0;
}

int ranap_free_mbmsrabreleaserequesties(
    RANAP_MBMSRABReleaseRequestIEs_t *mbmsrabReleaseRequestIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmsrabReleaseRequestIEs->cause);
    return 0;
}

int ranap_free_mbmsrabreleaseies(
    RANAP_MBMSRABReleaseIEs_t *mbmsrabReleaseIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmsrabReleaseIEs->cause);
    /* Optional field */
    if ((mbmsrabReleaseIEs->presenceMask & MBMSRABRELEASEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSRABRELEASEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsrabReleaseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_mbmsrabreleasefailureies(
    RANAP_MBMSRABReleaseFailureIEs_t *mbmsrabReleaseFailureIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_Cause, &mbmsrabReleaseFailureIEs->cause);
    /* Optional field */
    if ((mbmsrabReleaseFailureIEs->presenceMask & MBMSRABRELEASEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSRABRELEASEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &mbmsrabReleaseFailureIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_srvcc_cskeysresponseies(
    RANAP_SRVCC_CSKeysResponseIEs_t *srvcC_CSKeysResponseIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_IntegrityProtectionKey, &srvcC_CSKeysResponseIEs->integrityProtectionKey);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_EncryptionKey, &srvcC_CSKeysResponseIEs->encryptionKey);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_SRVCC_Information, &srvcC_CSKeysResponseIEs->srvcC_Information);
    /* Optional field */
    if ((srvcC_CSKeysResponseIEs->presenceMask & SRVCC_CSKEYSRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == SRVCC_CSKEYSRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_CriticalityDiagnostics, &srvcC_CSKeysResponseIEs->criticalityDiagnostics);
    return 0;
}

int ranap_free_ueradiocapabilitymatchresponseies(
    RANAP_UeRadioCapabilityMatchResponseIEs_t *ueRadioCapabilityMatchResponseIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RANAP_VoiceSupportMatchIndicator, &ueRadioCapabilityMatchResponseIEs->voiceSupportMatchIndicator);
    return 0;
}

