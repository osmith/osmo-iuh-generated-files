/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/sabp/SABP-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/sabp/sabp_common.h>
#include <osmocom/sabp/sabp_ies_defs.h>

int sabp_decode_write_replace_ies(
    SABP_Write_Replace_IEs_t *write_Replace_IEs,
    ANY_t *any_p) {

    SABP_Write_Replace_t *write_Replace_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(write_Replace_IEs != NULL);

    memset(write_Replace_IEs, 0, sizeof(SABP_Write_Replace_IEs_t));
    SABP_DEBUG("Decoding message SABP_Write_Replace_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Write_Replace, (void**)&write_Replace_p);

    if (tempDecoded < 0 || write_Replace_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Write_Replace_IEs failed\n");
        return -1;
    }

    for (i = 0; i < write_Replace_p->write_Replace_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = write_Replace_p->write_Replace_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&write_Replace_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            case SABP_ProtocolIE_ID_id_New_Serial_Number:
            {
                SABP_New_Serial_Number_t *sabP_NewSerialNumber_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_New_Serial_Number, (void**)&sabP_NewSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE new_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_New_Serial_Number, sabP_NewSerialNumber_p);
                memcpy(&write_Replace_IEs->new_Serial_Number, sabP_NewSerialNumber_p, sizeof(SABP_New_Serial_Number_t));
                FREEMEM(sabP_NewSerialNumber_p);
            } break;
            /* Optional field */
            case SABP_ProtocolIE_ID_id_Old_Serial_Number:
            {
                SABP_Old_Serial_Number_t *sabP_OldSerialNumber_p = NULL;
                write_Replace_IEs->presenceMask |= WRITE_REPLACE_IES_SABP_OLD_SERIAL_NUMBER_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Old_Serial_Number, (void**)&sabP_OldSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE old_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Old_Serial_Number, sabP_OldSerialNumber_p);
                memcpy(&write_Replace_IEs->old_Serial_Number, sabP_OldSerialNumber_p, sizeof(SABP_Old_Serial_Number_t));
                FREEMEM(sabP_OldSerialNumber_p);
            } break;
            case SABP_ProtocolIE_ID_id_Service_Areas_List:
            {
                SABP_Service_Areas_List_t *sabP_ServiceAreasList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Service_Areas_List, (void**)&sabP_ServiceAreasList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE service_Areas_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Service_Areas_List, sabP_ServiceAreasList_p);
                memcpy(&write_Replace_IEs->service_Areas_List, sabP_ServiceAreasList_p, sizeof(SABP_Service_Areas_List_t));
                FREEMEM(sabP_ServiceAreasList_p);
            } break;
            /* Optional field */
            case SABP_ProtocolIE_ID_id_Category:
            {
                SABP_Category_t *sabP_Category_p = NULL;
                write_Replace_IEs->presenceMask |= WRITE_REPLACE_IES_SABP_CATEGORY_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Category, (void**)&sabP_Category_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE category failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Category, sabP_Category_p);
                memcpy(&write_Replace_IEs->category, sabP_Category_p, sizeof(SABP_Category_t));
                FREEMEM(sabP_Category_p);
            } break;
            case SABP_ProtocolIE_ID_id_Repetition_Period:
            {
                SABP_Repetition_Period_t *sabP_RepetitionPeriod_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Repetition_Period, (void**)&sabP_RepetitionPeriod_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE repetition_Period failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Repetition_Period, sabP_RepetitionPeriod_p);
                memcpy(&write_Replace_IEs->repetition_Period, sabP_RepetitionPeriod_p, sizeof(SABP_Repetition_Period_t));
                FREEMEM(sabP_RepetitionPeriod_p);
            } break;
            case SABP_ProtocolIE_ID_id_Data_Coding_Scheme:
            {
                SABP_Data_Coding_Scheme_t *sabP_DataCodingScheme_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Data_Coding_Scheme, (void**)&sabP_DataCodingScheme_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE data_Coding_Scheme failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Data_Coding_Scheme, sabP_DataCodingScheme_p);
                memcpy(&write_Replace_IEs->data_Coding_Scheme, sabP_DataCodingScheme_p, sizeof(SABP_Data_Coding_Scheme_t));
                FREEMEM(sabP_DataCodingScheme_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message write_replace_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Write_Replace, write_Replace_p);
    return decoded;
}

int sabp_decode_write_replace_complete_ies(
    SABP_Write_Replace_Complete_IEs_t *write_Replace_Complete_IEs,
    ANY_t *any_p) {

    SABP_Write_Replace_Complete_t *write_Replace_Complete_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(write_Replace_Complete_IEs != NULL);

    memset(write_Replace_Complete_IEs, 0, sizeof(SABP_Write_Replace_Complete_IEs_t));
    SABP_DEBUG("Decoding message SABP_Write_Replace_Complete_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Write_Replace_Complete, (void**)&write_Replace_Complete_p);

    if (tempDecoded < 0 || write_Replace_Complete_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Write_Replace_Complete_IEs failed\n");
        return -1;
    }

    for (i = 0; i < write_Replace_Complete_p->write_Replace_Complete_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = write_Replace_Complete_p->write_Replace_Complete_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&write_Replace_Complete_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            case SABP_ProtocolIE_ID_id_New_Serial_Number:
            {
                SABP_New_Serial_Number_t *sabP_NewSerialNumber_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_New_Serial_Number, (void**)&sabP_NewSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE new_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_New_Serial_Number, sabP_NewSerialNumber_p);
                memcpy(&write_Replace_Complete_IEs->new_Serial_Number, sabP_NewSerialNumber_p, sizeof(SABP_New_Serial_Number_t));
                FREEMEM(sabP_NewSerialNumber_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message write_replace_complete_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Write_Replace_Complete, write_Replace_Complete_p);
    return decoded;
}

int sabp_decode_write_replace_failure_ies(
    SABP_Write_Replace_Failure_IEs_t *write_Replace_Failure_IEs,
    ANY_t *any_p) {

    SABP_Write_Replace_Failure_t *write_Replace_Failure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(write_Replace_Failure_IEs != NULL);

    memset(write_Replace_Failure_IEs, 0, sizeof(SABP_Write_Replace_Failure_IEs_t));
    SABP_DEBUG("Decoding message SABP_Write_Replace_Failure_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Write_Replace_Failure, (void**)&write_Replace_Failure_p);

    if (tempDecoded < 0 || write_Replace_Failure_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Write_Replace_Failure_IEs failed\n");
        return -1;
    }

    for (i = 0; i < write_Replace_Failure_p->write_Replace_Failure_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = write_Replace_Failure_p->write_Replace_Failure_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&write_Replace_Failure_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            case SABP_ProtocolIE_ID_id_New_Serial_Number:
            {
                SABP_New_Serial_Number_t *sabP_NewSerialNumber_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_New_Serial_Number, (void**)&sabP_NewSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE new_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_New_Serial_Number, sabP_NewSerialNumber_p);
                memcpy(&write_Replace_Failure_IEs->new_Serial_Number, sabP_NewSerialNumber_p, sizeof(SABP_New_Serial_Number_t));
                FREEMEM(sabP_NewSerialNumber_p);
            } break;
            case SABP_ProtocolIE_ID_id_Failure_List:
            {
                SABP_Failure_List_t *sabP_FailureList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Failure_List, (void**)&sabP_FailureList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE failure_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Failure_List, sabP_FailureList_p);
                memcpy(&write_Replace_Failure_IEs->failure_List, sabP_FailureList_p, sizeof(SABP_Failure_List_t));
                FREEMEM(sabP_FailureList_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message write_replace_failure_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Write_Replace_Failure, write_Replace_Failure_p);
    return decoded;
}

int sabp_decode_kill_ies(
    SABP_Kill_IEs_t *kill_IEs,
    ANY_t *any_p) {

    SABP_Kill_t *kill_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(kill_IEs != NULL);

    memset(kill_IEs, 0, sizeof(SABP_Kill_IEs_t));
    SABP_DEBUG("Decoding message SABP_Kill_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Kill, (void**)&kill_p);

    if (tempDecoded < 0 || kill_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Kill_IEs failed\n");
        return -1;
    }

    for (i = 0; i < kill_p->kill_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = kill_p->kill_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&kill_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            case SABP_ProtocolIE_ID_id_Old_Serial_Number:
            {
                SABP_Old_Serial_Number_t *sabP_OldSerialNumber_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Old_Serial_Number, (void**)&sabP_OldSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE old_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Old_Serial_Number, sabP_OldSerialNumber_p);
                memcpy(&kill_IEs->old_Serial_Number, sabP_OldSerialNumber_p, sizeof(SABP_Old_Serial_Number_t));
                FREEMEM(sabP_OldSerialNumber_p);
            } break;
            case SABP_ProtocolIE_ID_id_Service_Areas_List:
            {
                SABP_Service_Areas_List_t *sabP_ServiceAreasList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Service_Areas_List, (void**)&sabP_ServiceAreasList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE service_Areas_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Service_Areas_List, sabP_ServiceAreasList_p);
                memcpy(&kill_IEs->service_Areas_List, sabP_ServiceAreasList_p, sizeof(SABP_Service_Areas_List_t));
                FREEMEM(sabP_ServiceAreasList_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message kill_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Kill, kill_p);
    return decoded;
}

int sabp_decode_kill_complete_ies(
    SABP_Kill_Complete_IEs_t *kill_Complete_IEs,
    ANY_t *any_p) {

    SABP_Kill_Complete_t *kill_Complete_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(kill_Complete_IEs != NULL);

    memset(kill_Complete_IEs, 0, sizeof(SABP_Kill_Complete_IEs_t));
    SABP_DEBUG("Decoding message SABP_Kill_Complete_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Kill_Complete, (void**)&kill_Complete_p);

    if (tempDecoded < 0 || kill_Complete_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Kill_Complete_IEs failed\n");
        return -1;
    }

    for (i = 0; i < kill_Complete_p->kill_Complete_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = kill_Complete_p->kill_Complete_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&kill_Complete_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            case SABP_ProtocolIE_ID_id_Old_Serial_Number:
            {
                SABP_Old_Serial_Number_t *sabP_OldSerialNumber_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Old_Serial_Number, (void**)&sabP_OldSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE old_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Old_Serial_Number, sabP_OldSerialNumber_p);
                memcpy(&kill_Complete_IEs->old_Serial_Number, sabP_OldSerialNumber_p, sizeof(SABP_Old_Serial_Number_t));
                FREEMEM(sabP_OldSerialNumber_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message kill_complete_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Kill_Complete, kill_Complete_p);
    return decoded;
}

int sabp_decode_kill_failure_ies(
    SABP_Kill_Failure_IEs_t *kill_Failure_IEs,
    ANY_t *any_p) {

    SABP_Kill_Failure_t *kill_Failure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(kill_Failure_IEs != NULL);

    memset(kill_Failure_IEs, 0, sizeof(SABP_Kill_Failure_IEs_t));
    SABP_DEBUG("Decoding message SABP_Kill_Failure_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Kill_Failure, (void**)&kill_Failure_p);

    if (tempDecoded < 0 || kill_Failure_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Kill_Failure_IEs failed\n");
        return -1;
    }

    for (i = 0; i < kill_Failure_p->kill_Failure_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = kill_Failure_p->kill_Failure_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&kill_Failure_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            case SABP_ProtocolIE_ID_id_Old_Serial_Number:
            {
                SABP_Old_Serial_Number_t *sabP_OldSerialNumber_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Old_Serial_Number, (void**)&sabP_OldSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE old_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Old_Serial_Number, sabP_OldSerialNumber_p);
                memcpy(&kill_Failure_IEs->old_Serial_Number, sabP_OldSerialNumber_p, sizeof(SABP_Old_Serial_Number_t));
                FREEMEM(sabP_OldSerialNumber_p);
            } break;
            case SABP_ProtocolIE_ID_id_Failure_List:
            {
                SABP_Failure_List_t *sabP_FailureList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Failure_List, (void**)&sabP_FailureList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE failure_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Failure_List, sabP_FailureList_p);
                memcpy(&kill_Failure_IEs->failure_List, sabP_FailureList_p, sizeof(SABP_Failure_List_t));
                FREEMEM(sabP_FailureList_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message kill_failure_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Kill_Failure, kill_Failure_p);
    return decoded;
}

int sabp_decode_load_query_ies(
    SABP_Load_Query_IEs_t *load_Query_IEs,
    ANY_t *any_p) {

    SABP_Load_Query_t *load_Query_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(load_Query_IEs != NULL);

    memset(load_Query_IEs, 0, sizeof(SABP_Load_Query_IEs_t));
    SABP_DEBUG("Decoding message SABP_Load_Query_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Load_Query, (void**)&load_Query_p);

    if (tempDecoded < 0 || load_Query_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Load_Query_IEs failed\n");
        return -1;
    }

    for (i = 0; i < load_Query_p->load_Query_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = load_Query_p->load_Query_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Service_Areas_List:
            {
                SABP_Service_Areas_List_t *sabP_ServiceAreasList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Service_Areas_List, (void**)&sabP_ServiceAreasList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE service_Areas_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Service_Areas_List, sabP_ServiceAreasList_p);
                memcpy(&load_Query_IEs->service_Areas_List, sabP_ServiceAreasList_p, sizeof(SABP_Service_Areas_List_t));
                FREEMEM(sabP_ServiceAreasList_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message load_query_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Load_Query, load_Query_p);
    return decoded;
}

int sabp_decode_load_query_failure_ies(
    SABP_Load_Query_Failure_IEs_t *load_Query_Failure_IEs,
    ANY_t *any_p) {

    SABP_Load_Query_Failure_t *load_Query_Failure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(load_Query_Failure_IEs != NULL);

    memset(load_Query_Failure_IEs, 0, sizeof(SABP_Load_Query_Failure_IEs_t));
    SABP_DEBUG("Decoding message SABP_Load_Query_Failure_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Load_Query_Failure, (void**)&load_Query_Failure_p);

    if (tempDecoded < 0 || load_Query_Failure_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Load_Query_Failure_IEs failed\n");
        return -1;
    }

    for (i = 0; i < load_Query_Failure_p->load_Query_Failure_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = load_Query_Failure_p->load_Query_Failure_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Failure_List:
            {
                SABP_Failure_List_t *sabP_FailureList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Failure_List, (void**)&sabP_FailureList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE failure_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Failure_List, sabP_FailureList_p);
                memcpy(&load_Query_Failure_IEs->failure_List, sabP_FailureList_p, sizeof(SABP_Failure_List_t));
                FREEMEM(sabP_FailureList_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message load_query_failure_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Load_Query_Failure, load_Query_Failure_p);
    return decoded;
}

int sabp_decode_message_status_query_ies(
    SABP_Message_Status_Query_IEs_t *message_Status_Query_IEs,
    ANY_t *any_p) {

    SABP_Message_Status_Query_t *message_Status_Query_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(message_Status_Query_IEs != NULL);

    memset(message_Status_Query_IEs, 0, sizeof(SABP_Message_Status_Query_IEs_t));
    SABP_DEBUG("Decoding message SABP_Message_Status_Query_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Message_Status_Query, (void**)&message_Status_Query_p);

    if (tempDecoded < 0 || message_Status_Query_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Message_Status_Query_IEs failed\n");
        return -1;
    }

    for (i = 0; i < message_Status_Query_p->message_Status_Query_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = message_Status_Query_p->message_Status_Query_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&message_Status_Query_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            case SABP_ProtocolIE_ID_id_Old_Serial_Number:
            {
                SABP_Old_Serial_Number_t *sabP_OldSerialNumber_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Old_Serial_Number, (void**)&sabP_OldSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE old_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Old_Serial_Number, sabP_OldSerialNumber_p);
                memcpy(&message_Status_Query_IEs->old_Serial_Number, sabP_OldSerialNumber_p, sizeof(SABP_Old_Serial_Number_t));
                FREEMEM(sabP_OldSerialNumber_p);
            } break;
            case SABP_ProtocolIE_ID_id_Service_Areas_List:
            {
                SABP_Service_Areas_List_t *sabP_ServiceAreasList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Service_Areas_List, (void**)&sabP_ServiceAreasList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE service_Areas_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Service_Areas_List, sabP_ServiceAreasList_p);
                memcpy(&message_Status_Query_IEs->service_Areas_List, sabP_ServiceAreasList_p, sizeof(SABP_Service_Areas_List_t));
                FREEMEM(sabP_ServiceAreasList_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message message_status_query_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Message_Status_Query, message_Status_Query_p);
    return decoded;
}

int sabp_decode_message_status_query_complete_ies(
    SABP_Message_Status_Query_Complete_IEs_t *message_Status_Query_Complete_IEs,
    ANY_t *any_p) {

    SABP_Message_Status_Query_Complete_t *message_Status_Query_Complete_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(message_Status_Query_Complete_IEs != NULL);

    memset(message_Status_Query_Complete_IEs, 0, sizeof(SABP_Message_Status_Query_Complete_IEs_t));
    SABP_DEBUG("Decoding message SABP_Message_Status_Query_Complete_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Message_Status_Query_Complete, (void**)&message_Status_Query_Complete_p);

    if (tempDecoded < 0 || message_Status_Query_Complete_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Message_Status_Query_Complete_IEs failed\n");
        return -1;
    }

    for (i = 0; i < message_Status_Query_Complete_p->message_Status_Query_Complete_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = message_Status_Query_Complete_p->message_Status_Query_Complete_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&message_Status_Query_Complete_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            case SABP_ProtocolIE_ID_id_Old_Serial_Number:
            {
                SABP_Old_Serial_Number_t *sabP_OldSerialNumber_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Old_Serial_Number, (void**)&sabP_OldSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE old_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Old_Serial_Number, sabP_OldSerialNumber_p);
                memcpy(&message_Status_Query_Complete_IEs->old_Serial_Number, sabP_OldSerialNumber_p, sizeof(SABP_Old_Serial_Number_t));
                FREEMEM(sabP_OldSerialNumber_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message message_status_query_complete_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Message_Status_Query_Complete, message_Status_Query_Complete_p);
    return decoded;
}

int sabp_decode_message_status_query_failure_ies(
    SABP_Message_Status_Query_Failure_IEs_t *message_Status_Query_Failure_IEs,
    ANY_t *any_p) {

    SABP_Message_Status_Query_Failure_t *message_Status_Query_Failure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(message_Status_Query_Failure_IEs != NULL);

    memset(message_Status_Query_Failure_IEs, 0, sizeof(SABP_Message_Status_Query_Failure_IEs_t));
    SABP_DEBUG("Decoding message SABP_Message_Status_Query_Failure_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Message_Status_Query_Failure, (void**)&message_Status_Query_Failure_p);

    if (tempDecoded < 0 || message_Status_Query_Failure_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Message_Status_Query_Failure_IEs failed\n");
        return -1;
    }

    for (i = 0; i < message_Status_Query_Failure_p->message_Status_Query_Failure_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = message_Status_Query_Failure_p->message_Status_Query_Failure_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&message_Status_Query_Failure_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            case SABP_ProtocolIE_ID_id_Failure_List:
            {
                SABP_Failure_List_t *sabP_FailureList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Failure_List, (void**)&sabP_FailureList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE failure_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Failure_List, sabP_FailureList_p);
                memcpy(&message_Status_Query_Failure_IEs->failure_List, sabP_FailureList_p, sizeof(SABP_Failure_List_t));
                FREEMEM(sabP_FailureList_p);
            } break;
            case SABP_ProtocolIE_ID_id_Old_Serial_Number:
            {
                SABP_Old_Serial_Number_t *sabP_OldSerialNumber_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Old_Serial_Number, (void**)&sabP_OldSerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE old_Serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Old_Serial_Number, sabP_OldSerialNumber_p);
                memcpy(&message_Status_Query_Failure_IEs->old_Serial_Number, sabP_OldSerialNumber_p, sizeof(SABP_Old_Serial_Number_t));
                FREEMEM(sabP_OldSerialNumber_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message message_status_query_failure_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Message_Status_Query_Failure, message_Status_Query_Failure_p);
    return decoded;
}

int sabp_decode_reset_ies(
    SABP_Reset_IEs_t *reset_IEs,
    ANY_t *any_p) {

    SABP_Reset_t *reset_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(reset_IEs != NULL);

    memset(reset_IEs, 0, sizeof(SABP_Reset_IEs_t));
    SABP_DEBUG("Decoding message SABP_Reset_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Reset, (void**)&reset_p);

    if (tempDecoded < 0 || reset_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Reset_IEs failed\n");
        return -1;
    }

    for (i = 0; i < reset_p->reset_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = reset_p->reset_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Service_Areas_List:
            {
                SABP_Service_Areas_List_t *sabP_ServiceAreasList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Service_Areas_List, (void**)&sabP_ServiceAreasList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE service_Areas_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Service_Areas_List, sabP_ServiceAreasList_p);
                memcpy(&reset_IEs->service_Areas_List, sabP_ServiceAreasList_p, sizeof(SABP_Service_Areas_List_t));
                FREEMEM(sabP_ServiceAreasList_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message reset_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Reset, reset_p);
    return decoded;
}

int sabp_decode_reset_complete_ies(
    SABP_Reset_Complete_IEs_t *reset_Complete_IEs,
    ANY_t *any_p) {

    SABP_Reset_Complete_t *reset_Complete_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(reset_Complete_IEs != NULL);

    memset(reset_Complete_IEs, 0, sizeof(SABP_Reset_Complete_IEs_t));
    SABP_DEBUG("Decoding message SABP_Reset_Complete_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Reset_Complete, (void**)&reset_Complete_p);

    if (tempDecoded < 0 || reset_Complete_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Reset_Complete_IEs failed\n");
        return -1;
    }

    for (i = 0; i < reset_Complete_p->reset_Complete_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = reset_Complete_p->reset_Complete_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Service_Areas_List:
            {
                SABP_Service_Areas_List_t *sabP_ServiceAreasList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Service_Areas_List, (void**)&sabP_ServiceAreasList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE service_Areas_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Service_Areas_List, sabP_ServiceAreasList_p);
                memcpy(&reset_Complete_IEs->service_Areas_List, sabP_ServiceAreasList_p, sizeof(SABP_Service_Areas_List_t));
                FREEMEM(sabP_ServiceAreasList_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message reset_complete_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Reset_Complete, reset_Complete_p);
    return decoded;
}

int sabp_decode_reset_failure_ies(
    SABP_Reset_Failure_IEs_t *reset_Failure_IEs,
    ANY_t *any_p) {

    SABP_Reset_Failure_t *reset_Failure_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(reset_Failure_IEs != NULL);

    memset(reset_Failure_IEs, 0, sizeof(SABP_Reset_Failure_IEs_t));
    SABP_DEBUG("Decoding message SABP_Reset_Failure_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Reset_Failure, (void**)&reset_Failure_p);

    if (tempDecoded < 0 || reset_Failure_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Reset_Failure_IEs failed\n");
        return -1;
    }

    for (i = 0; i < reset_Failure_p->reset_Failure_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = reset_Failure_p->reset_Failure_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Failure_List:
            {
                SABP_Failure_List_t *sabP_FailureList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Failure_List, (void**)&sabP_FailureList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE failure_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Failure_List, sabP_FailureList_p);
                memcpy(&reset_Failure_IEs->failure_List, sabP_FailureList_p, sizeof(SABP_Failure_List_t));
                FREEMEM(sabP_FailureList_p);
            } break;
            /* Optional field */
            case SABP_ProtocolIE_ID_id_Service_Areas_List:
            {
                SABP_Service_Areas_List_t *sabP_ServiceAreasList_p = NULL;
                reset_Failure_IEs->presenceMask |= RESET_FAILURE_IES_SABP_SERVICE_AREAS_LIST_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Service_Areas_List, (void**)&sabP_ServiceAreasList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE service_Areas_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Service_Areas_List, sabP_ServiceAreasList_p);
                memcpy(&reset_Failure_IEs->service_Areas_List, sabP_ServiceAreasList_p, sizeof(SABP_Service_Areas_List_t));
                FREEMEM(sabP_ServiceAreasList_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message reset_failure_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Reset_Failure, reset_Failure_p);
    return decoded;
}

int sabp_decode_restart_ies(
    SABP_Restart_IEs_t *restart_IEs,
    ANY_t *any_p) {

    SABP_Restart_t *restart_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(restart_IEs != NULL);

    memset(restart_IEs, 0, sizeof(SABP_Restart_IEs_t));
    SABP_DEBUG("Decoding message SABP_Restart_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Restart, (void**)&restart_p);

    if (tempDecoded < 0 || restart_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Restart_IEs failed\n");
        return -1;
    }

    for (i = 0; i < restart_p->restart_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = restart_p->restart_ies.list.array[i];
        switch(ie_p->id) {
            case SABP_ProtocolIE_ID_id_Service_Areas_List:
            {
                SABP_Service_Areas_List_t *sabP_ServiceAreasList_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Service_Areas_List, (void**)&sabP_ServiceAreasList_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE service_Areas_List failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Service_Areas_List, sabP_ServiceAreasList_p);
                memcpy(&restart_IEs->service_Areas_List, sabP_ServiceAreasList_p, sizeof(SABP_Service_Areas_List_t));
                FREEMEM(sabP_ServiceAreasList_p);
            } break;
            /* Optional field */
            case SABP_ProtocolIE_ID_id_Recovery_Indication:
            {
                SABP_Recovery_Indication_t *sabP_RecoveryIndication_p = NULL;
                restart_IEs->presenceMask |= RESTART_IES_SABP_RECOVERY_INDICATION_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Recovery_Indication, (void**)&sabP_RecoveryIndication_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE recovery_Indication failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Recovery_Indication, sabP_RecoveryIndication_p);
                memcpy(&restart_IEs->recovery_Indication, sabP_RecoveryIndication_p, sizeof(SABP_Recovery_Indication_t));
                FREEMEM(sabP_RecoveryIndication_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message restart_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Restart, restart_p);
    return decoded;
}

int sabp_decode_error_indication_ies(
    SABP_Error_Indication_IEs_t *error_Indication_IEs,
    ANY_t *any_p) {

    SABP_Error_Indication_t *error_Indication_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(error_Indication_IEs != NULL);

    memset(error_Indication_IEs, 0, sizeof(SABP_Error_Indication_IEs_t));
    SABP_DEBUG("Decoding message SABP_Error_Indication_IEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_SABP_Error_Indication, (void**)&error_Indication_p);

    if (tempDecoded < 0 || error_Indication_p == NULL) {
        SABP_DEBUG("Decoding of message SABP_Error_Indication_IEs failed\n");
        return -1;
    }

    for (i = 0; i < error_Indication_p->error_Indication_ies.list.count; i++) {
        SABP_IE_t *ie_p;
        ie_p = error_Indication_p->error_Indication_ies.list.array[i];
        switch(ie_p->id) {
            /* Optional field */
            case SABP_ProtocolIE_ID_id_Message_Identifier:
            {
                SABP_Message_Identifier_t *sabP_MessageIdentifier_p = NULL;
                error_Indication_IEs->presenceMask |= ERROR_INDICATION_IES_SABP_MESSAGE_IDENTIFIER_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Message_Identifier, (void**)&sabP_MessageIdentifier_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE message_Identifier failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Message_Identifier, sabP_MessageIdentifier_p);
                memcpy(&error_Indication_IEs->message_Identifier, sabP_MessageIdentifier_p, sizeof(SABP_Message_Identifier_t));
                FREEMEM(sabP_MessageIdentifier_p);
            } break;
            /* Optional field */
            case SABP_ProtocolIE_ID_id_Serial_Number:
            {
                SABP_Serial_Number_t *sabP_SerialNumber_p = NULL;
                error_Indication_IEs->presenceMask |= ERROR_INDICATION_IES_SABP_SERIAL_NUMBER_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Serial_Number, (void**)&sabP_SerialNumber_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE serial_Number failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Serial_Number, sabP_SerialNumber_p);
                memcpy(&error_Indication_IEs->serial_Number, sabP_SerialNumber_p, sizeof(SABP_Serial_Number_t));
                FREEMEM(sabP_SerialNumber_p);
            } break;
            /* Optional field */
            case SABP_ProtocolIE_ID_id_Cause:
            {
                SABP_Cause_t *sabP_Cause_p = NULL;
                error_Indication_IEs->presenceMask |= ERROR_INDICATION_IES_SABP_CAUSE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_SABP_Cause, (void**)&sabP_Cause_p);
                if (tempDecoded < 0) {
                    SABP_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_SABP_Cause, sabP_Cause_p);
                memcpy(&error_Indication_IEs->cause, sabP_Cause_p, sizeof(SABP_Cause_t));
                FREEMEM(sabP_Cause_p);
            } break;
            default:
                SABP_DEBUG("Unknown protocol IE id (%d) for message error_indication_ies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_SABP_Error_Indication, error_Indication_p);
    return decoded;
}

int sabp_free_write_replace_ies(
    SABP_Write_Replace_IEs_t *write_Replace_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &write_Replace_IEs->message_Identifier);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_New_Serial_Number, &write_Replace_IEs->new_Serial_Number);
    /* Optional field */
    if ((write_Replace_IEs->presenceMask & WRITE_REPLACE_IES_SABP_OLD_SERIAL_NUMBER_PRESENT)
        == WRITE_REPLACE_IES_SABP_OLD_SERIAL_NUMBER_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Old_Serial_Number, &write_Replace_IEs->old_Serial_Number);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Service_Areas_List, &write_Replace_IEs->service_Areas_List);
    /* Optional field */
    if ((write_Replace_IEs->presenceMask & WRITE_REPLACE_IES_SABP_CATEGORY_PRESENT)
        == WRITE_REPLACE_IES_SABP_CATEGORY_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Category, &write_Replace_IEs->category);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Repetition_Period, &write_Replace_IEs->repetition_Period);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Data_Coding_Scheme, &write_Replace_IEs->data_Coding_Scheme);
    return 0;
}

int sabp_free_write_replace_complete_ies(
    SABP_Write_Replace_Complete_IEs_t *write_Replace_Complete_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &write_Replace_Complete_IEs->message_Identifier);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_New_Serial_Number, &write_Replace_Complete_IEs->new_Serial_Number);
    return 0;
}

int sabp_free_write_replace_failure_ies(
    SABP_Write_Replace_Failure_IEs_t *write_Replace_Failure_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &write_Replace_Failure_IEs->message_Identifier);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_New_Serial_Number, &write_Replace_Failure_IEs->new_Serial_Number);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Failure_List, &write_Replace_Failure_IEs->failure_List);
    return 0;
}

int sabp_free_kill_ies(
    SABP_Kill_IEs_t *kill_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &kill_IEs->message_Identifier);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Old_Serial_Number, &kill_IEs->old_Serial_Number);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Service_Areas_List, &kill_IEs->service_Areas_List);
    return 0;
}

int sabp_free_kill_complete_ies(
    SABP_Kill_Complete_IEs_t *kill_Complete_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &kill_Complete_IEs->message_Identifier);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Old_Serial_Number, &kill_Complete_IEs->old_Serial_Number);
    return 0;
}

int sabp_free_kill_failure_ies(
    SABP_Kill_Failure_IEs_t *kill_Failure_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &kill_Failure_IEs->message_Identifier);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Old_Serial_Number, &kill_Failure_IEs->old_Serial_Number);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Failure_List, &kill_Failure_IEs->failure_List);
    return 0;
}

int sabp_free_load_query_ies(
    SABP_Load_Query_IEs_t *load_Query_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Service_Areas_List, &load_Query_IEs->service_Areas_List);
    return 0;
}

int sabp_free_load_query_failure_ies(
    SABP_Load_Query_Failure_IEs_t *load_Query_Failure_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Failure_List, &load_Query_Failure_IEs->failure_List);
    return 0;
}

int sabp_free_message_status_query_ies(
    SABP_Message_Status_Query_IEs_t *message_Status_Query_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &message_Status_Query_IEs->message_Identifier);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Old_Serial_Number, &message_Status_Query_IEs->old_Serial_Number);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Service_Areas_List, &message_Status_Query_IEs->service_Areas_List);
    return 0;
}

int sabp_free_message_status_query_complete_ies(
    SABP_Message_Status_Query_Complete_IEs_t *message_Status_Query_Complete_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &message_Status_Query_Complete_IEs->message_Identifier);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Old_Serial_Number, &message_Status_Query_Complete_IEs->old_Serial_Number);
    return 0;
}

int sabp_free_message_status_query_failure_ies(
    SABP_Message_Status_Query_Failure_IEs_t *message_Status_Query_Failure_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &message_Status_Query_Failure_IEs->message_Identifier);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Failure_List, &message_Status_Query_Failure_IEs->failure_List);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Old_Serial_Number, &message_Status_Query_Failure_IEs->old_Serial_Number);
    return 0;
}

int sabp_free_reset_ies(
    SABP_Reset_IEs_t *reset_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Service_Areas_List, &reset_IEs->service_Areas_List);
    return 0;
}

int sabp_free_reset_complete_ies(
    SABP_Reset_Complete_IEs_t *reset_Complete_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Service_Areas_List, &reset_Complete_IEs->service_Areas_List);
    return 0;
}

int sabp_free_reset_failure_ies(
    SABP_Reset_Failure_IEs_t *reset_Failure_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Failure_List, &reset_Failure_IEs->failure_List);
    /* Optional field */
    if ((reset_Failure_IEs->presenceMask & RESET_FAILURE_IES_SABP_SERVICE_AREAS_LIST_PRESENT)
        == RESET_FAILURE_IES_SABP_SERVICE_AREAS_LIST_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Service_Areas_List, &reset_Failure_IEs->service_Areas_List);
    return 0;
}

int sabp_free_restart_ies(
    SABP_Restart_IEs_t *restart_IEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Service_Areas_List, &restart_IEs->service_Areas_List);
    /* Optional field */
    if ((restart_IEs->presenceMask & RESTART_IES_SABP_RECOVERY_INDICATION_PRESENT)
        == RESTART_IES_SABP_RECOVERY_INDICATION_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Recovery_Indication, &restart_IEs->recovery_Indication);
    return 0;
}

int sabp_free_error_indication_ies(
    SABP_Error_Indication_IEs_t *error_Indication_IEs) {

    /* Optional field */
    if ((error_Indication_IEs->presenceMask & ERROR_INDICATION_IES_SABP_MESSAGE_IDENTIFIER_PRESENT)
        == ERROR_INDICATION_IES_SABP_MESSAGE_IDENTIFIER_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Message_Identifier, &error_Indication_IEs->message_Identifier);
    /* Optional field */
    if ((error_Indication_IEs->presenceMask & ERROR_INDICATION_IES_SABP_SERIAL_NUMBER_PRESENT)
        == ERROR_INDICATION_IES_SABP_SERIAL_NUMBER_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Serial_Number, &error_Indication_IEs->serial_Number);
    /* Optional field */
    if ((error_Indication_IEs->presenceMask & ERROR_INDICATION_IES_SABP_CAUSE_PRESENT)
        == ERROR_INDICATION_IES_SABP_CAUSE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_SABP_Cause, &error_Indication_IEs->cause);
    return 0;
}

