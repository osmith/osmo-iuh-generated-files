/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/sabp/SABP-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/sabp/sabp_common.h>
#include <osmocom/sabp/sabp_ies_defs.h>

int sabp_encode_write_replace_ies(
    SABP_Write_Replace_t *write_Replace,
    SABP_Write_Replace_IEs_t *write_Replace_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Message_Identifier,
                          &write_Replace_IEs->message_Identifier)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace->write_Replace_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_New_Serial_Number,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_New_Serial_Number,
                          &write_Replace_IEs->new_Serial_Number)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace->write_Replace_ies.list, ie);

    /* Optional field */
    if ((write_Replace_IEs->presenceMask & WRITE_REPLACE_IES_SABP_OLD_SERIAL_NUMBER_PRESENT)
        == WRITE_REPLACE_IES_SABP_OLD_SERIAL_NUMBER_PRESENT) {
        if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Old_Serial_Number,
                              SABP_Criticality_ignore,
                              &asn_DEF_SABP_Old_Serial_Number,
                              &write_Replace_IEs->old_Serial_Number)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&write_Replace->write_Replace_ies.list, ie);
    }

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Service_Areas_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Service_Areas_List,
                          &write_Replace_IEs->service_Areas_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace->write_Replace_ies.list, ie);

    /* Optional field */
    if ((write_Replace_IEs->presenceMask & WRITE_REPLACE_IES_SABP_CATEGORY_PRESENT)
        == WRITE_REPLACE_IES_SABP_CATEGORY_PRESENT) {
        if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Category,
                              SABP_Criticality_ignore,
                              &asn_DEF_SABP_Category,
                              &write_Replace_IEs->category)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&write_Replace->write_Replace_ies.list, ie);
    }

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Repetition_Period,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Repetition_Period,
                          &write_Replace_IEs->repetition_Period)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace->write_Replace_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Data_Coding_Scheme,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Data_Coding_Scheme,
                          &write_Replace_IEs->data_Coding_Scheme)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace->write_Replace_ies.list, ie);

    return 0;
}

int sabp_encode_write_replace_complete_ies(
    SABP_Write_Replace_Complete_t *write_Replace_Complete,
    SABP_Write_Replace_Complete_IEs_t *write_Replace_Complete_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Message_Identifier,
                          &write_Replace_Complete_IEs->message_Identifier)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace_Complete->write_Replace_Complete_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_New_Serial_Number,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_New_Serial_Number,
                          &write_Replace_Complete_IEs->new_Serial_Number)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace_Complete->write_Replace_Complete_ies.list, ie);

    return 0;
}

int sabp_encode_write_replace_failure_ies(
    SABP_Write_Replace_Failure_t *write_Replace_Failure,
    SABP_Write_Replace_Failure_IEs_t *write_Replace_Failure_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Message_Identifier,
                          &write_Replace_Failure_IEs->message_Identifier)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace_Failure->write_Replace_Failure_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_New_Serial_Number,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_New_Serial_Number,
                          &write_Replace_Failure_IEs->new_Serial_Number)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace_Failure->write_Replace_Failure_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Failure_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Failure_List,
                          &write_Replace_Failure_IEs->failure_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&write_Replace_Failure->write_Replace_Failure_ies.list, ie);

    return 0;
}

int sabp_encode_kill_ies(
    SABP_Kill_t *kill,
    SABP_Kill_IEs_t *kill_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Message_Identifier,
                          &kill_IEs->message_Identifier)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&kill->kill_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Old_Serial_Number,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Old_Serial_Number,
                          &kill_IEs->old_Serial_Number)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&kill->kill_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Service_Areas_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Service_Areas_List,
                          &kill_IEs->service_Areas_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&kill->kill_ies.list, ie);

    return 0;
}

int sabp_encode_kill_complete_ies(
    SABP_Kill_Complete_t *kill_Complete,
    SABP_Kill_Complete_IEs_t *kill_Complete_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Message_Identifier,
                          &kill_Complete_IEs->message_Identifier)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&kill_Complete->kill_Complete_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Old_Serial_Number,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Old_Serial_Number,
                          &kill_Complete_IEs->old_Serial_Number)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&kill_Complete->kill_Complete_ies.list, ie);

    return 0;
}

int sabp_encode_kill_failure_ies(
    SABP_Kill_Failure_t *kill_Failure,
    SABP_Kill_Failure_IEs_t *kill_Failure_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Message_Identifier,
                          &kill_Failure_IEs->message_Identifier)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&kill_Failure->kill_Failure_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Old_Serial_Number,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Old_Serial_Number,
                          &kill_Failure_IEs->old_Serial_Number)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&kill_Failure->kill_Failure_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Failure_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Failure_List,
                          &kill_Failure_IEs->failure_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&kill_Failure->kill_Failure_ies.list, ie);

    return 0;
}

int sabp_encode_load_query_ies(
    SABP_Load_Query_t *load_Query,
    SABP_Load_Query_IEs_t *load_Query_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Service_Areas_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Service_Areas_List,
                          &load_Query_IEs->service_Areas_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&load_Query->load_Query_ies.list, ie);

    return 0;
}

int sabp_encode_load_query_failure_ies(
    SABP_Load_Query_Failure_t *load_Query_Failure,
    SABP_Load_Query_Failure_IEs_t *load_Query_Failure_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Failure_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Failure_List,
                          &load_Query_Failure_IEs->failure_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&load_Query_Failure->load_Query_Failure_ies.list, ie);

    return 0;
}

int sabp_encode_message_status_query_ies(
    SABP_Message_Status_Query_t *message_Status_Query,
    SABP_Message_Status_Query_IEs_t *message_Status_Query_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Message_Identifier,
                          &message_Status_Query_IEs->message_Identifier)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&message_Status_Query->message_Status_Query_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Old_Serial_Number,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Old_Serial_Number,
                          &message_Status_Query_IEs->old_Serial_Number)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&message_Status_Query->message_Status_Query_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Service_Areas_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Service_Areas_List,
                          &message_Status_Query_IEs->service_Areas_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&message_Status_Query->message_Status_Query_ies.list, ie);

    return 0;
}

int sabp_encode_message_status_query_complete_ies(
    SABP_Message_Status_Query_Complete_t *message_Status_Query_Complete,
    SABP_Message_Status_Query_Complete_IEs_t *message_Status_Query_Complete_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Message_Identifier,
                          &message_Status_Query_Complete_IEs->message_Identifier)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&message_Status_Query_Complete->message_Status_Query_Complete_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Old_Serial_Number,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Old_Serial_Number,
                          &message_Status_Query_Complete_IEs->old_Serial_Number)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&message_Status_Query_Complete->message_Status_Query_Complete_ies.list, ie);

    return 0;
}

int sabp_encode_message_status_query_failure_ies(
    SABP_Message_Status_Query_Failure_t *message_Status_Query_Failure,
    SABP_Message_Status_Query_Failure_IEs_t *message_Status_Query_Failure_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Message_Identifier,
                          &message_Status_Query_Failure_IEs->message_Identifier)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&message_Status_Query_Failure->message_Status_Query_Failure_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Failure_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Failure_List,
                          &message_Status_Query_Failure_IEs->failure_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&message_Status_Query_Failure->message_Status_Query_Failure_ies.list, ie);

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Old_Serial_Number,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Old_Serial_Number,
                          &message_Status_Query_Failure_IEs->old_Serial_Number)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&message_Status_Query_Failure->message_Status_Query_Failure_ies.list, ie);

    return 0;
}

int sabp_encode_reset_ies(
    SABP_Reset_t *reset,
    SABP_Reset_IEs_t *reset_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Service_Areas_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Service_Areas_List,
                          &reset_IEs->service_Areas_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&reset->reset_ies.list, ie);

    return 0;
}

int sabp_encode_reset_complete_ies(
    SABP_Reset_Complete_t *reset_Complete,
    SABP_Reset_Complete_IEs_t *reset_Complete_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Service_Areas_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Service_Areas_List,
                          &reset_Complete_IEs->service_Areas_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&reset_Complete->reset_Complete_ies.list, ie);

    return 0;
}

int sabp_encode_reset_failure_ies(
    SABP_Reset_Failure_t *reset_Failure,
    SABP_Reset_Failure_IEs_t *reset_Failure_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Failure_List,
                          SABP_Criticality_reject,
                          &asn_DEF_SABP_Failure_List,
                          &reset_Failure_IEs->failure_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&reset_Failure->reset_Failure_ies.list, ie);

    /* Optional field */
    if ((reset_Failure_IEs->presenceMask & RESET_FAILURE_IES_SABP_SERVICE_AREAS_LIST_PRESENT)
        == RESET_FAILURE_IES_SABP_SERVICE_AREAS_LIST_PRESENT) {
        if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Service_Areas_List,
                              SABP_Criticality_reject,
                              &asn_DEF_SABP_Service_Areas_List,
                              &reset_Failure_IEs->service_Areas_List)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&reset_Failure->reset_Failure_ies.list, ie);
    }

    return 0;
}

int sabp_encode_restart_ies(
    SABP_Restart_t *restart,
    SABP_Restart_IEs_t *restart_IEs) {

    SABP_IE_t *ie;

    if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Service_Areas_List,
                          SABP_Criticality_ignore,
                          &asn_DEF_SABP_Service_Areas_List,
                          &restart_IEs->service_Areas_List)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&restart->restart_ies.list, ie);

    /* Optional field */
    if ((restart_IEs->presenceMask & RESTART_IES_SABP_RECOVERY_INDICATION_PRESENT)
        == RESTART_IES_SABP_RECOVERY_INDICATION_PRESENT) {
        if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Recovery_Indication,
                              SABP_Criticality_ignore,
                              &asn_DEF_SABP_Recovery_Indication,
                              &restart_IEs->recovery_Indication)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&restart->restart_ies.list, ie);
    }

    return 0;
}

int sabp_encode_error_indication_ies(
    SABP_Error_Indication_t *error_Indication,
    SABP_Error_Indication_IEs_t *error_Indication_IEs) {

    SABP_IE_t *ie;

    /* Optional field */
    if ((error_Indication_IEs->presenceMask & ERROR_INDICATION_IES_SABP_MESSAGE_IDENTIFIER_PRESENT)
        == ERROR_INDICATION_IES_SABP_MESSAGE_IDENTIFIER_PRESENT) {
        if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Message_Identifier,
                              SABP_Criticality_ignore,
                              &asn_DEF_SABP_Message_Identifier,
                              &error_Indication_IEs->message_Identifier)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&error_Indication->error_Indication_ies.list, ie);
    }

    /* Optional field */
    if ((error_Indication_IEs->presenceMask & ERROR_INDICATION_IES_SABP_SERIAL_NUMBER_PRESENT)
        == ERROR_INDICATION_IES_SABP_SERIAL_NUMBER_PRESENT) {
        if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Serial_Number,
                              SABP_Criticality_ignore,
                              &asn_DEF_SABP_Serial_Number,
                              &error_Indication_IEs->serial_Number)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&error_Indication->error_Indication_ies.list, ie);
    }

    /* Optional field */
    if ((error_Indication_IEs->presenceMask & ERROR_INDICATION_IES_SABP_CAUSE_PRESENT)
        == ERROR_INDICATION_IES_SABP_CAUSE_PRESENT) {
        if ((ie = sabp_new_ie(SABP_ProtocolIE_ID_id_Cause,
                              SABP_Criticality_ignore,
                              &asn_DEF_SABP_Cause,
                              &error_Indication_IEs->cause)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&error_Indication->error_Indication_ies.list, ie);
    }

    return 0;
}

