/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/sabp/SABP-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/sabp/sabp_common.h>

#ifndef SABP_IES_DEFS_H_
#define SABP_IES_DEFS_H_

#define WRITE_REPLACE_IES_SABP_OLD_SERIAL_NUMBER_PRESENT (1 << 0)
#define WRITE_REPLACE_IES_SABP_CATEGORY_PRESENT      (1 << 1)

typedef struct SABP_Write_Replace_IEs_s {
    uint16_t             presenceMask;
    SABP_Message_Identifier_t message_Identifier;
    SABP_New_Serial_Number_t new_Serial_Number;
    SABP_Old_Serial_Number_t old_Serial_Number; ///< Optional field
    SABP_Service_Areas_List_t service_Areas_List;
    SABP_Category_t      category; ///< Optional field
    SABP_Repetition_Period_t repetition_Period;
    SABP_Data_Coding_Scheme_t data_Coding_Scheme;
} SABP_Write_Replace_IEs_t;

typedef struct SABP_Write_Replace_Complete_IEs_s {
    SABP_Message_Identifier_t message_Identifier;
    SABP_New_Serial_Number_t new_Serial_Number;
} SABP_Write_Replace_Complete_IEs_t;

typedef struct SABP_Write_Replace_Failure_IEs_s {
    SABP_Message_Identifier_t message_Identifier;
    SABP_New_Serial_Number_t new_Serial_Number;
    SABP_Failure_List_t  failure_List;
} SABP_Write_Replace_Failure_IEs_t;

typedef struct SABP_Kill_IEs_s {
    SABP_Message_Identifier_t message_Identifier;
    SABP_Old_Serial_Number_t old_Serial_Number;
    SABP_Service_Areas_List_t service_Areas_List;
} SABP_Kill_IEs_t;

typedef struct SABP_Kill_Complete_IEs_s {
    SABP_Message_Identifier_t message_Identifier;
    SABP_Old_Serial_Number_t old_Serial_Number;
} SABP_Kill_Complete_IEs_t;

typedef struct SABP_Kill_Failure_IEs_s {
    SABP_Message_Identifier_t message_Identifier;
    SABP_Old_Serial_Number_t old_Serial_Number;
    SABP_Failure_List_t  failure_List;
} SABP_Kill_Failure_IEs_t;

typedef struct SABP_Load_Query_IEs_s {
    SABP_Service_Areas_List_t service_Areas_List;
} SABP_Load_Query_IEs_t;

typedef struct SABP_Load_Query_Failure_IEs_s {
    SABP_Failure_List_t failure_List;
} SABP_Load_Query_Failure_IEs_t;

typedef struct SABP_Message_Status_Query_IEs_s {
    SABP_Message_Identifier_t message_Identifier;
    SABP_Old_Serial_Number_t old_Serial_Number;
    SABP_Service_Areas_List_t service_Areas_List;
} SABP_Message_Status_Query_IEs_t;

typedef struct SABP_Message_Status_Query_Complete_IEs_s {
    SABP_Message_Identifier_t message_Identifier;
    SABP_Old_Serial_Number_t old_Serial_Number;
} SABP_Message_Status_Query_Complete_IEs_t;

typedef struct SABP_Message_Status_Query_Failure_IEs_s {
    SABP_Message_Identifier_t message_Identifier;
    SABP_Failure_List_t  failure_List;
    SABP_Old_Serial_Number_t old_Serial_Number;
} SABP_Message_Status_Query_Failure_IEs_t;

typedef struct SABP_Reset_IEs_s {
    SABP_Service_Areas_List_t service_Areas_List;
} SABP_Reset_IEs_t;

typedef struct SABP_Reset_Complete_IEs_s {
    SABP_Service_Areas_List_t service_Areas_List;
} SABP_Reset_Complete_IEs_t;

#define RESET_FAILURE_IES_SABP_SERVICE_AREAS_LIST_PRESENT (1 << 0)

typedef struct SABP_Reset_Failure_IEs_s {
    uint16_t             presenceMask;
    SABP_Failure_List_t  failure_List;
    SABP_Service_Areas_List_t service_Areas_List; ///< Optional field
} SABP_Reset_Failure_IEs_t;

#define RESTART_IES_SABP_RECOVERY_INDICATION_PRESENT (1 << 0)

typedef struct SABP_Restart_IEs_s {
    uint16_t              presenceMask;
    SABP_Service_Areas_List_t service_Areas_List;
    SABP_Recovery_Indication_t recovery_Indication; ///< Optional field
} SABP_Restart_IEs_t;

#define ERROR_INDICATION_IES_SABP_MESSAGE_IDENTIFIER_PRESENT (1 << 0)
#define ERROR_INDICATION_IES_SABP_SERIAL_NUMBER_PRESENT (1 << 1)
#define ERROR_INDICATION_IES_SABP_CAUSE_PRESENT         (1 << 2)

typedef struct SABP_Error_Indication_IEs_s {
    uint16_t             presenceMask;
    SABP_Message_Identifier_t message_Identifier; ///< Optional field
    SABP_Serial_Number_t serial_Number; ///< Optional field
    SABP_Cause_t         cause; ///< Optional field
} SABP_Error_Indication_IEs_t;

typedef struct sabp_message_s {
    uint8_t procedureCode;
    uint8_t criticality;
    uint8_t direction;
    union {
        SABP_Error_Indication_IEs_t error_Indication_IEs;
        SABP_Kill_Complete_IEs_t kill_Complete_IEs;
        SABP_Kill_Failure_IEs_t kill_Failure_IEs;
        SABP_Kill_IEs_t kill_IEs;
        SABP_Load_Query_Failure_IEs_t load_Query_Failure_IEs;
        SABP_Load_Query_IEs_t load_Query_IEs;
        SABP_Message_Status_Query_Complete_IEs_t message_Status_Query_Complete_IEs;
        SABP_Message_Status_Query_Failure_IEs_t message_Status_Query_Failure_IEs;
        SABP_Message_Status_Query_IEs_t message_Status_Query_IEs;
        SABP_Reset_Complete_IEs_t reset_Complete_IEs;
        SABP_Reset_Failure_IEs_t reset_Failure_IEs;
        SABP_Reset_IEs_t reset_IEs;
        SABP_Restart_IEs_t restart_IEs;
        SABP_Write_Replace_Complete_IEs_t write_Replace_Complete_IEs;
        SABP_Write_Replace_Failure_IEs_t write_Replace_Failure_IEs;
        SABP_Write_Replace_IEs_t write_Replace_IEs;
    } msg;
} sabp_message;

/** \brief Decode function for Write-Replace-IEs ies.
 * \param write_Replace_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_write_replace_ies(
    SABP_Write_Replace_IEs_t *write_Replace_IEs,
    ANY_t *any_p);

/** \brief Encode function for Write-Replace-IEs ies.
 *  \param sabP_Write_Replace Pointer to the ASN1 structure.
 *  \param write_Replace_IEs Pointer to the IES structure.
 **/
int sabp_encode_write_replace_ies(
    SABP_Write_Replace_t *sabP_Write_Replace,
    SABP_Write_Replace_IEs_t *write_Replace_IEs);

/** \brief Decode function for Write-Replace-Complete-IEs ies.
 * \param write_Replace_Complete_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_write_replace_complete_ies(
    SABP_Write_Replace_Complete_IEs_t *write_Replace_Complete_IEs,
    ANY_t *any_p);

/** \brief Encode function for Write-Replace-Complete-IEs ies.
 *  \param sabP_Write_Replace_Complete Pointer to the ASN1 structure.
 *  \param write_Replace_Complete_IEs Pointer to the IES structure.
 **/
int sabp_encode_write_replace_complete_ies(
    SABP_Write_Replace_Complete_t *sabP_Write_Replace_Complete,
    SABP_Write_Replace_Complete_IEs_t *write_Replace_Complete_IEs);

/** \brief Decode function for Write-Replace-Failure-IEs ies.
 * \param write_Replace_Failure_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_write_replace_failure_ies(
    SABP_Write_Replace_Failure_IEs_t *write_Replace_Failure_IEs,
    ANY_t *any_p);

/** \brief Encode function for Write-Replace-Failure-IEs ies.
 *  \param sabP_Write_Replace_Failure Pointer to the ASN1 structure.
 *  \param write_Replace_Failure_IEs Pointer to the IES structure.
 **/
int sabp_encode_write_replace_failure_ies(
    SABP_Write_Replace_Failure_t *sabP_Write_Replace_Failure,
    SABP_Write_Replace_Failure_IEs_t *write_Replace_Failure_IEs);

/** \brief Decode function for Kill-IEs ies.
 * \param kill_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_kill_ies(
    SABP_Kill_IEs_t *kill_IEs,
    ANY_t *any_p);

/** \brief Encode function for Kill-IEs ies.
 *  \param sabP_Kill Pointer to the ASN1 structure.
 *  \param kill_IEs Pointer to the IES structure.
 **/
int sabp_encode_kill_ies(
    SABP_Kill_t *sabP_Kill,
    SABP_Kill_IEs_t *kill_IEs);

/** \brief Decode function for Kill-Complete-IEs ies.
 * \param kill_Complete_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_kill_complete_ies(
    SABP_Kill_Complete_IEs_t *kill_Complete_IEs,
    ANY_t *any_p);

/** \brief Encode function for Kill-Complete-IEs ies.
 *  \param sabP_Kill_Complete Pointer to the ASN1 structure.
 *  \param kill_Complete_IEs Pointer to the IES structure.
 **/
int sabp_encode_kill_complete_ies(
    SABP_Kill_Complete_t *sabP_Kill_Complete,
    SABP_Kill_Complete_IEs_t *kill_Complete_IEs);

/** \brief Decode function for Kill-Failure-IEs ies.
 * \param kill_Failure_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_kill_failure_ies(
    SABP_Kill_Failure_IEs_t *kill_Failure_IEs,
    ANY_t *any_p);

/** \brief Encode function for Kill-Failure-IEs ies.
 *  \param sabP_Kill_Failure Pointer to the ASN1 structure.
 *  \param kill_Failure_IEs Pointer to the IES structure.
 **/
int sabp_encode_kill_failure_ies(
    SABP_Kill_Failure_t *sabP_Kill_Failure,
    SABP_Kill_Failure_IEs_t *kill_Failure_IEs);

/** \brief Decode function for Load-Query-IEs ies.
 * \param load_Query_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_load_query_ies(
    SABP_Load_Query_IEs_t *load_Query_IEs,
    ANY_t *any_p);

/** \brief Encode function for Load-Query-IEs ies.
 *  \param sabP_Load_Query Pointer to the ASN1 structure.
 *  \param load_Query_IEs Pointer to the IES structure.
 **/
int sabp_encode_load_query_ies(
    SABP_Load_Query_t *sabP_Load_Query,
    SABP_Load_Query_IEs_t *load_Query_IEs);

/** \brief Decode function for Load-Query-Failure-IEs ies.
 * \param load_Query_Failure_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_load_query_failure_ies(
    SABP_Load_Query_Failure_IEs_t *load_Query_Failure_IEs,
    ANY_t *any_p);

/** \brief Encode function for Load-Query-Failure-IEs ies.
 *  \param sabP_Load_Query_Failure Pointer to the ASN1 structure.
 *  \param load_Query_Failure_IEs Pointer to the IES structure.
 **/
int sabp_encode_load_query_failure_ies(
    SABP_Load_Query_Failure_t *sabP_Load_Query_Failure,
    SABP_Load_Query_Failure_IEs_t *load_Query_Failure_IEs);

/** \brief Decode function for Message-Status-Query-IEs ies.
 * \param message_Status_Query_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_message_status_query_ies(
    SABP_Message_Status_Query_IEs_t *message_Status_Query_IEs,
    ANY_t *any_p);

/** \brief Encode function for Message-Status-Query-IEs ies.
 *  \param sabP_Message_Status_Query Pointer to the ASN1 structure.
 *  \param message_Status_Query_IEs Pointer to the IES structure.
 **/
int sabp_encode_message_status_query_ies(
    SABP_Message_Status_Query_t *sabP_Message_Status_Query,
    SABP_Message_Status_Query_IEs_t *message_Status_Query_IEs);

/** \brief Decode function for Message-Status-Query-Complete-IEs ies.
 * \param message_Status_Query_Complete_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_message_status_query_complete_ies(
    SABP_Message_Status_Query_Complete_IEs_t *message_Status_Query_Complete_IEs,
    ANY_t *any_p);

/** \brief Encode function for Message-Status-Query-Complete-IEs ies.
 *  \param sabP_Message_Status_Query_Complete Pointer to the ASN1 structure.
 *  \param message_Status_Query_Complete_IEs Pointer to the IES structure.
 **/
int sabp_encode_message_status_query_complete_ies(
    SABP_Message_Status_Query_Complete_t *sabP_Message_Status_Query_Complete,
    SABP_Message_Status_Query_Complete_IEs_t *message_Status_Query_Complete_IEs);

/** \brief Decode function for Message-Status-Query-Failure-IEs ies.
 * \param message_Status_Query_Failure_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_message_status_query_failure_ies(
    SABP_Message_Status_Query_Failure_IEs_t *message_Status_Query_Failure_IEs,
    ANY_t *any_p);

/** \brief Encode function for Message-Status-Query-Failure-IEs ies.
 *  \param sabP_Message_Status_Query_Failure Pointer to the ASN1 structure.
 *  \param message_Status_Query_Failure_IEs Pointer to the IES structure.
 **/
int sabp_encode_message_status_query_failure_ies(
    SABP_Message_Status_Query_Failure_t *sabP_Message_Status_Query_Failure,
    SABP_Message_Status_Query_Failure_IEs_t *message_Status_Query_Failure_IEs);

/** \brief Decode function for Reset-IEs ies.
 * \param reset_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_reset_ies(
    SABP_Reset_IEs_t *reset_IEs,
    ANY_t *any_p);

/** \brief Encode function for Reset-IEs ies.
 *  \param sabP_Reset Pointer to the ASN1 structure.
 *  \param reset_IEs Pointer to the IES structure.
 **/
int sabp_encode_reset_ies(
    SABP_Reset_t *sabP_Reset,
    SABP_Reset_IEs_t *reset_IEs);

/** \brief Decode function for Reset-Complete-IEs ies.
 * \param reset_Complete_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_reset_complete_ies(
    SABP_Reset_Complete_IEs_t *reset_Complete_IEs,
    ANY_t *any_p);

/** \brief Encode function for Reset-Complete-IEs ies.
 *  \param sabP_Reset_Complete Pointer to the ASN1 structure.
 *  \param reset_Complete_IEs Pointer to the IES structure.
 **/
int sabp_encode_reset_complete_ies(
    SABP_Reset_Complete_t *sabP_Reset_Complete,
    SABP_Reset_Complete_IEs_t *reset_Complete_IEs);

/** \brief Decode function for Reset-Failure-IEs ies.
 * \param reset_Failure_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_reset_failure_ies(
    SABP_Reset_Failure_IEs_t *reset_Failure_IEs,
    ANY_t *any_p);

/** \brief Encode function for Reset-Failure-IEs ies.
 *  \param sabP_Reset_Failure Pointer to the ASN1 structure.
 *  \param reset_Failure_IEs Pointer to the IES structure.
 **/
int sabp_encode_reset_failure_ies(
    SABP_Reset_Failure_t *sabP_Reset_Failure,
    SABP_Reset_Failure_IEs_t *reset_Failure_IEs);

/** \brief Decode function for Restart-IEs ies.
 * \param restart_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_restart_ies(
    SABP_Restart_IEs_t *restart_IEs,
    ANY_t *any_p);

/** \brief Encode function for Restart-IEs ies.
 *  \param sabP_Restart Pointer to the ASN1 structure.
 *  \param restart_IEs Pointer to the IES structure.
 **/
int sabp_encode_restart_ies(
    SABP_Restart_t *sabP_Restart,
    SABP_Restart_IEs_t *restart_IEs);

/** \brief Decode function for Error-Indication-IEs ies.
 * \param error_Indication_IEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int sabp_decode_error_indication_ies(
    SABP_Error_Indication_IEs_t *error_Indication_IEs,
    ANY_t *any_p);

/** \brief Encode function for Error-Indication-IEs ies.
 *  \param sabP_Error_Indication Pointer to the ASN1 structure.
 *  \param error_Indication_IEs Pointer to the IES structure.
 **/
int sabp_encode_error_indication_ies(
    SABP_Error_Indication_t *sabP_Error_Indication,
    SABP_Error_Indication_IEs_t *error_Indication_IEs);

int sabp_free_write_replace_ies(
    SABP_Write_Replace_IEs_t *write_Replace_IEs);

int sabp_free_write_replace_complete_ies(
    SABP_Write_Replace_Complete_IEs_t *write_Replace_Complete_IEs);

int sabp_free_write_replace_failure_ies(
    SABP_Write_Replace_Failure_IEs_t *write_Replace_Failure_IEs);

int sabp_free_kill_ies(
    SABP_Kill_IEs_t *kill_IEs);

int sabp_free_kill_complete_ies(
    SABP_Kill_Complete_IEs_t *kill_Complete_IEs);

int sabp_free_kill_failure_ies(
    SABP_Kill_Failure_IEs_t *kill_Failure_IEs);

int sabp_free_load_query_ies(
    SABP_Load_Query_IEs_t *load_Query_IEs);

int sabp_free_load_query_failure_ies(
    SABP_Load_Query_Failure_IEs_t *load_Query_Failure_IEs);

int sabp_free_message_status_query_ies(
    SABP_Message_Status_Query_IEs_t *message_Status_Query_IEs);

int sabp_free_message_status_query_complete_ies(
    SABP_Message_Status_Query_Complete_IEs_t *message_Status_Query_Complete_IEs);

int sabp_free_message_status_query_failure_ies(
    SABP_Message_Status_Query_Failure_IEs_t *message_Status_Query_Failure_IEs);

int sabp_free_reset_ies(
    SABP_Reset_IEs_t *reset_IEs);

int sabp_free_reset_complete_ies(
    SABP_Reset_Complete_IEs_t *reset_Complete_IEs);

int sabp_free_reset_failure_ies(
    SABP_Reset_Failure_IEs_t *reset_Failure_IEs);

int sabp_free_restart_ies(
    SABP_Restart_IEs_t *restart_IEs);

int sabp_free_error_indication_ies(
    SABP_Error_Indication_IEs_t *error_Indication_IEs);

#endif /* SABP_IES_DEFS_H_ */

