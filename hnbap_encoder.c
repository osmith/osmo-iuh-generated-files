/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/hnbap/HNBAP-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/hnbap/hnbap_common.h>
#include <osmocom/hnbap/hnbap_ies_defs.h>

int hnbap_encode_hnbregisterrequesties(
    HNBAP_HNBRegisterRequest_t *hnbRegisterRequest,
    HNBAP_HNBRegisterRequestIEs_t *hnbRegisterRequestIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_HNB_Identity,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_HNB_Identity,
                          &hnbRegisterRequestIEs->hnB_Identity)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbRegisterRequest->hnbRegisterRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_HNB_Location_Information,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_HNB_Location_Information,
                          &hnbRegisterRequestIEs->hnB_Location_Information)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbRegisterRequest->hnbRegisterRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_PLMNidentity,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_PLMNidentity,
                          &hnbRegisterRequestIEs->plmNidentity)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbRegisterRequest->hnbRegisterRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_CellIdentity,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_CellIdentity,
                          &hnbRegisterRequestIEs->cellIdentity)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbRegisterRequest->hnbRegisterRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_LAC,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_LAC,
                          &hnbRegisterRequestIEs->lac)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbRegisterRequest->hnbRegisterRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_RAC,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_RAC,
                          &hnbRegisterRequestIEs->rac)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbRegisterRequest->hnbRegisterRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_SAC,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_SAC,
                          &hnbRegisterRequestIEs->sac)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbRegisterRequest->hnbRegisterRequest_ies.list, ie);

    /* Optional field */
    if ((hnbRegisterRequestIEs->presenceMask & HNBREGISTERREQUESTIES_HNBAP_CSG_ID_PRESENT)
        == HNBREGISTERREQUESTIES_HNBAP_CSG_ID_PRESENT) {
        if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_CSG_ID,
                              HNBAP_Criticality_reject,
                              &asn_DEF_HNBAP_CSG_ID,
                              &hnbRegisterRequestIEs->csg_id)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&hnbRegisterRequest->hnbRegisterRequest_ies.list, ie);
    }

    return 0;
}

int hnbap_encode_hnbregisteraccepties(
    HNBAP_HNBRegisterAccept_t *hnbRegisterAccept,
    HNBAP_HNBRegisterAcceptIEs_t *hnbRegisterAcceptIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_RNC_ID,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_RNC_ID,
                          &hnbRegisterAcceptIEs->rnc_id)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbRegisterAccept->hnbRegisterAccept_ies.list, ie);

    return 0;
}

int hnbap_encode_hnbregisterrejecties(
    HNBAP_HNBRegisterReject_t *hnbRegisterReject,
    HNBAP_HNBRegisterRejectIEs_t *hnbRegisterRejectIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Cause,
                          HNBAP_Criticality_ignore,
                          &asn_DEF_HNBAP_Cause,
                          &hnbRegisterRejectIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbRegisterReject->hnbRegisterReject_ies.list, ie);

    /* Optional field */
    if ((hnbRegisterRejectIEs->presenceMask & HNBREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == HNBREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              HNBAP_Criticality_ignore,
                              &asn_DEF_HNBAP_CriticalityDiagnostics,
                              &hnbRegisterRejectIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&hnbRegisterReject->hnbRegisterReject_ies.list, ie);
    }

    /* Conditional field */
    if ((hnbRegisterRejectIEs->presenceMask & HNBREGISTERREJECTIES_HNBAP_BACKOFFTIMER_PRESENT)
        == HNBREGISTERREJECTIES_HNBAP_BACKOFFTIMER_PRESENT) {
        if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_BackoffTimer,
                              HNBAP_Criticality_reject,
                              &asn_DEF_HNBAP_BackoffTimer,
                              &hnbRegisterRejectIEs->backoffTimer)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&hnbRegisterReject->hnbRegisterReject_ies.list, ie);
    }

    return 0;
}

int hnbap_encode_hnbde_registeries(
    HNBAP_HNBDe_Register_t *hnbDe_Register,
    HNBAP_HNBDe_RegisterIEs_t *hnbDe_RegisterIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Cause,
                          HNBAP_Criticality_ignore,
                          &asn_DEF_HNBAP_Cause,
                          &hnbDe_RegisterIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbDe_Register->hnbDe_Register_ies.list, ie);

    /* Conditional field */
    if ((hnbDe_RegisterIEs->presenceMask & HNBDE_REGISTERIES_HNBAP_BACKOFFTIMER_PRESENT)
        == HNBDE_REGISTERIES_HNBAP_BACKOFFTIMER_PRESENT) {
        if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_BackoffTimer,
                              HNBAP_Criticality_reject,
                              &asn_DEF_HNBAP_BackoffTimer,
                              &hnbDe_RegisterIEs->backoffTimer)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&hnbDe_Register->hnbDe_Register_ies.list, ie);
    }

    return 0;
}

int hnbap_encode_ueregisterrequesties(
    HNBAP_UERegisterRequest_t *ueRegisterRequest,
    HNBAP_UERegisterRequestIEs_t *ueRegisterRequestIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_UE_Identity,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_UE_Identity,
                          &ueRegisterRequestIEs->uE_Identity)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueRegisterRequest->ueRegisterRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Registration_Cause,
                          HNBAP_Criticality_ignore,
                          &asn_DEF_HNBAP_Registration_Cause,
                          &ueRegisterRequestIEs->registration_Cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueRegisterRequest->ueRegisterRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_UE_Capabilities,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_UE_Capabilities,
                          &ueRegisterRequestIEs->uE_Capabilities)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueRegisterRequest->ueRegisterRequest_ies.list, ie);

    return 0;
}

int hnbap_encode_ueregisteraccepties(
    HNBAP_UERegisterAccept_t *ueRegisterAccept,
    HNBAP_UERegisterAcceptIEs_t *ueRegisterAcceptIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_UE_Identity,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_UE_Identity,
                          &ueRegisterAcceptIEs->uE_Identity)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueRegisterAccept->ueRegisterAccept_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Context_ID,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_Context_ID,
                          &ueRegisterAcceptIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueRegisterAccept->ueRegisterAccept_ies.list, ie);

    return 0;
}

int hnbap_encode_ueregisterrejecties(
    HNBAP_UERegisterReject_t *ueRegisterReject,
    HNBAP_UERegisterRejectIEs_t *ueRegisterRejectIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_UE_Identity,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_UE_Identity,
                          &ueRegisterRejectIEs->uE_Identity)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueRegisterReject->ueRegisterReject_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Cause,
                          HNBAP_Criticality_ignore,
                          &asn_DEF_HNBAP_Cause,
                          &ueRegisterRejectIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueRegisterReject->ueRegisterReject_ies.list, ie);

    /* Optional field */
    if ((ueRegisterRejectIEs->presenceMask & UEREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == UEREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              HNBAP_Criticality_ignore,
                              &asn_DEF_HNBAP_CriticalityDiagnostics,
                              &ueRegisterRejectIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ueRegisterReject->ueRegisterReject_ies.list, ie);
    }

    return 0;
}

int hnbap_encode_uede_registeries(
    HNBAP_UEDe_Register_t *ueDe_Register,
    HNBAP_UEDe_RegisterIEs_t *ueDe_RegisterIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Context_ID,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_Context_ID,
                          &ueDe_RegisterIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueDe_Register->ueDe_Register_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Cause,
                          HNBAP_Criticality_ignore,
                          &asn_DEF_HNBAP_Cause,
                          &ueDe_RegisterIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueDe_Register->ueDe_Register_ies.list, ie);

    return 0;
}

int hnbap_encode_csgmembershipupdateies(
    HNBAP_CSGMembershipUpdate_t *csgMembershipUpdate,
    HNBAP_CSGMembershipUpdateIEs_t *csgMembershipUpdateIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Context_ID,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_Context_ID,
                          &csgMembershipUpdateIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&csgMembershipUpdate->csgMembershipUpdate_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_CSGMembershipStatus,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_CSGMembershipStatus,
                          &csgMembershipUpdateIEs->csgMembershipStatus)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&csgMembershipUpdate->csgMembershipUpdate_ies.list, ie);

    return 0;
}

int hnbap_encode_tnlupdaterequesties(
    HNBAP_TNLUpdateRequest_t *tnlUpdateRequest,
    HNBAP_TNLUpdateRequestIEs_t *tnlUpdateRequestIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Context_ID,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_Context_ID,
                          &tnlUpdateRequestIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&tnlUpdateRequest->tnlUpdateRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_RABList,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_RABList,
                          &tnlUpdateRequestIEs->rabList)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&tnlUpdateRequest->tnlUpdateRequest_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Update_cause,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_Update_cause,
                          &tnlUpdateRequestIEs->update_cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&tnlUpdateRequest->tnlUpdateRequest_ies.list, ie);

    return 0;
}

int hnbap_encode_tnlupdateresponseies(
    HNBAP_TNLUpdateResponse_t *tnlUpdateResponse,
    HNBAP_TNLUpdateResponseIEs_t *tnlUpdateResponseIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Context_ID,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_Context_ID,
                          &tnlUpdateResponseIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&tnlUpdateResponse->tnlUpdateResponse_ies.list, ie);

    return 0;
}

int hnbap_encode_tnlupdatefailureies(
    HNBAP_TNLUpdateFailure_t *tnlUpdateFailure,
    HNBAP_TNLUpdateFailureIEs_t *tnlUpdateFailureIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Context_ID,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_Context_ID,
                          &tnlUpdateFailureIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&tnlUpdateFailure->tnlUpdateFailure_ies.list, ie);

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Cause,
                          HNBAP_Criticality_ignore,
                          &asn_DEF_HNBAP_Cause,
                          &tnlUpdateFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&tnlUpdateFailure->tnlUpdateFailure_ies.list, ie);

    /* Optional field */
    if ((tnlUpdateFailureIEs->presenceMask & TNLUPDATEFAILUREIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == TNLUPDATEFAILUREIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              HNBAP_Criticality_ignore,
                              &asn_DEF_HNBAP_CriticalityDiagnostics,
                              &tnlUpdateFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&tnlUpdateFailure->tnlUpdateFailure_ies.list, ie);
    }

    return 0;
}

int hnbap_encode_hnbconfigtransferrequesties(
    HNBAP_HNBConfigTransferRequest_t *hnbConfigTransferRequest,
    HNBAP_HNBConfigTransferRequestIEs_t *hnbConfigTransferRequestIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_NeighbourInfoRequestList,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_NeighbourInfoRequestList,
                          &hnbConfigTransferRequestIEs->neighbourInfoRequestList)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbConfigTransferRequest->hnbConfigTransferRequest_ies.list, ie);

    return 0;
}

int hnbap_encode_hnbconfigtransferresponseies(
    HNBAP_HNBConfigTransferResponse_t *hnbConfigTransferResponse,
    HNBAP_HNBConfigTransferResponseIEs_t *hnbConfigTransferResponseIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_NeighbourInfoList,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_NeighbourInfoList,
                          &hnbConfigTransferResponseIEs->neighbourInfoList)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&hnbConfigTransferResponse->hnbConfigTransferResponse_ies.list, ie);

    return 0;
}

int hnbap_encode_relocationcompleteies(
    HNBAP_RelocationComplete_t *relocationComplete,
    HNBAP_RelocationCompleteIEs_t *relocationCompleteIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Context_ID,
                          HNBAP_Criticality_ignore,
                          &asn_DEF_HNBAP_Context_ID,
                          &relocationCompleteIEs->context_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationComplete->relocationComplete_ies.list, ie);

    return 0;
}

int hnbap_encode_errorindicationies(
    HNBAP_ErrorIndication_t *errorIndication,
    HNBAP_ErrorIndicationIEs_t *errorIndicationIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_Cause,
                          HNBAP_Criticality_ignore,
                          &asn_DEF_HNBAP_Cause,
                          &errorIndicationIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&errorIndication->errorIndication_ies.list, ie);

    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == ERRORINDICATIONIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              HNBAP_Criticality_ignore,
                              &asn_DEF_HNBAP_CriticalityDiagnostics,
                              &errorIndicationIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&errorIndication->errorIndication_ies.list, ie);
    }

    return 0;
}

int hnbap_encode_u_rntiqueryrequesties(
    HNBAP_U_RNTIQueryRequest_t *u_RNTIQueryRequest,
    HNBAP_U_RNTIQueryRequestIEs_t *u_RNTIQueryRequestIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_U_RNTI,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_U_RNTI,
                          &u_RNTIQueryRequestIEs->u_rnti)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&u_RNTIQueryRequest->u_RNTIQueryRequest_ies.list, ie);

    return 0;
}

int hnbap_encode_u_rntiqueryresponseies(
    HNBAP_U_RNTIQueryResponse_t *u_RNTIQueryResponse,
    HNBAP_U_RNTIQueryResponseIEs_t *u_RNTIQueryResponseIEs) {

    HNBAP_IE_t *ie;

    if ((ie = hnbap_new_ie(HNBAP_ProtocolIE_ID_id_HNB_GWResponse,
                          HNBAP_Criticality_reject,
                          &asn_DEF_HNBAP_HNB_GWResponse,
                          &u_RNTIQueryResponseIEs->hnB_GWResponse)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&u_RNTIQueryResponse->u_RNTIQueryResponse_ies.list, ie);

    return 0;
}

