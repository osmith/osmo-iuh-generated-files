/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/ranap/RANAP-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/ranap/ranap_common.h>
#include <osmocom/ranap/ranap_ies_defs.h>

int ranap_encode_iu_releasecommandies(
    RANAP_Iu_ReleaseCommand_t *iu_ReleaseCommand,
    RANAP_Iu_ReleaseCommandIEs_t *iu_ReleaseCommandIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &iu_ReleaseCommandIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&iu_ReleaseCommand->iu_ReleaseCommand_ies.list, ie);

    return 0;
}

int ranap_encode_iu_releasecompleteies(
    RANAP_Iu_ReleaseComplete_t *iu_ReleaseComplete,
    RANAP_Iu_ReleaseCompleteIEs_t *iu_ReleaseCompleteIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((iu_ReleaseCompleteIEs->presenceMask & IU_RELEASECOMPLETEIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT)
        == IU_RELEASECOMPLETEIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_DataVolumeReportList,
                              &iu_ReleaseCompleteIEs->raB_DataVolumeReportList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&iu_ReleaseComplete->iu_ReleaseComplete_ies.list, ie);
    }

    /* Optional field */
    if ((iu_ReleaseCompleteIEs->presenceMask & IU_RELEASECOMPLETEIES_RANAP_RAB_RELEASEDLIST_IURELCOMP_PRESENT)
        == IU_RELEASECOMPLETEIES_RANAP_RAB_RELEASEDLIST_IURELCOMP_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ReleasedList_IuRelComp,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_ReleasedList_IuRelComp,
                              &iu_ReleaseCompleteIEs->raB_ReleasedList_IuRelComp)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&iu_ReleaseComplete->iu_ReleaseComplete_ies.list, ie);
    }

    /* Optional field */
    if ((iu_ReleaseCompleteIEs->presenceMask & IU_RELEASECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == IU_RELEASECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &iu_ReleaseCompleteIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&iu_ReleaseComplete->iu_ReleaseComplete_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_datavolumereportitemies(
    RANAP_RAB_DataVolumeReportList_t *raB_DataVolumeReportList,
    RANAP_RAB_DataVolumeReportItemIEs_t *raB_DataVolumeReportItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_DataVolumeReportItem,
                          &raB_DataVolumeReportItemIEs->raB_DataVolumeReportItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_DataVolumeReportList->raB_DataVolumeReportList_ies.list, ie);

    return 0;
}

int ranap_encode_rab_releaseditem_iurelcomp_ies(
    RANAP_RAB_ReleasedList_IuRelComp_t *raB_ReleasedList_IuRelComp,
    RANAP_RAB_ReleasedItem_IuRelComp_IEs_t *raB_ReleasedItem_IuRelComp_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ReleasedItem_IuRelComp,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ReleasedItem_IuRelComp,
                          &raB_ReleasedItem_IuRelComp_IEs->raB_ReleasedItem_IuRelComp)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_ReleasedList_IuRelComp->raB_ReleasedList_IuRelComp_ies.list, ie);

    return 0;
}

int ranap_encode_relocationrequiredies(
    RANAP_RelocationRequired_t *relocationRequired,
    RANAP_RelocationRequiredIEs_t *relocationRequiredIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RelocationType,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RelocationType,
                          &relocationRequiredIEs->relocationType)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationRequired->relocationRequired_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &relocationRequiredIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationRequired->relocationRequired_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SourceID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_SourceID,
                          &relocationRequiredIEs->sourceID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationRequired->relocationRequired_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TargetID,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_TargetID,
                          &relocationRequiredIEs->targetID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationRequired->relocationRequired_ies.list, ie);

    /* Optional field */
    if ((relocationRequiredIEs->presenceMask & RELOCATIONREQUIREDIES_RANAP_OLDBSS_TONEWBSS_INFORMATION_PRESENT)
        == RELOCATIONREQUIREDIES_RANAP_OLDBSS_TONEWBSS_INFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_OldBSS_ToNewBSS_Information,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_OldBSS_ToNewBSS_Information,
                              &relocationRequiredIEs->oldBSS_ToNewBSS_Information)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequired->relocationRequired_ies.list, ie);
    }

    return 0;
}

int ranap_encode_relocationcommandies(
    RANAP_RelocationCommand_t *relocationCommand,
    RANAP_RelocationCommandIEs_t *relocationCommandIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((relocationCommandIEs->presenceMask & RELOCATIONCOMMANDIES_RANAP_L3_INFORMATION_PRESENT)
        == RELOCATIONCOMMANDIES_RANAP_L3_INFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_L3_Information,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_L3_Information,
                              &relocationCommandIEs->l3_Information)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationCommand->relocationCommand_ies.list, ie);
    }

    /* Optional field */
    if ((relocationCommandIEs->presenceMask & RELOCATIONCOMMANDIES_RANAP_RAB_RELOCATIONRELEASELIST_PRESENT)
        == RELOCATIONCOMMANDIES_RANAP_RAB_RELOCATIONRELEASELIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_RelocationReleaseList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_RelocationReleaseList,
                              &relocationCommandIEs->raB_RelocationReleaseList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationCommand->relocationCommand_ies.list, ie);
    }

    /* Optional field */
    if ((relocationCommandIEs->presenceMask & RELOCATIONCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT)
        == RELOCATIONCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataForwardingList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_DataForwardingList,
                              &relocationCommandIEs->raB_DataForwardingList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationCommand->relocationCommand_ies.list, ie);
    }

    /* Optional field */
    if ((relocationCommandIEs->presenceMask & RELOCATIONCOMMANDIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONCOMMANDIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &relocationCommandIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationCommand->relocationCommand_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_relocationreleaseitemies(
    RANAP_RAB_RelocationReleaseList_t *raB_RelocationReleaseList,
    RANAP_RAB_RelocationReleaseItemIEs_t *raB_RelocationReleaseItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_RelocationReleaseItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_RelocationReleaseItem,
                          &raB_RelocationReleaseItemIEs->raB_RelocationReleaseItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_RelocationReleaseList->raB_RelocationReleaseList_ies.list, ie);

    return 0;
}

int ranap_encode_rab_dataforwardingitemies(
    RANAP_RAB_DataForwardingList_t *raB_DataForwardingList,
    RANAP_RAB_DataForwardingItemIEs_t *raB_DataForwardingItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataForwardingItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_DataForwardingItem,
                          &raB_DataForwardingItemIEs->raB_DataForwardingItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_DataForwardingList->raB_DataForwardingList_ies.list, ie);

    return 0;
}

int ranap_encode_relocationpreparationfailureies(
    RANAP_RelocationPreparationFailure_t *relocationPreparationFailure,
    RANAP_RelocationPreparationFailureIEs_t *relocationPreparationFailureIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &relocationPreparationFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationPreparationFailure->relocationPreparationFailure_ies.list, ie);

    /* Optional field */
    if ((relocationPreparationFailureIEs->presenceMask & RELOCATIONPREPARATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONPREPARATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &relocationPreparationFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationPreparationFailure->relocationPreparationFailure_ies.list, ie);
    }

    return 0;
}

int ranap_encode_relocationrequesties(
    RANAP_RelocationRequest_t *relocationRequest,
    RANAP_RelocationRequestIEs_t *relocationRequestIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((relocationRequestIEs->presenceMask & RELOCATIONREQUESTIES_RANAP_PERMANENTNAS_UE_ID_PRESENT)
        == RELOCATIONREQUESTIES_RANAP_PERMANENTNAS_UE_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_PermanentNAS_UE_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_PermanentNAS_UE_ID,
                              &relocationRequestIEs->permanentNAS_UE_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequest->relocationRequest_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &relocationRequestIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationRequest->relocationRequest_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &relocationRequestIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationRequest->relocationRequest_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Source_ToTarget_TransparentContainer,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_SourceRNC_ToTargetRNC_TransparentContainer,
                          &relocationRequestIEs->source_ToTarget_TransparentContainer)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationRequest->relocationRequest_ies.list, ie);

    /* Optional field */
    if ((relocationRequestIEs->presenceMask & RELOCATIONREQUESTIES_RANAP_RAB_SETUPLIST_RELOCREQ_PRESENT)
        == RELOCATIONREQUESTIES_RANAP_RAB_SETUPLIST_RELOCREQ_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupList_RelocReq,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_RAB_SetupList_RelocReq,
                              &relocationRequestIEs->raB_SetupList_RelocReq)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequest->relocationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((relocationRequestIEs->presenceMask & RELOCATIONREQUESTIES_RANAP_INTEGRITYPROTECTIONINFORMATION_PRESENT)
        == RELOCATIONREQUESTIES_RANAP_INTEGRITYPROTECTIONINFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IntegrityProtectionInformation,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_IntegrityProtectionInformation,
                              &relocationRequestIEs->integrityProtectionInformation)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequest->relocationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((relocationRequestIEs->presenceMask & RELOCATIONREQUESTIES_RANAP_ENCRYPTIONINFORMATION_PRESENT)
        == RELOCATIONREQUESTIES_RANAP_ENCRYPTIONINFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_EncryptionInformation,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_EncryptionInformation,
                              &relocationRequestIEs->encryptionInformation)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequest->relocationRequest_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IuSigConId,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_IuSignallingConnectionIdentifier,
                          &relocationRequestIEs->iuSigConId)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationRequest->relocationRequest_ies.list, ie);

    return 0;
}

int ranap_encode_rab_setupitem_relocreq_ies(
    RANAP_RAB_SetupList_RelocReq_t *raB_SetupList_RelocReq,
    RANAP_RAB_SetupItem_RelocReq_IEs_t *raB_SetupItem_RelocReq_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupItem_RelocReq,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_SetupItem_RelocReq,
                          &raB_SetupItem_RelocReq_IEs->raB_SetupItem_RelocReq)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_SetupList_RelocReq->raB_SetupList_RelocReq_ies.list, ie);

    return 0;
}

int ranap_encode_relocationrequestacknowledgeies(
    RANAP_RelocationRequestAcknowledge_t *relocationRequestAcknowledge,
    RANAP_RelocationRequestAcknowledgeIEs_t *relocationRequestAcknowledgeIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Target_ToSource_TransparentContainer,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TargetRNC_ToSourceRNC_TransparentContainer,
                              &relocationRequestAcknowledgeIEs->target_ToSource_TransparentContainer)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequestAcknowledge->relocationRequestAcknowledge_ies.list, ie);
    }

    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_SETUPLIST_RELOCREQACK_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_SETUPLIST_RELOCREQACK_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupList_RelocReqAck,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_SetupList_RelocReqAck,
                              &relocationRequestAcknowledgeIEs->raB_SetupList_RelocReqAck)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequestAcknowledge->relocationRequestAcknowledge_ies.list, ie);
    }

    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_FAILEDLIST_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_RAB_FAILEDLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_FailedList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_FailedList,
                              &relocationRequestAcknowledgeIEs->raB_FailedList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequestAcknowledge->relocationRequestAcknowledge_ies.list, ie);
    }

    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENINTEGRITYPROTECTIONALGORITHM_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENINTEGRITYPROTECTIONALGORITHM_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_ChosenIntegrityProtectionAlgorithm,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_ChosenIntegrityProtectionAlgorithm,
                              &relocationRequestAcknowledgeIEs->chosenIntegrityProtectionAlgorithm)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequestAcknowledge->relocationRequestAcknowledge_ies.list, ie);
    }

    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_ChosenEncryptionAlgorithm,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_ChosenEncryptionAlgorithm,
                              &relocationRequestAcknowledgeIEs->chosenEncryptionAlgorithm)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequestAcknowledge->relocationRequestAcknowledge_ies.list, ie);
    }

    /* Optional field */
    if ((relocationRequestAcknowledgeIEs->presenceMask & RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONREQUESTACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &relocationRequestAcknowledgeIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationRequestAcknowledge->relocationRequestAcknowledge_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_setupitem_relocreqack_ies(
    RANAP_RAB_SetupList_RelocReqAck_t *raB_SetupList_RelocReqAck,
    RANAP_RAB_SetupItem_RelocReqAck_IEs_t *raB_SetupItem_RelocReqAck_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupItem_RelocReqAck,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_SetupItem_RelocReqAck,
                          &raB_SetupItem_RelocReqAck_IEs->raB_SetupItem_RelocReqAck)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_SetupList_RelocReqAck->raB_SetupList_RelocReqAck_ies.list, ie);

    return 0;
}

int ranap_encode_rab_faileditemies(
    RANAP_RAB_FailedList_t *raB_FailedList,
    RANAP_RAB_FailedItemIEs_t *raB_FailedItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_FailedItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_FailedItem,
                          &raB_FailedItemIEs->raB_FailedItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_FailedList->raB_FailedList_ies.list, ie);

    return 0;
}

int ranap_encode_relocationfailureies(
    RANAP_RelocationFailure_t *relocationFailure,
    RANAP_RelocationFailureIEs_t *relocationFailureIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &relocationFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationFailure->relocationFailure_ies.list, ie);

    /* Optional field */
    if ((relocationFailureIEs->presenceMask & RELOCATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &relocationFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationFailure->relocationFailure_ies.list, ie);
    }

    return 0;
}

int ranap_encode_relocationcancelies(
    RANAP_RelocationCancel_t *relocationCancel,
    RANAP_RelocationCancelIEs_t *relocationCancelIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &relocationCancelIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&relocationCancel->relocationCancel_ies.list, ie);

    return 0;
}

int ranap_encode_relocationcancelacknowledgeies(
    RANAP_RelocationCancelAcknowledge_t *relocationCancelAcknowledge,
    RANAP_RelocationCancelAcknowledgeIEs_t *relocationCancelAcknowledgeIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((relocationCancelAcknowledgeIEs->presenceMask & RELOCATIONCANCELACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RELOCATIONCANCELACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &relocationCancelAcknowledgeIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&relocationCancelAcknowledge->relocationCancelAcknowledge_ies.list, ie);
    }

    return 0;
}

int ranap_encode_srns_contextrequesties(
    RANAP_SRNS_ContextRequest_t *srnS_ContextRequest,
    RANAP_SRNS_ContextRequestIEs_t *srnS_ContextRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataForwardingList_SRNS_CtxReq,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_DataForwardingList_SRNS_CtxReq,
                          &srnS_ContextRequestIEs->raB_DataForwardingList_SRNS_CtxReq)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&srnS_ContextRequest->srnS_ContextRequest_ies.list, ie);

    return 0;
}

int ranap_encode_rab_dataforwardingitem_srns_ctxreq_ies(
    RANAP_RAB_DataForwardingList_SRNS_CtxReq_t *raB_DataForwardingList_SRNS_CtxReq,
    RANAP_RAB_DataForwardingItem_SRNS_CtxReq_IEs_t *raB_DataForwardingItem_SRNS_CtxReq_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataForwardingItem_SRNS_CtxReq,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_DataForwardingItem_SRNS_CtxReq,
                          &raB_DataForwardingItem_SRNS_CtxReq_IEs->raB_DataForwardingItem_SRNS_CtxReq)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_DataForwardingList_SRNS_CtxReq->raB_DataForwardingList_SRNS_CtxReq_ies.list, ie);

    return 0;
}

int ranap_encode_srns_contextresponseies(
    RANAP_SRNS_ContextResponse_t *srnS_ContextResponse,
    RANAP_SRNS_ContextResponseIEs_t *srnS_ContextResponseIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((srnS_ContextResponseIEs->presenceMask & SRNS_CONTEXTRESPONSEIES_RANAP_RAB_CONTEXTLIST_PRESENT)
        == SRNS_CONTEXTRESPONSEIES_RANAP_RAB_CONTEXTLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ContextList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_ContextList,
                              &srnS_ContextResponseIEs->raB_ContextList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&srnS_ContextResponse->srnS_ContextResponse_ies.list, ie);
    }

    /* Optional field */
    if ((srnS_ContextResponseIEs->presenceMask & SRNS_CONTEXTRESPONSEIES_RANAP_RABS_CONTEXTFAILEDTOTRANSFERLIST_PRESENT)
        == SRNS_CONTEXTRESPONSEIES_RANAP_RABS_CONTEXTFAILEDTOTRANSFERLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ContextFailedtoTransferList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RABs_ContextFailedtoTransferList,
                              &srnS_ContextResponseIEs->raB_ContextFailedtoTransferList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&srnS_ContextResponse->srnS_ContextResponse_ies.list, ie);
    }

    /* Optional field */
    if ((srnS_ContextResponseIEs->presenceMask & SRNS_CONTEXTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == SRNS_CONTEXTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &srnS_ContextResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&srnS_ContextResponse->srnS_ContextResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_contextitemies(
    RANAP_RAB_ContextList_t *raB_ContextList,
    RANAP_RAB_ContextItemIEs_t *raB_ContextItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ContextItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ContextItem,
                          &raB_ContextItemIEs->raB_ContextItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_ContextList->raB_ContextList_ies.list, ie);

    return 0;
}

int ranap_encode_rabs_contextfailedtotransferitemies(
    RANAP_RABs_ContextFailedtoTransferList_t *raBs_ContextFailedtoTransferList,
    RANAP_RABs_ContextFailedtoTransferItemIEs_t *raBs_ContextFailedtoTransferItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ContextFailedtoTransferItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RABs_ContextFailedtoTransferItem,
                          &raBs_ContextFailedtoTransferItemIEs->raB_ContextFailedtoTransferItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raBs_ContextFailedtoTransferList->raBs_ContextFailedtoTransferList_ies.list, ie);

    return 0;
}

int ranap_encode_securitymodecommandies(
    RANAP_SecurityModeCommand_t *securityModeCommand,
    RANAP_SecurityModeCommandIEs_t *securityModeCommandIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IntegrityProtectionInformation,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_IntegrityProtectionInformation,
                          &securityModeCommandIEs->integrityProtectionInformation)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&securityModeCommand->securityModeCommand_ies.list, ie);

    /* Optional field */
    if ((securityModeCommandIEs->presenceMask & SECURITYMODECOMMANDIES_RANAP_ENCRYPTIONINFORMATION_PRESENT)
        == SECURITYMODECOMMANDIES_RANAP_ENCRYPTIONINFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_EncryptionInformation,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_EncryptionInformation,
                              &securityModeCommandIEs->encryptionInformation)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&securityModeCommand->securityModeCommand_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_KeyStatus,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_KeyStatus,
                          &securityModeCommandIEs->keyStatus)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&securityModeCommand->securityModeCommand_ies.list, ie);

    return 0;
}

int ranap_encode_securitymodecompleteies(
    RANAP_SecurityModeComplete_t *securityModeComplete,
    RANAP_SecurityModeCompleteIEs_t *securityModeCompleteIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_ChosenIntegrityProtectionAlgorithm,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_ChosenIntegrityProtectionAlgorithm,
                          &securityModeCompleteIEs->chosenIntegrityProtectionAlgorithm)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&securityModeComplete->securityModeComplete_ies.list, ie);

    /* Optional field */
    if ((securityModeCompleteIEs->presenceMask & SECURITYMODECOMPLETEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT)
        == SECURITYMODECOMPLETEIES_RANAP_CHOSENENCRYPTIONALGORITHM_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_ChosenEncryptionAlgorithm,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_ChosenEncryptionAlgorithm,
                              &securityModeCompleteIEs->chosenEncryptionAlgorithm)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&securityModeComplete->securityModeComplete_ies.list, ie);
    }

    /* Optional field */
    if ((securityModeCompleteIEs->presenceMask & SECURITYMODECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == SECURITYMODECOMPLETEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &securityModeCompleteIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&securityModeComplete->securityModeComplete_ies.list, ie);
    }

    return 0;
}

int ranap_encode_securitymoderejecties(
    RANAP_SecurityModeReject_t *securityModeReject,
    RANAP_SecurityModeRejectIEs_t *securityModeRejectIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &securityModeRejectIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&securityModeReject->securityModeReject_ies.list, ie);

    /* Optional field */
    if ((securityModeRejectIEs->presenceMask & SECURITYMODEREJECTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == SECURITYMODEREJECTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &securityModeRejectIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&securityModeReject->securityModeReject_ies.list, ie);
    }

    return 0;
}

int ranap_encode_datavolumereportrequesties(
    RANAP_DataVolumeReportRequest_t *dataVolumeReportRequest,
    RANAP_DataVolumeReportRequestIEs_t *dataVolumeReportRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportRequestList,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_DataVolumeReportRequestList,
                          &dataVolumeReportRequestIEs->raB_DataVolumeReportRequestList)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&dataVolumeReportRequest->dataVolumeReportRequest_ies.list, ie);

    return 0;
}

int ranap_encode_rab_datavolumereportrequestitemies(
    RANAP_RAB_DataVolumeReportRequestList_t *raB_DataVolumeReportRequestList,
    RANAP_RAB_DataVolumeReportRequestItemIEs_t *raB_DataVolumeReportRequestItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportRequestItem,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_DataVolumeReportRequestItem,
                          &raB_DataVolumeReportRequestItemIEs->raB_DataVolumeReportRequestItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_DataVolumeReportRequestList->raB_DataVolumeReportRequestList_ies.list, ie);

    return 0;
}

int ranap_encode_datavolumereporties(
    RANAP_DataVolumeReport_t *dataVolumeReport,
    RANAP_DataVolumeReportIEs_t *dataVolumeReportIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((dataVolumeReportIEs->presenceMask & DATAVOLUMEREPORTIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT)
        == DATAVOLUMEREPORTIES_RANAP_RAB_DATAVOLUMEREPORTLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataVolumeReportList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_DataVolumeReportList,
                              &dataVolumeReportIEs->raB_DataVolumeReportList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&dataVolumeReport->dataVolumeReport_ies.list, ie);
    }

    /* Optional field */
    if ((dataVolumeReportIEs->presenceMask & DATAVOLUMEREPORTIES_RANAP_RABS_FAILED_TO_REPORTLIST_PRESENT)
        == DATAVOLUMEREPORTIES_RANAP_RABS_FAILED_TO_REPORTLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_FailedtoReportList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RABs_failed_to_reportList,
                              &dataVolumeReportIEs->raB_FailedtoReportList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&dataVolumeReport->dataVolumeReport_ies.list, ie);
    }

    /* Optional field */
    if ((dataVolumeReportIEs->presenceMask & DATAVOLUMEREPORTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == DATAVOLUMEREPORTIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &dataVolumeReportIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&dataVolumeReport->dataVolumeReport_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rabs_failed_to_reportitemies(
    RANAP_RABs_failed_to_reportList_t *raBs_failed_to_reportList,
    RANAP_RABs_failed_to_reportItemIEs_t *raBs_failed_to_reportItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_FailedtoReportItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RABs_failed_to_reportItem,
                          &raBs_failed_to_reportItemIEs->raB_FailedtoReportItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raBs_failed_to_reportList->raBs_failed_to_reportList_ies.list, ie);

    return 0;
}

int ranap_encode_reseties(
    RANAP_Reset_t *reset,
    RANAP_ResetIEs_t *resetIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &resetIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&reset->reset_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &resetIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&reset->reset_ies.list, ie);

    /* Optional field */
    if ((resetIEs->presenceMask & RESETIES_RANAP_GLOBALRNC_ID_PRESENT)
        == RESETIES_RANAP_GLOBALRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalRNC_ID,
                              &resetIEs->globalRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&reset->reset_ies.list, ie);
    }

    return 0;
}

int ranap_encode_resetacknowledgeies(
    RANAP_ResetAcknowledge_t *resetAcknowledge,
    RANAP_ResetAcknowledgeIEs_t *resetAcknowledgeIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &resetAcknowledgeIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&resetAcknowledge->resetAcknowledge_ies.list, ie);

    /* Optional field */
    if ((resetAcknowledgeIEs->presenceMask & RESETACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RESETACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &resetAcknowledgeIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&resetAcknowledge->resetAcknowledge_ies.list, ie);
    }

    /* Optional field */
    if ((resetAcknowledgeIEs->presenceMask & RESETACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT)
        == RESETACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalRNC_ID,
                              &resetAcknowledgeIEs->globalRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&resetAcknowledge->resetAcknowledge_ies.list, ie);
    }

    return 0;
}

int ranap_encode_resetresourceies(
    RANAP_ResetResource_t *resetResource,
    RANAP_ResetResourceIEs_t *resetResourceIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &resetResourceIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&resetResource->resetResource_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &resetResourceIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&resetResource->resetResource_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IuSigConIdList,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_ResetResourceList,
                          &resetResourceIEs->iuSigConIdList)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&resetResource->resetResource_ies.list, ie);

    /* Optional field */
    if ((resetResourceIEs->presenceMask & RESETRESOURCEIES_RANAP_GLOBALRNC_ID_PRESENT)
        == RESETRESOURCEIES_RANAP_GLOBALRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalRNC_ID,
                              &resetResourceIEs->globalRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&resetResource->resetResource_ies.list, ie);
    }

    return 0;
}

int ranap_encode_resetresourceitemies(
    RANAP_ResetResourceList_t *resetResourceList,
    RANAP_ResetResourceItemIEs_t *resetResourceItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IuSigConIdItem,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_ResetResourceItem,
                          &resetResourceItemIEs->iuSigConIdItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&resetResourceList->resetResourceList_ies.list, ie);

    return 0;
}

int ranap_encode_resetresourceacknowledgeies(
    RANAP_ResetResourceAcknowledge_t *resetResourceAcknowledge,
    RANAP_ResetResourceAcknowledgeIEs_t *resetResourceAcknowledgeIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &resetResourceAcknowledgeIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&resetResourceAcknowledge->resetResourceAcknowledge_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IuSigConIdList,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_ResetResourceAckList,
                          &resetResourceAcknowledgeIEs->iuSigConIdList)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&resetResourceAcknowledge->resetResourceAcknowledge_ies.list, ie);

    /* Optional field */
    if ((resetResourceAcknowledgeIEs->presenceMask & RESETRESOURCEACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT)
        == RESETRESOURCEACKNOWLEDGEIES_RANAP_GLOBALRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalRNC_ID,
                              &resetResourceAcknowledgeIEs->globalRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&resetResourceAcknowledge->resetResourceAcknowledge_ies.list, ie);
    }

    /* Optional field */
    if ((resetResourceAcknowledgeIEs->presenceMask & RESETRESOURCEACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RESETRESOURCEACKNOWLEDGEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &resetResourceAcknowledgeIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&resetResourceAcknowledge->resetResourceAcknowledge_ies.list, ie);
    }

    return 0;
}

int ranap_encode_resetresourceackitemies(
    RANAP_ResetResourceAckList_t *resetResourceAckList,
    RANAP_ResetResourceAckItemIEs_t *resetResourceAckItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IuSigConIdItem,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_ResetResourceAckItem,
                          &resetResourceAckItemIEs->iuSigConIdItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&resetResourceAckList->resetResourceAckList_ies.list, ie);

    return 0;
}

int ranap_encode_rab_releaserequesties(
    RANAP_RAB_ReleaseRequest_t *raB_ReleaseRequest,
    RANAP_RAB_ReleaseRequestIEs_t *raB_ReleaseRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ReleaseList,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ReleaseList,
                          &raB_ReleaseRequestIEs->raB_ReleaseList)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_ReleaseRequest->raB_ReleaseRequest_ies.list, ie);

    return 0;
}

int ranap_encode_rab_releaseitemies(
    RANAP_RAB_ReleaseList_t *raB_ReleaseList,
    RANAP_RAB_ReleaseItemIEs_t *raB_ReleaseItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ReleaseItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ReleaseItem,
                          &raB_ReleaseItemIEs->raB_ReleaseItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_ReleaseList->raB_ReleaseList_ies.list, ie);

    return 0;
}

int ranap_encode_iu_releaserequesties(
    RANAP_Iu_ReleaseRequest_t *iu_ReleaseRequest,
    RANAP_Iu_ReleaseRequestIEs_t *iu_ReleaseRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &iu_ReleaseRequestIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&iu_ReleaseRequest->iu_ReleaseRequest_ies.list, ie);

    return 0;
}

int ranap_encode_enhancedrelocationcompleterequesties(
    RANAP_EnhancedRelocationCompleteRequest_t *enhancedRelocationCompleteRequest,
    RANAP_EnhancedRelocationCompleteRequestIEs_t *enhancedRelocationCompleteRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_OldIuSigConId,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_IuSignallingConnectionIdentifier,
                          &enhancedRelocationCompleteRequestIEs->oldIuSigConId)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&enhancedRelocationCompleteRequest->enhancedRelocationCompleteRequest_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IuSigConId,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_IuSignallingConnectionIdentifier,
                          &enhancedRelocationCompleteRequestIEs->iuSigConId)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&enhancedRelocationCompleteRequest->enhancedRelocationCompleteRequest_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Relocation_SourceRNC_ID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_GlobalRNC_ID,
                          &enhancedRelocationCompleteRequestIEs->relocation_SourceRNC_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&enhancedRelocationCompleteRequest->enhancedRelocationCompleteRequest_ies.list, ie);

    /* Optional field */
    if ((enhancedRelocationCompleteRequestIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_SOURCEEXTENDEDRNC_ID_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_SOURCEEXTENDEDRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Relocation_SourceExtendedRNC_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_ExtendedRNC_ID,
                              &enhancedRelocationCompleteRequestIEs->relocation_SourceExtendedRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&enhancedRelocationCompleteRequest->enhancedRelocationCompleteRequest_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Relocation_TargetRNC_ID,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_GlobalRNC_ID,
                          &enhancedRelocationCompleteRequestIEs->relocation_TargetRNC_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&enhancedRelocationCompleteRequest->enhancedRelocationCompleteRequest_ies.list, ie);

    /* Optional field */
    if ((enhancedRelocationCompleteRequestIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_TARGETEXTENDEDRNC_ID_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_ID_RELOCATION_TARGETEXTENDEDRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Relocation_TargetExtendedRNC_ID,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_ExtendedRNC_ID,
                              &enhancedRelocationCompleteRequestIEs->relocation_TargetExtendedRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&enhancedRelocationCompleteRequest->enhancedRelocationCompleteRequest_ies.list, ie);
    }

    /* Optional field */
    if ((enhancedRelocationCompleteRequestIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETEREQ_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETEREQUESTIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETEREQ_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupList_EnhancedRelocCompleteReq,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteReq,
                              &enhancedRelocationCompleteRequestIEs->raB_SetupList_EnhancedRelocCompleteReq)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&enhancedRelocationCompleteRequest->enhancedRelocationCompleteRequest_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_setupitem_enhancedreloccompletereq_ies(
    RANAP_RAB_SetupList_EnhancedRelocCompleteReq_t *raB_SetupList_EnhancedRelocCompleteReq,
    RANAP_RAB_SetupItem_EnhancedRelocCompleteReq_IEs_t *raB_SetupItem_EnhancedRelocCompleteReq_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupItem_EnhancedRelocCompleteReq,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_SetupItem_EnhancedRelocCompleteReq,
                          &raB_SetupItem_EnhancedRelocCompleteReq_IEs->raB_SetupItem_EnhancedRelocCompleteReq)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_SetupList_EnhancedRelocCompleteReq->raB_SetupList_EnhancedRelocCompleteReq_ies.list, ie);

    return 0;
}

int ranap_encode_enhancedrelocationcompleteresponseies(
    RANAP_EnhancedRelocationCompleteResponse_t *enhancedRelocationCompleteResponse,
    RANAP_EnhancedRelocationCompleteResponseIEs_t *enhancedRelocationCompleteResponseIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((enhancedRelocationCompleteResponseIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETERES_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_SETUPLIST_ENHANCEDRELOCCOMPLETERES_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupList_EnhancedRelocCompleteRes,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_SetupList_EnhancedRelocCompleteRes,
                              &enhancedRelocationCompleteResponseIEs->raB_SetupList_EnhancedRelocCompleteRes)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&enhancedRelocationCompleteResponse->enhancedRelocationCompleteResponse_ies.list, ie);
    }

    /* Optional field */
    if ((enhancedRelocationCompleteResponseIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_TOBERELEASEDLIST_ENHANCEDRELOCCOMPLETERES_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_RAB_TOBERELEASEDLIST_ENHANCEDRELOCCOMPLETERES_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ToBeReleasedList_EnhancedRelocCompleteRes,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes,
                              &enhancedRelocationCompleteResponseIEs->raB_ToBeReleasedList_EnhancedRelocCompleteRes)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&enhancedRelocationCompleteResponse->enhancedRelocationCompleteResponse_ies.list, ie);
    }

    /* Optional field */
    if ((enhancedRelocationCompleteResponseIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &enhancedRelocationCompleteResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&enhancedRelocationCompleteResponse->enhancedRelocationCompleteResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_setupitem_enhancedreloccompleteres_ies(
    RANAP_RAB_SetupList_EnhancedRelocCompleteRes_t *raB_SetupList_EnhancedRelocCompleteRes,
    RANAP_RAB_SetupItem_EnhancedRelocCompleteRes_IEs_t *raB_SetupItem_EnhancedRelocCompleteRes_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupItem_EnhancedRelocCompleteRes,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_SetupItem_EnhancedRelocCompleteRes,
                          &raB_SetupItem_EnhancedRelocCompleteRes_IEs->raB_SetupItem_EnhancedRelocCompleteRes)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_SetupList_EnhancedRelocCompleteRes->raB_SetupList_EnhancedRelocCompleteRes_ies.list, ie);

    return 0;
}

int ranap_encode_rab_tobereleaseditem_enhancedreloccompleteres_ies(
    RANAP_RAB_ToBeReleasedList_EnhancedRelocCompleteRes_t *raB_ToBeReleasedList_EnhancedRelocCompleteRes,
    RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs_t *raB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ToBeReleasedItem_EnhancedRelocCompleteRes,
                          &raB_ToBeReleasedItem_EnhancedRelocCompleteRes_IEs->raB_ToBeReleasedItem_EnhancedRelocCompleteRes)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_ToBeReleasedList_EnhancedRelocCompleteRes->raB_ToBeReleasedList_EnhancedRelocCompleteRes_ies.list, ie);

    return 0;
}

int ranap_encode_enhancedrelocationcompletefailureies(
    RANAP_EnhancedRelocationCompleteFailure_t *enhancedRelocationCompleteFailure,
    RANAP_EnhancedRelocationCompleteFailureIEs_t *enhancedRelocationCompleteFailureIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &enhancedRelocationCompleteFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&enhancedRelocationCompleteFailure->enhancedRelocationCompleteFailure_ies.list, ie);

    /* Optional field */
    if ((enhancedRelocationCompleteFailureIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &enhancedRelocationCompleteFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&enhancedRelocationCompleteFailure->enhancedRelocationCompleteFailure_ies.list, ie);
    }

    return 0;
}

int ranap_encode_enhancedrelocationcompleteconfirmies(
    RANAP_EnhancedRelocationCompleteConfirm_t *enhancedRelocationCompleteConfirm,
    RANAP_EnhancedRelocationCompleteConfirmIEs_t *enhancedRelocationCompleteConfirmIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((enhancedRelocationCompleteConfirmIEs->presenceMask & ENHANCEDRELOCATIONCOMPLETECONFIRMIES_RANAP_RAB_FAILEDLIST_PRESENT)
        == ENHANCEDRELOCATIONCOMPLETECONFIRMIES_RANAP_RAB_FAILEDLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_FailedList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_FailedList,
                              &enhancedRelocationCompleteConfirmIEs->raB_FailedList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&enhancedRelocationCompleteConfirm->enhancedRelocationCompleteConfirm_ies.list, ie);
    }

    return 0;
}

int ranap_encode_pagingies(
    RANAP_Paging_t *paging,
    RANAP_PagingIEs_t *pagingIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &pagingIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&paging->paging_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_PermanentNAS_UE_ID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_PermanentNAS_UE_ID,
                          &pagingIEs->permanentNAS_UE_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&paging->paging_ies.list, ie);

    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_TEMPORARYUE_ID_PRESENT)
        == PAGINGIES_RANAP_TEMPORARYUE_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TemporaryUE_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TemporaryUE_ID,
                              &pagingIEs->temporaryUE_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&paging->paging_ies.list, ie);
    }

    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_PAGINGAREAID_PRESENT)
        == PAGINGIES_RANAP_PAGINGAREAID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_PagingAreaID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_PagingAreaID,
                              &pagingIEs->pagingAreaID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&paging->paging_ies.list, ie);
    }

    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_PAGINGCAUSE_PRESENT)
        == PAGINGIES_RANAP_PAGINGCAUSE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_PagingCause,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_PagingCause,
                              &pagingIEs->pagingCause)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&paging->paging_ies.list, ie);
    }

    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_NONSEARCHINGINDICATION_PRESENT)
        == PAGINGIES_RANAP_NONSEARCHINGINDICATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_NonSearchingIndication,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_NonSearchingIndication,
                              &pagingIEs->nonSearchingIndication)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&paging->paging_ies.list, ie);
    }

    /* Optional field */
    if ((pagingIEs->presenceMask & PAGINGIES_RANAP_DRX_CYCLELENGTHCOEFFICIENT_PRESENT)
        == PAGINGIES_RANAP_DRX_CYCLELENGTHCOEFFICIENT_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_DRX_CycleLengthCoefficient,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_DRX_CycleLengthCoefficient,
                              &pagingIEs->drX_CycleLengthCoefficient)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&paging->paging_ies.list, ie);
    }

    return 0;
}

int ranap_encode_commonid_ies(
    RANAP_CommonID_t *commonID,
    RANAP_CommonID_IEs_t *commonID_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_PermanentNAS_UE_ID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_PermanentNAS_UE_ID,
                          &commonID_IEs->permanentNAS_UE_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&commonID->commonID_ies.list, ie);

    return 0;
}

int ranap_encode_cn_invoketraceies(
    RANAP_CN_InvokeTrace_t *cN_InvokeTrace,
    RANAP_CN_InvokeTraceIEs_t *cN_InvokeTraceIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((cN_InvokeTraceIEs->presenceMask & CN_INVOKETRACEIES_RANAP_TRACETYPE_PRESENT)
        == CN_INVOKETRACEIES_RANAP_TRACETYPE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TraceType,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TraceType,
                              &cN_InvokeTraceIEs->traceType)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&cN_InvokeTrace->cN_InvokeTrace_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TraceReference,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_TraceReference,
                          &cN_InvokeTraceIEs->traceReference)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&cN_InvokeTrace->cN_InvokeTrace_ies.list, ie);

    /* Optional field */
    if ((cN_InvokeTraceIEs->presenceMask & CN_INVOKETRACEIES_RANAP_TRIGGERID_PRESENT)
        == CN_INVOKETRACEIES_RANAP_TRIGGERID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TriggerID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TriggerID,
                              &cN_InvokeTraceIEs->triggerID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&cN_InvokeTrace->cN_InvokeTrace_ies.list, ie);
    }

    /* Optional field */
    if ((cN_InvokeTraceIEs->presenceMask & CN_INVOKETRACEIES_RANAP_UE_ID_PRESENT)
        == CN_INVOKETRACEIES_RANAP_UE_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_UE_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_UE_ID,
                              &cN_InvokeTraceIEs->ue_id)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&cN_InvokeTrace->cN_InvokeTrace_ies.list, ie);
    }

    /* Optional field */
    if ((cN_InvokeTraceIEs->presenceMask & CN_INVOKETRACEIES_RANAP_OMC_ID_PRESENT)
        == CN_INVOKETRACEIES_RANAP_OMC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_OMC_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_OMC_ID,
                              &cN_InvokeTraceIEs->omc_id)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&cN_InvokeTrace->cN_InvokeTrace_ies.list, ie);
    }

    return 0;
}

int ranap_encode_cn_deactivatetraceies(
    RANAP_CN_DeactivateTrace_t *cN_DeactivateTrace,
    RANAP_CN_DeactivateTraceIEs_t *cN_DeactivateTraceIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TraceReference,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_TraceReference,
                          &cN_DeactivateTraceIEs->traceReference)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&cN_DeactivateTrace->cN_DeactivateTrace_ies.list, ie);

    /* Optional field */
    if ((cN_DeactivateTraceIEs->presenceMask & CN_DEACTIVATETRACEIES_RANAP_TRIGGERID_PRESENT)
        == CN_DEACTIVATETRACEIES_RANAP_TRIGGERID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TriggerID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TriggerID,
                              &cN_DeactivateTraceIEs->triggerID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&cN_DeactivateTrace->cN_DeactivateTrace_ies.list, ie);
    }

    return 0;
}

int ranap_encode_locationreportingcontrolies(
    RANAP_LocationReportingControl_t *locationReportingControl,
    RANAP_LocationReportingControlIEs_t *locationReportingControlIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RequestType,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RequestType,
                          &locationReportingControlIEs->requestType)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&locationReportingControl->locationReportingControl_ies.list, ie);

    return 0;
}

int ranap_encode_locationreporties(
    RANAP_LocationReport_t *locationReport,
    RANAP_LocationReportIEs_t *locationReportIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((locationReportIEs->presenceMask & LOCATIONREPORTIES_RANAP_AREAIDENTITY_PRESENT)
        == LOCATIONREPORTIES_RANAP_AREAIDENTITY_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_AreaIdentity,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_AreaIdentity,
                              &locationReportIEs->areaIdentity)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&locationReport->locationReport_ies.list, ie);
    }

    /* Optional field */
    if ((locationReportIEs->presenceMask & LOCATIONREPORTIES_RANAP_CAUSE_PRESENT)
        == LOCATIONREPORTIES_RANAP_CAUSE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_Cause,
                              &locationReportIEs->cause)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&locationReport->locationReport_ies.list, ie);
    }

    /* Optional field */
    if ((locationReportIEs->presenceMask & LOCATIONREPORTIES_RANAP_REQUESTTYPE_PRESENT)
        == LOCATIONREPORTIES_RANAP_REQUESTTYPE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RequestType,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RequestType,
                              &locationReportIEs->requestType)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&locationReport->locationReport_ies.list, ie);
    }

    return 0;
}

int ranap_encode_initialue_messageies(
    RANAP_InitialUE_Message_t *initialUE_Message,
    RANAP_InitialUE_MessageIEs_t *initialUE_MessageIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &initialUE_MessageIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&initialUE_Message->initialUE_Message_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_LAI,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_LAI,
                          &initialUE_MessageIEs->lai)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&initialUE_Message->initialUE_Message_ies.list, ie);

    /* Conditional field */
    if ((initialUE_MessageIEs->presenceMask & INITIALUE_MESSAGEIES_RANAP_RAC_PRESENT)
        == INITIALUE_MESSAGEIES_RANAP_RAC_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAC,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAC,
                              &initialUE_MessageIEs->rac)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&initialUE_Message->initialUE_Message_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SAI,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_SAI,
                          &initialUE_MessageIEs->sai)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&initialUE_Message->initialUE_Message_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_NAS_PDU,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_NAS_PDU,
                          &initialUE_MessageIEs->nas_pdu)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&initialUE_Message->initialUE_Message_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IuSigConId,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_IuSignallingConnectionIdentifier,
                          &initialUE_MessageIEs->iuSigConId)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&initialUE_Message->initialUE_Message_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_GlobalRNC_ID,
                          &initialUE_MessageIEs->globalRNC_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&initialUE_Message->initialUE_Message_ies.list, ie);

    return 0;
}

int ranap_encode_directtransferies(
    RANAP_DirectTransfer_t *directTransfer,
    RANAP_DirectTransferIEs_t *directTransferIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_NAS_PDU,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_NAS_PDU,
                          &directTransferIEs->nas_pdu)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&directTransfer->directTransfer_ies.list, ie);

    /* Optional field */
    if ((directTransferIEs->presenceMask & DIRECTTRANSFERIES_RANAP_LAI_PRESENT)
        == DIRECTTRANSFERIES_RANAP_LAI_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_LAI,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_LAI,
                              &directTransferIEs->lai)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&directTransfer->directTransfer_ies.list, ie);
    }

    /* Optional field */
    if ((directTransferIEs->presenceMask & DIRECTTRANSFERIES_RANAP_RAC_PRESENT)
        == DIRECTTRANSFERIES_RANAP_RAC_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAC,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAC,
                              &directTransferIEs->rac)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&directTransfer->directTransfer_ies.list, ie);
    }

    /* Optional field */
    if ((directTransferIEs->presenceMask & DIRECTTRANSFERIES_RANAP_SAI_PRESENT)
        == DIRECTTRANSFERIES_RANAP_SAI_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SAI,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_SAI,
                              &directTransferIEs->sai)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&directTransfer->directTransfer_ies.list, ie);
    }

    /* Optional field */
    if ((directTransferIEs->presenceMask & DIRECTTRANSFERIES_RANAP_SAPI_PRESENT)
        == DIRECTTRANSFERIES_RANAP_SAPI_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SAPI,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_SAPI,
                              &directTransferIEs->sapi)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&directTransfer->directTransfer_ies.list, ie);
    }

    return 0;
}

int ranap_encode_redirectionindication_ies(
    RANAP_RedirectionIndication_t *redirectionIndication,
    RANAP_RedirectionIndication_IEs_t *redirectionIndication_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_NAS_PDU,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_NAS_PDU,
                          &redirectionIndication_IEs->nas_pdu)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&redirectionIndication->redirectionIndication_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RejectCauseValue,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RejectCauseValue,
                          &redirectionIndication_IEs->rejectCauseValue)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&redirectionIndication->redirectionIndication_ies.list, ie);

    /* Optional field */
    if ((redirectionIndication_IEs->presenceMask & REDIRECTIONINDICATION_IES_RANAP_NAS_SEQUENCENUMBER_PRESENT)
        == REDIRECTIONINDICATION_IES_RANAP_NAS_SEQUENCENUMBER_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_NAS_SequenceNumber,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_NAS_SequenceNumber,
                              &redirectionIndication_IEs->naS_SequenceNumber)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&redirectionIndication->redirectionIndication_ies.list, ie);
    }

    /* Optional field */
    if ((redirectionIndication_IEs->presenceMask & REDIRECTIONINDICATION_IES_RANAP_PERMANENTNAS_UE_ID_PRESENT)
        == REDIRECTIONINDICATION_IES_RANAP_PERMANENTNAS_UE_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_PermanentNAS_UE_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_PermanentNAS_UE_ID,
                              &redirectionIndication_IEs->permanentNAS_UE_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&redirectionIndication->redirectionIndication_ies.list, ie);
    }

    return 0;
}

int ranap_encode_overloadies(
    RANAP_Overload_t *overload,
    RANAP_OverloadIEs_t *overloadIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((overloadIEs->presenceMask & OVERLOADIES_RANAP_NUMBEROFSTEPS_PRESENT)
        == OVERLOADIES_RANAP_NUMBEROFSTEPS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_NumberOfSteps,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_NumberOfSteps,
                              &overloadIEs->numberOfSteps)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&overload->overload_ies.list, ie);
    }

    /* Optional field */
    if ((overloadIEs->presenceMask & OVERLOADIES_RANAP_GLOBALRNC_ID_PRESENT)
        == OVERLOADIES_RANAP_GLOBALRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalRNC_ID,
                              &overloadIEs->globalRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&overload->overload_ies.list, ie);
    }

    return 0;
}

int ranap_encode_errorindicationies(
    RANAP_ErrorIndication_t *errorIndication,
    RANAP_ErrorIndicationIEs_t *errorIndicationIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RANAP_CAUSE_PRESENT)
        == ERRORINDICATIONIES_RANAP_CAUSE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_Cause,
                              &errorIndicationIEs->cause)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&errorIndication->errorIndication_ies.list, ie);
    }

    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == ERRORINDICATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &errorIndicationIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&errorIndication->errorIndication_ies.list, ie);
    }

    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RANAP_CN_DOMAININDICATOR_PRESENT)
        == ERRORINDICATIONIES_RANAP_CN_DOMAININDICATOR_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CN_DomainIndicator,
                              &errorIndicationIEs->cN_DomainIndicator)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&errorIndication->errorIndication_ies.list, ie);
    }

    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RANAP_GLOBALRNC_ID_PRESENT)
        == ERRORINDICATIONIES_RANAP_GLOBALRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalRNC_ID,
                              &errorIndicationIEs->globalRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&errorIndication->errorIndication_ies.list, ie);
    }

    return 0;
}

int ranap_encode_srns_dataforwardcommandies(
    RANAP_SRNS_DataForwardCommand_t *srnS_DataForwardCommand,
    RANAP_SRNS_DataForwardCommandIEs_t *srnS_DataForwardCommandIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((srnS_DataForwardCommandIEs->presenceMask & SRNS_DATAFORWARDCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT)
        == SRNS_DATAFORWARDCOMMANDIES_RANAP_RAB_DATAFORWARDINGLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_DataForwardingList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_DataForwardingList,
                              &srnS_DataForwardCommandIEs->raB_DataForwardingList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&srnS_DataForwardCommand->srnS_DataForwardCommand_ies.list, ie);
    }

    return 0;
}

int ranap_encode_forwardsrns_contexties(
    RANAP_ForwardSRNS_Context_t *forwardSRNS_Context,
    RANAP_ForwardSRNS_ContextIEs_t *forwardSRNS_ContextIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ContextList,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ContextList,
                          &forwardSRNS_ContextIEs->raB_ContextList)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&forwardSRNS_Context->forwardSRNS_Context_ies.list, ie);

    return 0;
}

int ranap_encode_rab_assignmentrequesties(
    RANAP_RAB_AssignmentRequest_t *raB_AssignmentRequest,
    RANAP_RAB_AssignmentRequestIEs_t *raB_AssignmentRequestIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((raB_AssignmentRequestIEs->presenceMask & RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_SETUPORMODIFYLIST_PRESENT)
        == RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_SETUPORMODIFYLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupOrModifyList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_SetupOrModifyList,
                              &raB_AssignmentRequestIEs->raB_SetupOrModifyList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&raB_AssignmentRequest->raB_AssignmentRequest_ies.list, ie);
    }

    /* Optional field */
    if ((raB_AssignmentRequestIEs->presenceMask & RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_RELEASELIST_PRESENT)
        == RAB_ASSIGNMENTREQUESTIES_RANAP_RAB_RELEASELIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ReleaseList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_ReleaseList,
                              &raB_AssignmentRequestIEs->raB_ReleaseList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&raB_AssignmentRequest->raB_AssignmentRequest_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_assignmentresponseies(
    RANAP_RAB_AssignmentResponse_t *raB_AssignmentResponse,
    RANAP_RAB_AssignmentResponseIEs_t *raB_AssignmentResponseIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_SETUPORMODIFIEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_SETUPORMODIFIEDLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupOrModifiedList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_SetupOrModifiedList,
                              &raB_AssignmentResponseIEs->raB_SetupOrModifiedList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&raB_AssignmentResponse->raB_AssignmentResponse_ies.list, ie);
    }

    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEDLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ReleasedList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_ReleasedList,
                              &raB_AssignmentResponseIEs->raB_ReleasedList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&raB_AssignmentResponse->raB_AssignmentResponse_ies.list, ie);
    }

    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_QUEUEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_QUEUEDLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_QueuedList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_QueuedList,
                              &raB_AssignmentResponseIEs->raB_QueuedList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&raB_AssignmentResponse->raB_AssignmentResponse_ies.list, ie);
    }

    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_FAILEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_FAILEDLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_FailedList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_FailedList,
                              &raB_AssignmentResponseIEs->raB_FailedList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&raB_AssignmentResponse->raB_AssignmentResponse_ies.list, ie);
    }

    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEFAILEDLIST_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_RAB_RELEASEFAILEDLIST_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ReleaseFailedList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_ReleaseFailedList,
                              &raB_AssignmentResponseIEs->raB_ReleaseFailedList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&raB_AssignmentResponse->raB_AssignmentResponse_ies.list, ie);
    }

    /* Optional field */
    if ((raB_AssignmentResponseIEs->presenceMask & RAB_ASSIGNMENTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RAB_ASSIGNMENTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &raB_AssignmentResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&raB_AssignmentResponse->raB_AssignmentResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_setupormodifieditemies(
    RANAP_RAB_SetupOrModifiedList_t *raB_SetupOrModifiedList,
    RANAP_RAB_SetupOrModifiedItemIEs_t *raB_SetupOrModifiedItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupOrModifiedItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_SetupOrModifiedItem,
                          &raB_SetupOrModifiedItemIEs->raB_SetupOrModifiedItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_SetupOrModifiedList->raB_SetupOrModifiedList_ies.list, ie);

    return 0;
}

int ranap_encode_rab_releaseditemies(
    RANAP_RAB_ReleasedList_t *raB_ReleasedList,
    RANAP_RAB_ReleasedItemIEs_t *raB_ReleasedItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ReleasedItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ReleasedItem,
                          &raB_ReleasedItemIEs->raB_ReleasedItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_ReleasedList->raB_ReleasedList_ies.list, ie);

    return 0;
}

int ranap_encode_rab_queueditemies(
    RANAP_RAB_QueuedList_t *raB_QueuedList,
    RANAP_RAB_QueuedItemIEs_t *raB_QueuedItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_QueuedItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_QueuedItem,
                          &raB_QueuedItemIEs->raB_QueuedItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_QueuedList->raB_QueuedList_ies.list, ie);

    return 0;
}

int ranap_encode_geran_iumode_rab_failed_rabassgntresponse_itemies(
    RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_List_t *geraN_Iumode_RAB_Failed_RABAssgntResponse_List,
    RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs_t *geraN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GERAN_Iumode_RAB_Failed_RABAssgntResponse_Item,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_GERAN_Iumode_RAB_Failed_RABAssgntResponse_Item,
                          &geraN_Iumode_RAB_Failed_RABAssgntResponse_ItemIEs->geraN_Iumode_RAB_Failed_RABAssgntResponse_Item)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&geraN_Iumode_RAB_Failed_RABAssgntResponse_List->geraN_Iumode_RAB_Failed_RABAssgntResponse_List_ies.list, ie);

    return 0;
}

int ranap_encode_ranap_relocationinformationies(
    RANAP_RANAP_RelocationInformation_t *ranaP_RelocationInformation,
    RANAP_RANAP_RelocationInformationIEs_t *ranaP_RelocationInformationIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((ranaP_RelocationInformationIEs->presenceMask & RANAP_RELOCATIONINFORMATIONIES_RANAP_RAB_CONTEXTLIST_RANAP_RELOCINF_PRESENT)
        == RANAP_RELOCATIONINFORMATIONIES_RANAP_RAB_CONTEXTLIST_RANAP_RELOCINF_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ContextList_RANAP_RelocInf,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_ContextList_RANAP_RelocInf,
                              &ranaP_RelocationInformationIEs->raB_ContextList_RANAP_RelocInf)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_RelocationInformation->ranaP_RelocationInformation_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_contextitemies_ranap_relocinf(
    RANAP_RAB_ContextList_RANAP_RelocInf_t *raB_ContextList_RANAP_RelocInf,
    RANAP_RAB_ContextItemIEs_RANAP_RelocInf_t *raB_ContextItemIEs_RANAP_RelocInf) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ContextItem_RANAP_RelocInf,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ContextItem_RANAP_RelocInf,
                          &raB_ContextItemIEs_RANAP_RelocInf->raB_ContextItem_RANAP_RelocInf)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_ContextList_RANAP_RelocInf->raB_ContextList_RANAP_RelocInf_ies.list, ie);

    return 0;
}

int ranap_encode_ranap_enhancedrelocationinformationrequesties(
    RANAP_RANAP_EnhancedRelocationInformationRequest_t *ranaP_EnhancedRelocationInformationRequest,
    RANAP_RANAP_EnhancedRelocationInformationRequestIEs_t *ranaP_EnhancedRelocationInformationRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Source_ToTarget_TransparentContainer,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_SourceRNC_ToTargetRNC_TransparentContainer,
                          &ranaP_EnhancedRelocationInformationRequestIEs->source_ToTarget_TransparentContainer)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDCS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDCS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_OldIuSigConIdCS,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_IuSignallingConnectionIdentifier,
                              &ranaP_EnhancedRelocationInformationRequestIEs->oldIuSigConIdCS)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDCS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDCS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_IDCS,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &ranaP_EnhancedRelocationInformationRequestIEs->globalCN_IDCS)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDPS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_OLDIUSIGCONIDPS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_OldIuSigConIdPS,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_IuSignallingConnectionIdentifier,
                              &ranaP_EnhancedRelocationInformationRequestIEs->oldIuSigConIdPS)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDPS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_ID_GLOBALCN_IDPS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_IDPS,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &ranaP_EnhancedRelocationInformationRequestIEs->globalCN_IDPS)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_RAB_SETUPLIST_ENHRELOCINFOREQ_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_RAB_SETUPLIST_ENHRELOCINFOREQ_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupList_EnhRelocInfoReq,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoReq,
                              &ranaP_EnhancedRelocationInformationRequestIEs->raB_SetupList_EnhRelocInfoReq)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_SNA_ACCESS_INFORMATION_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_SNA_ACCESS_INFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SNA_Access_Information,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_SNA_Access_Information,
                              &ranaP_EnhancedRelocationInformationRequestIEs->snA_Access_Information)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_UESBI_IU_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_UESBI_IU_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_UESBI_Iu,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_UESBI_Iu,
                              &ranaP_EnhancedRelocationInformationRequestIEs->uesbI_Iu)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_PLMNIDENTITY_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_PLMNIDENTITY_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SelectedPLMN_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_PLMNidentity,
                              &ranaP_EnhancedRelocationInformationRequestIEs->selectedPLMN_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationRequestIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_CNMBMSLINKINGINFORMATION_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONREQUESTIES_RANAP_CNMBMSLINKINGINFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CNMBMSLinkingInformation,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CNMBMSLinkingInformation,
                              &ranaP_EnhancedRelocationInformationRequestIEs->cnmbmsLinkingInformation)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationRequest->ranaP_EnhancedRelocationInformationRequest_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_setupitem_enhrelocinforeq_ies(
    RANAP_RAB_SetupList_EnhRelocInfoReq_t *raB_SetupList_EnhRelocInfoReq,
    RANAP_RAB_SetupItem_EnhRelocInfoReq_IEs_t *raB_SetupItem_EnhRelocInfoReq_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupItem_EnhRelocInfoReq,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_SetupItem_EnhRelocInfoReq,
                          &raB_SetupItem_EnhRelocInfoReq_IEs->raB_SetupItem_EnhRelocInfoReq)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_SetupList_EnhRelocInfoReq->raB_SetupList_EnhRelocInfoReq_ies.list, ie);

    return 0;
}

int ranap_encode_ranap_enhancedrelocationinformationresponseies(
    RANAP_RANAP_EnhancedRelocationInformationResponse_t *ranaP_EnhancedRelocationInformationResponse,
    RANAP_RANAP_EnhancedRelocationInformationResponseIEs_t *ranaP_EnhancedRelocationInformationResponseIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationResponseIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_TARGETRNC_TOSOURCERNC_TRANSPARENTCONTAINER_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Target_ToSource_TransparentContainer,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TargetRNC_ToSourceRNC_TransparentContainer,
                              &ranaP_EnhancedRelocationInformationResponseIEs->target_ToSource_TransparentContainer)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationResponse->ranaP_EnhancedRelocationInformationResponse_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationResponseIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_SETUPLIST_ENHRELOCINFORES_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_SETUPLIST_ENHRELOCINFORES_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupList_EnhRelocInfoRes,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_SetupList_EnhRelocInfoRes,
                              &ranaP_EnhancedRelocationInformationResponseIEs->raB_SetupList_EnhRelocInfoRes)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationResponse->ranaP_EnhancedRelocationInformationResponse_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationResponseIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_FAILEDLIST_ENHRELOCINFORES_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_RAB_FAILEDLIST_ENHRELOCINFORES_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_FailedList_EnhRelocInfoRes,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAB_FailedList_EnhRelocInfoRes,
                              &ranaP_EnhancedRelocationInformationResponseIEs->raB_FailedList_EnhRelocInfoRes)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationResponse->ranaP_EnhancedRelocationInformationResponse_ies.list, ie);
    }

    /* Optional field */
    if ((ranaP_EnhancedRelocationInformationResponseIEs->presenceMask & RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == RANAP_ENHANCEDRELOCATIONINFORMATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &ranaP_EnhancedRelocationInformationResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ranaP_EnhancedRelocationInformationResponse->ranaP_EnhancedRelocationInformationResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_rab_setupitem_enhrelocinfores_ies(
    RANAP_RAB_SetupList_EnhRelocInfoRes_t *raB_SetupList_EnhRelocInfoRes,
    RANAP_RAB_SetupItem_EnhRelocInfoRes_IEs_t *raB_SetupItem_EnhRelocInfoRes_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_SetupItem_EnhRelocInfoRes,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_SetupItem_EnhRelocInfoRes,
                          &raB_SetupItem_EnhRelocInfoRes_IEs->raB_SetupItem_EnhRelocInfoRes)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_SetupList_EnhRelocInfoRes->raB_SetupList_EnhRelocInfoRes_ies.list, ie);

    return 0;
}

int ranap_encode_rab_faileditem_enhrelocinfores_ies(
    RANAP_RAB_FailedList_EnhRelocInfoRes_t *raB_FailedList_EnhRelocInfoRes,
    RANAP_RAB_FailedItem_EnhRelocInfoRes_IEs_t *raB_FailedItem_EnhRelocInfoRes_IEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_FailedItem_EnhRelocInfoRes,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_FailedItem_EnhRelocInfoRes,
                          &raB_FailedItem_EnhRelocInfoRes_IEs->raB_FailedItem_EnhRelocInfoRes)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_FailedList_EnhRelocInfoRes->raB_FailedList_EnhRelocInfoRes_ies.list, ie);

    return 0;
}

int ranap_encode_rab_modifyrequesties(
    RANAP_RAB_ModifyRequest_t *raB_ModifyRequest,
    RANAP_RAB_ModifyRequestIEs_t *raB_ModifyRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ModifyList,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ModifyList,
                          &raB_ModifyRequestIEs->raB_ModifyList)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_ModifyRequest->raB_ModifyRequest_ies.list, ie);

    return 0;
}

int ranap_encode_rab_modifyitemies(
    RANAP_RAB_ModifyList_t *raB_ModifyList,
    RANAP_RAB_ModifyItemIEs_t *raB_ModifyItemIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_ModifyItem,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_RAB_ModifyItem,
                          &raB_ModifyItemIEs->raB_ModifyItem)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&raB_ModifyList->raB_ModifyList_ies.list, ie);

    return 0;
}

int ranap_encode_locationrelateddatarequesties(
    RANAP_LocationRelatedDataRequest_t *locationRelatedDataRequest,
    RANAP_LocationRelatedDataRequestIEs_t *locationRelatedDataRequestIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((locationRelatedDataRequestIEs->presenceMask & LOCATIONRELATEDDATAREQUESTIES_RANAP_LOCATIONRELATEDDATAREQUESTTYPE_PRESENT)
        == LOCATIONRELATEDDATAREQUESTIES_RANAP_LOCATIONRELATEDDATAREQUESTTYPE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_LocationRelatedDataRequestType,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_LocationRelatedDataRequestType,
                              &locationRelatedDataRequestIEs->locationRelatedDataRequestType)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&locationRelatedDataRequest->locationRelatedDataRequest_ies.list, ie);
    }

    return 0;
}

int ranap_encode_locationrelateddataresponseies(
    RANAP_LocationRelatedDataResponse_t *locationRelatedDataResponse,
    RANAP_LocationRelatedDataResponseIEs_t *locationRelatedDataResponseIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((locationRelatedDataResponseIEs->presenceMask & LOCATIONRELATEDDATARESPONSEIES_RANAP_BROADCASTASSISTANCEDATADECIPHERINGKEYS_PRESENT)
        == LOCATIONRELATEDDATARESPONSEIES_RANAP_BROADCASTASSISTANCEDATADECIPHERINGKEYS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_BroadcastAssistanceDataDecipheringKeys,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_BroadcastAssistanceDataDecipheringKeys,
                              &locationRelatedDataResponseIEs->broadcastAssistanceDataDecipheringKeys)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&locationRelatedDataResponse->locationRelatedDataResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_locationrelateddatafailureies(
    RANAP_LocationRelatedDataFailure_t *locationRelatedDataFailure,
    RANAP_LocationRelatedDataFailureIEs_t *locationRelatedDataFailureIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &locationRelatedDataFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&locationRelatedDataFailure->locationRelatedDataFailure_ies.list, ie);

    return 0;
}

int ranap_encode_informationtransferindicationies(
    RANAP_InformationTransferIndication_t *informationTransferIndication,
    RANAP_InformationTransferIndicationIEs_t *informationTransferIndicationIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_InformationTransferID,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_InformationTransferID,
                          &informationTransferIndicationIEs->informationTransferID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferIndication->informationTransferIndication_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_ProvidedData,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_ProvidedData,
                          &informationTransferIndicationIEs->providedData)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferIndication->informationTransferIndication_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &informationTransferIndicationIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferIndication->informationTransferIndication_ies.list, ie);

    /* Optional field */
    if ((informationTransferIndicationIEs->presenceMask & INFORMATIONTRANSFERINDICATIONIES_RANAP_GLOBALCN_ID_PRESENT)
        == INFORMATIONTRANSFERINDICATIONIES_RANAP_GLOBALCN_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &informationTransferIndicationIEs->globalCN_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&informationTransferIndication->informationTransferIndication_ies.list, ie);
    }

    return 0;
}

int ranap_encode_informationtransferconfirmationies(
    RANAP_InformationTransferConfirmation_t *informationTransferConfirmation,
    RANAP_InformationTransferConfirmationIEs_t *informationTransferConfirmationIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_InformationTransferID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_InformationTransferID,
                          &informationTransferConfirmationIEs->informationTransferID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferConfirmation->informationTransferConfirmation_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &informationTransferConfirmationIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferConfirmation->informationTransferConfirmation_ies.list, ie);

    /* Optional field */
    if ((informationTransferConfirmationIEs->presenceMask & INFORMATIONTRANSFERCONFIRMATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == INFORMATIONTRANSFERCONFIRMATIONIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &informationTransferConfirmationIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&informationTransferConfirmation->informationTransferConfirmation_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_GlobalRNC_ID,
                          &informationTransferConfirmationIEs->globalRNC_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferConfirmation->informationTransferConfirmation_ies.list, ie);

    return 0;
}

int ranap_encode_informationtransferfailureies(
    RANAP_InformationTransferFailure_t *informationTransferFailure,
    RANAP_InformationTransferFailureIEs_t *informationTransferFailureIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_InformationTransferID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_InformationTransferID,
                          &informationTransferFailureIEs->informationTransferID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferFailure->informationTransferFailure_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &informationTransferFailureIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferFailure->informationTransferFailure_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &informationTransferFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferFailure->informationTransferFailure_ies.list, ie);

    /* Optional field */
    if ((informationTransferFailureIEs->presenceMask & INFORMATIONTRANSFERFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == INFORMATIONTRANSFERFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &informationTransferFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&informationTransferFailure->informationTransferFailure_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_GlobalRNC_ID,
                          &informationTransferFailureIEs->globalRNC_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&informationTransferFailure->informationTransferFailure_ies.list, ie);

    return 0;
}

int ranap_encode_uespecificinformationindicationies(
    RANAP_UESpecificInformationIndication_t *ueSpecificInformationIndication,
    RANAP_UESpecificInformationIndicationIEs_t *ueSpecificInformationIndicationIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((ueSpecificInformationIndicationIEs->presenceMask & UESPECIFICINFORMATIONINDICATIONIES_RANAP_UESBI_IU_PRESENT)
        == UESPECIFICINFORMATIONINDICATIONIES_RANAP_UESBI_IU_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_UESBI_Iu,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_UESBI_Iu,
                              &ueSpecificInformationIndicationIEs->uesbI_Iu)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&ueSpecificInformationIndication->ueSpecificInformationIndication_ies.list, ie);
    }

    return 0;
}

int ranap_encode_directinformationtransferies(
    RANAP_DirectInformationTransfer_t *directInformationTransfer,
    RANAP_DirectInformationTransferIEs_t *directInformationTransferIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((directInformationTransferIEs->presenceMask & DIRECTINFORMATIONTRANSFERIES_RANAP_INTERSYSTEMINFORMATIONTRANSFERTYPE_PRESENT)
        == DIRECTINFORMATIONTRANSFERIES_RANAP_INTERSYSTEMINFORMATIONTRANSFERTYPE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_InterSystemInformationTransferType,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_InterSystemInformationTransferType,
                              &directInformationTransferIEs->interSystemInformationTransferType)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&directInformationTransfer->directInformationTransfer_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &directInformationTransferIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&directInformationTransfer->directInformationTransfer_ies.list, ie);

    /* Optional field */
    if ((directInformationTransferIEs->presenceMask & DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALRNC_ID_PRESENT)
        == DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalRNC_ID,
                              &directInformationTransferIEs->globalRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&directInformationTransfer->directInformationTransfer_ies.list, ie);
    }

    /* Optional field */
    if ((directInformationTransferIEs->presenceMask & DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALCN_ID_PRESENT)
        == DIRECTINFORMATIONTRANSFERIES_RANAP_GLOBALCN_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &directInformationTransferIEs->globalCN_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&directInformationTransfer->directInformationTransfer_ies.list, ie);
    }

    return 0;
}

int ranap_encode_uplinkinformationexchangerequesties(
    RANAP_UplinkInformationExchangeRequest_t *uplinkInformationExchangeRequest,
    RANAP_UplinkInformationExchangeRequestIEs_t *uplinkInformationExchangeRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_InformationExchangeID,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_InformationExchangeID,
                          &uplinkInformationExchangeRequestIEs->informationExchangeID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&uplinkInformationExchangeRequest->uplinkInformationExchangeRequest_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_InformationExchangeType,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_InformationExchangeType,
                          &uplinkInformationExchangeRequestIEs->informationExchangeType)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&uplinkInformationExchangeRequest->uplinkInformationExchangeRequest_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &uplinkInformationExchangeRequestIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&uplinkInformationExchangeRequest->uplinkInformationExchangeRequest_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_GlobalRNC_ID,
                          &uplinkInformationExchangeRequestIEs->globalRNC_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&uplinkInformationExchangeRequest->uplinkInformationExchangeRequest_ies.list, ie);

    return 0;
}

int ranap_encode_uplinkinformationexchangeresponseies(
    RANAP_UplinkInformationExchangeResponse_t *uplinkInformationExchangeResponse,
    RANAP_UplinkInformationExchangeResponseIEs_t *uplinkInformationExchangeResponseIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_InformationExchangeID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_InformationExchangeID,
                          &uplinkInformationExchangeResponseIEs->informationExchangeID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&uplinkInformationExchangeResponse->uplinkInformationExchangeResponse_ies.list, ie);

    /* Optional field */
    if ((uplinkInformationExchangeResponseIEs->presenceMask & UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_INFORMATIONREQUESTED_PRESENT)
        == UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_INFORMATIONREQUESTED_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_InformationRequested,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_InformationRequested,
                              &uplinkInformationExchangeResponseIEs->informationRequested)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&uplinkInformationExchangeResponse->uplinkInformationExchangeResponse_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &uplinkInformationExchangeResponseIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&uplinkInformationExchangeResponse->uplinkInformationExchangeResponse_ies.list, ie);

    /* Optional field */
    if ((uplinkInformationExchangeResponseIEs->presenceMask & UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_GLOBALCN_ID_PRESENT)
        == UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_GLOBALCN_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &uplinkInformationExchangeResponseIEs->globalCN_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&uplinkInformationExchangeResponse->uplinkInformationExchangeResponse_ies.list, ie);
    }

    /* Optional field */
    if ((uplinkInformationExchangeResponseIEs->presenceMask & UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == UPLINKINFORMATIONEXCHANGERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &uplinkInformationExchangeResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&uplinkInformationExchangeResponse->uplinkInformationExchangeResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_uplinkinformationexchangefailureies(
    RANAP_UplinkInformationExchangeFailure_t *uplinkInformationExchangeFailure,
    RANAP_UplinkInformationExchangeFailureIEs_t *uplinkInformationExchangeFailureIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_InformationExchangeID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_InformationExchangeID,
                          &uplinkInformationExchangeFailureIEs->informationExchangeID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&uplinkInformationExchangeFailure->uplinkInformationExchangeFailure_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CN_DomainIndicator,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_CN_DomainIndicator,
                          &uplinkInformationExchangeFailureIEs->cN_DomainIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&uplinkInformationExchangeFailure->uplinkInformationExchangeFailure_ies.list, ie);

    /* Optional field */
    if ((uplinkInformationExchangeFailureIEs->presenceMask & UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_GLOBALCN_ID_PRESENT)
        == UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_GLOBALCN_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &uplinkInformationExchangeFailureIEs->globalCN_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&uplinkInformationExchangeFailure->uplinkInformationExchangeFailure_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &uplinkInformationExchangeFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&uplinkInformationExchangeFailure->uplinkInformationExchangeFailure_ies.list, ie);

    /* Optional field */
    if ((uplinkInformationExchangeFailureIEs->presenceMask & UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == UPLINKINFORMATIONEXCHANGEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &uplinkInformationExchangeFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&uplinkInformationExchangeFailure->uplinkInformationExchangeFailure_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmssessionstarties(
    RANAP_MBMSSessionStart_t *mbmsSessionStart,
    RANAP_MBMSSessionStartIEs_t *mbmsSessionStartIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TMGI,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_TMGI,
                          &mbmsSessionStartIEs->tmgi)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);

    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONIDENTITY_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONIDENTITY_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_MBMSSessionIdentity,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_MBMSSessionIdentity,
                              &mbmsSessionStartIEs->mbmsSessionIdentity)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_MBMSBearerServiceType,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_MBMSBearerServiceType,
                          &mbmsSessionStartIEs->mbmsBearerServiceType)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IuSigConId,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_IuSignallingConnectionIdentifier,
                          &mbmsSessionStartIEs->iuSigConId)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAB_Parameters,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_RAB_Parameters,
                          &mbmsSessionStartIEs->raB_Parameters)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);

    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_PDP_TYPEINFORMATION_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_PDP_TYPEINFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_PDP_TypeInformation,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_PDP_TypeInformation,
                              &mbmsSessionStartIEs->pdP_TypeInformation)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_MBMSSessionDuration,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_MBMSSessionDuration,
                          &mbmsSessionStartIEs->mbmsSessionDuration)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_MBMSServiceArea,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_MBMSServiceArea,
                          &mbmsSessionStartIEs->mbmsServiceArea)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);

    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_FREQUENCELAYERCONVERGENCEFLAG_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_FREQUENCELAYERCONVERGENCEFLAG_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_FrequenceLayerConvergenceFlag,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_FrequenceLayerConvergenceFlag,
                              &mbmsSessionStartIEs->frequenceLayerConvergenceFlag)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_RALISTOFIDLEMODEUES_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_RALISTOFIDLEMODEUES_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_RAListofIdleModeUEs,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_RAListofIdleModeUEs,
                              &mbmsSessionStartIEs->raListofIdleModeUEs)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_GLOBALCN_ID_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_GLOBALCN_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_ID,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &mbmsSessionStartIEs->globalCN_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsSessionStartIEs->presenceMask & MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONREPETITIONNUMBER_PRESENT)
        == MBMSSESSIONSTARTIES_RANAP_MBMSSESSIONREPETITIONNUMBER_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_MBMSSessionRepetitionNumber,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_MBMSSessionRepetitionNumber,
                              &mbmsSessionStartIEs->mbmsSessionRepetitionNumber)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TimeToMBMSDataTransfer,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_TimeToMBMSDataTransfer,
                          &mbmsSessionStartIEs->timeToMBMSDataTransfer)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionStart->mbmsSessionStart_ies.list, ie);

    return 0;
}

int ranap_encode_mbmssessionstartresponseies(
    RANAP_MBMSSessionStartResponse_t *mbmsSessionStartResponse,
    RANAP_MBMSSessionStartResponseIEs_t *mbmsSessionStartResponseIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((mbmsSessionStartResponseIEs->presenceMask & MBMSSESSIONSTARTRESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT)
        == MBMSSESSIONSTARTRESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TransportLayerInformation,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TransportLayerInformation,
                              &mbmsSessionStartResponseIEs->transportLayerInformation)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStartResponse->mbmsSessionStartResponse_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsSessionStartResponseIEs->presenceMask & MBMSSESSIONSTARTRESPONSEIES_RANAP_CAUSE_PRESENT)
        == MBMSSESSIONSTARTRESPONSEIES_RANAP_CAUSE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_Cause,
                              &mbmsSessionStartResponseIEs->cause)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStartResponse->mbmsSessionStartResponse_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsSessionStartResponseIEs->presenceMask & MBMSSESSIONSTARTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONSTARTRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsSessionStartResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStartResponse->mbmsSessionStartResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmssessionstartfailureies(
    RANAP_MBMSSessionStartFailure_t *mbmsSessionStartFailure,
    RANAP_MBMSSessionStartFailureIEs_t *mbmsSessionStartFailureIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &mbmsSessionStartFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionStartFailure->mbmsSessionStartFailure_ies.list, ie);

    /* Optional field */
    if ((mbmsSessionStartFailureIEs->presenceMask & MBMSSESSIONSTARTFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONSTARTFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsSessionStartFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStartFailure->mbmsSessionStartFailure_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmssessionupdateies(
    RANAP_MBMSSessionUpdate_t *mbmsSessionUpdate,
    RANAP_MBMSSessionUpdateIEs_t *mbmsSessionUpdateIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SessionUpdateID,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_SessionUpdateID,
                          &mbmsSessionUpdateIEs->sessionUpdateID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionUpdate->mbmsSessionUpdate_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_DeltaRAListofIdleModeUEs,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_DeltaRAListofIdleModeUEs,
                          &mbmsSessionUpdateIEs->deltaRAListofIdleModeUEs)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionUpdate->mbmsSessionUpdate_ies.list, ie);

    return 0;
}

int ranap_encode_mbmssessionupdateresponseies(
    RANAP_MBMSSessionUpdateResponse_t *mbmsSessionUpdateResponse,
    RANAP_MBMSSessionUpdateResponseIEs_t *mbmsSessionUpdateResponseIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SessionUpdateID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_SessionUpdateID,
                          &mbmsSessionUpdateResponseIEs->sessionUpdateID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionUpdateResponse->mbmsSessionUpdateResponse_ies.list, ie);

    /* Optional field */
    if ((mbmsSessionUpdateResponseIEs->presenceMask & MBMSSESSIONUPDATERESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT)
        == MBMSSESSIONUPDATERESPONSEIES_RANAP_TRANSPORTLAYERINFORMATION_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TransportLayerInformation,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TransportLayerInformation,
                              &mbmsSessionUpdateResponseIEs->transportLayerInformation)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionUpdateResponse->mbmsSessionUpdateResponse_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsSessionUpdateResponseIEs->presenceMask & MBMSSESSIONUPDATERESPONSEIES_RANAP_CAUSE_PRESENT)
        == MBMSSESSIONUPDATERESPONSEIES_RANAP_CAUSE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_Cause,
                              &mbmsSessionUpdateResponseIEs->cause)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionUpdateResponse->mbmsSessionUpdateResponse_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsSessionUpdateResponseIEs->presenceMask & MBMSSESSIONUPDATERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONUPDATERESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsSessionUpdateResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionUpdateResponse->mbmsSessionUpdateResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmssessionupdatefailureies(
    RANAP_MBMSSessionUpdateFailure_t *mbmsSessionUpdateFailure,
    RANAP_MBMSSessionUpdateFailureIEs_t *mbmsSessionUpdateFailureIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SessionUpdateID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_SessionUpdateID,
                          &mbmsSessionUpdateFailureIEs->sessionUpdateID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionUpdateFailure->mbmsSessionUpdateFailure_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &mbmsSessionUpdateFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionUpdateFailure->mbmsSessionUpdateFailure_ies.list, ie);

    /* Optional field */
    if ((mbmsSessionUpdateFailureIEs->presenceMask & MBMSSESSIONUPDATEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONUPDATEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsSessionUpdateFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionUpdateFailure->mbmsSessionUpdateFailure_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmssessionstopies(
    RANAP_MBMSSessionStop_t *mbmsSessionStop,
    RANAP_MBMSSessionStopIEs_t *mbmsSessionStopIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_MBMSCNDe_Registration,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_MBMSCNDe_Registration,
                          &mbmsSessionStopIEs->mbmscnDe_Registration)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsSessionStop->mbmsSessionStop_ies.list, ie);

    return 0;
}

int ranap_encode_mbmssessionstopresponseies(
    RANAP_MBMSSessionStopResponse_t *mbmsSessionStopResponse,
    RANAP_MBMSSessionStopResponseIEs_t *mbmsSessionStopResponseIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((mbmsSessionStopResponseIEs->presenceMask & MBMSSESSIONSTOPRESPONSEIES_RANAP_CAUSE_PRESENT)
        == MBMSSESSIONSTOPRESPONSEIES_RANAP_CAUSE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_Cause,
                              &mbmsSessionStopResponseIEs->cause)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStopResponse->mbmsSessionStopResponse_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsSessionStopResponseIEs->presenceMask & MBMSSESSIONSTOPRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSSESSIONSTOPRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsSessionStopResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsSessionStopResponse->mbmsSessionStopResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmsuelinkingrequesties(
    RANAP_MBMSUELinkingRequest_t *mbmsueLinkingRequest,
    RANAP_MBMSUELinkingRequestIEs_t *mbmsueLinkingRequestIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((mbmsueLinkingRequestIEs->presenceMask & MBMSUELINKINGREQUESTIES_RANAP_JOINEDMBMSBEARERSERVICE_IES_PRESENT)
        == MBMSUELINKINGREQUESTIES_RANAP_JOINEDMBMSBEARERSERVICE_IES_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_JoinedMBMSBearerServicesList,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_JoinedMBMSBearerService_IEs,
                              &mbmsueLinkingRequestIEs->joinedMBMSBearerServicesList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsueLinkingRequest->mbmsueLinkingRequest_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsueLinkingRequestIEs->presenceMask & MBMSUELINKINGREQUESTIES_RANAP_LEFTMBMSBEARERSERVICE_IES_PRESENT)
        == MBMSUELINKINGREQUESTIES_RANAP_LEFTMBMSBEARERSERVICE_IES_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_LeftMBMSBearerServicesList,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_LeftMBMSBearerService_IEs,
                              &mbmsueLinkingRequestIEs->leftMBMSBearerServicesList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsueLinkingRequest->mbmsueLinkingRequest_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmsuelinkingresponseies(
    RANAP_MBMSUELinkingResponse_t *mbmsueLinkingResponse,
    RANAP_MBMSUELinkingResponseIEs_t *mbmsueLinkingResponseIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((mbmsueLinkingResponseIEs->presenceMask & MBMSUELINKINGRESPONSEIES_RANAP_UNSUCCESSFULLINKING_IES_PRESENT)
        == MBMSUELINKINGRESPONSEIES_RANAP_UNSUCCESSFULLINKING_IES_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_UnsuccessfulLinkingList,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_UnsuccessfulLinking_IEs,
                              &mbmsueLinkingResponseIEs->unsuccessfulLinkingList)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsueLinkingResponse->mbmsueLinkingResponse_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsueLinkingResponseIEs->presenceMask & MBMSUELINKINGRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSUELINKINGRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsueLinkingResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsueLinkingResponse->mbmsueLinkingResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmsregistrationrequesties(
    RANAP_MBMSRegistrationRequest_t *mbmsRegistrationRequest,
    RANAP_MBMSRegistrationRequestIEs_t *mbmsRegistrationRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_MBMSRegistrationRequestType,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_MBMSRegistrationRequestType,
                          &mbmsRegistrationRequestIEs->mbmsRegistrationRequestType)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsRegistrationRequest->mbmsRegistrationRequest_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TMGI,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_TMGI,
                          &mbmsRegistrationRequestIEs->tmgi)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsRegistrationRequest->mbmsRegistrationRequest_ies.list, ie);

    /* Optional field */
    if ((mbmsRegistrationRequestIEs->presenceMask & MBMSREGISTRATIONREQUESTIES_RANAP_GLOBALRNC_ID_PRESENT)
        == MBMSREGISTRATIONREQUESTIES_RANAP_GLOBALRNC_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_GlobalRNC_ID,
                              &mbmsRegistrationRequestIEs->globalRNC_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsRegistrationRequest->mbmsRegistrationRequest_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmsregistrationresponseies(
    RANAP_MBMSRegistrationResponse_t *mbmsRegistrationResponse,
    RANAP_MBMSRegistrationResponseIEs_t *mbmsRegistrationResponseIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((mbmsRegistrationResponseIEs->presenceMask & MBMSREGISTRATIONRESPONSEIES_RANAP_TMGI_PRESENT)
        == MBMSREGISTRATIONRESPONSEIES_RANAP_TMGI_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TMGI,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TMGI,
                              &mbmsRegistrationResponseIEs->tmgi)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsRegistrationResponse->mbmsRegistrationResponse_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsRegistrationResponseIEs->presenceMask & MBMSREGISTRATIONRESPONSEIES_RANAP_GLOBALCN_ID_PRESENT)
        == MBMSREGISTRATIONRESPONSEIES_RANAP_GLOBALCN_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &mbmsRegistrationResponseIEs->globalCN_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsRegistrationResponse->mbmsRegistrationResponse_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsRegistrationResponseIEs->presenceMask & MBMSREGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSREGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsRegistrationResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsRegistrationResponse->mbmsRegistrationResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmsregistrationfailureies(
    RANAP_MBMSRegistrationFailure_t *mbmsRegistrationFailure,
    RANAP_MBMSRegistrationFailureIEs_t *mbmsRegistrationFailureIEs) {

    RANAP_IE_t *ie;

    /* Optional field */
    if ((mbmsRegistrationFailureIEs->presenceMask & MBMSREGISTRATIONFAILUREIES_RANAP_TMGI_PRESENT)
        == MBMSREGISTRATIONFAILUREIES_RANAP_TMGI_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TMGI,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_TMGI,
                              &mbmsRegistrationFailureIEs->tmgi)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsRegistrationFailure->mbmsRegistrationFailure_ies.list, ie);
    }

    /* Optional field */
    if ((mbmsRegistrationFailureIEs->presenceMask & MBMSREGISTRATIONFAILUREIES_RANAP_GLOBALCN_ID_PRESENT)
        == MBMSREGISTRATIONFAILUREIES_RANAP_GLOBALCN_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_ID,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &mbmsRegistrationFailureIEs->globalCN_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsRegistrationFailure->mbmsRegistrationFailure_ies.list, ie);
    }

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &mbmsRegistrationFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsRegistrationFailure->mbmsRegistrationFailure_ies.list, ie);

    /* Optional field */
    if ((mbmsRegistrationFailureIEs->presenceMask & MBMSREGISTRATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSREGISTRATIONFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsRegistrationFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsRegistrationFailure->mbmsRegistrationFailure_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmscnde_registrationrequesties(
    RANAP_MBMSCNDe_RegistrationRequest_t *mbmscnDe_RegistrationRequest,
    RANAP_MBMSCNDe_RegistrationRequestIEs_t *mbmscnDe_RegistrationRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TMGI,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_TMGI,
                          &mbmscnDe_RegistrationRequestIEs->tmgi)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmscnDe_RegistrationRequest->mbmscnDe_RegistrationRequest_ies.list, ie);

    /* Optional field */
    if ((mbmscnDe_RegistrationRequestIEs->presenceMask & MBMSCNDE_REGISTRATIONREQUESTIES_RANAP_GLOBALCN_ID_PRESENT)
        == MBMSCNDE_REGISTRATIONREQUESTIES_RANAP_GLOBALCN_ID_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalCN_ID,
                              RANAP_Criticality_reject,
                              &asn_DEF_RANAP_GlobalCN_ID,
                              &mbmscnDe_RegistrationRequestIEs->globalCN_ID)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmscnDe_RegistrationRequest->mbmscnDe_RegistrationRequest_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmscnde_registrationresponseies(
    RANAP_MBMSCNDe_RegistrationResponse_t *mbmscnDe_RegistrationResponse,
    RANAP_MBMSCNDe_RegistrationResponseIEs_t *mbmscnDe_RegistrationResponseIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TMGI,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_TMGI,
                          &mbmscnDe_RegistrationResponseIEs->tmgi)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmscnDe_RegistrationResponse->mbmscnDe_RegistrationResponse_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_GlobalRNC_ID,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_GlobalRNC_ID,
                          &mbmscnDe_RegistrationResponseIEs->globalRNC_ID)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmscnDe_RegistrationResponse->mbmscnDe_RegistrationResponse_ies.list, ie);

    /* Optional field */
    if ((mbmscnDe_RegistrationResponseIEs->presenceMask & MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CAUSE_PRESENT)
        == MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CAUSE_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_Cause,
                              &mbmscnDe_RegistrationResponseIEs->cause)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmscnDe_RegistrationResponse->mbmscnDe_RegistrationResponse_ies.list, ie);
    }

    /* Optional field */
    if ((mbmscnDe_RegistrationResponseIEs->presenceMask & MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSCNDE_REGISTRATIONRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmscnDe_RegistrationResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmscnDe_RegistrationResponse->mbmscnDe_RegistrationResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmsrabestablishmentindicationies(
    RANAP_MBMSRABEstablishmentIndication_t *mbmsrabEstablishmentIndication,
    RANAP_MBMSRABEstablishmentIndicationIEs_t *mbmsrabEstablishmentIndicationIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_TransportLayerInformation,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_TransportLayerInformation,
                          &mbmsrabEstablishmentIndicationIEs->transportLayerInformation)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsrabEstablishmentIndication->mbmsrabEstablishmentIndication_ies.list, ie);

    return 0;
}

int ranap_encode_mbmsrabreleaserequesties(
    RANAP_MBMSRABReleaseRequest_t *mbmsrabReleaseRequest,
    RANAP_MBMSRABReleaseRequestIEs_t *mbmsrabReleaseRequestIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &mbmsrabReleaseRequestIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsrabReleaseRequest->mbmsrabReleaseRequest_ies.list, ie);

    return 0;
}

int ranap_encode_mbmsrabreleaseies(
    RANAP_MBMSRABRelease_t *mbmsrabRelease,
    RANAP_MBMSRABReleaseIEs_t *mbmsrabReleaseIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &mbmsrabReleaseIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsrabRelease->mbmsrabRelease_ies.list, ie);

    /* Optional field */
    if ((mbmsrabReleaseIEs->presenceMask & MBMSRABRELEASEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSRABRELEASEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsrabReleaseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsrabRelease->mbmsrabRelease_ies.list, ie);
    }

    return 0;
}

int ranap_encode_mbmsrabreleasefailureies(
    RANAP_MBMSRABReleaseFailure_t *mbmsrabReleaseFailure,
    RANAP_MBMSRABReleaseFailureIEs_t *mbmsrabReleaseFailureIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_Cause,
                          RANAP_Criticality_ignore,
                          &asn_DEF_RANAP_Cause,
                          &mbmsrabReleaseFailureIEs->cause)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&mbmsrabReleaseFailure->mbmsrabReleaseFailure_ies.list, ie);

    /* Optional field */
    if ((mbmsrabReleaseFailureIEs->presenceMask & MBMSRABRELEASEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == MBMSRABRELEASEFAILUREIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &mbmsrabReleaseFailureIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&mbmsrabReleaseFailure->mbmsrabReleaseFailure_ies.list, ie);
    }

    return 0;
}

int ranap_encode_srvcc_cskeysresponseies(
    RANAP_SRVCC_CSKeysResponse_t *srvcC_CSKeysResponse,
    RANAP_SRVCC_CSKeysResponseIEs_t *srvcC_CSKeysResponseIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_IntegrityProtectionKey,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_IntegrityProtectionKey,
                          &srvcC_CSKeysResponseIEs->integrityProtectionKey)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&srvcC_CSKeysResponse->srvcC_CSKeysResponse_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_EncryptionKey,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_EncryptionKey,
                          &srvcC_CSKeysResponseIEs->encryptionKey)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&srvcC_CSKeysResponse->srvcC_CSKeysResponse_ies.list, ie);

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_SRVCC_Information,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_SRVCC_Information,
                          &srvcC_CSKeysResponseIEs->srvcC_Information)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&srvcC_CSKeysResponse->srvcC_CSKeysResponse_ies.list, ie);

    /* Optional field */
    if ((srvcC_CSKeysResponseIEs->presenceMask & SRVCC_CSKEYSRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT)
        == SRVCC_CSKEYSRESPONSEIES_RANAP_CRITICALITYDIAGNOSTICS_PRESENT) {
        if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_CriticalityDiagnostics,
                              RANAP_Criticality_ignore,
                              &asn_DEF_RANAP_CriticalityDiagnostics,
                              &srvcC_CSKeysResponseIEs->criticalityDiagnostics)) == NULL) {
            return -1;
        }
        ASN_SEQUENCE_ADD(&srvcC_CSKeysResponse->srvcC_CSKeysResponse_ies.list, ie);
    }

    return 0;
}

int ranap_encode_ueradiocapabilitymatchresponseies(
    RANAP_UeRadioCapabilityMatchResponse_t *ueRadioCapabilityMatchResponse,
    RANAP_UeRadioCapabilityMatchResponseIEs_t *ueRadioCapabilityMatchResponseIEs) {

    RANAP_IE_t *ie;

    if ((ie = ranap_new_ie(RANAP_ProtocolIE_ID_id_VoiceSupportMatchIndicator,
                          RANAP_Criticality_reject,
                          &asn_DEF_RANAP_VoiceSupportMatchIndicator,
                          &ueRadioCapabilityMatchResponseIEs->voiceSupportMatchIndicator)) == NULL) {
        return -1;
    }
    ASN_SEQUENCE_ADD(&ueRadioCapabilityMatchResponse->ueRadioCapabilityMatchResponse_ies.list, ie);

    return 0;
}

