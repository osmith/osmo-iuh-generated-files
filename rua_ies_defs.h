/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/rua/RUA-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/rua/rua_common.h>

#ifndef RUA_IES_DEFS_H_
#define RUA_IES_DEFS_H_

#define CONNECTIES_RUA_INTRADOMAINNASNODESELECTOR_PRESENT (1 << 0)

typedef struct RUA_ConnectIEs_s {
    uint16_t                     presenceMask;
    RUA_CN_DomainIndicator_t     cN_DomainIndicator;
    RUA_Context_ID_t             context_ID;
    RUA_IntraDomainNasNodeSelector_t intraDomainNasNodeSelector; ///< Optional field
    RUA_Establishment_Cause_t    establishment_Cause;
    RUA_RANAP_Message_t          ranaP_Message;
} RUA_ConnectIEs_t;

typedef struct RUA_DirectTransferIEs_s {
    RUA_CN_DomainIndicator_t cN_DomainIndicator;
    RUA_Context_ID_t     context_ID;
    RUA_RANAP_Message_t  ranaP_Message;
} RUA_DirectTransferIEs_t;

#define DISCONNECTIES_RUA_RANAP_MESSAGE_PRESENT  (1 << 0)

typedef struct RUA_DisconnectIEs_s {
    uint16_t             presenceMask;
    RUA_CN_DomainIndicator_t cN_DomainIndicator;
    RUA_Context_ID_t     context_ID;
    RUA_Cause_t          cause;
    RUA_RANAP_Message_t  ranaP_Message; ///< Conditional field
} RUA_DisconnectIEs_t;

typedef struct RUA_ConnectionlessTransferIEs_s {
    RUA_RANAP_Message_t ranaP_Message;
} RUA_ConnectionlessTransferIEs_t;

#define ERRORINDICATIONIES_RUA_CRITICALITYDIAGNOSTICS_PRESENT (1 << 0)

typedef struct RUA_ErrorIndicationIEs_s {
    uint16_t                 presenceMask;
    RUA_Cause_t              cause;
    RUA_CriticalityDiagnostics_t criticalityDiagnostics; ///< Optional field
} RUA_ErrorIndicationIEs_t;

typedef struct rua_message_s {
    uint8_t procedureCode;
    uint8_t criticality;
    uint8_t direction;
    union {
        RUA_ConnectIEs_t connectIEs;
        RUA_ConnectionlessTransferIEs_t connectionlessTransferIEs;
        RUA_DirectTransferIEs_t directTransferIEs;
        RUA_DisconnectIEs_t disconnectIEs;
        RUA_ErrorIndicationIEs_t errorIndicationIEs;
    } msg;
} rua_message;

/** \brief Decode function for ConnectIEs ies.
 * \param connectIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int rua_decode_connecties(
    RUA_ConnectIEs_t *connectIEs,
    ANY_t *any_p);

/** \brief Encode function for ConnectIEs ies.
 *  \param ruA_Connect Pointer to the ASN1 structure.
 *  \param connectIEs Pointer to the IES structure.
 **/
int rua_encode_connecties(
    RUA_Connect_t *ruA_Connect,
    RUA_ConnectIEs_t *connectIEs);

/** \brief Decode function for DirectTransferIEs ies.
 * \param directTransferIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int rua_decode_directtransferies(
    RUA_DirectTransferIEs_t *directTransferIEs,
    ANY_t *any_p);

/** \brief Encode function for DirectTransferIEs ies.
 *  \param ruA_DirectTransfer Pointer to the ASN1 structure.
 *  \param directTransferIEs Pointer to the IES structure.
 **/
int rua_encode_directtransferies(
    RUA_DirectTransfer_t *ruA_DirectTransfer,
    RUA_DirectTransferIEs_t *directTransferIEs);

/** \brief Decode function for DisconnectIEs ies.
 * \param disconnectIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int rua_decode_disconnecties(
    RUA_DisconnectIEs_t *disconnectIEs,
    ANY_t *any_p);

/** \brief Encode function for DisconnectIEs ies.
 *  \param ruA_Disconnect Pointer to the ASN1 structure.
 *  \param disconnectIEs Pointer to the IES structure.
 **/
int rua_encode_disconnecties(
    RUA_Disconnect_t *ruA_Disconnect,
    RUA_DisconnectIEs_t *disconnectIEs);

/** \brief Decode function for ConnectionlessTransferIEs ies.
 * \param connectionlessTransferIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int rua_decode_connectionlesstransferies(
    RUA_ConnectionlessTransferIEs_t *connectionlessTransferIEs,
    ANY_t *any_p);

/** \brief Encode function for ConnectionlessTransferIEs ies.
 *  \param ruA_ConnectionlessTransfer Pointer to the ASN1 structure.
 *  \param connectionlessTransferIEs Pointer to the IES structure.
 **/
int rua_encode_connectionlesstransferies(
    RUA_ConnectionlessTransfer_t *ruA_ConnectionlessTransfer,
    RUA_ConnectionlessTransferIEs_t *connectionlessTransferIEs);

/** \brief Decode function for ErrorIndicationIEs ies.
 * \param errorIndicationIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int rua_decode_errorindicationies(
    RUA_ErrorIndicationIEs_t *errorIndicationIEs,
    ANY_t *any_p);

/** \brief Encode function for ErrorIndicationIEs ies.
 *  \param ruA_ErrorIndication Pointer to the ASN1 structure.
 *  \param errorIndicationIEs Pointer to the IES structure.
 **/
int rua_encode_errorindicationies(
    RUA_ErrorIndication_t *ruA_ErrorIndication,
    RUA_ErrorIndicationIEs_t *errorIndicationIEs);

int rua_free_connecties(
    RUA_ConnectIEs_t *connectIEs);

int rua_free_directtransferies(
    RUA_DirectTransferIEs_t *directTransferIEs);

int rua_free_disconnecties(
    RUA_DisconnectIEs_t *disconnectIEs);

int rua_free_connectionlesstransferies(
    RUA_ConnectionlessTransferIEs_t *connectionlessTransferIEs);

int rua_free_errorindicationies(
    RUA_ErrorIndicationIEs_t *errorIndicationIEs);

#endif /* RUA_IES_DEFS_H_ */

