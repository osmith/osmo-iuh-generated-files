/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/hnbap/HNBAP-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/hnbap/hnbap_common.h>

#ifndef HNBAP_IES_DEFS_H_
#define HNBAP_IES_DEFS_H_

#define HNBREGISTERREQUESTIES_HNBAP_CSG_ID_PRESENT             (1 << 0)

typedef struct HNBAP_HNBRegisterRequestIEs_s {
    uint16_t                   presenceMask;
    HNBAP_HNB_Identity_t       hnB_Identity;
    HNBAP_HNB_Location_Information_t hnB_Location_Information;
    HNBAP_PLMNidentity_t       plmNidentity;
    HNBAP_CellIdentity_t       cellIdentity;
    HNBAP_LAC_t                lac;
    HNBAP_RAC_t                rac;
    HNBAP_SAC_t                sac;
    HNBAP_CSG_ID_t             csg_id; ///< Optional field
} HNBAP_HNBRegisterRequestIEs_t;

typedef struct HNBAP_HNBRegisterAcceptIEs_s {
    HNBAP_RNC_ID_t rnc_id;
} HNBAP_HNBRegisterAcceptIEs_t;

#define HNBREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT (1 << 0)
#define HNBREGISTERREJECTIES_HNBAP_BACKOFFTIMER_PRESENT     (1 << 1)

typedef struct HNBAP_HNBRegisterRejectIEs_s {
    uint16_t                 presenceMask;
    HNBAP_Cause_t            cause;
    HNBAP_CriticalityDiagnostics_t criticalityDiagnostics; ///< Optional field
    HNBAP_BackoffTimer_t     backoffTimer; ///< Conditional field
} HNBAP_HNBRegisterRejectIEs_t;

#define HNBDE_REGISTERIES_HNBAP_BACKOFFTIMER_PRESENT (1 << 0)

typedef struct HNBAP_HNBDe_RegisterIEs_s {
    uint16_t       presenceMask;
    HNBAP_Cause_t  cause;
    HNBAP_BackoffTimer_t backoffTimer; ///< Conditional field
} HNBAP_HNBDe_RegisterIEs_t;

typedef struct HNBAP_UERegisterRequestIEs_s {
    HNBAP_UE_Identity_t  uE_Identity;
    HNBAP_Registration_Cause_t registration_Cause;
    HNBAP_UE_Capabilities_t uE_Capabilities;
} HNBAP_UERegisterRequestIEs_t;

typedef struct HNBAP_UERegisterAcceptIEs_s {
    HNBAP_UE_Identity_t uE_Identity;
    HNBAP_Context_ID_t context_ID;
} HNBAP_UERegisterAcceptIEs_t;

#define UEREGISTERREJECTIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT (1 << 0)

typedef struct HNBAP_UERegisterRejectIEs_s {
    uint16_t                 presenceMask;
    HNBAP_UE_Identity_t      uE_Identity;
    HNBAP_Cause_t            cause;
    HNBAP_CriticalityDiagnostics_t criticalityDiagnostics; ///< Optional field
} HNBAP_UERegisterRejectIEs_t;

typedef struct HNBAP_UEDe_RegisterIEs_s {
    HNBAP_Context_ID_t context_ID;
    HNBAP_Cause_t cause;
} HNBAP_UEDe_RegisterIEs_t;

typedef struct HNBAP_CSGMembershipUpdateIEs_s {
    HNBAP_Context_ID_t    context_ID;
    HNBAP_CSGMembershipStatus_t csgMembershipStatus;
} HNBAP_CSGMembershipUpdateIEs_t;

typedef struct HNBAP_TNLUpdateRequestIEs_s {
    HNBAP_Context_ID_t context_ID;
    HNBAP_RABList_t rabList;
    HNBAP_Update_cause_t update_cause;
} HNBAP_TNLUpdateRequestIEs_t;

typedef struct HNBAP_TNLUpdateResponseIEs_s {
    HNBAP_Context_ID_t context_ID;
} HNBAP_TNLUpdateResponseIEs_t;

#define TNLUPDATEFAILUREIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT (1 << 0)

typedef struct HNBAP_TNLUpdateFailureIEs_s {
    uint16_t                 presenceMask;
    HNBAP_Context_ID_t       context_ID;
    HNBAP_Cause_t            cause;
    HNBAP_CriticalityDiagnostics_t criticalityDiagnostics; ///< Optional field
} HNBAP_TNLUpdateFailureIEs_t;

typedef struct HNBAP_HNBConfigTransferRequestIEs_s {
    HNBAP_NeighbourInfoRequestList_t neighbourInfoRequestList;
} HNBAP_HNBConfigTransferRequestIEs_t;

typedef struct HNBAP_HNBConfigTransferResponseIEs_s {
    HNBAP_NeighbourInfoList_t neighbourInfoList;
} HNBAP_HNBConfigTransferResponseIEs_t;

typedef struct HNBAP_RelocationCompleteIEs_s {
    HNBAP_Context_ID_t context_ID;
} HNBAP_RelocationCompleteIEs_t;

#define ERRORINDICATIONIES_HNBAP_CRITICALITYDIAGNOSTICS_PRESENT (1 << 0)

typedef struct HNBAP_ErrorIndicationIEs_s {
    uint16_t                 presenceMask;
    HNBAP_Cause_t            cause;
    HNBAP_CriticalityDiagnostics_t criticalityDiagnostics; ///< Optional field
} HNBAP_ErrorIndicationIEs_t;

typedef struct HNBAP_U_RNTIQueryRequestIEs_s {
    HNBAP_U_RNTI_t u_rnti;
} HNBAP_U_RNTIQueryRequestIEs_t;

typedef struct HNBAP_U_RNTIQueryResponseIEs_s {
    HNBAP_HNB_GWResponse_t hnB_GWResponse;
} HNBAP_U_RNTIQueryResponseIEs_t;

typedef struct hnbap_message_s {
    uint8_t procedureCode;
    uint8_t criticality;
    uint8_t direction;
    union {
        HNBAP_CSGMembershipUpdateIEs_t csgMembershipUpdateIEs;
        HNBAP_ErrorIndicationIEs_t errorIndicationIEs;
        HNBAP_HNBConfigTransferRequestIEs_t hnbConfigTransferRequestIEs;
        HNBAP_HNBConfigTransferResponseIEs_t hnbConfigTransferResponseIEs;
        HNBAP_HNBDe_RegisterIEs_t hnbDe_RegisterIEs;
        HNBAP_HNBRegisterAcceptIEs_t hnbRegisterAcceptIEs;
        HNBAP_HNBRegisterRejectIEs_t hnbRegisterRejectIEs;
        HNBAP_HNBRegisterRequestIEs_t hnbRegisterRequestIEs;
        HNBAP_RelocationCompleteIEs_t relocationCompleteIEs;
        HNBAP_TNLUpdateFailureIEs_t tnlUpdateFailureIEs;
        HNBAP_TNLUpdateRequestIEs_t tnlUpdateRequestIEs;
        HNBAP_TNLUpdateResponseIEs_t tnlUpdateResponseIEs;
        HNBAP_U_RNTIQueryRequestIEs_t u_RNTIQueryRequestIEs;
        HNBAP_U_RNTIQueryResponseIEs_t u_RNTIQueryResponseIEs;
        HNBAP_UEDe_RegisterIEs_t ueDe_RegisterIEs;
        HNBAP_UERegisterAcceptIEs_t ueRegisterAcceptIEs;
        HNBAP_UERegisterRejectIEs_t ueRegisterRejectIEs;
        HNBAP_UERegisterRequestIEs_t ueRegisterRequestIEs;
    } msg;
} hnbap_message;

/** \brief Decode function for HNBRegisterRequestIEs ies.
 * \param hnbRegisterRequestIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_hnbregisterrequesties(
    HNBAP_HNBRegisterRequestIEs_t *hnbRegisterRequestIEs,
    ANY_t *any_p);

/** \brief Encode function for HNBRegisterRequestIEs ies.
 *  \param hnbaP_HNBRegisterRequest Pointer to the ASN1 structure.
 *  \param hnbRegisterRequestIEs Pointer to the IES structure.
 **/
int hnbap_encode_hnbregisterrequesties(
    HNBAP_HNBRegisterRequest_t *hnbaP_HNBRegisterRequest,
    HNBAP_HNBRegisterRequestIEs_t *hnbRegisterRequestIEs);

/** \brief Decode function for HNBRegisterAcceptIEs ies.
 * \param hnbRegisterAcceptIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_hnbregisteraccepties(
    HNBAP_HNBRegisterAcceptIEs_t *hnbRegisterAcceptIEs,
    ANY_t *any_p);

/** \brief Encode function for HNBRegisterAcceptIEs ies.
 *  \param hnbaP_HNBRegisterAccept Pointer to the ASN1 structure.
 *  \param hnbRegisterAcceptIEs Pointer to the IES structure.
 **/
int hnbap_encode_hnbregisteraccepties(
    HNBAP_HNBRegisterAccept_t *hnbaP_HNBRegisterAccept,
    HNBAP_HNBRegisterAcceptIEs_t *hnbRegisterAcceptIEs);

/** \brief Decode function for HNBRegisterRejectIEs ies.
 * \param hnbRegisterRejectIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_hnbregisterrejecties(
    HNBAP_HNBRegisterRejectIEs_t *hnbRegisterRejectIEs,
    ANY_t *any_p);

/** \brief Encode function for HNBRegisterRejectIEs ies.
 *  \param hnbaP_HNBRegisterReject Pointer to the ASN1 structure.
 *  \param hnbRegisterRejectIEs Pointer to the IES structure.
 **/
int hnbap_encode_hnbregisterrejecties(
    HNBAP_HNBRegisterReject_t *hnbaP_HNBRegisterReject,
    HNBAP_HNBRegisterRejectIEs_t *hnbRegisterRejectIEs);

/** \brief Decode function for HNBDe-RegisterIEs ies.
 * \param hnbDe_RegisterIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_hnbde_registeries(
    HNBAP_HNBDe_RegisterIEs_t *hnbDe_RegisterIEs,
    ANY_t *any_p);

/** \brief Encode function for HNBDe-RegisterIEs ies.
 *  \param hnbaP_HNBDe_Register Pointer to the ASN1 structure.
 *  \param hnbDe_RegisterIEs Pointer to the IES structure.
 **/
int hnbap_encode_hnbde_registeries(
    HNBAP_HNBDe_Register_t *hnbaP_HNBDe_Register,
    HNBAP_HNBDe_RegisterIEs_t *hnbDe_RegisterIEs);

/** \brief Decode function for UERegisterRequestIEs ies.
 * \param ueRegisterRequestIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_ueregisterrequesties(
    HNBAP_UERegisterRequestIEs_t *ueRegisterRequestIEs,
    ANY_t *any_p);

/** \brief Encode function for UERegisterRequestIEs ies.
 *  \param hnbaP_UERegisterRequest Pointer to the ASN1 structure.
 *  \param ueRegisterRequestIEs Pointer to the IES structure.
 **/
int hnbap_encode_ueregisterrequesties(
    HNBAP_UERegisterRequest_t *hnbaP_UERegisterRequest,
    HNBAP_UERegisterRequestIEs_t *ueRegisterRequestIEs);

/** \brief Decode function for UERegisterAcceptIEs ies.
 * \param ueRegisterAcceptIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_ueregisteraccepties(
    HNBAP_UERegisterAcceptIEs_t *ueRegisterAcceptIEs,
    ANY_t *any_p);

/** \brief Encode function for UERegisterAcceptIEs ies.
 *  \param hnbaP_UERegisterAccept Pointer to the ASN1 structure.
 *  \param ueRegisterAcceptIEs Pointer to the IES structure.
 **/
int hnbap_encode_ueregisteraccepties(
    HNBAP_UERegisterAccept_t *hnbaP_UERegisterAccept,
    HNBAP_UERegisterAcceptIEs_t *ueRegisterAcceptIEs);

/** \brief Decode function for UERegisterRejectIEs ies.
 * \param ueRegisterRejectIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_ueregisterrejecties(
    HNBAP_UERegisterRejectIEs_t *ueRegisterRejectIEs,
    ANY_t *any_p);

/** \brief Encode function for UERegisterRejectIEs ies.
 *  \param hnbaP_UERegisterReject Pointer to the ASN1 structure.
 *  \param ueRegisterRejectIEs Pointer to the IES structure.
 **/
int hnbap_encode_ueregisterrejecties(
    HNBAP_UERegisterReject_t *hnbaP_UERegisterReject,
    HNBAP_UERegisterRejectIEs_t *ueRegisterRejectIEs);

/** \brief Decode function for UEDe-RegisterIEs ies.
 * \param ueDe_RegisterIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_uede_registeries(
    HNBAP_UEDe_RegisterIEs_t *ueDe_RegisterIEs,
    ANY_t *any_p);

/** \brief Encode function for UEDe-RegisterIEs ies.
 *  \param hnbaP_UEDe_Register Pointer to the ASN1 structure.
 *  \param ueDe_RegisterIEs Pointer to the IES structure.
 **/
int hnbap_encode_uede_registeries(
    HNBAP_UEDe_Register_t *hnbaP_UEDe_Register,
    HNBAP_UEDe_RegisterIEs_t *ueDe_RegisterIEs);

/** \brief Decode function for CSGMembershipUpdateIEs ies.
 * \param csgMembershipUpdateIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_csgmembershipupdateies(
    HNBAP_CSGMembershipUpdateIEs_t *csgMembershipUpdateIEs,
    ANY_t *any_p);

/** \brief Encode function for CSGMembershipUpdateIEs ies.
 *  \param hnbaP_CSGMembershipUpdate Pointer to the ASN1 structure.
 *  \param csgMembershipUpdateIEs Pointer to the IES structure.
 **/
int hnbap_encode_csgmembershipupdateies(
    HNBAP_CSGMembershipUpdate_t *hnbaP_CSGMembershipUpdate,
    HNBAP_CSGMembershipUpdateIEs_t *csgMembershipUpdateIEs);

/** \brief Decode function for TNLUpdateRequestIEs ies.
 * \param tnlUpdateRequestIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_tnlupdaterequesties(
    HNBAP_TNLUpdateRequestIEs_t *tnlUpdateRequestIEs,
    ANY_t *any_p);

/** \brief Encode function for TNLUpdateRequestIEs ies.
 *  \param hnbaP_TNLUpdateRequest Pointer to the ASN1 structure.
 *  \param tnlUpdateRequestIEs Pointer to the IES structure.
 **/
int hnbap_encode_tnlupdaterequesties(
    HNBAP_TNLUpdateRequest_t *hnbaP_TNLUpdateRequest,
    HNBAP_TNLUpdateRequestIEs_t *tnlUpdateRequestIEs);

/** \brief Decode function for TNLUpdateResponseIEs ies.
 * \param tnlUpdateResponseIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_tnlupdateresponseies(
    HNBAP_TNLUpdateResponseIEs_t *tnlUpdateResponseIEs,
    ANY_t *any_p);

/** \brief Encode function for TNLUpdateResponseIEs ies.
 *  \param hnbaP_TNLUpdateResponse Pointer to the ASN1 structure.
 *  \param tnlUpdateResponseIEs Pointer to the IES structure.
 **/
int hnbap_encode_tnlupdateresponseies(
    HNBAP_TNLUpdateResponse_t *hnbaP_TNLUpdateResponse,
    HNBAP_TNLUpdateResponseIEs_t *tnlUpdateResponseIEs);

/** \brief Decode function for TNLUpdateFailureIEs ies.
 * \param tnlUpdateFailureIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_tnlupdatefailureies(
    HNBAP_TNLUpdateFailureIEs_t *tnlUpdateFailureIEs,
    ANY_t *any_p);

/** \brief Encode function for TNLUpdateFailureIEs ies.
 *  \param hnbaP_TNLUpdateFailure Pointer to the ASN1 structure.
 *  \param tnlUpdateFailureIEs Pointer to the IES structure.
 **/
int hnbap_encode_tnlupdatefailureies(
    HNBAP_TNLUpdateFailure_t *hnbaP_TNLUpdateFailure,
    HNBAP_TNLUpdateFailureIEs_t *tnlUpdateFailureIEs);

/** \brief Decode function for HNBConfigTransferRequestIEs ies.
 * \param hnbConfigTransferRequestIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_hnbconfigtransferrequesties(
    HNBAP_HNBConfigTransferRequestIEs_t *hnbConfigTransferRequestIEs,
    ANY_t *any_p);

/** \brief Encode function for HNBConfigTransferRequestIEs ies.
 *  \param hnbaP_HNBConfigTransferRequest Pointer to the ASN1 structure.
 *  \param hnbConfigTransferRequestIEs Pointer to the IES structure.
 **/
int hnbap_encode_hnbconfigtransferrequesties(
    HNBAP_HNBConfigTransferRequest_t *hnbaP_HNBConfigTransferRequest,
    HNBAP_HNBConfigTransferRequestIEs_t *hnbConfigTransferRequestIEs);

/** \brief Decode function for HNBConfigTransferResponseIEs ies.
 * \param hnbConfigTransferResponseIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_hnbconfigtransferresponseies(
    HNBAP_HNBConfigTransferResponseIEs_t *hnbConfigTransferResponseIEs,
    ANY_t *any_p);

/** \brief Encode function for HNBConfigTransferResponseIEs ies.
 *  \param hnbaP_HNBConfigTransferResponse Pointer to the ASN1 structure.
 *  \param hnbConfigTransferResponseIEs Pointer to the IES structure.
 **/
int hnbap_encode_hnbconfigtransferresponseies(
    HNBAP_HNBConfigTransferResponse_t *hnbaP_HNBConfigTransferResponse,
    HNBAP_HNBConfigTransferResponseIEs_t *hnbConfigTransferResponseIEs);

/** \brief Decode function for RelocationCompleteIEs ies.
 * \param relocationCompleteIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_relocationcompleteies(
    HNBAP_RelocationCompleteIEs_t *relocationCompleteIEs,
    ANY_t *any_p);

/** \brief Encode function for RelocationCompleteIEs ies.
 *  \param hnbaP_RelocationComplete Pointer to the ASN1 structure.
 *  \param relocationCompleteIEs Pointer to the IES structure.
 **/
int hnbap_encode_relocationcompleteies(
    HNBAP_RelocationComplete_t *hnbaP_RelocationComplete,
    HNBAP_RelocationCompleteIEs_t *relocationCompleteIEs);

/** \brief Decode function for ErrorIndicationIEs ies.
 * \param errorIndicationIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_errorindicationies(
    HNBAP_ErrorIndicationIEs_t *errorIndicationIEs,
    ANY_t *any_p);

/** \brief Encode function for ErrorIndicationIEs ies.
 *  \param hnbaP_ErrorIndication Pointer to the ASN1 structure.
 *  \param errorIndicationIEs Pointer to the IES structure.
 **/
int hnbap_encode_errorindicationies(
    HNBAP_ErrorIndication_t *hnbaP_ErrorIndication,
    HNBAP_ErrorIndicationIEs_t *errorIndicationIEs);

/** \brief Decode function for U-RNTIQueryRequestIEs ies.
 * \param u_RNTIQueryRequestIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_u_rntiqueryrequesties(
    HNBAP_U_RNTIQueryRequestIEs_t *u_RNTIQueryRequestIEs,
    ANY_t *any_p);

/** \brief Encode function for U-RNTIQueryRequestIEs ies.
 *  \param hnbaP_U_RNTIQueryRequest Pointer to the ASN1 structure.
 *  \param u_RNTIQueryRequestIEs Pointer to the IES structure.
 **/
int hnbap_encode_u_rntiqueryrequesties(
    HNBAP_U_RNTIQueryRequest_t *hnbaP_U_RNTIQueryRequest,
    HNBAP_U_RNTIQueryRequestIEs_t *u_RNTIQueryRequestIEs);

/** \brief Decode function for U-RNTIQueryResponseIEs ies.
 * \param u_RNTIQueryResponseIEs Pointer to ASN1 structure in which data will be stored
 *  \param any_p Pointer to the ANY value to decode.
 **/
int hnbap_decode_u_rntiqueryresponseies(
    HNBAP_U_RNTIQueryResponseIEs_t *u_RNTIQueryResponseIEs,
    ANY_t *any_p);

/** \brief Encode function for U-RNTIQueryResponseIEs ies.
 *  \param hnbaP_U_RNTIQueryResponse Pointer to the ASN1 structure.
 *  \param u_RNTIQueryResponseIEs Pointer to the IES structure.
 **/
int hnbap_encode_u_rntiqueryresponseies(
    HNBAP_U_RNTIQueryResponse_t *hnbaP_U_RNTIQueryResponse,
    HNBAP_U_RNTIQueryResponseIEs_t *u_RNTIQueryResponseIEs);

int hnbap_free_hnbregisterrequesties(
    HNBAP_HNBRegisterRequestIEs_t *hnbRegisterRequestIEs);

int hnbap_free_hnbregisteraccepties(
    HNBAP_HNBRegisterAcceptIEs_t *hnbRegisterAcceptIEs);

int hnbap_free_hnbregisterrejecties(
    HNBAP_HNBRegisterRejectIEs_t *hnbRegisterRejectIEs);

int hnbap_free_hnbde_registeries(
    HNBAP_HNBDe_RegisterIEs_t *hnbDe_RegisterIEs);

int hnbap_free_ueregisterrequesties(
    HNBAP_UERegisterRequestIEs_t *ueRegisterRequestIEs);

int hnbap_free_ueregisteraccepties(
    HNBAP_UERegisterAcceptIEs_t *ueRegisterAcceptIEs);

int hnbap_free_ueregisterrejecties(
    HNBAP_UERegisterRejectIEs_t *ueRegisterRejectIEs);

int hnbap_free_uede_registeries(
    HNBAP_UEDe_RegisterIEs_t *ueDe_RegisterIEs);

int hnbap_free_csgmembershipupdateies(
    HNBAP_CSGMembershipUpdateIEs_t *csgMembershipUpdateIEs);

int hnbap_free_tnlupdaterequesties(
    HNBAP_TNLUpdateRequestIEs_t *tnlUpdateRequestIEs);

int hnbap_free_tnlupdateresponseies(
    HNBAP_TNLUpdateResponseIEs_t *tnlUpdateResponseIEs);

int hnbap_free_tnlupdatefailureies(
    HNBAP_TNLUpdateFailureIEs_t *tnlUpdateFailureIEs);

int hnbap_free_hnbconfigtransferrequesties(
    HNBAP_HNBConfigTransferRequestIEs_t *hnbConfigTransferRequestIEs);

int hnbap_free_hnbconfigtransferresponseies(
    HNBAP_HNBConfigTransferResponseIEs_t *hnbConfigTransferResponseIEs);

int hnbap_free_relocationcompleteies(
    HNBAP_RelocationCompleteIEs_t *relocationCompleteIEs);

int hnbap_free_errorindicationies(
    HNBAP_ErrorIndicationIEs_t *errorIndicationIEs);

int hnbap_free_u_rntiqueryrequesties(
    HNBAP_U_RNTIQueryRequestIEs_t *u_RNTIQueryRequestIEs);

int hnbap_free_u_rntiqueryresponseies(
    HNBAP_U_RNTIQueryResponseIEs_t *u_RNTIQueryResponseIEs);

#endif /* HNBAP_IES_DEFS_H_ */

