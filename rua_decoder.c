/*******************************************************************************
 * This file had been created by asn1tostruct.py script v0.5osmo1
 * Please do not modify this file but regenerate it via script.
 * Created on: 2022-11-14 12:42:18
 * from ['../../../src/osmo-iuh/asn1/rua/RUA-PDU-Contents.asn']
 ******************************************************************************/
#include <osmocom/rua/rua_common.h>
#include <osmocom/rua/rua_ies_defs.h>

int rua_decode_connecties(
    RUA_ConnectIEs_t *connectIEs,
    ANY_t *any_p) {

    RUA_Connect_t *connect_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(connectIEs != NULL);

    memset(connectIEs, 0, sizeof(RUA_ConnectIEs_t));
    RUA_DEBUG("Decoding message RUA_ConnectIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RUA_Connect, (void**)&connect_p);

    if (tempDecoded < 0 || connect_p == NULL) {
        RUA_DEBUG("Decoding of message RUA_ConnectIEs failed\n");
        return -1;
    }

    for (i = 0; i < connect_p->connect_ies.list.count; i++) {
        RUA_IE_t *ie_p;
        ie_p = connect_p->connect_ies.list.array[i];
        switch(ie_p->id) {
            case RUA_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RUA_CN_DomainIndicator_t *ruA_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_CN_DomainIndicator, (void**)&ruA_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_CN_DomainIndicator, ruA_CNDomainIndicator_p);
                memcpy(&connectIEs->cN_DomainIndicator, ruA_CNDomainIndicator_p, sizeof(RUA_CN_DomainIndicator_t));
                FREEMEM(ruA_CNDomainIndicator_p);
            } break;
            case RUA_ProtocolIE_ID_id_Context_ID:
            {
                RUA_Context_ID_t *ruA_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_Context_ID, (void**)&ruA_ContextID_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_Context_ID, ruA_ContextID_p);
                memcpy(&connectIEs->context_ID, ruA_ContextID_p, sizeof(RUA_Context_ID_t));
                FREEMEM(ruA_ContextID_p);
            } break;
            /* Optional field */
            case RUA_ProtocolIE_ID_id_IntraDomainNasNodeSelector:
            {
                RUA_IntraDomainNasNodeSelector_t *ruA_IntraDomainNasNodeSelector_p = NULL;
                connectIEs->presenceMask |= CONNECTIES_RUA_INTRADOMAINNASNODESELECTOR_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_IntraDomainNasNodeSelector, (void**)&ruA_IntraDomainNasNodeSelector_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE intraDomainNasNodeSelector failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_IntraDomainNasNodeSelector, ruA_IntraDomainNasNodeSelector_p);
                memcpy(&connectIEs->intraDomainNasNodeSelector, ruA_IntraDomainNasNodeSelector_p, sizeof(RUA_IntraDomainNasNodeSelector_t));
                FREEMEM(ruA_IntraDomainNasNodeSelector_p);
            } break;
            case RUA_ProtocolIE_ID_id_Establishment_Cause:
            {
                RUA_Establishment_Cause_t *ruA_EstablishmentCause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_Establishment_Cause, (void**)&ruA_EstablishmentCause_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE establishment_Cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_Establishment_Cause, ruA_EstablishmentCause_p);
                memcpy(&connectIEs->establishment_Cause, ruA_EstablishmentCause_p, sizeof(RUA_Establishment_Cause_t));
                FREEMEM(ruA_EstablishmentCause_p);
            } break;
            case RUA_ProtocolIE_ID_id_RANAP_Message:
            {
                RUA_RANAP_Message_t *ruA_RANAPMessage_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_RANAP_Message, (void**)&ruA_RANAPMessage_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE ranaP_Message failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_RANAP_Message, ruA_RANAPMessage_p);
                memcpy(&connectIEs->ranaP_Message, ruA_RANAPMessage_p, sizeof(RUA_RANAP_Message_t));
                FREEMEM(ruA_RANAPMessage_p);
            } break;
            default:
                RUA_DEBUG("Unknown protocol IE id (%d) for message connecties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RUA_Connect, connect_p);
    return decoded;
}

int rua_decode_directtransferies(
    RUA_DirectTransferIEs_t *directTransferIEs,
    ANY_t *any_p) {

    RUA_DirectTransfer_t *directTransfer_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(directTransferIEs != NULL);

    memset(directTransferIEs, 0, sizeof(RUA_DirectTransferIEs_t));
    RUA_DEBUG("Decoding message RUA_DirectTransferIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RUA_DirectTransfer, (void**)&directTransfer_p);

    if (tempDecoded < 0 || directTransfer_p == NULL) {
        RUA_DEBUG("Decoding of message RUA_DirectTransferIEs failed\n");
        return -1;
    }

    for (i = 0; i < directTransfer_p->directTransfer_ies.list.count; i++) {
        RUA_IE_t *ie_p;
        ie_p = directTransfer_p->directTransfer_ies.list.array[i];
        switch(ie_p->id) {
            case RUA_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RUA_CN_DomainIndicator_t *ruA_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_CN_DomainIndicator, (void**)&ruA_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_CN_DomainIndicator, ruA_CNDomainIndicator_p);
                memcpy(&directTransferIEs->cN_DomainIndicator, ruA_CNDomainIndicator_p, sizeof(RUA_CN_DomainIndicator_t));
                FREEMEM(ruA_CNDomainIndicator_p);
            } break;
            case RUA_ProtocolIE_ID_id_Context_ID:
            {
                RUA_Context_ID_t *ruA_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_Context_ID, (void**)&ruA_ContextID_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_Context_ID, ruA_ContextID_p);
                memcpy(&directTransferIEs->context_ID, ruA_ContextID_p, sizeof(RUA_Context_ID_t));
                FREEMEM(ruA_ContextID_p);
            } break;
            case RUA_ProtocolIE_ID_id_RANAP_Message:
            {
                RUA_RANAP_Message_t *ruA_RANAPMessage_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_RANAP_Message, (void**)&ruA_RANAPMessage_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE ranaP_Message failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_RANAP_Message, ruA_RANAPMessage_p);
                memcpy(&directTransferIEs->ranaP_Message, ruA_RANAPMessage_p, sizeof(RUA_RANAP_Message_t));
                FREEMEM(ruA_RANAPMessage_p);
            } break;
            default:
                RUA_DEBUG("Unknown protocol IE id (%d) for message directtransferies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RUA_DirectTransfer, directTransfer_p);
    return decoded;
}

int rua_decode_disconnecties(
    RUA_DisconnectIEs_t *disconnectIEs,
    ANY_t *any_p) {

    RUA_Disconnect_t *disconnect_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(disconnectIEs != NULL);

    memset(disconnectIEs, 0, sizeof(RUA_DisconnectIEs_t));
    RUA_DEBUG("Decoding message RUA_DisconnectIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RUA_Disconnect, (void**)&disconnect_p);

    if (tempDecoded < 0 || disconnect_p == NULL) {
        RUA_DEBUG("Decoding of message RUA_DisconnectIEs failed\n");
        return -1;
    }

    for (i = 0; i < disconnect_p->disconnect_ies.list.count; i++) {
        RUA_IE_t *ie_p;
        ie_p = disconnect_p->disconnect_ies.list.array[i];
        switch(ie_p->id) {
            case RUA_ProtocolIE_ID_id_CN_DomainIndicator:
            {
                RUA_CN_DomainIndicator_t *ruA_CNDomainIndicator_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_CN_DomainIndicator, (void**)&ruA_CNDomainIndicator_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE cN_DomainIndicator failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_CN_DomainIndicator, ruA_CNDomainIndicator_p);
                memcpy(&disconnectIEs->cN_DomainIndicator, ruA_CNDomainIndicator_p, sizeof(RUA_CN_DomainIndicator_t));
                FREEMEM(ruA_CNDomainIndicator_p);
            } break;
            case RUA_ProtocolIE_ID_id_Context_ID:
            {
                RUA_Context_ID_t *ruA_ContextID_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_Context_ID, (void**)&ruA_ContextID_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE context_ID failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_Context_ID, ruA_ContextID_p);
                memcpy(&disconnectIEs->context_ID, ruA_ContextID_p, sizeof(RUA_Context_ID_t));
                FREEMEM(ruA_ContextID_p);
            } break;
            case RUA_ProtocolIE_ID_id_Cause:
            {
                RUA_Cause_t *ruA_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_Cause, (void**)&ruA_Cause_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_Cause, ruA_Cause_p);
                memcpy(&disconnectIEs->cause, ruA_Cause_p, sizeof(RUA_Cause_t));
                FREEMEM(ruA_Cause_p);
            } break;
            /* Conditional field */
            case RUA_ProtocolIE_ID_id_RANAP_Message:
            {
                RUA_RANAP_Message_t *ruA_RANAPMessage_p = NULL;
                disconnectIEs->presenceMask |= DISCONNECTIES_RUA_RANAP_MESSAGE_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_RANAP_Message, (void**)&ruA_RANAPMessage_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE ranaP_Message failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_RANAP_Message, ruA_RANAPMessage_p);
                memcpy(&disconnectIEs->ranaP_Message, ruA_RANAPMessage_p, sizeof(RUA_RANAP_Message_t));
                FREEMEM(ruA_RANAPMessage_p);
            } break;
            default:
                RUA_DEBUG("Unknown protocol IE id (%d) for message disconnecties\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RUA_Disconnect, disconnect_p);
    return decoded;
}

int rua_decode_connectionlesstransferies(
    RUA_ConnectionlessTransferIEs_t *connectionlessTransferIEs,
    ANY_t *any_p) {

    RUA_ConnectionlessTransfer_t *connectionlessTransfer_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(connectionlessTransferIEs != NULL);

    memset(connectionlessTransferIEs, 0, sizeof(RUA_ConnectionlessTransferIEs_t));
    RUA_DEBUG("Decoding message RUA_ConnectionlessTransferIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RUA_ConnectionlessTransfer, (void**)&connectionlessTransfer_p);

    if (tempDecoded < 0 || connectionlessTransfer_p == NULL) {
        RUA_DEBUG("Decoding of message RUA_ConnectionlessTransferIEs failed\n");
        return -1;
    }

    for (i = 0; i < connectionlessTransfer_p->connectionlessTransfer_ies.list.count; i++) {
        RUA_IE_t *ie_p;
        ie_p = connectionlessTransfer_p->connectionlessTransfer_ies.list.array[i];
        switch(ie_p->id) {
            case RUA_ProtocolIE_ID_id_RANAP_Message:
            {
                RUA_RANAP_Message_t *ruA_RANAPMessage_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_RANAP_Message, (void**)&ruA_RANAPMessage_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE ranaP_Message failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_RANAP_Message, ruA_RANAPMessage_p);
                memcpy(&connectionlessTransferIEs->ranaP_Message, ruA_RANAPMessage_p, sizeof(RUA_RANAP_Message_t));
                FREEMEM(ruA_RANAPMessage_p);
            } break;
            default:
                RUA_DEBUG("Unknown protocol IE id (%d) for message connectionlesstransferies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RUA_ConnectionlessTransfer, connectionlessTransfer_p);
    return decoded;
}

int rua_decode_errorindicationies(
    RUA_ErrorIndicationIEs_t *errorIndicationIEs,
    ANY_t *any_p) {

    RUA_ErrorIndication_t *errorIndication_p = NULL;
    int i, decoded = 0;
    int tempDecoded = 0;
    assert(any_p != NULL);
    assert(errorIndicationIEs != NULL);

    memset(errorIndicationIEs, 0, sizeof(RUA_ErrorIndicationIEs_t));
    RUA_DEBUG("Decoding message RUA_ErrorIndicationIEs (%s:%d)\n", __FILE__, __LINE__);

    tempDecoded = ANY_to_type_aper(any_p, &asn_DEF_RUA_ErrorIndication, (void**)&errorIndication_p);

    if (tempDecoded < 0 || errorIndication_p == NULL) {
        RUA_DEBUG("Decoding of message RUA_ErrorIndicationIEs failed\n");
        return -1;
    }

    for (i = 0; i < errorIndication_p->errorIndication_ies.list.count; i++) {
        RUA_IE_t *ie_p;
        ie_p = errorIndication_p->errorIndication_ies.list.array[i];
        switch(ie_p->id) {
            case RUA_ProtocolIE_ID_id_Cause:
            {
                RUA_Cause_t *ruA_Cause_p = NULL;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_Cause, (void**)&ruA_Cause_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE cause failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_Cause, ruA_Cause_p);
                memcpy(&errorIndicationIEs->cause, ruA_Cause_p, sizeof(RUA_Cause_t));
                FREEMEM(ruA_Cause_p);
            } break;
            /* Optional field */
            case RUA_ProtocolIE_ID_id_CriticalityDiagnostics:
            {
                RUA_CriticalityDiagnostics_t *ruA_CriticalityDiagnostics_p = NULL;
                errorIndicationIEs->presenceMask |= ERRORINDICATIONIES_RUA_CRITICALITYDIAGNOSTICS_PRESENT;
                tempDecoded = ANY_to_type_aper(&ie_p->value, &asn_DEF_RUA_CriticalityDiagnostics, (void**)&ruA_CriticalityDiagnostics_p);
                if (tempDecoded < 0) {
                    RUA_DEBUG("Decoding of IE criticalityDiagnostics failed\n");
                    return -1;
                }
                decoded += tempDecoded;
                if (asn1_xer_print)
                    xer_fprint(stdout, &asn_DEF_RUA_CriticalityDiagnostics, ruA_CriticalityDiagnostics_p);
                memcpy(&errorIndicationIEs->criticalityDiagnostics, ruA_CriticalityDiagnostics_p, sizeof(RUA_CriticalityDiagnostics_t));
                FREEMEM(ruA_CriticalityDiagnostics_p);
            } break;
            default:
                RUA_DEBUG("Unknown protocol IE id (%d) for message errorindicationies\n", (int)ie_p->id);
                return -1;
        }
    }
    ASN_STRUCT_FREE(asn_DEF_RUA_ErrorIndication, errorIndication_p);
    return decoded;
}

int rua_free_connecties(
    RUA_ConnectIEs_t *connectIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_CN_DomainIndicator, &connectIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_Context_ID, &connectIEs->context_ID);
    /* Optional field */
    if ((connectIEs->presenceMask & CONNECTIES_RUA_INTRADOMAINNASNODESELECTOR_PRESENT)
        == CONNECTIES_RUA_INTRADOMAINNASNODESELECTOR_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_IntraDomainNasNodeSelector, &connectIEs->intraDomainNasNodeSelector);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_Establishment_Cause, &connectIEs->establishment_Cause);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_RANAP_Message, &connectIEs->ranaP_Message);
    return 0;
}

int rua_free_directtransferies(
    RUA_DirectTransferIEs_t *directTransferIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_CN_DomainIndicator, &directTransferIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_Context_ID, &directTransferIEs->context_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_RANAP_Message, &directTransferIEs->ranaP_Message);
    return 0;
}

int rua_free_disconnecties(
    RUA_DisconnectIEs_t *disconnectIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_CN_DomainIndicator, &disconnectIEs->cN_DomainIndicator);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_Context_ID, &disconnectIEs->context_ID);
    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_Cause, &disconnectIEs->cause);
    /* Conditional field */
    if ((disconnectIEs->presenceMask & DISCONNECTIES_RUA_RANAP_MESSAGE_PRESENT)
        == DISCONNECTIES_RUA_RANAP_MESSAGE_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_RANAP_Message, &disconnectIEs->ranaP_Message);
    return 0;
}

int rua_free_connectionlesstransferies(
    RUA_ConnectionlessTransferIEs_t *connectionlessTransferIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_RANAP_Message, &connectionlessTransferIEs->ranaP_Message);
    return 0;
}

int rua_free_errorindicationies(
    RUA_ErrorIndicationIEs_t *errorIndicationIEs) {

    ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_Cause, &errorIndicationIEs->cause);
    /* Optional field */
    if ((errorIndicationIEs->presenceMask & ERRORINDICATIONIES_RUA_CRITICALITYDIAGNOSTICS_PRESENT)
        == ERRORINDICATIONIES_RUA_CRITICALITYDIAGNOSTICS_PRESENT) 
        ASN_STRUCT_FREE_CONTENTS_ONLY(asn_DEF_RUA_CriticalityDiagnostics, &errorIndicationIEs->criticalityDiagnostics);
    return 0;
}

